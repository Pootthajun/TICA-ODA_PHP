<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class pdf {
 
    function pdf()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load($param=NULL)
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
 
        if ($params == NULL)
        {
            $param = '"en-GB-x","A4","","",50,50,50,50,20,30,';
        }
 
        return new mPDF($param);
    }
}