<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Datethai
{
    public function monththai($date,$type=1){
        
 
         
		$abbarrmonth = array( '01' => 'ม.ค.' ,  '02' => 'ก.พ.' ,  '03' => 'มี.ค.' , '04' => 'เม.ย.' ,  '05' => 'พ.ค.' ,  '06' => 'มิ.ย', '07' => 'ก.ค.' ,  '08' => 'ส.ค.' ,  '09' => 'ก.ย.' , '10' => 'ต.ค.' , '11' => 'พ.ย.' ,  '12' => 'ธ.ค.' );

		$arrmonth = array( '01' => 'มกราคม' ,  '02' => 'กุมภาพันธ์' ,  '03' => 'มีนาคม' , '04' => 'เมษายน' ,  '05' => 'พฤษภาคม' ,  '06' => 'มิถุนายน', '07' => 'กรกฎาคม' ,  '08' => 'สิงหาคม' ,  '09' => 'กันยายน' , '10' => 'ตุลาคม' , '11' => 'พฤศจิกายน' ,  '12' => 'ธันวาคม' );
		 
		 if ( !is_numeric( $date ) ){ 
		   return 'รูปแบบวันที่ต้องมีเฉพาะตัวเลขเท่านั้น';
		   exit();
		 }

		 if( $type == 1 ) { $arr = $arrmonth ; }else { $arr = $abbarrmonth; }               
		  $month = substr( $date , 2 , 2 );
		  $lenyear = strlen( substr( $date , 4 ) );
		  if ( $lenyear ==2 ){ $year = substr( substr( $date , 4) + 543 ,1 ,2 ) ; }
		  if ( $lenyear ==4 ){ $year = substr( $date , 4) + 543 ; }
		  //return substr( $date , 0 , 2 ).' '.$arr[$month].' '.$year;
                  return $arr[$month];
    }
}

?>