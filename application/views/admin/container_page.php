<?php $this->load->view($this->config->item('wconfig_template_welcome') .'header'); ?>


<body>
<?php $this->load->view($this->config->item('wconfig_template_welcome') .'menu'); ?>
<div id="bannertop" style="display:none;">
  <div>
    <ul class="allinone_bannerRotator_list">
      <li data-text-id="#allinone_bannerRotator_photoText1"><img src="<?php echo base_url(); ?>assets/welcome/images/banners/01_universal.jpg" alt="" /></li>
      <li data-text-id="#allinone_bannerRotator_photoText2"><img src="<?php echo base_url(); ?>assets/welcome/images/banners/02_universal.jpg" alt="" /></li>
      <li data-text-id="#allinone_bannerRotator_photoText3"><img src="<?php echo base_url(); ?>assets/welcome/images/banners/03_universal.jpg" alt="" /></li>
    </ul>
  </div>
</div>
<div class="spacer"></div>
<div id="container"> 
  
  <!--start key message-->
  <div id="keymessage"></div>
  <div class="spacer"></div>
  
  <!--start content middle-->
  
   <div id="content">
    <div class="cfixed">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><div class="breadcrumb"><img src="<?php echo base_url(); ?>assets/welcome/images/icon-map.png" align="absmiddle"/><span>คุณอยู่ที่ : <a href="<?php echo site_url(); ?>">Home</a> / </span><span class="breadcrumbstay"><?php echo $header; ?></span> <span style="float:right; line-height:40px;"><a href="<?php echo site_url(); ?>news/index">All News</a></span></div></td>
        </tr>
        <tr>
          <td  height="5"></td>
        </tr>
        <tr>
          <td><img src="<?php echo base_url(); ?>assets/welcome/images/line.jpg" height="4" width="1080" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="720" valign="top"> <?php  $this->load->view($module."/".$page); ?></td>
                <td width="20">&nbsp;</td>
                <td width="350" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>
                      <?php  if($this->uri->segment(1) == "news"){
                     echo  '<h2>Related news</h2>';
					  }else{
						    echo  '<h2>Feature</h2>';
					  }
					  ?>
					  </td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <?php
						 if($this->uri->segment(1) == "news"){
						$this->db->where('news_id != ',$t['news_id']);
						$this->db->limit('5');
						$results = $this->db->get('news');						
						foreach($results->result_array() as $listtravel) {
							?>
                            <tr>
                                <td width="74" valign="top"><img src="<?php echo base_url()."upload/news/".$listtravel['news_thumnail']; ?>" name="mainImage" width="68" height="68" class="borderimagethumb" id="mainImage"/></td>
                              <td width="664">
                              <a href="<?php echo base_url(); ?>/news/newsdetail/<?php echo $listtravel['news_id']; ?>">
                              
                                <?php 
						echo $listtravel['news_title']; 					
					 ?><br />
                              <img src="<?php echo base_url(); ?>assets/welcome/images/ico-updates.png" width="30" height="30" align="absmiddle" />
                              <span class="detailexpand"> <?php echo $listtravel['news_create']; ?></span>
                              </a></td>
                            </tr>
                            <tr>
                              <td valign="top">&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <?php } ?>
                            
                           <?php   }else{ 
                           
                           $this->db->where('feature_id != ',$t['feature_id']);
						$this->db->limit('5');
						$results = $this->db->get('feature');						
						foreach($results->result_array() as $listtravel) {
							?>
                            <tr>
                            
                                <td width="74" valign="top">
                                <?php if(!empty($listtravel['feature_thumnail'])){ ?>
                                <img src="<?php echo base_url()."upload/feature/".$listtravel['feature_thumnail']; ?>" name="mainImage" width="68" height="68" class="borderimagethumb" id="mainImage"/>
                                <?php } ?>
                                </td>
                              <td width="664">
                              <a href="<?php echo base_url(); ?>/feature/featuredetail/<?php echo $listtravel['feature_id']; ?>">
                              
                                <?php 
						echo $listtravel['feature_title']; 					
					 ?><br />
                              <img src="<?php echo base_url(); ?>assets/welcome/images/ico-updates.png" width="30" height="30" align="absmiddle" />
                              <span class="detailexpand"> <?php echo $listtravel['feature_create']; ?></span>
                              </a></td>
                            </tr>
                            <tr>
                              <td valign="top">&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <?php } ?>
							 
					<?php } ?>

       

                         
                          </table></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
  </div>
  <div class="spacer"></div>
  
  <!--start content parthner-->
  <?php $this->load->view($this->config->item('wconfig_template_welcome') .'link'); ?>
  <div class="spacer"></div>
  

    
   
<?php $this->load->view($this->config->item('wconfig_template_welcome') .'footer'); ?>