<div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->	
  <style>
a.tooltips {
  position: relative;
  display: inline; 
}
a.tooltips span {
  position: absolute;
  color: #121212;
  background: #FFFFFF;
  border: 2px solid #389CFF;
  height: 30px;
  line-height: 30px;
  text-align: center;
  visibility: hidden;
  border-radius: 5px;
  margin-bottom:5%;
    font-size:12px;
	padding-left:10px;
	padding-right:10px;
	z-index: 999999;
	
}
a.tooltips span:before {
  /*content: '';*/
  position: absolute;
  top: 100%;
  left: 90%;
  margin-left: -12px;
  width: 0; height: 0;
  border-top: 12px solid #389CFF;
  border-right: 12px solid transparent;
  border-left: 12px solid transparent;
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
/*  left: 50%;*/
  margin-left: -8px;
 height: 0;
  border-top: 8px solid #F7F7F7;
  border-right: 8px solid transparent;
  border-left: 8px solid transparent;
}
a:hover.tooltips span { 
  	visibility: visible;
  	opacity: 0.8;
  	bottom: 30px;
/*  left: 50%;*/
  	margin-left: -76px;
  	z-index: 999999;
  	overflow-y: inherit;
}
  </style>		
  <script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
    <style>
  label {
    display: inline-block;
    width: 5em;
  }
  </style>
			<!-- Logo (221px wide) -->
			<center><a href="#"><img id="logo" src="<?php echo base_url(); ?>assets/admin/images/logo-inside.png"  /></a></center>
		  
			<!-- Sidebar Profile links -->
			<div id="profile-links">
				<a href="<?php echo site_url(); ?>user/admin/edituser/<?php echo $this->session->userdata('userid');?>"   title="แก้ไขข้อมูลส่วนตัว"> Welecome <?php echo $this->session->userdata('fullname');?> </a> 
			</div>        
			<div id="profile-links">
			<a href="<?php echo base_url(); ?>assets/คู่มือการใช้งานระบบ.pdf" target="_blank"  title="คู่มือการใช้งาน">  Help	</a>|||<a href="<?php echo base_url(); ?>" target="_blank" title="หน้าเว็บไซต์"> View site </a> |||  <a href="<?php echo site_url(); ?>admin/logout/" title="ออกจากระบบ">  Logout</a>
			</div>      
			
			<ul id="main-nav">  <!-- Accordion Menu -->
				
				<li>
					<a href="<?php echo site_url(); ?>dashboard/admin/" class="tooltips nav-top-item no-submenu <?php if($this->uri->segment(1) == "dashboard"){ echo "current";} ?>" > <!-- Add the class "no-submenu" to menu items with no sub menu -->
					 <span>แผงควบคุม</span>	Dashboard
                       
					</a>       
				</li>
                <li>					<a href="#" class="tooltips nav-top-item  <?php if($this->uri->segment(1) == "report"){ echo "current";} ?>"><span>รายงาน</span>  Report</a>
<ul>
						<li><a href="<?php echo site_url(); ?>report/recipientreport/" class="tooltips <?php if($this->uri->segment(2) == "recipientreport"){ echo "current";} ?>">
                        <span>รายงานรายละเอียดผู้รับทุน</span>Recipient Detail</a></li>
					<li><a href="<?php echo site_url(); ?>report/recipientlist/"   class="tooltips <?php if($this->uri->segment(2) == "recipientlist"){ echo "current";} ?>">
					 <span>รายงานรายชื่อผู้รับทุน</span>	Recipient List
			  </a></li>
                    	<li><a href="<?php echo site_url(); ?>report/projectreport/" class="tooltips <?php if($this->uri->segment(2) == "projectreport"){ echo "current";} ?>">
                        <span>รายงาน Project</span>
                        Project</a></li>						
						<li><a href="<?php echo site_url(); ?>report/financereport/" class="tooltips <?php if($this->uri->segment(2) == "financereport"){ echo "current";} ?>">
                        <span>รายงาน Finance</span>
                        Finance</a></li> 
                       <li><a href="<?php echo site_url(); ?>report/financeyear/" class="tooltips <?php if($this->uri->segment(2) == "financeyear"){ echo "current";} ?>">
                          <span>รายงาน Finance of year</span>Finance of year</a></li>
						
						
						
							
                       <!-- <li><a href="<?php //echo site_url(); ?>report/aid/" <?php //if($this->uri->segment(2) == "aid"){ echo "current";} ?>">AID</a></li>-->
                        <li><a href="<?php echo site_url(); ?>report/aidtypereport/" class="tooltips <?php if($this->uri->segment(2) == "aidtypereport"){ echo "current";} ?>">
                          <span>รายงาน Aid Type</span>
                         Aid Type </a></li>
                        <li><a href="<?php echo site_url(); ?>report/cooperationcountry/" class="tooltips <?php if($this->uri->segment(2) == "cooperationcountry"){ echo "current";} ?>"> 
                        <span>รายงาน Cooperation Country</span>
                        Cooperation Country</a></li>
                       <li><a href="<?php echo site_url(); ?>report/aidcourse/" class="tooltips <?php if($this->uri->segment(2) == "aidcourse"){ echo "current";} ?>">
                        <span>รายงาน  Aid Course</span>
                       Aid Course</a></li> 
                    <li><a href="<?php echo site_url(); ?>report/aidcomponent/" class="tooltips <?php if($this->uri->segment(2) == "aidcomponent"){ echo "current";} ?>">
                    <span>รายงาน  Aid Component</span>
                    Aid Component</a></li> 
                     <li><a href="<?php echo site_url(); ?>report/tricooperation/" class="tooltips <?php if($this->uri->segment(2) == "tricooperation"){ echo "current";} ?>">
                     <span>รายงาน  Tri-cooperation</span>
                     Tri-cooperation</a></li> 
                        <li><a href="<?php echo site_url(); ?>report/cooperationregnial/" class="tooltips <?php if($this->uri->segment(2) == "cooperationregnial"){ echo "current";} ?>">
                        <span>รายงาน  Aid Regional running </span>
                        Aid Regenial running </a></li> 
                    </ul>
	</li>
	
	                <!-- เพิ่มเมนู report การเงิน -->
	                <li><a href="#" class="tooltips nav-top-item  <?php if($this->uri->segment(1) == "report"){ echo "current";} ?>"><span>รายงานการเงิน</span>  Finance Report</a>
<ul>
						<li><a href="http://tit-tech.co.th/oda/RPT_Finance_Recipient.aspx" class="tooltips <?php if($this->uri->segment(2) == "recipientreport"){ echo "current";} ?>">
                        <span>เงินอุดหนุน บุคคล</span>Recipient Assistance</a></li>
					<li><a href="http://tit-tech.co.th/oda/RPT_Finance_project.aspx"   class="tooltips <?php if($this->uri->segment(2) == "recipientlist"){ echo "current";} ?>">
					 <span>การจัดสรรงบประมาณ</span>Allocate Budget
			  </a></li>
                    	<li><a href="http://tit-tech.co.th/oda/RPT_Finance_country.aspx" class="tooltips <?php if($this->uri->segment(2) == "projectreport"){ echo "current";} ?>">
                        <span>การจัดสรรงบประมาณรายประเทศ</span>
                        Countey Budget</a></li>						                        
                </ul>
	</li>
				
				
				
				
                <li>
					<a href="#" class="tooltips nav-top-item <?php if(($this->uri->segment(1) == "about") || ($this->uri->segment(1) == "feature")  || ($this->uri->segment(1) == "policy") || ($this->uri->segment(1) == "feature") ||  ($this->uri->segment(1) == "weblink") ||  ($this->uri->segment(1) == "general")){ echo "current";} ?>">
					 <span>จัดการบทความ</span>	Content
				  </a>
					<ul>
						<li><a href="<?php echo site_url(); ?>about/admin" class="tooltips <?php if($this->uri->segment(1) == "about"){ echo "class='current'";} ?>">
                        <span>รายละเอียดเกี่ยวกับเรา</span>
                        About</a></li>
						<li><a href="<?php echo site_url(); ?>news/admin" class="tooltips <?php if($this->uri->segment(1) == "news"){ echo "class='current'";} ?>">
                        <span>จัดการข่าว</span>
                        News</a></li>
                        <li><a href="<?php echo site_url(); ?>feature/admin" class="tooltips <?php if($this->uri->segment(1) == "feature"){ echo "class='current'";} ?>">
                          <span>รายละเอียด Feature</span>
                        Feature</a></li>
                       <li><a href="<?php echo site_url(); ?>general/admin/" class="tooltips <?php if($this->uri->segment(1) == "general"){ echo "current";} ?>">
                     <span>รายละเอียด General</span>
                       General</a></li>
                       <li><a href="<?php echo site_url(); ?>weblink/admin/" class="tooltips <?php if($this->uri->segment(1) == "weblink"){ echo "current";} ?>">
                       <span>จัดการ Weblink</span>
                       Weblink</a></li>
					</ul>
				</li>
                
				
				<li>
					<a href="#" class="tooltips nav-top-item <?php if($this->uri->segment(1) == "finance"){ echo "current";} ?>">
					<span>ระบบการเงิน</span>	Finance 
				  </a>
					<ul>
						<li><a href="<?php echo site_url(); ?>finance/budget" class="tooltips <?php if($this->uri->segment(2) == "budget"){ echo "current";} ?>">
                        <span>ระบบการเงิน งบประมาณ</span>	
                        งบประมาณ</a></li>
                        <?php
						     $this->db->order_by('tgroup_id',"ASC");
							$Q = $this->db->get('aid_type_group');
							foreach($Q->result_array() as $row){
							?>
 
 						<li>
                        
                      	 		<a href="<?php echo site_url(); ?>finance/pay/index/<?php echo $row['tgroup_id']; ?>/<?php echo $row['project_type']; ?>" class="tooltips <?php 
								if(($this->uri->segment(3)."/".$this->uri->segment(4)) ==  ("index/".$row['tgroup_id'])){ echo "current";} ?>">  <span>ระบบการเงิน <?php echo $row['tgroup_name']; ?></span>	
                           		<?php echo $row['tgroup_name']; ?></a>
                         </li>
                         <?php } ?>
                       
                         
                        <li><a href="<?php echo base_url(); ?>finance/loan" class="tooltips <?php if($this->uri->segment(2) == "loan"){ echo "current";} ?>">
                         <span>ระบบการเงิน  เงินยืม</span>
                         เงินยืม</a></li>
					</ul>
				</li>
				
				
                <li><a href="<?php echo site_url(); ?>recipient/admin/" class="tooltips nav-top-item no-submenu  <?php if($this->uri->segment(1) == "recipient"){ echo "current";} ?>">
				<span>จัดการผู้รับทุน</span>		Recipient 
			  </a></li>
               
                  		
    <li>
					<a href="#" class="tooltips nav-top-item <?php if(($this->uri->segment(1) == "user")|| ($this->uri->segment(1) == "general") || ($this->uri->segment(1) == "transfer") || ($this->uri->segment(1) == "userauthen") ||  ($this->uri->segment(1) == "expense") ||  ($this->uri->segment(1) == "plan") ||  ($this->uri->segment(1) == "project") ||  ($this->uri->segment(1) == "aid")  ||  ($this->uri->segment(1) == "aidtype")  ||  ($this->uri->segment(1) == "activity") ||  ($this->uri->segment(1) == "projecttype") ||  ($this->uri->segment(1) == "budgettype") || ($this->uri->segment(1) == "region") || ($this->uri->segment(1) == "country")  || ($this->uri->segment(1) == "cooperationtype") || ($this->uri->segment(1) == "cooperation")  || ($this->uri->segment(1) == "oecd")  || ($this->uri->segment(1) == "sector") ||  ($this->uri->segment(1) == "funding")  ||  ($this->uri->segment(1) == "executing") ||  ($this->uri->segment(1) == "multilateral") ||  ($this->uri->segment(1) == "subsector") ||  ($this->uri->segment(1) == "institute")  ||  ($this->uri->segment(1) == "implement")   ||  ($this->uri->segment(1) == "countrygroup") ){ echo "current";} ?>">
					<span>จัดการข้อมูลพื้นฐาน</span>	Master 
				  </a>
					<ul>
                    
                    <li><a href="<?php echo site_url(); ?>plan/admin/" class="tooltips <?php if($this->uri->segment(1) == "plan"){ echo "current";} ?>">
                   <span>จัดการแผน</span>
                    Plan</a></li>
   
						
                        <li><a href="<?php echo site_url(); ?>project/admin/" class="tooltips <?php if($this->uri->segment(1) == "project"){ echo "current";} ?>">
                        <span>จัดการโครงการ</span>
                        Project</a></li>
						<li><a href="<?php echo site_url(); ?>activity/admin/" class="tooltips <?php if($this->uri->segment(1) == "activity"){ echo "current";} ?>">
                        <span>จัดการกิจกรรม</span>
                        Activity</a></li>

                        <li><a href="<?php echo site_url(); ?>aid/admin/" class="tooltips <?php if($this->uri->segment(1) == "aid"){ echo "current";} ?>">
                          <span>จัดการ AID</span>
                        AID</a></li>
                        <li><a href="<?php echo site_url(); ?>transfer/admin/" class="tooltips <?php if(($this->uri->segment(1) == "transfer") ){ echo "current";} ?>">
                         <span>จัดการ Transfer Project </span>
                        Transfer Project </a></li>
                        <li><a href="<?php echo site_url(); ?>projecttype/admin" class="tooltips <?php if($this->uri->segment(1) == "projecttype"){ echo "current";} ?>">
                        <span>จัดการ Project Type</span>
                        Project Type</a></li>						
                        <li><a href="<?php echo site_url(); ?>aidtype/admin/" class="tooltips <?php if($this->uri->segment(1) == "aidtype"){ echo "current";} ?>">
                        <span>จัดการ AID Type</span>
                        AID Type</a></li>
                        <li><a href="<?php echo site_url(); ?>budgettype/admin/" class="tooltips <?php if($this->uri->segment(1) == "budgettype"){ echo "current";} ?>">
                        <span>จัดการ Budget Type</span>
                        Budget Type</a></li>
                        
                        <li><a href="<?php echo site_url(); ?>region/admin/" class="tooltips <?php if($this->uri->segment(1) == "region"){ echo "current";} ?>">
                        <span>จัดการ Region(OECD)</span>Region(OECD)</a></li>
                        <li><a href="<?php echo site_url(); ?>countrygroup/admin/" class="tooltips <?php if($this->uri->segment(1) == "countrygroup"){ echo "current";} ?>">
                        <span>จัดการ Country group</span>
                        Country group</a></li>
                        <li><a href="<?php echo site_url(); ?>country/admin/" class="tooltips <?php if($this->uri->segment(1) == "country"){ echo "current";} ?>">
                        <span>จัดการ Country</span>
                        Country</a></li>
                        <li><a href="<?php echo site_url(); ?>cooperation/admin/" class="tooltips <?php if($this->uri->segment(1) == "cooperation"){ echo "current";} ?>">
                         <span>จัดการ Cooperation Framework</span>Cooperation Framework </a></li>
                        <li><a href="<?php echo site_url(); ?>cooperationtype/admin/" class="tooltips <?php if($this->uri->segment(1) == "cooperationtype"){ echo "current";} ?>">
                        <span>จัดการ Cooperation Type</span>
                        Cooperation Type</a></li>
                        <li><a href="<?php echo site_url(); ?>oecd/admin/" class="tooltips <?php if($this->uri->segment(1) == "oecd"){ echo "current";} ?>">
                        <span>จัดการ OECD</span>
                        OECD</a></li>
                        <li><a href="<?php echo site_url(); ?>sector/admin/" class="tooltips <?php if($this->uri->segment(1) == "sector"){ echo "current";} ?>">
                         <span>จัดการ Sector</span>
                        Sector</a></li>
                        <li><a href="<?php echo site_url(); ?>subsector/admin/" class="tooltips <?php if($this->uri->segment(1) == "subsector"){ echo "current";} ?>">
                         <span>จัดการ Subsector</span>Subsector</a></li>
                        <li><a href="<?php echo site_url(); ?>funding/admin/" class="tooltips <?php if($this->uri->segment(1) == "funding"){ echo "current";} ?>">Funding</a></li>
                        <li><a href="<?php echo site_url(); ?>executing/admin/" class="tooltips <?php if($this->uri->segment(1) == "executing"){ echo "current";} ?>">
                        <span>จัดการ Executing</span>Executing</a></li>
                        <li><a href="<?php echo site_url(); ?>institute/admin/" class="tooltips <?php if($this->uri->segment(1) == "institute"){ echo "current";} ?>">
                        <span>จัดการ Institute</span>
                        Institute</a></li>
                    <!--    <li><a href="<?php //echo site_url(); ?>multilateral/admin/" <?php //if($this->uri->segment(1) == "multilateral"){ echo "current";} ?>">Multilateral</a></li>-->
                        <li><a href="<?php echo site_url(); ?>implement/admin/" class="tooltips <?php if($this->uri->segment(1) == "implement"){ echo "current";} ?>">
                        <span>จัดการ Implement</span>
                        Implement</a></li>
                        
                        <li><a href="<?php echo site_url(); ?>expense/admin/" class="tooltips <?php if($this->uri->segment(1) == "expense"){ echo "current";} ?>">
                        <span>จัดการ Expense</span>
                        Expense</a></li>
						 <li><a href="<?php echo site_url(); ?>kindtype/admin/" class="tooltips <?php if(($this->uri->segment(1) == "kindtype") ){ echo "current";} ?>">
                        <span>จัดการ IN KIND</span>
                        IN KIND</a></li>
                        <li><a href="<?php echo site_url(); ?>user/admin/welcome" class="tooltips <?php if(($this->uri->segment(1) == "user")){ echo "current";} ?>">
                         <span>จัดการผู้ใช้งาน</span>
                        User</a></li>
                        <li><a href="<?php echo site_url(); ?>userauthen/admin/" class="tooltips <?php if(($this->uri->segment(1) == "userauthen") ){ echo "current";} ?>">
                        <span>กำหนดสิทธิผู้ใช้งาน</span>
                        User Authen</a></li>
						

                      
                        
					</ul>
				</li>
    </ul> 
			<!-- End #main-nav -->
			
		</div>
        <div id="alert_box">
                 <ul>
                        <li><a  title="ข้อมูลรออนุมัติ" href="<?php echo site_url(); ?>recipient/admin/waitapprov" >
                        <?php
					    $QP = $this->db->query("SELECT * FROM recipient WHERE status = 1 and rec_id not in (select rec_id from money_paid)");
						$numwaitapprove = $QP->num_rows(); 
						?>
                        Waiting for approve(<?php echo $numwaitapprove; ?>)
                        
                        </a></li>
                        <li><a title="ข้อมูลรอเบิกจ่าย"  href="<?php echo site_url(); ?>finance/pay/waitpay">
                        <?php 
						$QP = $this->db->query("SELECT * FROM recipient WHERE status = 2 and rec_id not in (select rec_id from money_paid)");
						$numwaitpay = $QP->num_rows(); 
						?>
                        Waiting for pay(<?php echo $numwaitpay; ?>)
                        </a></li>
                        <li>
                        <?php
                        $current =date('Y-m-d');
						$QP = $this->db->query("SELECT expire_passport FROM recipient WHERE expire_passport < '$current' and (end_date_funded > '$current' or end_date_extend > '$current' )  and end_date_funded != '' ");
						$numPassport = $QP->num_rows();
						?> 
                        <a title="ข้อมูลใบอนุญาตหมดอายุ"  href="<?php echo site_url(); ?>recipient/admin/expirepassport">Passport Expire(<?php echo $numPassport; ?>)</a></li>
                        <li>
                        <?php
						$QI = $this->db->query("SELECT expire_insurance FROM recipient WHERE expire_insurance < '$current'  and (end_date_funded > '$current' or end_date_extend > '$current' )  and end_date_funded != '' ");
						$numInsurance = $QI->num_rows();
						?>
                        <a title="ข้อมูลประกันหมดอายุ"  href="<?php echo site_url(); ?>recipient/admin/expireInsurance">Insurance Expire(<?php echo $numInsurance; ?>)</a></li>
                        <li>
                        <?php
						$QC = $this->db->query("SELECT expire_visa FROM recipient WHERE expire_visa < '$current'  and (end_date_funded > '$current' or end_date_extend > '$current' )  and end_date_funded != '' ");
						$numvisa = $QC->num_rows();
						?>
                        <a title="ข้อมูลหนังสือเดินทางหมดอายุ" href="<?php echo site_url(); ?>recipient/admin/expirevisa">VISA Expire(<?php echo $numvisa; ?>)</a></li>
                        <li>
                        <?php
						$QP = $this->db->query("SELECT project_status FROM project WHERE project_status = '0'");
						$numproject = $QP->num_rows();
						?>
                        <a title="ข้อมูลโครงการที่ยังไม่สิ้นสุด"  href="<?php echo site_url(); ?>project/admin/Projectnotcomplete">Project is not complete(<?php echo $numproject; ?>)</a></li>
                </ul>
                <div id="icon_alert"><img id="warming" src="<?php echo base_url(); ?>assets/admin/images/icons/warning.png"  width="28" height="28"/></div>
        </div>
        <br class="clear">
		<div id="project_box">
                <ul>
                        <li>
                        <a href="<?php echo site_url(); ?>project/admin/index/1" >
                        <?php
					    $QP = $this->db->query("SELECT * FROM project WHERE project_type_id = 1  ");
						$numwaitapprove = $QP->num_rows(); 
						$typeproject = 1;
						?>
                        Project (<?php echo $numwaitapprove;$typeproject; ?>)
                        </a>
                        </li>
                        
                        <li>
                        <a href="<?php echo site_url(); ?>project/admin/index/2">
                        <?php 
						$QP = $this->db->query("SELECT * FROM project WHERE project_type_id = 2 ");
						$numwaitpay = $QP->num_rows(); 
						?>
                        Non Project (<?php echo $numwaitpay; ?>)
                        </a>
                        </li>
                        
                        <li>
                        <?php
                        $current =date('Y-m-d');
						$QP = $this->db->query(" SELECT * FROM project WHERE project_type_id = 3 ");
						$numPassport = $QP->num_rows();
						?> 
                        <a href="<?php echo site_url(); ?>project/admin/index/3">
						Loan (<?php echo $numPassport; ?>)</a>
                        </li>
                        
                        <li>
                        <?php
						$QI = $this->db->query(" SELECT * FROM project WHERE project_type_id = 4 ");
						$numInsurance = $QI->num_rows();
						?>
                        <a href="<?php echo site_url(); ?>project/admin/index/4">
                        Contribution (<?php echo $numInsurance; ?>)</a>
                        </li>
                </ul>
        </div>
        
        </div>
<script>
function blink(){
    $('#warming').delay(300).fadeTo(300,0.5).delay(300).fadeTo(300,1, blink);
}

$(document).ready(function() {
    blink();
});
</script>        