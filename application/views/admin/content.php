
       
          <div id="body-wrapper"> 
          <!-- Wrapper for the radial gradient background -->
          <?php //if($this->session->userdata('level') == 1){ ?>
		<?php $this->load->view($this->config->item('wconfig_template_admin') .'sidebar'); ?>
        <?php //}else{ ?>
        <?php //$this->load->view($this->config->item('wconfig_template_admin') .'setting_menu'); ?>
        <?php //} ?>
		 <!-- End #sidebar -->
		
		<div id="main-content"> <!-- Main Content Section with everything --><!-- Page Head -->
        <div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
				  <h3><?php echo $header; ?></h3>
                  <?php if($this->uri->segment(3) == "projectdetail" or $this->uri->segment(3) == "recipientdetail"  or $this->uri->segment(3) == "paydetail"){ ?>
                  <div style="width:150px; float:right; padding-top : 10px; text-align:right; padding-right: 10px;"> 
                  <a href="<?php echo site_url(); ?>report/<?php echo $this->uri->segment(2); ?>/exportpdf<?php echo $this->uri->segment(2); ?>/<?php echo $this->uri->segment('4'); ?>"  target="_blank">PDF </a>|  <a href="<?php echo site_url(); ?>report/<?php echo $this->uri->segment(2); ?>/exportword<?php echo $this->uri->segment(2); ?>/<?php echo $this->uri->segment('4'); ?>"  target="_blank"> Word</a> |  <a href="<?php echo site_url(); ?>report/<?php echo $this->uri->segment(2); ?>/exportexcel<?php echo $this->uri->segment(2); ?>/<?php echo $this->uri->segment('4'); ?>"  target="_blank">Excel</a>
                  </div>
              <?php } ?>
              <?php if(($this->uri->segment(1) == "report") and ($this->uri->segment(2) == "aid")){ ?>
                  <div style="width:150px; float:right; padding-top : 10px; text-align:right; padding-right: 10px;"> 
                  <a href="<?php echo site_url(); ?>report/<?php echo $this->uri->segment(2); ?>/exportpdf<?php echo $this->uri->segment(2); ?><?php if(!empty($_SERVER['QUERY_STRING'])){echo '?'.$_SERVER['QUERY_STRING'];} ?>/"  target="_blank">PDF </a>|  <a href="<?php echo site_url(); ?>report/<?php echo $this->uri->segment(2); ?>/exportword<?php echo $this->uri->segment(2); ?><?php if(!empty($_SERVER['QUERY_STRING'])){echo '?'.$_SERVER['QUERY_STRING'];} ?>"  target="_blank"> Word</a> |  <a href="<?php echo site_url(); ?>report/<?php echo $this->uri->segment(2); ?>/exportexcel<?php echo $this->uri->segment(2); ?><?php if(!empty($_SERVER['QUERY_STRING'])){echo '?'.$_SERVER['QUERY_STRING'];} ?>"  target="_blank">Excel</a>
                  </div>
              <?php } ?>
              <?php if(($this->uri->segment(1) == "report") and ($this->uri->segment(2) == "aidtypereport") or ($this->uri->segment(2) == "cooperationcountry") or ($this->uri->segment(2) == "aidcourse") or ($this->uri->segment(2) == "aidcomponent") or ($this->uri->segment(2) == "tricooperation") or  ($this->uri->segment(2) == "cooperationregnial") or  ($this->uri->segment(2) == "recipientlist")){ ?>
                  <div style="width:150px; float:right; padding-top : 10px; text-align:right; padding-right: 10px;"> 
                  <a href="<?php echo site_url(); ?>report/<?php echo $this->uri->segment(2); ?>/exportpdf<?php echo $this->uri->segment(2); ?>"  target="_blank" id="genpdf">PDF </a>|  <a href="<?php echo site_url(); ?>report/<?php echo $this->uri->segment(2); ?>/exportword<?php echo $this->uri->segment(2); ?>"  target="_blank"  id="genword"> Word</a> |  <a href="<?php echo site_url(); ?>report/<?php echo $this->uri->segment(2); ?>/exportexcel<?php echo $this->uri->segment(2); ?>"  target="_blank" id="genexcel">Excel</a>
                  </div>
              <?php } ?>
				  <div class="clear"></div>
				
				</div> <!-- End .content-box-header -->
				
  <div class="content-box-content">
       <?php print displayStatus();?>
            <?php echo $this->load->view($page); ?>
              </div> 
				<!-- End .content-box-content -->
				
			</div>
			
		   <!-- End .content-box -->
		  <div class="clear"></div>
			
			
			<!-- Start Notifications --><!-- End Notifications -->
			
			<div id="footer">
				<small> <!-- Remove this notice or replace it with whatever you want -->
						&#169; Copyright 2015 The Government Complex Ratthaprasasanabhakti (B) Building, South Zone, 8th Floor, 120 Moo 3 Chaengwattana Road, Thungsonghong, Laksi District, Bangkok 10210  <br />
Tel. 02 203 5000 Fax. 02 143 9327 | <a href="#">Top</a>
				</small>
		  </div>
			<!-- End #footer -->
			
		</div> <!-- End #main-content -->
		
	</div>