<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<title><?php echo $header; ?></title>
		
		<!--                       CSS                       -->
	  
		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/reset.css" type="text/css" media="screen" />
	  
		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" type="text/css" media="screen" />
		
		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/invalid.css" type="text/css" media="screen" />	
        
        
		
		<!-- Colour Schemes
	  
		Default colour scheme is green. Uncomment prefered stylesheet to use it.
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/blue.css" type="text/css" media="screen" />
		
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/red.css" type="text/css" media="screen" />  
	 
		-->
		
		<!-- Internet Explorer Fixes Stylesheet -->
		
		<!--[if lte IE 7]>
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/ie.css" type="text/css" media="screen" />
		<![endif]-->
		
		<!--                       Javascripts                       -->
  
		<!-- jQuery -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery.js"></script>
		

		<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
        
        <!--datatable-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery.dataTables.css" type="text/css" />
        
 	     <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.js"></script>
        
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery.number.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css" type="text/css" />
		<!-- Internet Explorer .png-fix -->
		
	<!--[if IE 6]>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/scripts/DD_belatedPNG_0.0.7a.js"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('.png_bg, img, li');
			</script>
		<![endif]-->
        
        
            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/chosen/docsupport/prism.css">
            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/chosen/chosen.css">
           <script src="<?php echo base_url(); ?>assets/admin/chosen/chosen.jquery.js" type="text/javascript"></script>
           <script src="<?php echo base_url(); ?>assets/admin/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
           
           		<!-- jQuery Configuration -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/scripts/simpla.jquery.configuration.js"></script>


		

 </head>
  
	<body>