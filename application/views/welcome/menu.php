
<div id="tophead">
  <div id="bglogo" > <a href="#" class="logotica">Thailand International Cooperation Agency (TICA)</a> </div>
  <div id="bgnavmenu">
    <div class="navtop">
      <table width="1140" height="6" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="bgnavmenumain"> 
    		<nav class="menu">
        	<ul class="sf-menu" id="example">
              <li class="mhome"><a href="<?php echo site_url(); ?>"  <?php if(($this->uri->segment('1') == "welcome") and ($this->uri->segment('2') == "")){echo 'class="hit"'; } ?>>home</a></li>
              <li class="mfeature"><a href="<?php echo site_url(); ?>feature/index" <?php if($this->uri->segment('1') == "feature"){echo 'class="hit"'; } ?>>feature</a></li>
              <li class="mnews"><a href="<?php echo site_url(); ?>news/index" <?php if($this->uri->segment('1') == "news"){echo 'class="hit"'; } ?>>news</a></li>
              <li class="mstat"><a href="#" <?php if(($this->uri->segment('2') == "typeofaid") || ($this->uri->segment('2') == "recipientcountry") 
			   || ($this->uri->segment('2') == "recipientregional") || ($this->uri->segment('2') == "sector") || ($this->uri->segment('2') == "fundingproject")
			   || ($this->uri->segment('2') == "fundingsector") || ($this->uri->segment('2') == "fundingexcuting") || ($this->uri->segment('2') == "fundrecipientcountry")
			  ){echo 'class="hit"'; } ?>>statistic </a>
             		<ul >
              			<li class="sub1"><a href="<?php echo site_url(); ?>welcome/typeofaid">Type of Aid </a></li>
                        <li class="sub1"><a href="#">Recipient Country </a> 
                                <ul >
                                    <li class="sub2"><a href="<?php echo site_url(); ?>welcome/recipientcountry">Recipient Country </a></li>
                                    <li class="sub2"><a href="<?php echo site_url(); ?>welcome/recipientregional">Recipient Regional  </a></li> 
                             	</ul>
                         </li>
                        <li class="sub1"><a href="<?php echo site_url(); ?>welcome/sector">Sector </a></li>
                       <li class="sub1"><a href="#">Funding Agency  </a> 
                                <ul >
                                    <li class="sub2"><a href="<?php echo site_url(); ?>welcome/fundingproject">Project </a></li>
                                    <li class="sub2"><a href="<?php echo site_url(); ?>welcome/fundingsector">Sector </a></li> 
                                    <li class="sub2"><a href="<?php echo site_url(); ?>welcome/fundingexcuting">Executing Agency </a></li>
                                    <li class="sub2"><a href="<?php echo site_url(); ?>welcome/fundrecipientcountry">Recipient Country  </a></li> 
                             	</ul>
                         </li>
                     </ul>
              </li>
              <li class="mappform"><a href="<?php echo site_url(); ?>recipient/index">Application Form</a></li>
              <li class="mcontact"><a href="<?php echo site_url(); ?>welcome/contact" <?php if($this->uri->segment('2') == "contact"){echo 'class="hit"'; } ?>>contact us</a></li>
            </ul>
            </nav> 
          </td>

        </tr>
      </table>
    </div>
  </div>
</div>