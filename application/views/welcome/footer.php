   <!--start footer-->
  <div id="footer">
 <div id="footercontent">
      <div class="fixedfooter">
        <div id="topcontent"><a href="#">top to TICA</a></div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="33%" valign="top" height="25"></td>
            <td valign="top" height="25"></td>
            <td width="33%" valign="top" height="25"></td>
          </tr>
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><h2>link TICA</h2></td>
                </tr>
                <tr>
                  <td height="8"></td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;&nbsp;&nbsp;- <a href="<?php echo site_url(); ?>" class="linkmenu">home</a><br />
                    &nbsp;&nbsp;&nbsp;- <a href="<?php echo site_url(); ?>feature/index" class="linkmenu">feature</a><br />
                    &nbsp;&nbsp;&nbsp;- <a href="<?php echo site_url(); ?>news/index" class="linkmenu">news</a><br />
                    &nbsp;&nbsp;&nbsp;- <a href="#" class="linkmenu">statistic</a><br />
                    &nbsp;&nbsp;&nbsp;- <a href="<?php echo site_url(); ?>welcome/contact" class="linkmenu">contact us</a></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><h2>latest news</h2></td>
                </tr>
                <tr>
                  <td height="8"></td>
                </tr>
                <tr>
                  <td valign="top">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <?php
						$this->db->order_by('news_id','DESC');
						$this->db->limit('3');
						$results = $this->db->get('news');						
						foreach($results->result_array() as $listtravel) {
							?>
                            <tr>
                                <td width="74" valign="top"><img src="<?php echo base_url(); ?>upload/news/<?php echo $listtravel['news_thumnail']; ?>" width="68" height="68" /></td>
                              <td width="664">
                              <a href="<?php echo base_url(); ?>news/newsdetail/<?php echo $listtravel['news_id']; ?>">
                              
                                <?php 
						echo $listtravel['news_title']; 					
					 ?><br />
                              <img src="<?php echo base_url(); ?>assets/welcome/images/ico-updates.png" width="30" height="30" align="absmiddle" />
                              <span class="detailexpand"><?php echo $listtravel['news_create']; ?></span>
                              </a></td>
                            </tr>
                            <tr>
                              <td valign="top">&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <?php } ?>
                            
                            </table></td>
                </tr>
              </table></td>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><h2>contact</h2></td>
                </tr>
                <tr>
                  <td height="8"></td>
                </tr>
                <tr>
                  <td valign="top">The Government Complex Ratthaprasasanabhakti (B) Building, South Zone,<br />
                  8th Floor, 120 Moo 3 Chaengwattana Road, Thungsonghong, Laksi District, Bangkok 10210 <br />
                  Tel. 02 203 5000 Fax. 02 143 9327</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><h2>social media</h2></td>
                </tr>
                <tr>
                  <td height="8"></td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/welcome/images/ico-fb.png" width="37" height="37" align="absmiddle" /> <a href="https://www.facebook.com/ticacooperation" class="linksocial" target="_blank">ThaiMFA</a></td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/welcome/images/ico-yt.png" width="37" height="37" align="absmiddle" /> <a href="https://www.youtube.com/user/ticacooperation" class="linksocial" target="_blank">mFaThailand</a></td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/welcome/images/ico-tw.png" width="37" height="37" align="absmiddle" /> <a href="https://twitter.com/mfathai_pr_en" target="_blank" class="linksocial">mFaThai</a></td>
                </tr>
              </table></td>
          </tr>
        </table>
      </div>
    </div>
 <div id="copyright">
      <div class="fixedfooter">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>© Copyright  2016, Thailand International Cooperation Agency (TICA) ,Ministry of Foreign Affairs. Kingdom of Thailand. All rights reserved. <a href="http://www.jjsofttech.com/" class="linkcopyright">TIT</a></td>
            <td></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
</body>
</html>
