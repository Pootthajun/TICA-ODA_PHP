<?php $this->load->view($this->config->item('wconfig_template_welcome') .'header'); ?>
<link href="<?php echo base_url(); ?>assets/welcome/css/status.css" rel="stylesheet" type="text/css" />


<body>
<?php $this->load->view($this->config->item('wconfig_template_welcome') .'menu'); ?>
<div id="bannertop" style="display:none;">
  <div>
    <ul class="allinone_bannerRotator_list">
      <li data-text-id="#allinone_bannerRotator_photoText1"><img src="<?php echo base_url(); ?>assets/welcome/images/banners/01_universal.jpg" alt="" /></li>
      <li data-text-id="#allinone_bannerRotator_photoText2"><img src="<?php echo base_url(); ?>assets/welcome/images/banners/02_universal.jpg" alt="" /></li>
      <li data-text-id="#allinone_bannerRotator_photoText3"><img src="<?php echo base_url(); ?>assets/welcome/images/banners/03_universal.jpg" alt="" /></li>
    </ul>
  </div>
</div>
<div class="spacer"></div>
<div id="container"> 
  
  <!--start key message-->
<!--  <div id="keymessage"></div>
  <div class="spacer"></div>-->
  
  <!--start content middle-->
  
   <div id="content">
    <div class="cfixed">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><div class="breadcrumb"><img src="<?php echo base_url(); ?>assets/welcome/images/icon-map.png" align="absmiddle"/><span>คุณอยู่ที่ : <a href="<?php echo site_url(); ?>">Home</a> / </span><span class="breadcrumbstay"><?php echo $header; ?></span> <span style="float:right; line-height:40px;"><a href="javascript:history.back();">< Back</a></span></div></td>
        </tr>
        <tr>
          <td  height="5"></td>
        </tr>
        <tr>
          <td><img src="<?php echo base_url(); ?>assets/welcome/images/line.jpg" height="4" width="1080" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="720" valign="top">
                  <?php print displayStatus();?>
                 <?php  $this->load->view($module."/".$page); ?></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
  </div>
  <div class="spacer"></div>
  
  <!--start content parthner-->
  <?php $this->load->view($this->config->item('wconfig_template_welcome') .'link'); ?>
  <div class="spacer"></div>
  

    
   
<?php $this->load->view($this->config->item('wconfig_template_welcome') .'footer'); ?>