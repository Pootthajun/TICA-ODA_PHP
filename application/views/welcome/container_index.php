<?php $this->load->view($this->config->item('wconfig_template_welcome') .'header'); ?>


<body>
<?php $this->load->view($this->config->item('wconfig_template_welcome') .'menu'); ?>
<div id="bannertop" style="display:none;">
  <div>
    <ul class="allinone_bannerRotator_list">
      <li data-text-id="#allinone_bannerRotator_photoText1"><img src="<?php echo base_url(); ?>assets/welcome/images/banners/01_universal.jpg" alt="" /></li>
      <li data-text-id="#allinone_bannerRotator_photoText2"><img src="<?php echo base_url(); ?>assets/welcome/images/banners/02_universal.jpg" alt="" /></li>
      <li data-text-id="#allinone_bannerRotator_photoText3"><img src="<?php echo base_url(); ?>assets/welcome/images/banners/03_universal.jpg" alt="" /></li>
    </ul>
  </div>
</div>
<div class="spacer"></div>
<div id="container"> 
  
  <!--start key message-->
<!--  <div id="keymessage"></div>
  <div class="spacer"></div>-->
  
  <!--start content middle-->
  <div id="content">
  <div class="cfixed">
    <?php print displayStatus();?>
   <?php  $this->load->view($module."/".$page); ?>
     </div>
</div>
  <div class="spacer"></div>
  
  <!--start content parthner-->
  <?php $this->load->view($this->config->item('wconfig_template_welcome') .'link'); ?>
  <div class="spacer"></div>
  

    
   
<?php $this->load->view($this->config->item('wconfig_template_welcome') .'footer'); ?>