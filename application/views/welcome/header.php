<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Thailand International Cooperation Agency (TICA)</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />-->
<meta name="Description" content="Thailand International Cooperation Agency (TICA) กรมความร่วมมือระหว่างประเทศ"/>
<meta name="Keywords" content="" />
<meta name="author" content="" />
<meta name="robots" content="index,follow" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:url" content="#" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/welcome/css/reset.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/welcome/css/stylebasic.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/welcome/css/layouts.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/welcome/css/status.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/welcome/css/superfish.css" />

<link rel="shortcut icon" href="" />

<!-- //super fish only don't tuch/ -->
 <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery.js"></script>
 <script src="<?php echo base_url(); ?>assets/welcome/js/hoverIntent.js"></script>
<script src="<?php echo base_url(); ?>assets/welcome/js/superfish.js"></script>
<script>
	jQuery(document).ready(function($) {	 
	// end banner
		var example = $('#example').superfish({
			delay:       1000,                            // one second delay on mouseout
			animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
			autoArrows:  true   
		});    
                });
</script>
<!-- //super fish only/ -->

<!--<script src="js/jquery-1.11.1.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/welcome/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/welcome/js/jquery-ui.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/welcome/js/script.js"></script>
<!-- Banner Rotator -->
<script src="<?php echo base_url(); ?>assets/welcome/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/welcome/js/allinone_bannerRotator.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/welcome/js/jquery.nicescroll.js" type="text/javascript"></script>


<!-- png fixed -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/welcome/js/jquery.pngFix.js"></script>
<script>
	jQuery(document).ready(function($) {	
			/*
			$("html").niceScroll();
			$("#getdata").niceScroll();
			// scroll-to-top animate
			$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
				return false;
			});
			*/
			//banner
			jQuery('#bannertop').allinone_bannerRotator({
				skin: 'universal',
				width: 1600,
				height: 650,
				width100Proc:true,
				responsive:true,
				thumbsWrapperMarginBottom:45,				
				autoHideBottomNav:false,
				showPreviewThumbs:false,
				defaultEffect: 'fade'
			});	
			
			// end banner
	/*			var example = $('#example').superfish({
					animation:   {opacity:'show',height:'show'}, 
					delay:         800, 
 				});  */ 
				
	$(document).pngFix(); 
	// ^^^ pngfix
		});
		
	 
</script>
</head>