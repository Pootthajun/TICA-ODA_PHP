<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Admin extends MY_Controller{

	function index(){
            if($this->session->userdata('userlogin') == TRUE){                
                redirect( 'dashboard/admin/index','refresh');
            }else{
		$data['page'] = $this->config->item('wconfig_template_login').'login';
                $this->load->view($this->_container_login,$data);
            }
	}
        
        function checklogin(){
            $this->db->where('user_username',$this->input->post('username'));
            $this->db->where('user_password', md5($this->input->post('password')));
            $Q = $this->db->get('user');
            $row = $Q->row();
            
            if (isset($row))
            {                
                //set session
                $this->session->set_userdata('userid', $row->user_id);
                $this->session->set_userdata('level', $row->user_level);
		$this->session->set_userdata('fullname', $row->user_fullname);
                $this->session->set_userdata('userlogin',TRUE);
                
               /* setcookie("userlogin",TRUE,time()+60*60*24*30, '/');
                setcookie("username",$row->user_username,time()+60*60*24*30, '/');
                setcookie("userid",$row->user_id,time()+60*60*24*30, '/');
                setcookie("group",$row->user_group,time()+60*60*24*30, '/');
                * 
                */
                
                //update last login
                $data = array(
                    'user_update'=>(date('Y-m-d H:i:s'))
                );
                $this->db->where('user_id',$this->session->userdata('userid'));
                $this->db->update('user',$data);
                //flashMsg('success',$this->lang->line('login_success'));
                redirect( 'dashboard/admin/index','refresh');
            }else{
                //flashMsg('warning',$this->lang->line('user_cannotfound'));
	     	redirect( 'admin/index','refresh');
            }

                
        }
		
	function logout(){
            $this->session->sess_destroy();
            redirect( 'admin/index','refresh');

		}    

}
