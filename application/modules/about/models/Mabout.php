<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MAbout extends CI_Model{   
    
    function update(){
        $data = array(
            'about_detail' => nl2br($this->input->post('about_detail')),
        );
        $this->db->update('about',$data);
    }
    
    function getAbout(){
        $data = array();
        $query = $this->db->get('about'); 
        $data = $query->row_array();
        $query->free_result();
        return $data;
    }
    
}