<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "About Us";
        $data['edit'] = $this->MAbout->getAbout();
    	$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function update(){
        $this->MAbout->update();
        flashMsg('success',$this->lang->line('insert_success'));
        redirect( 'about/admin','refresh');
    }
}