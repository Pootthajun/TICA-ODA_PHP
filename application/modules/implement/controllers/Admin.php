<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Implementing  Agency Table";
        $data['all'] = $this->MImplement->listallImplement();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addImplement(){
        $data['header'] = "Implementing  Agency";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addimplement';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertImplement(){
        $this->MImplement->insertimplement();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'implement/admin/','refresh');
    }
    
    function editimplement($implementid){
        $data['header'] = "Edit Implementing  Agency";
        $data['edit'] = $this->MImplement->getimplementbyid($implementid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editimplement';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateImplement(){
        $this->MImplement->updateImplement();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'implement/admin/','refresh');;
    }
    
    function deleteImplement($Implementid){
        $query = $this->db->get_where('project', array('implement_id' => $Implementid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'implement/admin/','refresh');            
        }else{
            $this->MImplement->deleteImplement($Implementid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'implement/admin/','refresh');
        }
    }
}