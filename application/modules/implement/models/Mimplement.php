<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MImplement extends CI_Model{
    
    function insertimplement(){
        $data = array(
            'implement_name'=>$this->input->post('implement_name'), 
            'implement_by'=>$this->session->userdata('userid'), 
            'implement_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('implement',$data);
        
    }
    
    function listallImplement(){
        $data = array();
        $this->db->order_by('implement_id',"ASC");
        $Q = $this->db->get('implement');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
	
		function getImplementByfunding($funding){
                    $data = array();
			$this->db->where('funding_id',$funding);
			$Q = $this->db->get('implement');
			foreach($Q->result_array() as $row){
           			 $data[] = $row;
			}
				$Q->free_result();
				return $data;
			}
    
    function getimplementbyid($implement){
        $data = array();
        $this->db->where('implement_id',$implement);
        $Q = $this->db->get('implement');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;        
    }
   
    function updateimplement(){
        $data = array(
            'implement_name'=>$this->input->post('implement_name'),   
            'implement_by'=>$this->session->userdata('userid'), 
            'implement_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('implement_id',$this->input->post('implement'));
        $this->db->update('implement',$data);
    }
    
    function deleteimplement($implement){              
           $this->db->where('implement_id',$implement);
           $this->db->delete('implement');       
    }
    

}