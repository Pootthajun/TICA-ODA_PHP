       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="4%">Id</th>
          <th width="22%">Region Name</th>
          <th width="22%">Country Name</th>
          <th width="29%">Country Group</th>

        </tr>
      </thead>
      <tbody>
<?php 
$count = 1;
foreach($all as $list){ ?>
        <tr>
          <td><?php echo $count++; ?></td>
          <td><?php 
		   $this->db->where('region_id',$list['region_id']);
			$Q = $this->db->get('region');
			$regionname = $Q->row_array();
			echo $regionname['region_name'];
			$Q->free_result();
		   ?></td>
          <td><?php echo $list['country_name']; ?></td>
          <td><?php 
		    $this->db->where('country_group_id',$list['country_group_id']);
			$QG = $this->db->get('country_group');
			$groupname = $QG->row_array();
			echo $groupname['country_group_name'];
			$QG->free_result();
		   ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
