<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MCountry extends CI_Model{
    
    function insertcountry(){
        $data = array(
            'region_id'=>$this->input->post('region_id'), 
            'country_name'=>$this->input->post('country_name'), 
            'country_group_id'=>$this->input->post('country_group_id'), 
            'flowtype'=>$this->input->post('flowtype'),
            'detail'=>$this->input->post('detail'),
            'countrytype'=>$this->input->post('countrytype'),
            'regionzoneoda'=>$this->input->post('regionzoneoda'),
        );
        $this->db->insert('country',$data);
        
    }
    
    function listallcountry(){
        $data = array();
		$this->db->where('countrytype',"1");
        $this->db->order_by('country_name',"ASC");
        $Q = $this->db->get('country');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function listallcountrytype(){
        $data = array();
        $this->db->where('country_group_id',"1");
        $this->db->order_by('country_id',"ASC");
        $Q = $this->db->get('country');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }

    function listallcountryrecipient(){
        $data = array();
        $this->db->group_by('country_id');
        $Q = $this->db->get('recipient');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
	
    function listallcgroup(){
        $data = array();
        $this->db->order_by('country_group_id',"ASC");
        $Q = $this->db->get('country_group');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getCountrytypebyid($countryid){
        //$data = array();getcountrytypebyid
        $this->db->where('country_id',$countryid);
        $Q = $this->db->get('country');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updtecountry(){
        $data = array(
            'region_id'=>$this->input->post('region_id'), 
            'country_name'=>$this->input->post('country_name'), 
            'country_group_id'=>$this->input->post('country_group_id'), 
            'flowtype'=>$this->input->post('flowtype'),
            'detail'=>$this->input->post('detail'),
            'countrytype'=>$this->input->post('countrytype'),
            'regionzoneoda'=>$this->input->post('regionzoneoda'),     
        );
        $this->db->where('country_id',$this->input->post('countryid'));
        $this->db->update('country',$data);
    }
    
    function deletecountry($countryid){              
           $this->db->where('country_id',$countryid);
           $this->db->delete('country');       
    }
    function getAllCountryIndex(){
        $data = array();
		$queryc = "select * from (
						select country_id from disbureseme group by country_id 
				UNION 
						select country_id from recipient group by country_id
		) country group by country.country_id ";
		$res = $this->db->query($queryc); 
        foreach($res->result_array() as $row){
            $data[] = $row;
        }
        $res->free_result();
        return $data;
    }
    
    function getAllCountrypay(){
        $data = array();
		$queryc = "select c.country_name,r.country_id from country as c"
                        . " left join recipient r on r.country_id = c.country_id"
                        . " group by r.country_id ";
		
		$res = $this->db->query($queryc); 
        foreach($res->result_array() as $row){
            $data[] = $row;
        }
        $res->free_result();
        return $data;
    }
    
    function getallFlowtype(){
        $data = array();
	$this->db->order_by('flowtype_id','ASC');		
	$res = $this->db->get('flowtype'); 
        foreach($res->result_array() as $row){
            $data[] = $row;
        }
        $res->free_result();
        return $data;
    }
    
    function getallCountryZone(){
        $data = array();
	$this->db->order_by('regionoda_id','ASC');		
	$res = $this->db->get('regionzoneoda'); 
        foreach($res->result_array() as $row){
            $data[] = $row;
        }
        $res->free_result();
        return $data;
    }
    
    function getallCountryType(){
        $data = array();
	$this->db->order_by('ctype_id','ASC');		
	$res = $this->db->get('country_type'); 
        foreach($res->result_array() as $row){
            $data[] = $row;
        }
        $res->free_result();
        return $data;
    }
  

}