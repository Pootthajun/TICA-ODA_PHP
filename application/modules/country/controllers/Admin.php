<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Country Table";
        $data['all'] = $this->MCountry->listallCountry();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addCountry(){
        $data['header'] = "Add Country name";
        $data['region'] = $this->MRegion->listallregion();
        $data['group'] = $this->MCountry->listallcgroup();
		$data['flow']=$this->MCountry->getallFlowtype();
		$data['zone'] = $this->MCountry->getallCountryZone();
		$data['type'] = $this->MCountry->getallCountryType();
    	$data['page'] = $this->config->item('wconfig_template_admin').'addcountry';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertCountry(){
		
        $this->MCountry->insertCountry();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'country/admin/','refresh');
    }
    
    function editCountry($countryid = 0){
        $data['header'] = "Country Table";
        $data['region'] = $this->MRegion->listallregion();
        $data['edit'] = $this->MCountry->getCountrytypebyid($countryid);
        $data['flow']=$this->MCountry->getallFlowtype();
        $data['zone'] = $this->MCountry->getallCountryZone();
        $data['type'] = $this->MCountry->getallCountryType();
        $data['group'] = $this->MCountry->listallcgroup();
    	$data['page'] = $this->config->item('wconfig_template_admin').'editcountry';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateCountry(){
        if($this->input->post('countryid') != 0){
            $this->insertCountry();
        }else{
        $this->MCountry->updtecountry();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'country/admin/','refresh');
        }
    }
    
    function deleteCountry($countryid){
        $query = $this->db->get_where('recipient', array('country_id' => $countryid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'country/admin/','refresh');            
        }else{
            $this->MCountry->deleteCountry($countryid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'country/admin/','refresh');
        }
    }
}