 <p class="addbutton"> 
<?php  if( !empty( $menu ) ) { ?>
  <a href="<?php echo site_url(); ?>course/admin/addcourse" class="button green">Add</a>  
  <?php } ?>
  </p>
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="13%">Id</th>
          <th width="64%">Course Name</th>
         <?php  if( !empty( $menu ) ) { ?>
          <th>&nbsp;</th>
		<?php } ?>
        </tr>
      </thead>
      <tbody>
<?php 
$count = 1;
foreach($all as $list){ ?>
        <tr>
          <td><?php echo $count++; ?></td>
          <td><?php echo $list['course_name']; ?></td>
          <?php  if( !empty( $menu ) ) { ?>
          <td width="120" align="right"><a href="<?php echo site_url(); ?>course/admin/editcourse/<?php echo $list['course_id']; ?>" class="button blue">Edit</a><a href="<?php echo site_url(); ?>course/admin/deletecourse/<?php echo $list['course_id']; ?>"  onclick="return confirm('Are you sure you want to delete Course name <?php echo $list['course_name']; ?> ?')" class="button red"><?php echo $this->lang->line('delete'); ?></a></td>
      <?php } ?>
        </tr>
        <?php } ?>
      </tbody>
    </table>
