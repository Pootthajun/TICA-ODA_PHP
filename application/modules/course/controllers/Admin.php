<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Course";
        $data['all'] = $this->MCourse->listallcourse();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addcourse(){
        $data['header'] = "Add course name";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addcourse';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertcourse(){
        $this->MCourse->insertcourse();
        flashMsg('success',$this->lang->line('insert_success'));
	    redirect( 'course/admin/','refresh');
    }
    
    function editcourse($courseid){
        $data['header'] = "Edit Course";
        $data['edit'] = $this->MCourse->getcoursetypebyid($courseid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editcourse';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updatecourse(){
        $this->MCourse->updtecourse();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'course/admin/','refresh');;
    }
    
    function deletecourse($courseid){
        $query = $this->db->get_where('recipient', array('course' => $courseid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'course/admin/','refresh');            
        }else{
            $this->MCourse->deletecourse($courseid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'course/admin/','refresh');
        }
    }
}