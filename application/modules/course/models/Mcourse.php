<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MCourse extends CI_Model{
    
    function insertcourse(){
        $data = array(
            'course_name'=>$this->input->post('course_name'),          
        );
        $this->db->insert('course',$data);
        
    }
    
    function listallcourse(){
        $data = array();
        $this->db->order_by('course_id',"ASC");
        $Q = $this->db->get('course');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getcoursetypebyid($courseid){
        //$data = array();getcoursetypebyid
        $this->db->where('course_id',$courseid);
        $Q = $this->db->get('course');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updtecourse(){
        $data = array(
            'course_name'=>$this->input->post('course_name'),         
        );
        $this->db->where('course_id',$this->input->post('courseid'));
        $this->db->update('course',$data);
    }
    
    function deletecourse($courseid){              
           $this->db->where('course_id',$courseid);
           $this->db->delete('course');       
    }
    

}