<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MKindtype extends CI_Model{
    
    function insertkindtype(){
        $data = array(
            'kind_type_name'=>$this->input->post('kind_type_name'),
            'kind_type_by'=>$this->session->userdata('userid'),
            'kind_type_create'=>date('Y-m-d')            
        );
        $this->db->insert('kind_type',$data);
        
    }
    
    function listallKindtype(){
        $data = array();
        $this->db->order_by('kind_type_id',"ASC");
        $Q = $this->db->get('kind_type');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getKindtypetypebyid($kindtypeid){
        //$data = array();
        $this->db->where('kind_type_id',$kindtypeid);
        $Q = $this->db->get('kind_type');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;        
    }
    
    function updatekindtype(){
        $data = array(
            'kind_type_name'=>$this->input->post('kind_type_name'),
            'kind_type_by'=>$this->session->userdata('userid'),
            'kind_type_update'=>date('Y-m-d')            
        );
        $this->db->where('kind_type_id',$this->input->post('kindtypeid'));
        $this->db->update('kind_type',$data);
    }
    
    function deletekindtype($kindtypeid){              
           $this->db->where('kind_type_id',$kindtypeid);
           $this->db->delete('kind_type');       
    }
    
}