<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));      
		
		
	
    }
	
    
    function index(){

        $data['header'] = "Kind Type Table";		
        $data['all'] = $this->MKindtype->listallKindtype();
        $data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addkindtype(){
        $data['header'] = "Kind Type Table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addkindtype';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertkindtype(){
        $this->MKindtype->insertkindtype();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'kindtype/admin/','refresh');
    }
    
    function editkindtype($kindtypeid){
        $data['header'] = "Edit Kind type type table";
        $data['edit'] = $this->MKindtype->getKindtypetypebyid($kindtypeid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editkindtype';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updatekindtype(){
        $this->MKindtype->updatekindtype();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'kindtype/admin/','refresh');;
    }
    
    function deletekindtype($kindtypeid){
        $query = $this->db->get_where('project',array('project_kind_id' => $kindtypeid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'kindtype/admin/','refresh');            
        }else{
            $this->MKindtype->deletekindtype($kindtypeid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'kindtype/admin/','refresh');
        }
    }
}