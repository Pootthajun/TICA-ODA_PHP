<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "INKind Table";
        $data['all'] = $this->Mkind->listallKind();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addKind(){
        $data['header'] = "Kind table";
		$data['project']=$this->MProject->listallproject();
    	$data['page'] = $this->config->item('wconfig_template_admin').'addkind';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertKind(){
        $this->MKind->insertkind();
        flashMsg('success',$this->lang->line('insert_success'));
	    redirect( 'kind/admin/','refresh');
    }
    
    function editkind($kindid){
        $data['header'] = "Edit Kind name";
        $data['edit'] = $this->MKind->getkindbyid($kindid);
        $data['project']=$this->MProject->listallproject();
    	$data['page'] = $this->config->item('wconfig_template_admin').'editkind';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateKind(){
        $this->MKind->updateKind();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'kind/admin/','refresh');;
    }
    
    function deleteKind($Kindid){
        $query = $this->db->get_where('project', array('bt_id' => $Kindid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'kind/admin/','refresh');            
        }else{
            $this->MKind->deleteKind($Kindid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'kind/admin/','refresh');
        }
    }
}