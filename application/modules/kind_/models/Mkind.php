<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MKind extends CI_Model{
    
    function insertkind(){
        $data = array(
            'kind_name'=>$this->input->post('kind_name'), 
        );
        $this->db->insert('web_inkind',$data);
        
    }
    
    function listallKind(){
        $data = array();
        $this->db->order_by('kind_id',"ASC");
        $Q = $this->db->get('web_inkind');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getkindbyid($kind){ 
        //$data = array();
        $this->db->where('kind_id',$kind);
        $Q = $this->db->get('web_inkind');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }

    function getKindByprojectid($projectid){
        $data = array();
        $this->db->where('project_id',$projectid);
        $Q = $this->db->get('project_kind');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function updatekind(){
        $data = array(
            'kind_name'=>$this->input->post('kind_name'),   
        );
        $this->db->where('kind_id',$this->input->post('kind'));
        $this->db->update('web_inkind',$data);
    }
    
    function deletekind($kind){              
           $this->db->where('kind_id',$kind);
           $this->db->delete('web_inkind');       
    }
    

}