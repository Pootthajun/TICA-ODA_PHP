<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Dashboard";
        $data['plan'] = $this->MPlan->listAllplan();
        $data['project'] = $this->MProject->listallproject();
        $data['activity'] = $this->MActivity->listallActivity();
        $data['aid'] = $this->MAid->listallAidforDashboard();
        $data['recipient'] = $this->MRecipient->listAllrecipient();
        $data['country'] = $this->MCountry->listallcountryrecipient();
        $data['user'] = $this->MUser->listalluser();
    	$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
}
