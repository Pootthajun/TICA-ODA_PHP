     <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
		
      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([		
		<?php 
		//all recipient
		$Q = $this->db->query("select rec_id from recipient ");
		$total_receipion = $Q->num_rows();
		
		//reject percent
		$Q = $this->db->query("select rec_id from recipient where status = 3");
		$total_reject = $Q->num_rows();
		$percent_reject = (100 * $total_reject)  / $total_receipion;

		//waiting percent
		$Q = $this->db->query("select rec_id from recipient where status = 1");
		$total_waiting = $Q->num_rows();
		$percent_waiting = (100 * $total_waiting)  / $total_receipion;
		
		//approve percent
		$Q = $this->db->query("select rec_id from recipient where status = 2 and rec_id not in (SELECT rec_id FROM money_paid) ");
		$total_approve = $Q->num_rows();
		$percent_approve = (100 * $total_approve)  / $total_receipion;
		
		//paid percent
		$Q = $this->db->query("select rec_id from recipient where status = 2 and rec_id in (SELECT rec_id FROM money_paid) ");
		$total_paid = $Q->num_rows();
		$percent_paid = (100 * $total_paid)  / $total_receipion;
		?>
		
          ['Paid', <?php echo (int)$percent_paid; ?>],
          ['Reject', <?php echo (int)$percent_reject; ?>],
		   ['Waiting', <?php echo (int)$percent_waiting; ?>],
          ['Approved', <?php echo (int)$percent_approve; ?>],
          
        ]);

        // Set chart options
        var options = {'title':'Recipient Status',
                       'width':900,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
	  
	  
	  

      	google.load('visualization', '1', {packages: ['corechart', 'bar']});
		google.setOnLoadCallback(drawDualY);

function drawDualY() {
      	var data = new google.visualization.DataTable();
		data.addColumn('string', 'Year of budget');
		data.addColumn('number', 'Cooperation(Million Bath) ');
		 
		<?php
		//Your database query goes here
		$q = $this->db->query("SELECT min(mp_year) as start_year, max(mp_year) as end_year FROM money_paid  ");
		$rowq = $q->row_array(); 
		$start_date = $rowq['start_year'];
		$end_date = $rowq['end_year'];
		$data = array(); 
		$data_for_chart = 0;
		if(!empty($start_date)){
			for($i=$start_date; $i <= $end_date; $i++){
				$thaiyear = $i-543;
				$list = $this->db->query("
				select sum(total) as yeartotal from (
					SELECT  sum(mp_amount) as total, mp_year as years  FROM  money_paid where  mp_year = $i 
					UNION 
					SELECT  sum(dis_budget) as total, YEAR(dis_date)+543 as years   FROM  disbureseme  where  YEAR(dis_date) = $thaiyear 
					) as tablesummary where years = $i 
				");
	 
				$row = $list->row_array(); 
				 $data[] = "['".$i."',".$row['yeartotal']."]"; 
			}			  
			$data_for_chart = implode(", ",$data);
		}
		?>
		data.addRows([
         <?php echo $data_for_chart; ?>
         ]);
		  
      var options = {
        chart: {
          title: 'Cooperation Budget',
          subtitle: 'Expense' //2550-2558
        }   
      };

     var material = new google.charts.Bar(document.getElementById('chart_div2'));
      material.draw(data, options);
    }

    </script>
<div class="summary_bar">
<ul class="shortcut-buttons-set">
				
				<li></li>
				
				<li><a href="#" class="shortcut-button"><span>
				<div class="under_line">Plan</div> 
					<div class="number_dashboard"><?php echo count($plan); ?></div> 
				</span></a></li>
				
				<li><a href="#" class="shortcut-button"><span>
					<div class="under_line">Project</div> 
					<div class="number_dashboard"><?php echo count($project); ?></div> 
				</span></a></li>
				
				<li><a href="#" class="shortcut-button"><span>
					<div class="under_line">Activity</div> 
					<div class="number_dashboard"><?php echo count($activity); ?></div> 
				</span></a></li>
				
				<li><a rel="modal" href="#messages" class="shortcut-button"><span>
					<div class="under_line">AID</div> 
					<div class="number_dashboard"><?php echo count($aid); ?></div> 
				</span></a></li>
                
                <li><a rel="modal" href="#messages" class="shortcut-button"><span>
					<div class="under_line">Recipient</div> 
					<div class="number_dashboard"><?php echo count($recipient); ?></div> 
				</span></a></li>
                
                 <li><a rel="modal" href="#messages" class="shortcut-button"><span>
					<div class="under_line">Country</div> 
					<div class="number_dashboard"><?php echo count($country); ?></div> 
				</span></a></li>
                
                 <li><a rel="modal" href="#messages" class="shortcut-button"><span>
					<div class="under_line">User</div> 
					<div class="number_dashboard"><?php echo count($user); ?></div> 
				</span></a></li>
				
			</ul>
</div>
<p><br><br><br><br></p>
    <!--Div that will hold the pie chart-->

    <div id="chart_div"  ></div>
<p></p>

    <div id="chart_div2" style=" width: 900px; height: 300px;"  ></div>
    
    <!--<div class="summary_bar">
<ul class="shortcut-buttons-set">
				
				<li></li>
				
				<li><a href="#" class="shortcut-button"><span>
			 Backup<br /> Database 
					 
				</span></a></li>
				
				<li><a href="#" class="shortcut-button"><span>
					Repair <br />Database
				</span></a></li>
				
				 
				
			</ul>
</div>-->
<p><br><br><br><br></p>