<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
        $this->load->library('upload');
	
    }

    function index(){ 
        $data['header'] = "Recipient Table";
        $data['all'] = $this->MRecipient->listAllrecipient();
        $data['plan'] = $this->MPlan->listAllplan();
        $data['aid'] = $this->MAid->listallAid();
        $data['project'] = $this->MProject->listallproject();
        $data['activity'] = $this->MActivity->listallActivity();
    	$data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'index';    
    	$this->load->view($this->_container_admin,$data);
    }

    function insert(){		
        $this->MRecipient->insert();
        flashMsg('success',$this->lang->line('insert_success'));
        redirect( 'recipient/admin/','refresh');
			
    }

    function deleterecipient($recid){
        $filedel = $this->MRecipient->getRecipientById($recid);
        @unlink('./upload/'.$filedel['file']);
        @unlink('./upload/signature/'.$filedel['signature']);
        $this->MRecipient->delete($recid);
        flashMsg('success',$this->lang->line('delete_success'));
        redirect( 'recipient/admin/','refresh');
    }

    function addrecipient($recid =0){		
	$data['edit'] = $this->MRecipient->getRecipientById($recid);	
	if(empty($recid)) { 
	$aidid = $this->input->post('aid_id');
	}else{
	$aidid = $data['edit']['aid_id'];			
	}
	if(empty($aidid)) {		
       // flashMsg('warning',"Please select aid name");
        redirect( 'recipient/admin/','refresh');
        }else{		
        $data['header'] = "Recipient detail";
        $data['edit'] = $this->MRecipient->getRecipientById($recid);		
		}
	$data['agecal'] = $this->calAgeDate($data['edit']['birthday']);
		//$aidid = $data['edit']['aid_id'];
        $data['aid'] = $this->MAid->getAidbyid($aidid);
        $data['in'] = $this->MInstitute->listallInstitute();
        $data['country'] = $this->MCountry->listallcountry();
	$data['user'] = $this->MUser->listalluser();
        $data['page'] = $this->config->item('wconfig_template_admin').'editrecipient';        
        $this->load->view($this->_container_admin,$data);
    }
	
	private function calAgeDate($birthdate){ 
			if(!empty($birthdate)){
				$year_curent = date('Y',time());
				$month_curent = date('m',time()); 
				
				$date_from = explode('-',$birthdate);
				$year_from = $date_from[0];
				$month_from = $date_from[1];
	
				// Calculate total month of Age
				$total_mon = (12 - $month_from);
				$total_mon += (($year_curent - $year_from - 1)*12);
				$total_mon += $month_curent+1;

				// Get Year/Month of Age
				$year = floor($total_mon/12);
				$month = ($total_mon%12);
 				$agecal = '';
				if ($month==0) {
					 $agecal = $year . ' Year' ;
				} else {
					 $agecal = $year . ' Year '  .  $month .' Month';
				}

			} else {
				$agecal = '';
			}
			return $agecal;
		}

    function update(){
	/*$this->load->library('form_validation');
    $this->form_validation->set_rules('country_id', 'Please select country', 'required');
	$this->form_validation->set_rules('approve_by', 'Please select user approve', 'required');*/
	    
                        if($this->input->post('recid') == ""){
						$this->addrecipient();
						}else{
							$recid = $this->input->post('recid');
							$this->addrecipient($recid);
						}
						//redirect( 'recipient/admin/addrecipient','refresh');
              
		
					if($this->input->post('recid') == ""){
						//$this->insert();
					$this->MRecipient->insert();
				   flashMsg('success',$this->lang->line('insert_success'));
					redirect( 'recipient/admin/','refresh');
					 exit();
					}else{
				   $this->MRecipient->update();
				   flashMsg('success',$this->lang->line('update_success'));
					redirect( 'recipient/admin/','refresh');
				}
			
	
    }
	
	function getRecipientbyAid(){	
	     $id=$this->input->post('id');
             $status=$this->input->post('status'); 
             if(!empty($status)){  
			 	$qstatus = "and r.status =' $status' ";
			 }else{
				 $qstatus = "";
			 }
             $Q=$this->db->query("select r.*,a.aid_name,c.country_name from recipient as r 
			  inner join aid as a on r.aid_id = a.aid_id
              inner join country as c on r.country_id = c.country_id
			  where r.aid_id = '$id'
                              ". $qstatus."
			   ");
             
	$data['all'] = $Q->result_array();
	$data['menu'] = $this->MUser->getMenubyUser();    
        $data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
	$this->load->view($this->_view_subsector,$data);
	}
	
	function getrecipientbyplanid(){
		   $id=$this->input->post('id');  
			$status=$this->input->post('status');   
		//	
			if(!empty($status)){  
			 	$qstatus = "and r.status =' $status' ";
			 }else{
				 $qstatus = "";
			 }
             $Q=$this->db->query("select r.*,a.aid_name,a.activity_id,c.country_name,
             p.plan_id,project.plan_id,activity.project_id
              from recipient as r 
			  inner join aid as a on r.aid_id = a.aid_id
              inner join country as c on r.country_id = c.country_id
              inner join activity as activity on activity.activity_id=a.activity_id
              inner join project as project on project.project_id=activity.project_id
              inner join plan as p on p.plan_id=project.plan_id
			  where p.plan_id = '$id'
			 ". $qstatus."
			   ");
		     $data['all'] = $Q->result_array();
			$data['menu'] = $this->MUser->getMenubyUser();    
			$data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
			$this->load->view($this->_view_subsector,$data);
	 	}
	
		function getprojectlistbyplanid(){
			 $id=$this->input->post('id');
           $Q=$this->db->query("select * from project where  plan_id='$id'"); 
           $data['option'] = $Q->result_array();	          
			$data['page'] = $this->config->item('wconfig_template_admin').'projectlist';      
			$this->load->view($this->_view_subsector,$data); 
	}
	
	function getrecipientbyprojectid(){
			$projectid=$this->input->post('projectid');   
			$planid=$this->input->post('planid');
                        $status=$this->input->post('status');   
            if(!empty($status)){  
			 	$qstatus = "and r.status =' $status' ";
			 }else{
				 $qstatus = "";
			 }
             $Q=$this->db->query("select r.*,a.aid_name,a.activity_id,c.country_name,
             p.plan_id,project.project_id,project.plan_id,activity.project_id
              from recipient as r 
			  inner join aid as a on r.aid_id = a.aid_id
              inner join country as c on r.country_id = c.country_id
              inner join activity as activity on activity.activity_id=a.activity_id
			  inner join project as project on project.project_id=activity.project_id
              inner join plan as p on p.plan_id='$planid'
			  where project.project_id='$projectid'
			  ".$qstatus."
			   ");
			   
			  $data['all'] = $Q->result_array();
			$data['menu'] = $this->MUser->getMenubyUser();    
			$data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
			$this->load->view($this->_view_subsector,$data);
	}
	
	
		function getActivitylistbyprojectid(){
				 $id=$this->input->post('id');
          		 $Q=$this->db->query("select * from activity where  project_id='$id'"); 
                 $data['option'] = $Q->result_array();	          
				$data['page'] = $this->config->item('wconfig_template_admin').'activityselectlist';      
				$this->load->view($this->_view_subsector,$data); 
	}
	
		function getAidlistbyActiivityid(){
				$id=$this->input->post('id');
          		 $Q=$this->db->query("select * from aid where  activity_id='$id'  "); 
                $data['option'] = $Q->result_array();	          
				$data['page'] = $this->config->item('wconfig_template_admin').'aidselectlist';      
				$this->load->view($this->_view_subsector,$data); 
	}
	
	function getrecipienttbyActiivityid(){
			$id=$this->input->post('id');  
                        $status=$this->input->post('status'); 
                        if(!empty($status)){  
			 	$qstatus = "and r.status =' $status' ";
			 }else{
				 $qstatus = "";
			 }
            $Q=$this->db->query("select r.*,a.aid_name,a.activity_id,c.country_name,
             p.plan_id,project.plan_id,activity.project_id
             from recipient as r 
			inner join aid as a on r.aid_id = a.aid_id
            inner join country as c on r.country_id = c.country_id
           inner join activity as activity on activity.activity_id=a.activity_id
			inner join project as project on project.project_id=activity.project_id
             inner join plan as p on p.plan_id=project.plan_id
			  where activity.activity_id='$id'
                              ".$qstatus."
			   ");
			    $data['all'] = $Q->result_array();
				$data['menu'] = $this->MUser->getMenubyUser();    
				$data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
				$this->load->view($this->_view_subsector,$data);
	}
	
	function getrecipientbystatus(){
			$plan = $this->input->post('plan');  
			$project = $this->input->post('project'); 
			$activity = $this->input->post('activity'); 
			$aid = $this->input->post('aid'); 
			$status = $this->input->post('status'); 
			$qplan = "";
			$qproject="";
			$qactivity="";
			$qaid="";
                        $qstatus="";
                        
                        if(empty($plan) && empty($project)&& empty($activity)&& empty($aid)){
                                $qstatus = "
                                inner join aid as a on r.aid_id = a.aid_id
                                inner join country as c on r.country_id = c.country_id
                                inner join activity as activity on activity.activity_id=a.activity_id
                                inner join project as project on project.project_id=activity.project_id
                                inner join plan as p on p.plan_id=project.plan_id
                                where r.status= '$status' " ;
                            }
                            
                            if(!empty($plan) && empty($project)&& empty($activity)&& empty($aid)){
                                $qplan = "
                                inner join aid as a on r.aid_id = a.aid_id
                                inner join country as c on r.country_id = c.country_id
                                inner join activity as activity on activity.activity_id=a.activity_id
                                inner join project as project on project.project_id=activity.project_id
                                inner join plan as p on p.plan_id=project.plan_id
                                where p.plan_id = '$plan' 
                                and r.status= '$status' " ;
                            }
			if((!empty($plan)) && (!empty($project))&& (empty($activity))&& (empty($aid))){
				$qproject = "
                                inner join aid as a on r.aid_id = a.aid_id
                                inner join country as c on r.country_id = c.country_id
                                inner join activity as activity on activity.activity_id=a.activity_id
                                inner join project as project on project.project_id=activity.project_id
                                inner join plan as p on p.plan_id='$plan'
                                where project.project_id='$project'
                                and r.status= '$status'
                                " ;
			 }
			if((!empty($plan)) && (!empty($project))&& (!empty($activity))&& (empty($aid))){
				$qactivity = "
                                    select r.*,a.aid_name,a.activity_id,c.country_name,
                                    p.plan_id,project.plan_id,activity.project_id
                                    from recipient as r 
                                    inner join aid as a on r.aid_id = a.aid_id
                                    inner join country as c on r.country_id = c.country_id
                                    inner join activity as activity on activity.activity_id=a.activity_id
                                    inner join project as project on project.project_id=activity.project_id
                                    inner join plan as p on p.plan_id=project.plan_id
                                    where activity.activity_id='$id'
                                    and r.status= '$status'
                                " ;
			 }
			if((!empty($plan)) && (!empty($project))&& (!empty($activity))&& (!empty($aid))){
				$qaid = "
                                    select r.*,a.aid_name,c.country_name from recipient as r 
                                    inner join aid as a on r.aid_id = a.aid_id
                                    inner join country as c on r.country_id = c.country_id
                                    where r.aid_id = '$id'
                                    and r.status= '$status'
                                " ;
			 }
                         $sql = "select r.*,a.aid_name,a.activity_id,c.country_name,
                                p.plan_id,project.plan_id,activity.project_id
                                from recipient as r              
                                ".$qaid."
                                ".$qactivity."
                                ".$qproject."
                                ".$qplan."
                                ".$qstatus."
			  ";
            $Q=$this->db->query($sql);
			   //echo $sql;
			    $data['all'] = $Q->result_array();
				$data['menu'] = $this->MUser->getMenubyUser();    
				$data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
				$this->load->view($this->_view_subsector,$data);
	}
        
        function waitapprov(){
        $status = 1;
        $data['header'] = "Recipient Waite for approve";
        $data['all'] = $this->MRecipient->listAllrecipientbyStatus($status);
        $data['plan'] = $this->MPlan->listAllplan();
        $data['aid'] = $this->MAid->listallAid();
        $data['project'] = $this->MProject->listallproject();
        $data['activity'] = $this->MActivity->listallActivity();
    	$data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'waitapprov';    
    	$this->load->view($this->_container_admin,$data);
        }
        
        function expirepassport(){
        $data['header'] = "Recipient Expire Passport";
        $data['all'] = $this->MRecipient->expirepassport();
        $data['plan'] = $this->MPlan->listAllplan();
        $data['aid'] = $this->MAid->listallAid();
        $data['project'] = $this->MProject->listallproject();
        $data['activity'] = $this->MActivity->listallActivity();
    	$data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'expirepassport';    
    	$this->load->view($this->_container_admin,$data);  
        }
        
        function expireInsurance(){
        $data['header'] = "Recipient Expire Insurance";
        $data['all'] = $this->MRecipient->expireInsurance();
        $data['plan'] = $this->MPlan->listAllplan();
        $data['aid'] = $this->MAid->listallAid();
        $data['project'] = $this->MProject->listallproject();
        $data['activity'] = $this->MActivity->listallActivity();
    	$data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'expireInsurance';    
    	$this->load->view($this->_container_admin,$data);  
        }
        
        function expirevisa(){
        $data['header'] = "Recipient Expire Visa";
        $data['all'] = $this->MRecipient->expirevisa();
        $data['plan'] = $this->MPlan->listAllplan();
        $data['aid'] = $this->MAid->listallAid();
        $data['project'] = $this->MProject->listallproject();
        $data['activity'] = $this->MActivity->listallActivity();
    	$data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'expirevisa';    
    	$this->load->view($this->_container_admin,$data);    
        }
        
        function getuserproject(){
		$id=$this->input->post('id');
		$user = $this->session->userdata('userid');                
		$project_assistant = '';
                $project_owner = '';
                $this->db->where('project_id',$id);
                $Q = $this->db->get('project_assistant'); 
                foreach($Q->result_array() as $listp){
			$project_assistant .= $listp['user_id'].",";  
		}
                
                $this->db->where('project_id',$id);
                $Q = $this->db->get('project'); 
                foreach($Q->result_array() as $listp){
			$project_owner = $listp['project_owner'];  
		}
		
		$auser = $project_assistant.$project_owner;
 
		  $ex = explode(",",$auser);
			 for($i=0;$i<count($ex);$i++){ 
					  if($ex[$i] == $this->session->userdata('userid')){ 
					
							echo '<input type="submit" name="button" id="button" value="Add" class="button green" />';
					}
			 }
	}
        


}