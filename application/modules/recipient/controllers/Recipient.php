<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recipient extends Welcome_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));   
        
        if($this->session->userdata('userwelcome') == FALSE){
                
                redirect('welcome/cannotregister','refresh'); 
        }     
	
    }
    
    function index(){
        $data['header'] = "Application Form"; 
        $data['module'] = $this->module;
	$data['page'] = $this->config->item('wconfig_template_welcome').'index';      
    	$this->load->view($this->_container_contact,$data); 
    }
    
    function newregister($recid = 0){
        $data['header'] = "Application Form"; 
        $data['edit'] = $this->MRecipient->getRecipientById($recid);
        $data['agecal'] = $this->calAgeDate($data['edit']['birthday']);
        $data['in'] = $this->MInstitute->listallInstitute();
        $data['country'] = $this->MCountry->listallcountrytype();

        $data['aid'] = $this->MAid->listallAidadmin();
        $data['module'] = $this->module;
	$data['page'] = $this->config->item('wconfig_template_welcome').'form1';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function listrecipientaid(){
        $member = $this->session->userdata('memberid');
        $data['header'] = "Application Form"; 
        $data['all'] = $this->MRecipient->listallrecbyrecid($member);
        $data['module'] = $this->module;
	$data['page'] = $this->config->item('wconfig_template_welcome').'listrecipientaid';      
    	$this->load->view($this->_container_contact,$data); 
    }
            
    function edit($recid){
        $data['header'] = "Application Form"; 
        $data['edit'] = $this->MRecipient->getRecipientById($recid);
        $data['agecal'] = $this->calAgeDate($data['edit']['birthday']);
        $data['in'] = $this->MInstitute->listallInstitute();
        $data['country'] = $this->MCountry->listallcountrytype();
        $data['aid'] = $this->MAid->listallAidadmin();
        $data['module'] = $this->module;
	$data['page'] = $this->config->item('wconfig_template_welcome').'form1';      
    	$this->load->view($this->_container_contact,$data); 
    }
    private function calAgeDate($birthdate){ 
			if(!empty($birthdate)){
				$year_curent = date('Y',time());
				$month_curent = date('m',time()); 
				
				$date_from = explode('-',$birthdate);
				$year_from = $date_from[0];
				$month_from = $date_from[1];
	
				// Calculate total month of Age
				$total_mon = (12 - $month_from);
				$total_mon += (($year_curent - $year_from - 1)*12);
				$total_mon += $month_curent+1;

				// Get Year/Month of Age
				$year = floor($total_mon/12);
				$month = ($total_mon%12);
 				$agecal = '';
				if ($month==0) {
					 $agecal = $year . ' Year' ;
				} else {
					 $agecal = $year . ' Year '  .  $month .' Month';
				}

			} else {
				$agecal = '';
			}
			return $agecal;
		}
    
    function form2($recid = 0){
        $data['header'] = "Application Form"; 
        $data['edit'] = $this->MRecipient->getRecipientById($recid);
        $data['in'] = $this->MInstitute->listallInstitute();
        $data['country'] = $this->MCountry->listallcountry();
        $data['module'] = $this->module;
	$data['page'] = $this->config->item('wconfig_template_welcome').'form2';      
    	$this->load->view($this->_container_contact,$data); 
    }
    
    function form3($recid = 0){
        $this->load->helper('captcha');
        $data['header'] = "Contact Us";
        $vals = array(
        'font_size'	=> 18,
        'word_length'	=> 5,
        'img_path'      => './captcha/',
        'img_width'	=> '150',
	'img_height'	=> '40',
        'img_url'       => base_url().'captcha/',
        'font_path'       => './system/fonts/texc.ttf',
            'colors'	=> array(
				'background'	=> array(200,200,255),
				'border'	=> array(200,200,255),
				'text'		=> array(61, 147, 218 ),
				'grid'		=> array(200,200,255),
			)
        );

        $cap = create_captcha($vals);
        $data = array(
                'captcha_time'  => $cap['time'],
                'ip_address'    => $this->input->ip_address(),
                'session_id'  => session_id(),
                'word'          => $cap['word']
        );
        $query = $this->db->insert_string('captcha', $data);
        $this->db->query($query);
        $data['capchar'] = $cap['image'];
        $data['header'] = "Application Form"; 
        $data['edit'] = $this->MRecipient->getRecipientById($recid);
        $data['in'] = $this->MInstitute->listallInstitute();
        $data['country'] = $this->MCountry->listallcountry();
        $data['module'] = $this->module;
	$data['page'] = $this->config->item('wconfig_template_welcome').'form3';      
    	$this->load->view($this->_container_contact,$data);  
    }
    
    function inserform1(){
        if($this->input->post('recid') == ""){
        $this->MRecipient->insertform1();
        $recid = $this->MRecipient->getRecipientMaxid();
        flashMsg('success','APPLICATION FORM Step 2');
        redirect( 'recipient/form2/'.$recid['rec_id'],'refresh');
        }else{
        
        $this->MRecipient->updateform1();
        $recid = $this->input->post('recid');
        flashMsg('success','APPLICATION FORM Step 2');
        redirect( 'recipient/form2/'.$recid,'refresh');   
        }
        
    }
    
    function updatestep2(){
        $this->MRecipient->updatestep2();
        flashMsg('success','APPLICATION FORM Step 3');
        redirect( 'recipient/form3/'.$this->input->post('recid'),'refresh');
    }
    
    function updatestep3(){
        $this->MRecipient->updatestep3();
        flashMsg('success','Wait for approve');
        redirect( 'recipient/success/','refresh');
    }
    
    function success(){
        $member = $this->session->userdata('memberid');
        $data['header'] = "Application Form"; 
        $data['all'] = $this->MRecipient->listallrecbyrecid($member);
        $data['module'] = $this->module;
	$data['page'] = $this->config->item('wconfig_template_welcome').'success';      
    	$this->load->view($this->_container_contact,$data); 
    }
    
    function editprofile(){
        $member = $this->session->userdata('memberid');
        $data['header'] = "Edit Profile"; 
        $data['edit'] = $this->MWelcome->getprofile($member);
        $data['module'] = $this->module;
	$data['page'] = $this->config->item('wconfig_template_welcome').'editprofile';      
    	$this->load->view($this->_container_contact,$data); 
    }
    
    function updateprofile(){
        $this->MWelcome->updateprofile();
        flashMsg('success','Your profile has been successfully updated.');
        redirect( 'welcome/index/','refresh');
    }
    

}