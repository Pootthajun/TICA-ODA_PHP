<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MRecipient extends CI_Model{



	function insert(){
            $this->db->where('fullname',$this->input->post('fullname'));
		 $this->db->where('aid_id',$this->input->post('aid_id'));
		 $this->db->where('birthday',$this->input->post('birthday'));
        $Q = $this->db->get('recipient');
		if($Q->num_rows() > 0){
			 flashMsg('warning',"Can not add more because the recipient already funded. ");
             redirect( 'recipient/admin/','refresh');
		}else{
		$data = array(
			'aid_id'=>$this->input->post('aid_id'),
			'prefix_id'=>$this->input->post('prefix_id'),
			'fullname'=>$this->input->post('fullname'),
			'givenname'=>$this->input->post('givenname'),
			'sex_id'=>$this->input->post('sex_id'),
			'age'=>$this->input->post('age_hide'),
			'birthday'=>$this->input->post('birthday'),
			'student_id'=>$this->input->post('student_id'),
			'passport_id'=>$this->input->post('passport_id'),
			'expire_passport'=>$this->input->post('expire_passport'),
			'expire_insurance'=>$this->input->post('expire_insurance'),
			'expire_visa'=>$this->input->post('expire_visa'),
			'study_status'=>$this->input->post('study_status'),
			'departure'=>$this->input->post('departure'),
			'email'=>$this->input->post('email'),
			'fax'=>$this->input->post('fax'),
			'mobile'=>$this->input->post('mobile'),
			'telephone'=>$this->input->post('telephone'),
			'work_address'=>$this->input->post('work_address'),
			'home_address'=>$this->input->post('home_address'),
                        'home_tel'=>$this->input->post('home_tel'),
                        'home_fax'=>$this->input->post('home_fax'),
			'religion'=>$this->input->post('religion'),
			'marital'=>$this->input->post('marital'),
			'nationality'=>$this->input->post('nationality'),
			'city_of_birth'=>$this->input->post('city_of_birth'),
			'institute_id'=>$this->input->post('institute_id'),
			'position'=>$this->input->post('position'),
			'country_id'=>$this->input->post('country_id'),
			'course'=>$this->input->post('course'),
			'emergency_name'=>$this->input->post('emergency_name'),
			'emergency_relation'=>$this->input->post('emergency_relation'),
			'emergency_address'=>$this->input->post('emergency_address'),
			'emergency_telephone'=>$this->input->post('emergency_telephone'),
			'approve_by'=>$this->input->post('approve_by'),
			'gov_organisation'=>$this->input->post('gov_organisation'),
			'gov_title'=>$this->input->post('gov_title'),
			'gov_date'=>$this->input->post('gov_date'),
			'gov_address'=>$this->input->post('gov_address'),
			'gov_duties'=>$this->input->post('gov_duties'),
			'gov_post_title'=>$this->input->post('gov_post_title'),
			'previous_detail'=>$this->input->post('previous_detail'),
			'previous_address'=>$this->input->post('previous_address'),
			'previous_type'=>$this->input->post('previous_type'),
			'previous_name'=>$this->input->post('previous_name'),
			'previous_title'=>$this->input->post('previous_title'),
			'previous_datestart'=>$this->input->post('previous_datestart'),
			'previous_dateend'=>$this->input->post('previous_dateend'),
                        'present_title'=>$this->input->post('present_title'),
			'present_detail'=>$this->input->post('present_detail'),
			'present_address'=>$this->input->post('present_address'),
			'present_type'=>$this->input->post('present_type'),
			'present_name'=>$this->input->post('present_name'),
			'present_datestart'=>$this->input->post('present_datestart'),
			'present_dateend'=>$this->input->post('present_dateend'),
			'candidate'=>$this->input->post('candidate'),
			'before_come'=>$this->input->post('before_come'),
			'is_ielts'=>$this->input->post('is_ielts'),
			'ielts_score'=>$this->input->post('ielts_score'),
			'is_toefl'=>$this->input->post('is_toefl'),
			'toefl_score'=>$this->input->post('toefl_score'),
			'is_orther'=>$this->input->post('is_orther'),
			'orther_score'=>$this->input->post('orther_score'),
			'mother_tongue'=>$this->input->post('mother_tongue'),
			'eng_speak'=>$this->input->post('eng_speak'),
			'eng_write'=>$this->input->post('eng_write'),
			'eng_read'=>$this->input->post('eng_read'),
			'other_lang'=>$this->input->post('other_lang'),
			'other_speak'=>$this->input->post('other_speak'),
			'other_write'=>$this->input->post('other_write'),
			'other_read'=>$this->input->post('other_read'),
			'expect'=>$this->input->post('expect'),
			'printed'=>$this->input->post('printed'),
			'status'=>$this->input->post('status'),
                        'date_capital'=>$this->input->post('date_capital'),
                        'date_funded'=>$this->input->post('date_funded'),
                        'end_date_funded'=>$this->input->post('end_date_funded'),
                        'start_date_extend'=>$this->input->post('start_date_extend'),
                        'end_date_extend'=>$this->input->post('end_date_extend'),
			'comment'=>$this->input->post('comment'),
			'rec_by'=>$this->session->userdata('userid'),
			'rec_create'=>date('Y:m:d H:i:s'),
			);

		if($_FILES['imageprofile']['tmp_name']){
			$config['upload_path']          = './upload/profile/';
            $config['allowed_types']        = 'gif|jpg|png|';


            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload('imageprofile'))
            {
                $error = array('error' => $this->upload->display_errors());
				exit("Cannot Upload Userfile");
            }
            else
            {
            $image = $this->upload->data();
			if ($image['file_name']){        
			$data['imageprofile'] = $image['file_name'];
            }
        	}

		}

		

		if(!empty($_FILES['signature']['tmp_name'])){
			$config['upload_path']          = './upload/signature/';
            $config['allowed_types']        = 'gif|jpg|png';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload('signature'))
            {
                $error = array('error' => $this->upload->display_errors());
				exit("Cannot Upload signature File");
            }
            else
            {
            $image = $this->upload->data();
			if ($image['file_name']){        
			$data['signature'] = $image['file_name'];
            }
        	}

		}

		$this->db->insert('recipient',$data);
                }
	}

	function listAllrecipient(){
	$data = array();
        $this->db->order_by('status',"ASC");
        $Q = $this->db->get('recipient');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function listAllrecipientbyStatus($status){
        $data = array();
        $this->db->where('status',$status);
        $this->db->order_by('rec_id',"ASC");
        $Q = $this->db->get('recipient');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }

    function getRecipientById($recid){
    	$this->db->where('rec_id',$recid);
        $Q = $this->db->get('recipient');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
    
    function getRecipientByMember($member){
        $this->db->where('member_id',$member);
        $Q = $this->db->get('recipient');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
            
    function getRecipientMaxid(){
        $this->db->select_max('rec_id');
        $Q = $this->db->get('recipient');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }

    function delete($recid){
    	$this->db->where('rec_id',$recid);
        $this->db->delete('recipient');       
    }

    function update(){
    	$data = array(
			'aid_id'=>$this->input->post('aid_id'),
			'prefix_id'=>$this->input->post('prefix_id'),
			'fullname'=>$this->input->post('fullname'),
			'givenname'=>$this->input->post('givenname'),
			'sex_id'=>$this->input->post('sex_id'),
			'age'=>$this->input->post('age_hide'),
			'birthday'=>$this->input->post('birthday'),
			'student_id'=>$this->input->post('student_id'),
			'passport_id'=>$this->input->post('passport_id'),
			'expire_passport'=>$this->input->post('expire_passport'),
			'expire_insurance'=>$this->input->post('expire_insurance'),
			'expire_visa'=>$this->input->post('expire_visa'),
			'study_status'=>$this->input->post('study_status'),
			'departure'=>$this->input->post('departure'),
			'email'=>$this->input->post('email'),
			'fax'=>$this->input->post('fax'),
			'mobile'=>$this->input->post('mobile'),
			'telephone'=>$this->input->post('telephone'),
			'work_address'=>$this->input->post('work_address'),
			'home_address'=>$this->input->post('home_address'),
			'religion'=>$this->input->post('religion'),
			'marital'=>$this->input->post('marital'),
			'nationality'=>$this->input->post('nationality'),
			'city_of_birth'=>$this->input->post('city_of_birth'),
			'institute_id'=>$this->input->post('institute_id'),
			'position'=>$this->input->post('position'),
			'country_id'=>$this->input->post('country_id'),
			'course'=>$this->input->post('course'),
			'emergency_name'=>$this->input->post('emergency_name'),
			'emergency_relation'=>$this->input->post('emergency_relation'),
			'emergency_address'=>$this->input->post('emergency_address'),
			'emergency_telephone'=>$this->input->post('emergency_telephone'),
			'approve_by'=>$this->input->post('approve_by'),
			'gov_organisation'=>$this->input->post('gov_organisation'),
			'gov_title'=>$this->input->post('gov_title'),
			'gov_date'=>$this->input->post('gov_date'),
			'gov_address'=>$this->input->post('gov_address'),
			'gov_duties'=>$this->input->post('gov_duties'),
			'gov_post_title'=>$this->input->post('gov_post_title'),
			'previous_detail'=>$this->input->post('previous_detail'),
			'previous_address'=>$this->input->post('previous_address'),
			'previous_type'=>$this->input->post('previous_type'),
			'previous_name'=>$this->input->post('previous_name'),
			'previous_title'=>$this->input->post('previous_title'),
			'previous_datestart'=>$this->input->post('previous_datestart'),
			'previous_dateend'=>$this->input->post('previous_dateend'),
			'present_detail'=>$this->input->post('present_detail'),
			'present_address'=>$this->input->post('present_address'),
			'present_type'=>$this->input->post('present_type'),
                        'present_title'=>$this->input->post('present_title'),
			'present_name'=>$this->input->post('present_name'),
			'present_datestart'=>$this->input->post('present_datestart'),
			'present_dateend'=>$this->input->post('present_dateend'),
			'candidate'=>$this->input->post('candidate'),
			'before_come'=>$this->input->post('before_come'),
			'is_ielts'=>$this->input->post('is_ielts'),
			'ielts_score'=>$this->input->post('ielts_score'),
			'is_toefl'=>$this->input->post('is_toefl'),
			'toefl_score'=>$this->input->post('toefl_score'),
			'is_orther'=>$this->input->post('is_orther'),
			'orther_score'=>$this->input->post('orther_score'),
			'mother_tongue'=>$this->input->post('mother_tongue'),
                        'mother_speak'=>$this->input->post('mother_speak'),
			'mother_write'=>$this->input->post('mother_write'),
			'mother_read'=>$this->input->post('mother_read'),
			'eng_speak'=>$this->input->post('eng_speak'),
			'eng_write'=>$this->input->post('eng_write'),
			'eng_read'=>$this->input->post('eng_read'),
			'other_lang'=>$this->input->post('other_lang'),
			'other_speak'=>$this->input->post('other_speak'),
			'other_write'=>$this->input->post('other_write'),
			'other_read'=>$this->input->post('other_read'),
			'expect'=>$this->input->post('expect'),
			'printed'=>$this->input->post('printed'),
                        'status'=>$this->input->post('status'),
                        'date_capital'=>$this->input->post('date_capital'),
			'comment'=>$this->input->post('comment'),
			'rec_by'=>$this->session->userdata('userid'),
			'rec_create'=>date('Y:m:d H:i:s'),
			);

		

		if(!empty($_FILES['imageprofile']['tmp_name'])){
			$config['upload_path']          = './upload/profile/';
            $config['allowed_types']        = 'gif|jpg|png|';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload('imageprofile'))
            {
                $error = array('error' => $this->upload->display_errors());
				exit("Cannot Upload Userfile");
            }
            else
            {
            $image = $this->upload->data();
			if ($image['file_name']){        
			$data['imageprofile'] = $image['file_name'];
            }
        	}

		}
		if(!empty($_FILES['signature']['tmp_name'])){
			$config['upload_path']          = './upload/signature/';
            $config['allowed_types']        = 'gif|jpg|png';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload('signature'))
            {
                $error = array('error' => $this->upload->display_errors());
				exit("Cannot Upload signature File");
            }
            else
            {
            $image = $this->upload->data();
			if ($image['file_name']){        
			$data['signature'] = $image['file_name'];
			//del image
			$recid = $this->input->post('recid');
			$filedel = $this->getRecipientById($recid);
	        @unlink('./upload/signature/'.$filedel['file']);
            }
        	}

		}
		$this->db->where('rec_id',$this->input->post('recid'));
		$this->db->update('recipient',$data);
    }
    
    function getRecipientbyAid($aid){
        $data =array();
             $Q=$this->db->query("select r.*,a.aid_name,c.country_name from recipient as r 
			  inner join aid as a on r.aid_id = a.aid_id
              inner join country as c on r.country_id = c.country_id
			  where r.aid_id = '$aid' and r.status = 2
			   ");
	foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
	}
        
        function getRecipientJoin($recid){
            $Q=$this->db->query("select r.*, a.aid_name,a.aid_recipient, p.plan_name, project.project_name, activity.activity_name ,c.country_name
                                from recipient as r 
                                inner join aid as a on r.aid_id = a.aid_id 
                                inner join country as c on r.country_id = c.country_id
                                inner join activity as activity on activity.activity_id=a.activity_id 
                                inner join project as project on project.project_id=activity.project_id 
                                inner join plan as p on p.plan_id=project.plan_id 
                                WHERE r.rec_id = '$recid'
			   ");
             $data = $Q->row_array();
        $Q->free_result();
        return $data;
        }
        
        function expirepassport(){
            $current =date('Y-m-d');
            $QP = $this->db->query("SELECT * FROM recipient WHERE expire_passport < '$current'");
	    foreach($QP->result_array() as $row){
            $data[] = $row;
            }
            $QP->free_result();
            return $data;
        }
        
        function expireInsurance(){
            $current =date('Y-m-d');
            $QP = $this->db->query("SELECT * FROM recipient WHERE expire_insurance < '$current'");
	    foreach($QP->result_array() as $row){
            $data[] = $row;
            }
            $QP->free_result();
            return $data;
        }
        
        function expireVisa(){
            $current =date('Y-m-d');
            $QP = $this->db->query("SELECT * FROM recipient WHERE expire_visa < '$current'");
	    foreach($QP->result_array() as $row){
            $data[] = $row;
            }
            $QP->free_result();
            return $data;
        }
    
        
        function insertform1(){
            $data = array(
			'aid_id'=>$this->input->post('aid_id'),
			'prefix_id'=>$this->input->post('prefix_id'),
			'fullname'=>$this->input->post('fullname'),
			'givenname'=>$this->input->post('givenname'),
			'sex_id'=>$this->input->post('sex_id'),
			'age'=>$this->input->post('age_hide'),
			'birthday'=>$this->input->post('birthday'),
			'student_id'=>$this->input->post('student_id'),
			'passport_id'=>$this->input->post('passport_id'),
			'expire_passport'=>$this->input->post('expire_passport'),
			'expire_insurance'=>$this->input->post('expire_insurance'),
			'expire_visa'=>$this->input->post('expire_visa'),
			'study_status'=>$this->input->post('study_status'),
			'departure'=>$this->input->post('departure'),
			'email'=>$this->input->post('email'),
			'fax'=>$this->input->post('fax'),
			'mobile'=>$this->input->post('mobile'),
			'telephone'=>$this->input->post('telephone'),
			'work_address'=>$this->input->post('work_address'),
			'home_address'=>$this->input->post('home_address'),
            'home_tel'=>$this->input->post('home_tel'),
             'home_fax'=>$this->input->post('home_fax'),
			'religion'=>$this->input->post('religion'),
			'marital'=>$this->input->post('marital'),
			'nationality'=>$this->input->post('nationality'),
			'city_of_birth'=>$this->input->post('city_of_birth'),
			'institute_id'=>$this->input->post('institute_id'),
			'country_id'=>$this->input->post('country_id'),
			'course'=>$this->input->post('course'),
			'emergency_name'=>$this->input->post('emergency_name'),
			'emergency_relation'=>$this->input->post('emergency_relation'),
			'emergency_address'=>$this->input->post('emergency_address'),
			'emergency_telephone'=>$this->input->post('emergency_telephone'),
                'status'=>$this->input->post('status'),
                'member_id'=>$this->input->post('memberid'),
			);
		   if($_FILES['imageprofile']['tmp_name']){
			$config['upload_path']          = './upload/profile/';
            $config['allowed_types']        = 'gif|jpg|png|';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('imageprofile'))
                {
                $error = array('error' => $this->upload->display_errors());
				exit("Cannot Upload Userfile");
                    }
                    else
                    {
                        $image = $this->upload->data();
			if ($image['file_name']){        
			$data['imageprofile'] = $image['file_name'];
                    }
        	}
                }
                        $this->db->insert('recipient',$data);

        }
        
        function updateform1(){
            $data = array(
			'aid_id'=>$this->input->post('aid_id'),
			'prefix_id'=>$this->input->post('prefix_id'),
			'fullname'=>$this->input->post('fullname'),
			'givenname'=>$this->input->post('givenname'),
			'sex_id'=>$this->input->post('sex_id'),
			'age'=>$this->input->post('age_hide'),
			'birthday'=>$this->input->post('birthday'),
			'student_id'=>$this->input->post('student_id'),
			'passport_id'=>$this->input->post('passport_id'),
			'expire_passport'=>$this->input->post('expire_passport'),
			'expire_insurance'=>$this->input->post('expire_insurance'),
			'expire_visa'=>$this->input->post('expire_visa'),
			'study_status'=>$this->input->post('study_status'),
			'departure'=>$this->input->post('departure'),
			'email'=>$this->input->post('email'),
			'fax'=>$this->input->post('fax'),
			'mobile'=>$this->input->post('mobile'),
			'telephone'=>$this->input->post('telephone'),
			'work_address'=>$this->input->post('work_address'),
			'home_address'=>$this->input->post('home_address'),
            'home_tel'=>$this->input->post('home_tel'),
             'home_fax'=>$this->input->post('home_fax'),
			'religion'=>$this->input->post('religion'),
			'marital'=>$this->input->post('marital'),
			'nationality'=>$this->input->post('nationality'),
			'city_of_birth'=>$this->input->post('city_of_birth'),
			'institute_id'=>$this->input->post('institute_id'),
			'country_id'=>$this->input->post('country_id'),
			'course'=>$this->input->post('course'),
			'emergency_name'=>$this->input->post('emergency_name'),
			'emergency_relation'=>$this->input->post('emergency_relation'),
			'emergency_address'=>$this->input->post('emergency_address'),
			'emergency_telephone'=>$this->input->post('emergency_telephone'),
                'status'=>$this->input->post('status'),
                'member_id'=>$this->input->post('memberid'),
			);
		   if($_FILES['imageprofile']['tmp_name']){
			$config['upload_path']          = './upload/profile/';
            $config['allowed_types']        = 'gif|jpg|png|';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('imageprofile'))
                {
                $error = array('error' => $this->upload->display_errors());
				exit("Cannot Upload Userfile");
                    }
                    else
                    {
                        $image = $this->upload->data();
			if ($image['file_name']){        
			$data['imageprofile'] = $image['file_name'];
                    }
        	}
                }
                $this->db->where('rec_id',$this->input->post('recid'));
                $this->db->update('recipient',$data);
        }
                
        function updatestep2(){
            $data = array(
                        'previous_detail'=>$this->input->post('previous_detail'),
			'previous_address'=>$this->input->post('previous_address'),
			'previous_type'=>$this->input->post('previous_type'),
			'previous_name'=>$this->input->post('previous_name'),
			'previous_title'=>$this->input->post('previous_title'),
			'previous_datestart'=>$this->input->post('previous_datestart'),
			'previous_dateend'=>$this->input->post('previous_dateend'),
			'present_detail'=>$this->input->post('present_detail'),
			'present_address'=>$this->input->post('present_address'),
			'present_type'=>$this->input->post('present_type'),
                        'present_title'=>$this->input->post('present_title'),
			'present_name'=>$this->input->post('present_name'),
			'present_datestart'=>$this->input->post('present_datestart'),
			'present_dateend'=>$this->input->post('present_dateend'),						
			'is_ielts'=>$this->input->post('is_ielts'),
			'ielts_score'=>$this->input->post('ielts_score'),
			'is_toefl'=>$this->input->post('is_toefl'),
			'toefl_score'=>$this->input->post('toefl_score'),
			'is_orther'=>$this->input->post('is_orther'),
                        'candidate'=>$this->input->post('candidate'),
			'before_come'=>$this->input->post('before_come'),
                        'edu1'=>$this->input->post('edu1'),
                        'edu2'=>$this->input->post('edu2'),
                        'edu3'=>$this->input->post('edu3'),
                        'city1'=>$this->input->post('city1'),
                        'city2'=>$this->input->post('city2'),
                        'city3'=>$this->input->post('city3'),
                        'year1'=>$this->input->post('year1'),
                        'year2'=>$this->input->post('year2'),
                        'year3'=>$this->input->post('year3'),
                        'yearto1'=>$this->input->post('yearto1'),
                        'yearto2'=>$this->input->post('yearto2'),
                        'yearto3'=>$this->input->post('yearto3'),
                        'degree1'=>$this->input->post('degree1'),
                        'degree2'=>$this->input->post('degree2'),
                        'degree3'=>$this->input->post('degree3'),
                        'special1'=>$this->input->post('special1'),
                        'special2'=>$this->input->post('special2'),
                        'special3'=>$this->input->post('special3'),
			'orther_score'=>$this->input->post('orther_score'),
			'mother_tongue'=>$this->input->post('mother_tongue'),
                        'mother_speak'=>$this->input->post('mother_speak'),
			'mother_write'=>$this->input->post('mother_write'),
			'mother_read'=>$this->input->post('mother_read'),
			'eng_speak'=>$this->input->post('eng_speak'),
			'eng_write'=>$this->input->post('eng_write'),
			'eng_read'=>$this->input->post('eng_read'),
			'other_lang'=>$this->input->post('other_lang'),
			'other_speak'=>$this->input->post('other_speak'),
			'other_write'=>$this->input->post('other_write'),
			'other_read'=>$this->input->post('other_read'),
            );
            $this->db->where('rec_id',$this->input->post('recid'));
            $this->db->update('recipient',$data);
        }
        
        function updatestep3(){
            $data = array(
			'gov_organisation'=>$this->input->post('gov_organisation'),
			'gov_title'=>$this->input->post('gov_title'),
			'gov_date'=>$this->input->post('gov_date'),
			'gov_address'=>$this->input->post('gov_address'),
			'gov_duties'=>$this->input->post('gov_duties'),
			'gov_post_title'=>$this->input->post('gov_post_title'),
			'expect'=>$this->input->post('expect'),
			'printed'=>$this->input->post('printed'),
			'rec_create'=>$this->input->post('rec_create'),
			);
            if(!empty($_FILES['signature']['tmp_name'])){
			$config['upload_path']          = './upload/signature/';
            $config['allowed_types']        = 'gif|jpg|png';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload('signature'))
            {
                $error = array('error' => $this->upload->display_errors());
				exit("Cannot Upload signature File");
            }
            else
            {
            $image = $this->upload->data();
			if ($image['file_name']){        
			$data['signature'] = $image['file_name'];
            }
            }
            }
            $this->db->where('rec_id',$this->input->post('recid'));
            $this->db->update('recipient',$data);
        }
        
        function listallrecbyrecid($member){   
            $data = array();
            $this->db->where('member_id',$member);
            $Q = $this->db->get('recipient');
            foreach($Q->result_array() as $row){
            $data[] = $row;
            }
            $Q->free_result();
            return $data;
        }
        
        function getrecipientbygrouptype($groupid,$projecttype){
            $data = array();
            if($projecttype==1){
            	$sql = "select r.*, a.aid_name,a.aid_recipient,at.aid_type_group, p.plan_name, project.project_name,project.project_type_id, activity.activity_name ,c.country_name
                                from recipient as r 
                                inner join aid as a on r.aid_id = a.aid_id 
                                inner join aid_type as at on a.aid_type_id = at.aid_type_id 
                                inner join country as c on r.country_id = c.country_id
                                inner join activity as activity on activity.activity_id=a.activity_id 
                                inner join project as project on project.project_id=activity.project_id 
                                inner join plan as p on p.plan_id=project.plan_id 
                                WHERE r.status = '2'  
                                and project.project_type_id = '$projecttype'
                    ";
            }else{
            	$sql = "select r.*, a.aid_name,a.aid_recipient,at.aid_type_group, p.plan_name, project.project_name,project.project_type_id, activity.activity_name ,c.country_name
                                from recipient as r 
                                inner join aid as a on r.aid_id = a.aid_id 
                                inner join aid_type as at on a.aid_type_id = at.aid_type_id 
                                inner join country as c on r.country_id = c.country_id
                                inner join activity as activity on activity.activity_id=a.activity_id 
                                inner join project as project on project.project_id=activity.project_id 
                                inner join plan as p on p.plan_id=project.plan_id 
                                WHERE r.status = '2' and at.aid_type_group = '$groupid'
                                and project.project_type_id = '$projecttype'
                    ";
            }
            
             $Q=$this->db->query($sql);
             foreach($Q->result_array() as $row){
            $data[] = $row;
            }
        $Q->free_result();
        return $data;
        }




}