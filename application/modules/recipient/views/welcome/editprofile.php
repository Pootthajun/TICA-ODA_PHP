<div id="odaTab">

  <div class="tabcontent">
        <div class="content">	

<?php echo validation_errors(); ?>
<form action="<?php echo base_url(); ?>recipient/updateprofile/" method="post" name="form1" id="form1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" style="border:1px solid #e5e5e5;"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:15px;">
      <tr>
        <td width="167"><p align="right"><span class="labeltxt">Name - Surname</span></p></td>
        <td><span style="padding-left:8px;">
          <input name="member_fullname" type="text"  required="required" class="txtformbox" id="member_fullname" value="<?php echo $edit['member_fullname']; ?>"/>
        </span></td>
      </tr>
      <tr>
        <td><p align="right"><span class="labeltxt">E-mail</span></p></td>
        <td><span style="padding-left:8px;">
          <input type="text" name="member_email" id="member_email" class="txtformbox required email" value="<?php echo $edit['member_email']; ?>"/>
        </span></td>
      </tr>
      <tr>
        <td><p align="right"><span class="labeltxt">Telephone</span></p></td>
        <td><span style="padding-left:8px;">
          <input type="text" name="member_tel" id="member_tel" class="txtformbox"  value="<?php echo $edit['member_tel']; ?>" />
        </span></td>
      </tr>
      <tr>
        <td><p align="right"><span class="labeltxt">Username</span></p></td>
        <td><span style="padding-left:8px;">
          <input type="text" name="member_username" id="member_username" class="txtformbox" required="required"   value="<?php echo $edit['member_username']; ?>"/>
        </span></td>
      </tr>
      <tr>
        <td><p align="right"><span class="labeltxt">Password</span></p></td>
        <td><span style="padding-left:8px;">
          <input type="password" name="member_password" id="member_password" class="txtformbox"/>
          <input name="member_status" type="hidden" id="member_status" value="<?php echo $edit['member_status']; ?>" />
        </span></td>
      </tr>
      <tr>
        <td height="8"></td>
        <td height="8"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="image" name="imageField" id="imageField" src="<?php echo base_url(); ?>assets/welcome/images/btn-send.png" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

</form>



    </div>
    </div>


		
       
		<div class="clearfix"></div>
        </div>

<style>

#odaTab{
	margin: 40px 10px 10px 10px;
	background: #b4d1ea;
}

#odaTab .tabhead{
	clear: both;
	background: #b4d1ea;
	padding: 20px 0 5px 20px;
}

#odaTab .tabhead ul{
	list-style: none;
	padding: 0;
	margin: 0;
}

#odaTab .tabhead ul li{
	list-style: none;
	padding: 0;
	margin: 0;
	display: block;
	float: left;
	border-right: 2px solid #858e96;
}

#odaTab .tabhead ul li:last-child{
	border: none;
}

#odaTab .tabhead ul li a{
	display: block;
	padding: 2px 15px 2px 15px;
	background: none;
	color: white;
	font-weight: bold;
	text-decoration: none;
	font-size: 18px;
	margin: 0 2px 0 0;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
}

#odaTab .tabhead ul li a:hover{
	color: #3d7fb9;
	font-size:18px;
}

#odaTab .tabhead ul li a.on{
	background: none;
	color: #08559d;
	font-size:18px;
}

#odaTab .tabcontent{
	background: #b4d1ea;
	padding: 15px 15px 25px 15px;
	border-radius: 5px;
	border-top-left-radius: 0;
}

#odaTab .tabcontent .content{
	background: white;
	border: 1px solid #CACACA;
	padding: 5px;
	font-size: 14px;
	border-radius: 3px;
	height: 320px;

}
#odaTab .tabcontent .content a{
	font-size: 18px;
	color:#08559d;
}

.krut{
	height: 92px;
	background: url('images/krut.png') center top no-repeat;
}



.agreeSubmitArea{
	padding-top: 10px;
	margin-bottom:20px;

}

.agreeSubmitArea label{
	font-size: 16px;
	
}

.agreeArrow{
	display: block;
	position: absolute;

	width: 72px;
	height: 38px;
	margin: -38px 0 0 220px;
}

.clearfix {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    clear: both;
    height: 0;
    width: 100%;
}
</style>