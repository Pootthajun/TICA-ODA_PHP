<form name="form1" method="post" action="<?php echo base_url(); ?>recipient/updatestep3">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top"><h2>application form</h2></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><p align="center"><img src="<?php echo base_url(); ?>assets/welcome/images/topmail.png" /></p></td>
        </tr>
        <tr>
          <td>    <h4 align="center">Ministry of Foreign Affairs<br/>
The Government Complex, Building B, Chaengwattana Road, Bangkok 10210, Thailand<br/>
Tel. (662) 203 5000 ext. 40502 Fax. (662) 143 9327<br/>
Email: tica@mfa.go.th    Website: www.mfa.go.th<br/>
TICA APPLICATION FORM
 </h4></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><div id="step">
              <li class="three">step 3</li>
            </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td valign="top" style="border:1px solid #e5e5e5;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="tdblue">c. EXPECTATIONS</td>
              </tr>
              <tr>
                <td></td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="500" height="9"></td>
                      <td height="9"></td>
                    </tr>
                    <tr>
                      <td width="500"><p style="padding-left:15px;"><span class="subtextdetail"> Please describe the practical use you will make of this training/study on your return home in relation to the responsibilities you expect to assume and the conditions existing in your country in the field of your training.(give the attached paper, if necessary) </span></p></td>
                      <td><label for="gov_title"></label>
                        <textarea name="expect" cols="42" rows="8" class="checklisttxt-area2" required="required" id="expect"><?php echo $edit['expect']; ?></textarea>
                        <span class="labelimportant">*</span></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                      <td class="tdblue">d. REFERENCES</td>
                      <td width="50%" class="tdblue">&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="25" colspan="2"><p style="padding-left:15px;"><span class="subtextdetail"> (only a candidate for a degree program please attach the recommendation letters from two persons acquainted with your academic and professional experiences.) </span></p></td>
                    </tr>
                    <tr>
                      <td height="9" colspan="2" style="border-bottom:1px #e5e5e5 solid"></td>
                    </tr>
                    <tr>
                      <td height="9" colspan="2"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td><p style="padding:0px 30px 0px 12px;">I certify that my statements in answer to the foregoing questions are   true, complete and correct to the best of my knowledge and belief. </p>
                  <br />
                  <p style="padding:0px 30px 30px 30px;">If accepted for a training award, I undertake to :- <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) 	carry out such instructions and abide by such conditions as may be stipulated by both the nominating government and the host government <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  in respect of this course of training; <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) 	follow the course of training, and abide by the rules of the University or other institutions or establishment in which I undertake to train; <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) 	refrain from engaging in political activities, or any form of employment for profit or gain; <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(d) 	submit any progress reports which may be prescribed; <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(e) 	return to my home country promptly upon the completion of my course of training </p></td>
              </tr>
              <tr>
                <td height="9" colspan="2" style="border-bottom:1px #e5e5e5 solid"></td>
              </tr>
              <tr>
                <td height="9" colspan="2"></td>
              </tr>
              <tr>
                <td><p style="padding:0px 30px 0px 12px;">I also fully understand that if I am granted a fellowship award, it may   be subsequently withdrawn if I fail to make adequate progress or for   other sufficient cause determined by the host Government.</p></td>
              </tr>
              <tr>
                <td height="9" colspan="2" style="border-bottom:1px #e5e5e5 solid"></td>
              </tr>
              <tr>
                <td height="9" colspan="2"></td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="550" valign="top">&nbsp;</td>
                      <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="180"><p align="right"><span class="labeltxt">Signature of applicant:</span></p></td>
                            <td><p>
                             <?php if(!empty($edit['signature'])){?> <img src="<?php echo base_url(); ?>upload/signature/<?php echo $edit['signature']; ?>" width="170" height="63"  style="padding:5px 15px 5px 8px;" align="absmiddle" /><?php }else{ ?>
                            <img  id="blah" src="<?php echo base_url(); ?>assets/welcome/images/signature.jpg" width="170" height="63"  style="padding:5px 15px 5px 8px;" align="absmiddle" /></p>
                           <?php } ?>
                              <p> 
                              <div class="fileUpload">
                          <div  class="signatureupload"></div>
                              <input type="file" name="signature" id="signature"  class="uploadsignature" onchange="readsignature(this);"/>
                              </div></p></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Printed name:</span></p></td>
                            <td><span style="padding-left:8px;">
                              <input name="printed" type="text"  required class="txtformbox3" id="textfield2" value="<?php echo $edit['printed']; ?>"/>
                              <span class="labelimportant">*</span></span></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Date :</span></p></td>
                            <td><span style="padding-left:8px;">
                              <input name="rec_create" type="text" required class="txtformbox3" id="datepicker4" value="<?php echo $edit['rec_create']; ?>" />
                              <span class="labelimportant">*</span>                              <img src="<?php echo base_url(); ?>assets/welcome/images/date_picker.gif" width="15" height="12" align="absmiddle" /></span></td>
                          </tr>
                        </table>
                      <label for="gov_post_title"></label></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td height="9"></td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                      <td class="tdblue">e. GOVERNMENT AUTHORISATION</td>
                      <td width="50%" class="tdblue">&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="25" colspan="2"><p style="padding-left:15px;"><span class="subtextdetail"> To be completed by the nominating Government or the
                          agency from whom the nomination has been invited.</span></p></td>
                    </tr>
                    <tr>
                      <td height="9" colspan="2" style="border-bottom:1px #e5e5e5 solid"></td>
                    </tr>
                    <tr>
                      <td height="9" colspan="2"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td><p style="padding:0px 30px 30px 30px;">I certify that, to the best of my knowledge,<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) all information supplied by the nominee is complete and correct ; <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) the nominee has adequate knowledge and experience in   related fields and has adequate English proficiency for the purpose of   the<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  fellowship in Thailand. <br />
                    On return from the fellowship, the nominee will be employed in the following position : </p></td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="450"><p align="right"><span class="labeltxt">Title of post :</span></p></td>
                      <td><label for="captcha"></label>
                        <input name="gov_post_title" type="text" required class="txtformbox4" id="gov_post_title" value="<?php echo $edit['gov_post_title']; ?>" />
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Duties and responsibilities :</span></p></td>
                      <td><input name="gov_duties" type="text"  required class="txtformbox4" id="textfield4" value="<?php echo $edit['gov_duties']; ?>"/>
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td valign="top"><table width="96%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="180" height="45" style="border-bottom:1px dotted #999999">&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="45"><p align="center"><span class="subtextdetail">Signature of responsible Government official </span></p></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="550" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td colspan="2"><p class="labeltxt" style="padding-left:15px;">Official stamp : </p></td>
                                </tr>
                                <tr>
                                  <td width="180"><p align="right"><span class="labeltxt">Organisation :</span></p></td>
                                  <td><input name="gov_organisation" type="text" class="txtformbox" id="textfield6" value="<?php echo $edit['gov_organisation']; ?>" /></td>
                                </tr>
                              </table></td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="180"><p align="right"><span class="labeltxt">Title :</span></p></td>
                                  <td><span style="padding-left:8px;">
                                    <input name="gov_title" type="text"  required class="txtformbox3" id="textfield8" value="<?php echo $edit['gov_post_title']; ?>"/>
                                    <span class="labelimportant">*</span></span></td>
                                </tr>
                                <tr>
                                  <td><p align="right"><span class="labeltxt">Official Address:</span></p></td>
                                  <td><span style="padding-left:8px;">
                                    <textarea name="gov_address" cols="42" rows="5" required class="checklisttxt-area" id="gov_address"><?php echo $edit['gov_address']; ?></textarea>
                                    <span class="labelimportant">*</span></span></td>
                                </tr>
                                <tr>
                                  <td><p align="right"><span class="labeltxt">Date :</span></p></td>
                                  <td><span style="padding-left:8px;">
                                    <input name="gov_date" type="text" required class="txtformbox3" id="datepicker2" value="<?php echo $edit['gov_date']; ?>" />
                                    <span class="labelimportant">*</span>                                    <img src="<?php echo base_url(); ?>assets/welcome/images/date_picker.gif" width="15" height="12" align="absmiddle" /></span></td>
                                </tr>
                              </table>
                            <label for="gov_duties"></label></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="125" valign="middle" style="border-top:1px solid #e5e5e5;"><p align="center"><span class="labeltxt">Secret Code :</span><span style="padding-left:15px;">
                          <input type="text" name="captcha" id="captcha" class="txtformbox3"  required="required"/>
                          <span class="labelimportant">*</span> </span>&nbsp;<?php echo $capchar; ?>&nbsp;</p>
                      <p align="center">
                      <a> <img style="padding:0px 5px 0px 0px;" src="<?php echo base_url(); ?>assets/welcome/images/btn-reset.png" onclick="return confirm('Do you want to reset your informaiton!!!')"></a>
                        <input type="image" name="imageField" id="imageField" src="<?php echo base_url(); ?>assets/welcome/images/btn-register.png">
                     <input name="recid" type="hidden" id="recid" value="<?php echo $this->uri->segment(3); ?>" /></td>
                    </tr>
                    <tr>
                      <td height="44" style="border-top:1px solid #e5e5e5;"><p style="padding-left:8px; font-weight:bold; color:#00549d; font-style:italic;">Page 3 of 3 Pages</p></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
</form>
