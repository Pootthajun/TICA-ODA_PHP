<form name="form1" method="post" action="<?php echo base_url(); ?>recipient/updatestep2">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top"><h2>application form</h2></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><p align="center"><img src="<?php echo base_url(); ?>assets/welcome/images/topmail.png" /></p></td>
        </tr>
        <tr>
          <td>    <h4 align="center">Ministry of Foreign Affairs<br/>
The Government Complex, Building B, Chaengwattana Road, Bangkok 10210, Thailand<br/>
Tel. (662) 203 5000 ext. 40502 Fax. (662) 143 9327<br/>
Email: tica@mfa.go.th    Website: www.mfa.go.th<br/>
TICA APPLICATION FORM</h4></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><div id="step">
              <li class="two">step 2</li>
            </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td valign="top" style="border:1px solid #e5e5e5;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="tdgray">infomations</td>
              </tr>
              <tr>
                <td valign="top"><div id="intable">
                  <table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                      <td colspan="2" rowspan="2" class="header">Languages </td>
                      <td colspan="3" align="center" class="header">Read</td>
                      <td colspan="3" align="center" class="header">Write</td>
                      <td colspan="3" align="center" class="header">Speak</td>
                    </tr>
                    <tr>
                      <td width="9%" align="center" class="header">Excellent</td>
                      <td width="6%" align="center" class="header">Good</td>
                      <td width="4%" align="center" class="header">Fair</td>
                      <td width="9%" align="center" class="header">Excellent</td>
                      <td width="6%" align="center" class="header">Good</td>
                      <td width="4%" align="center" class="header">Fair</td>
                      <td width="9%" align="center" class="header">Excellent</td>
                      <td width="6%" align="center" class="header">Good</td>
                      <td width="5%" align="center" class="header">Fair</td>
                    </tr>
                    <tr>
                      <td width="14%"><span  class="labeltxt">Mother tongue :</span></td>
                      <td width="28%"><input name="mother_tongue" type="text" class="txtformbox2" id="mother_tongue" value="<?php echo $edit['mother_tongue']; ?>"/>
                      <span class="labelimportant">*</span></td>
                      <td><p align="center">
                        <input type="radio" name="mother_read" id="radio" value="3" <?php if($edit['mother_read'] == "3"){ echo 'checked="checked"';} ?> />
                      </p>
                        <label for="mother_speak"></label></td>
                      <td><p align="center">
                        <input type="radio" name="mother_read" id="radio3" value="2" <?php if($edit['mother_read'] == "2"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="mother_read" id="radio10" value="1" <?php if($edit['mother_read'] == "1"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="mother_write" id="radio17" value="3" <?php if($edit['mother_write'] == "3"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="mother_write" id="radio18" value="2" <?php if($edit['mother_write'] == "2"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="mother_write" id="radio19" value="1" <?php if($edit['mother_write'] == "1"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="mother_speak" id="radio19" value="3" <?php if($edit['mother_speak'] == "3"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="mother_speak" id="radio20" value="2" <?php if($edit['mother_speak'] == "2"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="mother_speak" id="radio21" value="1" <?php if($edit['mother_speak'] == "1"){ echo 'checked="checked"';} ?> />
                      </p></td>
                    </tr>
                    <tr>
                      <td colspan="2"><span class="labeltxt">English : </span></td>
                      <td><p align="center">
                        <input type="radio" name="eng_read" id="radio21" value="3" <?php if($edit['eng_read'] == "3"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="eng_read" id="radio23" value="2" <?php if($edit['eng_read'] == "2"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="eng_read" id="radio25" value="1" <?php if($edit['eng_read'] == "1"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="eng_write" id="radio17" value="3" <?php if($edit['eng_write'] == "3"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="eng_write" id="radio18" value="2" <?php if($edit['eng_write'] == "2"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="eng_write" id="radio19" value="1" <?php if($edit['eng_write'] == "1"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="eng_speak" id="radio19" value="3" <?php if($edit['eng_speak'] == "3"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="eng_speak" id="radio20" value="2" <?php if($edit['eng_speak'] == "2"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="eng_speak" id="radio21" value="1" <?php if($edit['eng_speak'] == "1"){ echo 'checked="checked"';} ?> />
                      </p></td>
                    </tr>
                    <tr>
                      <td><span class="labeltxt">Other :</span></td>
                      <td><input name="other_lang" type="text" class="txtformbox2" id="other_lang" value="<?php echo $edit['other_lang']; ?>"/></td>
                      <td><p align="center">
                        <input type="radio" name="other_read" id="radio22" value="3" <?php if($edit['other_read'] == "3"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="other_read" id="radio23" value="2" <?php if($edit['other_read'] == "2"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="other_read" id="radio27" value="1" <?php if($edit['other_read'] == "1"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="other_write" id="radio24" value="3" <?php if($edit['other_write'] == "3"){ echo 'checked="checked"';} ?>  />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="other_write" id="radio25" value="2" <?php if($edit['other_write'] == "2"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="other_write" id="radio26" value="1" <?php if($edit['other_write'] == "1"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="other_speak" id="radio27" value="3" <?php if($edit['other_speak'] == "3"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="other_speak" id="radio28" value="2" <?php if($edit['other_speak'] == "2"){ echo 'checked="checked"';} ?> />
                      </p></td>
                      <td><p align="center">
                        <input type="radio" name="other_speak" id="radio29" value="1" <?php if($edit['other_speak'] == "1"){ echo 'checked="checked"';} ?> />
                      </p></td>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="250" height="9"></td>
                      <td height="9" colspan="3"></td>
                    </tr>
                    <tr>
                      <td width="250"><p align="right"><span class="labeltxt">English Proficiency Test :</span></p>
                        <p align="right" class="subtextdetail">(please attach)
                          (only a candidate for a degree course) </p></td>
                      <td width="181"><strong>
                      <input name="is_toefl" type="checkbox" value="1" id="is_toefl" <?php if($edit['is_toefl'] == "1"){ echo 'checked="checked"';} ?> />
                      </strong>
                        <label for="checkbox">TOEFL    Score&nbsp;&nbsp;&nbsp;&nbsp;</label></td>
                      <td width="181"><input name="toefl_score" type="text" class="txtformbox2" id="toefl_score" value="<?php echo $edit['toefl_score']; ?>"/></td>
                      <td width="468"><input name="is_ielts" type="checkbox" id="is_ielts" value="1" <?php if($edit['is_ielts'] == "1"){ echo 'checked="checked"';} ?>/>
                        <label for="checkbox2"> IELTs    Score
                          <input name="ielts_score" type="text" class="txtformbox2 " id="ielts_score" value="<?php echo $edit['ielts_score']; ?>"/>
                      </label></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td><strong>
                      <input name="is_orther" type="checkbox" value="1" <?php if($edit['is_orther'] == "1"){ echo 'checked="checked"';} ?> />
                      </strong>
                        <label for="checkbox"> Other (specify)                      </label></td>
                      <td><input name="orther_score" type="text" class="txtformbox2 " id="orther_score" value="<?php echo $edit['orther_score']; ?>"/></td>
                      <td>&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td class="tdgray">EDUCATION RECORD</td>
              </tr>
              <tr>
                <td valign="top"><div id="intable">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td rowspan="2" class="header">Education Institution</td>
                        <td rowspan="2" class="header">City / Country</td>
                        <td colspan="2" class="header">Years Attended</td>
                        <td rowspan="2" class="header">Degrees, Diplomas <br />
                          and Certificates</td>
                        <td rowspan="2" class="header">Special fields of study</td>
                      </tr>
                      <tr>
                        <td class="header">Year</td>
                        <td class="header">To</td>
                      </tr>
                      <tr>
                        <td><input name="edu1" type="text" class="txtformbox3" id="textfield13" value="<?php echo $edit['edu1']; ?>" /></td>
                        <td><input name="city1" type="text" class="txtformbox3" id="textfield14" value="<?php echo $edit['city1']; ?>" /></td>
                        <td><input name="year1" type="text" class="txtformbox3" id="textfield15" value="<?php echo $edit['year1']; ?>" /></td>
                        <td><input name="yearto1" type="text" class="txtformbox3" id="textfield16" value="<?php echo $edit['yearto1']; ?>" /></td>
                        <td><input name="degree1" type="text" class="txtformbox3" id="textfield17" value="<?php echo $edit['degree1']; ?>" /></td>
                        <td><input name="special1" type="text" class="txtformbox3" id="textfield18" value="<?php echo $edit['special1']; ?>" /></td>
                      </tr>
                      <tr>
                        <td><input name="edu2" type="text" class="txtformbox3" id="textfield19" value="<?php echo $edit['edu2']; ?>" /></td>
                        <td><input name="city2" type="text" class="txtformbox3" id="textfield20" value="<?php echo $edit['city2']; ?>" /></td>
                        <td><input name="year2" type="text" class="txtformbox3" id="textfield21" value="<?php echo $edit['year2']; ?>" /></td>
                        <td><input name="yearto2" type="text" class="txtformbox3" id="textfield22" value="<?php echo $edit['yearto2']; ?>" /></td>
                        <td><input name="degree2" type="text" class="txtformbox3" id="textfield23" value="<?php echo $edit['degree2']; ?>" /></td>
                        <td><input name="special2" type="text" class="txtformbox3" id="textfield24" value="<?php echo $edit['special2']; ?>" /></td>
                      </tr>
                      <tr>
                        <td><input name="edu3" type="text" class="txtformbox3" id="textfield25" value="<?php echo $edit['edu3']; ?>" /></td>
                        <td><input name="city3" type="text" class="txtformbox3" id="textfield26" value="<?php echo $edit['city3']; ?>" /></td>
                        <td><input name="year3" type="text" class="txtformbox3" id="textfield27" value="<?php echo $edit['year3']; ?>" /></td>
                        <td><input name="yearto3" type="text" class="txtformbox3" id="textfield28" value="<?php echo $edit['yearto3']; ?>" /></td>
                        <td><input name="degree3" type="text" class="txtformbox3" id="textfield29" value="<?php echo $edit['degree3']; ?>" /></td>
                        <td><input name="special3" type="text" class="txtformbox3" id="textfield30" value="<?php echo $edit['special3']; ?>" /></td>
                      </tr>
                    </table>
                  </div></td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="180" height="9"></td>
                      <td height="9"></td>
                    </tr>
                    <tr>
                      <td width="500"><p align="right"><span class="labeltxt">Have you ever been trained in Thailand? If yes,</span> <br />
                          <span class="labeltxt">please specify title of the course,where and for how long? :</span></p></td>
                      <td><label for="mother_tongue"></label>
                        <input name="before_come" type="radio" value="1" <?php if($edit['before_come'] == "1"){ echo 'checked="checked"';} ?> />
                        Yes<strong>
<input name="before_come" type="radio" value="0" <?php if($edit['before_come'] == "0"){ echo 'checked="checked"';} ?> />
                      </strong>No <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">For a candidate for a degree program, please give a list of relevant</span><br />
                          </span><span class="labeltxt">publications/researches (do not attach details)  :</span></p></td>
                      <td><input name="candidate" type="text" class="txtformbox" id="textfield31" value="<?php echo $edit['candidate']; ?>" />
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                      <td class="tdblue">B. EMPLOYMENT RECORD </td>
                      <td class="tdblue" width="50%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="25" colspan="2"><p style="padding-left:15px;"><span class="subtextdetail"> It is important to give complete information. For each post you have
                          occupied, give details of your duties and responsibilities. </span></p></td>
                    </tr>
                    <tr>
                      <td height="9" colspan="2"></td>
                    </tr>
                    <tr>
                      <td height="25" colspan="2"><p style="padding-left:15px;"><span class="labeltxt">Present or most recent post :</span>
                          
                          <input name="present_datestart" type="text" required="required" class="txtformbox3" id="datepicker1" value="<?php echo $edit['present_datestart']; ?>" />
                          <span style="padding-left:8px;"><img src="<?php echo base_url(); ?>assets/welcome/images/date_picker.gif" width="15" height="12" align="absmiddle" /></span>to
                          <input name="present_dateend" type="text"  required class="txtformbox3" id="datepicker2" value="<?php echo $edit['present_dateend']; ?>"/>
                        <span style="padding-left:8px;"><img src="<?php echo base_url(); ?>assets/welcome/images/date_picker.gif" width="15" height="12" align="absmiddle" /></span></p></td>
                    </tr>
                    <tr>
                      <td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="180" height="9"></td>
                            <td height="9"></td>
                          </tr>
                          <tr>
                            <td width="180"><p align="right"><span class="labeltxt">Title of your  post:</span></p></td>
                            <td><label for="textfield3">
                                <input name="present_title" type="text" required class="txtformbox" id="textfield11" value="<?php echo $edit['present_title']; ?>" />
                            </label></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Name of organisation:</span></p></td>
                            <td><label for="edu2">
                                <input name="present_name" type="text" required class="txtformbox" id="textfield35" value="<?php echo $edit['present_name']; ?>"/>
                            </label></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Type of organisation:</span></p></td>
                            <td><label for="candidate">
                                <input name="present_type" type="text" required class="txtformbox" id="textfield36" value="<?php echo $edit['present_type']; ?>" />
                            </label></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Official address:</span></p></td>
                            <td><label for="before_come">
                                <input name="present_address" type="text" required class="txtformbox" id="textfield37" value="<?php echo $edit['present_address']; ?>"/>
                            </label></td>
                          </tr>
                        </table></td>
                      <td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="175" height="9"></td>
                          </tr>
                          <tr>
                            <td><span class="labeltxt">Description of your work :</span></td>
                          </tr>
                          <tr>
                            <td><textarea name="present_detail" cols="42" rows="8" class="checklisttxt-area2" id="present_detail"><?php echo $edit['present_detail']; ?></textarea></td>
                          </tr>
                          <tr>
                            <td><span class="subtextdetail"> including your personal responsibilities </span></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td height="9" colspan="2" style="border-bottom:1px #e5e5e5 solid"></td>
                    </tr>
                    <tr>
                      <td height="9" colspan="2"></td>
                    </tr>
                    <tr>
                      <td height="25" colspan="2"><p style="padding-left:15px;"><span class="labeltxt">Previous:</span>
                          <input name="previous_datestart" type="text" class="txtformbox3" id="datepicker3" value="<?php echo $edit['previous_datestart']; ?>" />
                          <span style="padding-left:8px;"><img src="<?php echo base_url(); ?>assets/welcome/images/date_picker.gif" width="15" height="12" align="absmiddle" /></span> to
                          <input name="previous_dateend" type="text" class="txtformbox3" id="datepicker4" value="<?php echo $edit['previous_dateend']; ?>" />
                        <span style="padding-left:8px;"><img src="<?php echo base_url(); ?>assets/welcome/images/date_picker.gif" width="15" height="12" align="absmiddle" /></span></p></td>
                    </tr>
                    <tr>
                      <td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="180" height="9"></td>
                            <td height="9"></td>
                          </tr>
                          <tr>
                            <td width="180"><p align="right"><span class="labeltxt">Title of your  post:</span></p></td>
                            <td><label for="textfield3">
                                <input name="previous_title" type="text" class="txtformbox" id="textfield11" value="<?php echo $edit['previous_title']; ?>" />
                            </label></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Name of organisation:</span></p></td>
                            <td><label for="edu2">
                                <input name="previous_name" type="text" class="txtformbox" id="textfield35" value="<?php echo $edit['previous_name']; ?>" />
                            </label></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Type of organisation:</span></p></td>
                            <td><label for="candidate">
                                <input name="previous_type" type="text" class="txtformbox" id="textfield36" value="<?php echo $edit['previous_type']; ?>" />
                            </label></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Official address:</span></p></td>
                            <td><label for="before_come">
                                <input name="previous_address" type="text" class="txtformbox" id="textfield37" value="<?php echo $edit['previous_address']; ?>" />
                            </label></td>
                          </tr>
                        </table></td>
                      <td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="175" height="9"></td>
                          </tr>
                          <tr>
                            <td><span class="labeltxt">Description of your work :</span></td>
                          </tr>
                          <tr>
                            <td><textarea name="previous_detail" cols="42" rows="8" class="checklisttxt-area2" id="previous_detail"><?php echo $edit['previous_detail']; ?></textarea></td>
                          </tr>
                          <tr>
                            <td><span class="subtextdetail"> including your personal responsibilities </span></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                      <td style="border-top:1px solid #e5e5e5;"><p style="padding-left:15px; font-weight:bold; color:#00549d; font-style:italic;">Page 2 of 3 Pages</p></td>
                      <td width="50%" style="border-top:1px solid #e5e5e5;"><p align="right" style="padding:15px;"> 
                        <input name="recid" type="hidden" id="recid" value="<?php echo $this->uri->segment(3); ?>" />
                      <input type="image" name="imageField" id="imageField" src="<?php echo base_url(); ?>assets/welcome/images/btnnext.png"></p></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
</form>
