<div id="odaTab">
		<div class="tabhead">
		<ul>
			<li><a class="on" href="1">Descriptions</a></li>
			<li><a href="2">Agreement</a></li>
			<li><a href="3">Download Form</a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
	<div class="tabcontent">
		<div class="ccc" id="c_1">	
			<div class="content"><p>Members occasionally request the Secretariat’s view as to whether a particular expenditure should be reported as official development assistance (ODA). This paper outlines the reasoning the Secretariat uses to answer such enquiries, and discusses some specific cases. It should not be taken as a definitive guide to ODA eligibility, since only the DAC may determine such eligibility. Further details are provided in the Statistical Reporting Directives (available at www.oecd/dac/stats/dac/ FACTSHEET - May 2007directives).</p></div>
		</div>
		<div class="hidden ccc" id="c_2">	
			<div class="content"><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Thailand has provided official development assistance to other countries since 1992.<br>Demonstrating our advancement and the capacity to share knowledge and experience with other countries is seen as a significant step forward for Thailand.<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; The development assistance in recent years has encompassed a broad range of areas including: agricultural extension, healthcare and better education, elimination of human trafficking and narcotic drugs, construction of roads, dams and other infrastructure. The key government agencies responsible for the administration of development assistance are Thailand International Development Cooperation Agency (TICA) under the Ministry of Foreign Affairs, and the Neighbouring Countries Economic Development Cooperation Agency (NEDA), a public organisation. The delivery of Thailand’s development assistance will be in accordance with Official Development Assistance (ODA)1 that refers to assistance provided to countries on the official recipient list of the DAC (Development Assistance Committee)2 and multilateral development agencies. The assistance must be provided by government agencies-either a central or local government, or state agencies with the objective of economic development and enhancing the welfare of developing countries. ODA consists of concessional loan and grant aid. Grant aid includes technical assistance and funding support under bilateral, trilateral and multilateral framework. Thailand’s aid programme targets the neighbouring countries, that is, Lao PDR, Cambodia, Myanmar and Vietnam, in accordance with the government’s foreign policy which is aimed at helping our neighbouring countries in achieving their development goals. Thailand also provides assistance to other developing countries through North-South Cooperation and South-South Cooperation as to achieve the Millennium Development Goals (MDGs).</p></div>
		</div>
		<div class="hidden ccc" id="c_3">	
			<div class="content">
									<a target="_blank" href="<?php echo base_url(); ?>form/127.0.0.1-1408784616.24.xls">1. Academic Record/certificate</a>
					<br>
										<a target="_blank" href="<?php echo base_url(); ?>form/127.0.0.1-1408784616.24.xls">2. Copy of Passport</a>
					<br>
										<a target="_blank" href="<?php echo base_url(); ?>form/127.0.0.1-1408784616.24.xls">3. Medical Report</a>
					<br>
										<a target="_blank" href="<?php echo base_url(); ?>form/127.0.0.1-1408784616.24.xls">4. Other document</a>
					<br>
										<a target="_blank" href="<?php echo base_url(); ?>form/127.0.0.1-1408784616.24.xls">5. Recommendation Letter</a>
					<br>
										<a target="_blank" href="<?php echo base_url(); ?>form/127.0.0.1-1408784616.24.xls">6. Thesis proposal </a>
					<br>
		  </div>
		</div>
	</div>
	<div class="clearfix"></div>
      </div>
	</div>
	<div class="agreeSubmitArea">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input type="checkbox" id="agree">
		<label for="agree">I Agree all descriptions and agreement above</label></td>
    <td align="right"> <input type="image" border="0" class="fr" style="margin: 0 20px 0 0;" src="<?php echo base_url(); ?>assets/welcome/images/next.jpg" id="agreeandgo"></td>
  </tr>
</table>

		
		
       
		<div class="clearfix"></div>

    
    <script>
	$(document).ready(function(){

	$('#odaTab .tabhead ul li a').click(function(){

		if($(this).hasClass('on') == false){

			$('#odaTab .tabhead ul li a.on').removeClass('on');
			var id = $(this).attr('href');
			$('.ccc').addClass('hidden');
			$('#c_'+id).removeClass('hidden');
			$(this).addClass('on');

		}

		return false;

	});

	var d1 = $('#odaTab');
	var d2 = $('#updateFormArea');

	$('a.registerlink').click(function(){

		d1.show();
		d2.hide();
		return false;

	});

	$('a.update').click(function(){

		d1.hide();
		d2.show();
		return false;

	});

	$('#agreeandgo').click(function(){

		if($('#agree').prop('checked') == true){
			document.location = 'newregister';
		}

	});

});
</script>
<style>

#odaTab{
	margin: 40px 10px 10px 10px;
	background: #b4d1ea;
}

#odaTab .tabhead{
	clear: both;
	background: #b4d1ea;
	padding: 20px 0 5px 20px;
}

#odaTab .tabhead ul{
	list-style: none;
	padding: 0;
	margin: 0;
}

#odaTab .tabhead ul li{
	list-style: none;
	padding: 0;
	margin: 0;
	display: block;
	float: left;
	border-right: 2px solid #858e96;
}

#odaTab .tabhead ul li:last-child{
	border: none;
}

#odaTab .tabhead ul li a{
	display: block;
	padding: 2px 15px 2px 15px;
	background: none;
	color: white;
	font-weight: bold;
	text-decoration: none;
	font-size: 18px;
	margin: 0 2px 0 0;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
}

#odaTab .tabhead ul li a:hover{
	color: #3d7fb9;
	font-size:18px;
}

#odaTab .tabhead ul li a.on{
	background: none;
	color: #08559d;
	font-size:18px;
}

#odaTab .tabcontent{
	background: #b4d1ea;
	padding: 15px 15px 25px 15px;
	border-radius: 5px;
	border-top-left-radius: 0;
}

#odaTab .tabcontent .content{
	background: white;
	border: 1px solid #CACACA;
	padding: 5px;
	font-size: 14px;
	border-radius: 3px;
	height: 200px;
	overflow-x: hidden;
	overflow-y: scroll;
}
#odaTab .tabcontent .content a{
	font-size: 18px;
	color:#08559d;
}

.krut{
	height: 92px;
	background: url('images/krut.png') center top no-repeat;
}



.agreeSubmitArea{
	padding-top: 10px;
	margin-bottom:20px;

}

.agreeSubmitArea label{
	font-size: 16px;
	
}

.agreeArrow{
	display: block;
	position: absolute;

	width: 72px;
	height: 38px;
	margin: -38px 0 0 220px;
}

.clearfix {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    clear: both;
    height: 0;
    width: 100%;
}
</style>