<div id="odaTab">
<div class="tabhead">
List Application
</div>
		<div class="tabcontent">
        <div class="content">	

        <table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#A7A7A7">
        <thead>
  <tr>
    <th width="95" bgcolor="#FAFAFA">No.</th>
    <th width="277" bgcolor="#FAFAFA">Aid</th>
    <th width="245" bgcolor="#FAFAFA">Course</th>
    <th width="217" bgcolor="#FAFAFA">Status</th>
    <th width="129" bgcolor="#FAFAFA">&nbsp;</th>
  </tr>
  </thead>
  <?php 
  $count = 1;
  foreach($all as $list){ ?>
  <tr>
    <td bgcolor="#FFFFFF"><?php echo $count++; ?></td>
    <td bgcolor="#FFFFFF"><a href="<?php echo base_url()."recipient/edit/".$list['rec_id']; ?>">
      <?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?>
      </a></td>
    <td bgcolor="#FFFFFF"><?php echo $list['course']; ?></td>
    <td align="center" bgcolor="#FFFFFF"><?php 
		
		if($list['status'] == 1){ ?>
      <img src="<?php echo base_url(); ?>assets/admin/images/icons/exclamation.png" title="Wait" width="16" height="16" />
      <?php }else if($list['status'] == 2){  ?>
      <img src="<?php echo base_url(); ?>assets/admin/images/icons/tick_circle.png" title="Approve" width="16" height="16" />
      <?php }else if($list['status'] == 3){  ?>
      <img src="<?php echo base_url(); ?>assets/admin/images/icons/cross.png" title="Reject" width="16" height="16" />
      <?php } ?></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <?php } ?>
</table>

</div>
    </div>


		
       
		<div class="clearfix"></div>
        </div>

<style>

#odaTab{
	margin: 40px 10px 10px 10px;
	background: #b4d1ea;
}

#odaTab .tabhead{
	clear: both;
	background: #b4d1ea;
	padding: 20px 0 5px 20px;
}

#odaTab .tabhead ul{
	list-style: none;
	padding: 0;
	margin: 0;
}

#odaTab .tabhead ul li{
	list-style: none;
	padding: 0;
	margin: 0;
	display: block;
	float: left;
	border-right: 2px solid #858e96;
}

#odaTab .tabhead ul li:last-child{
	border: none;
}

#odaTab .tabhead ul li a{
	display: block;
	padding: 2px 15px 2px 15px;
	background: none;
	color: white;
	font-weight: bold;
	text-decoration: none;
	font-size: 18px;
	margin: 0 2px 0 0;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
}

#odaTab .tabhead ul li a:hover{
	color: #3d7fb9;
	font-size:18px;
}

#odaTab .tabhead ul li a.on{
	background: none;
	color: #08559d;
	font-size:18px;
}

#odaTab .tabcontent{
	background: #b4d1ea;
	padding: 15px 15px 25px 15px;
	border-radius: 5px;
	border-top-left-radius: 0;
}

#odaTab .tabcontent .content{
	background: white;
	border: 1px solid #CACACA;
	padding: 5px;
	font-size: 14px;
	border-radius: 3px;
	height: 200px;
	overflow-x: hidden;
	overflow-y: scroll;
}
#odaTab .tabcontent .content a{
	font-size: 18px;
	color:#08559d;
}

.krut{
	height: 92px;
	background: url('images/krut.png') center top no-repeat;
}



.agreeSubmitArea{
	padding-top: 10px;
	margin-bottom:20px;

}

.agreeSubmitArea label{
	font-size: 16px;
	
}

.agreeArrow{
	display: block;
	position: absolute;

	width: 72px;
	height: 38px;
	margin: -38px 0 0 220px;
}

.clearfix {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    clear: both;
    height: 0;
    width: 100%;
}
</style>