<form action="<?php echo base_url(); ?>recipient/inserform1" method="post" enctype="multipart/form-data" name="form1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top"><h2>application form</h2></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><p align="center"><img src="<?php echo base_url(); ?>assets/welcome/images/topmail.png" /></p></td>
        </tr>
        <tr>
          <td>
          




          <h4 align="center"> Ministry of Foreign Affairs<br/>
The Government Complex, Building B, Chaengwattana Road, Bangkok 10210, Thailand<br/>
Tel. (662) 203 5000 ext. 40502 Fax. (662) 143 9327<br/>
Email: tica@mfa.go.th    Website: www.mfa.go.th<br/>
TICA APPLICATION FORM </h4></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><div id="step">
              <li class="one">step 1</li>
            </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td valign="top" style="border:1px solid #e5e5e5;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="tdblue">Instructions</td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td style=" border-bottom:1px solid #e5e5e5"><p style="padding:25px;">This application form is composed for five parts (part A to part E) and sholud be complete in triplicate. Part A to part D sholud be completed by the candidate and part E by the government authority in typewritten form. <span style="text-decoration:underline; color:#0b5699">Each queastion must be answered clearly and completely. Detailed answer are required in order to make the most appropriate arragements</span>. Official authority of the nominating Gorvernment will then forward three copies of the certified application forms to the Ministry of Foreign Affairs, The Government Complex, Building B, Chaengwattana Road, Bangkok 10210, Thailand, through the Royal Thai Embassy in the nominating country. The nominee is required to attach medical report or health status certification. </p></td>
                      <td width="235" valign="top" style="background:#f2f2f2;">

                      <div align="center">
                        <?php if($edit['imageprofile'] != ""){ ?>
                       
  <img src="<?php echo base_url(); ?>upload/profile/<?php echo $edit['imageprofile']; ?>"  width="170" height="170" style="padding:15px 0px 0px 0px;" id="blah" />
  <div class="fileUpload">
                          <div  class="imagesupload"></div>
                          <input type="file" name="imageprofile" id="imageprofile" class="upload" onchange="readURL(this);" />
  							<?php }else{ ?>
                         <img id="blah"  alt="Recipient image" src="<?php echo base_url(); ?>assets/welcome/images/profiles.jpg" width="170" height="170" style="padding:15px 0px 0px 0px;" /> <span class="labelimportant">*</span>
                        
                        
                          <div class="fileUpload">
                          <div  class="imagesupload"></div>
                          <input type="file" name="imageprofile" id="imageprofile" class="upload" onchange="readURL(this);" />
                           <?php } ?>
                          </div>
                        </div></div></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="180" height="9"></td>
                      <td height="9"></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Title</span></p></td>
                      <td><label for="select"></label>
                        <select name="aid_id" id="aid_id" required class="checklisttxt-l">
                          <option value=""></option>
                          <?php foreach($aid as $listaid){ ?>
                          <option value="<?php echo $listaid['aid_id']; ?>"  <?php if($edit['aid_id'] == $listaid['aid_id']){ echo "selected='selected'"; } ?>><?php echo $listaid['aid_name']; ?></option>
                          <?php } ?>
                      </select></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Course Name  :</span></p></td>
                      <td>
                      <input name="course" type="text" required class="txtformbox" id="textfield2"   value="<?php echo $edit['course']; ?>" size="100" />
                  
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td class="tdblue">A. personal history</td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="180" height="9"></td>
                      <td height="9" colspan="2"></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2"><strong>Family name (as shown in passport)</strong></td>
                    </tr>
                    <tr>
                      <td width="180"><p align="right"><span class="labeltxt">Title :</span></p></td>
                      <td colspan="2"><select  id="prefix_id" name="prefix_id" class="chosen-select checklisttxt-l" style="width:100px;"> 
                        <option value="Ms." <?php if($edit['prefix_id'] == "Ms."){ echo "selected=\"selected\"";} ?>>Ms.</option>
                          <option value="Mrs." <?php if($edit['prefix_id'] == "Mrs."){ echo  "selected=\"selected\"";} ?>>Mrs.</option>
                          <option value="Mr." <?php if($edit['prefix_id'] == "Mr."){ echo  "selected=\"selected\"";} ?>>Mr.</option>
                      </select>
                        <label for="course"></label>
                        <input type="text" required name="fullname" id="textfield2" class="txtformbox"   value="<?php echo $edit['fullname']; ?>" />
                      <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Giving Name  :</span></p></td>
                      <td colspan="2"><input type="text" required name="givenname" id="givenname" class="txtformbox"  value="<?php echo $edit['givenname']; ?>"  />
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Date of Birth :</span></p></td>
                      <td colspan="2"><label for="textfield13"></label>
                        <input name="birthday" type="text" class="txtformbox" id="date_receipion_birth"  required value="<?php echo $edit['birthday']; ?>" />
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Age :</span></p></td>
                      <td colspan="2"><label for="textfield14"></label>
                        <input type="text" required name="age" class="txtformbox"  id="age"  value="<?php echo $agecal;?>" /><input type="hidden" name="age_hide" id="age_real" value="" />
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Sex :</span></p></td>
                      <td colspan="2"><label for="textfield15"></label>
                        <select name="sex_id" id="sex_id" class="chosen-select checklisttxt-l" style="width:160px" required>
        <option value="Female" <?php if($edit['sex_id'] == "Female"){ echo 'selected="selected"';} ?>>Female</option>
        <option value="Male" <?php if($edit['sex_id'] == "Male"){ echo 'selected="selected"';} ?>>Male</option>
        </select>
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Marital Status  :</span></p></td>
                      <td colspan="2"><label for="textfield16"></label>
                        <select name="marital" class="checklisttxt-l" style="width:160px;">
           <option value="Single" <?php if($edit['marital'] == "Single"){ echo 'selected="selected"';} ?>>Single</option>
            <option value="Married" <?php if($edit['marital'] == "Married"){ echo 'selected="selected"';} ?>>Married</option>
            <option value="Separated" <?php if($edit['marital'] == "Separated"){ echo 'selected="selected"';} ?>>Separated</option>
            <option value="Divorced" <?php if($edit['marital'] == "Divorced"){ echo 'selected="selected"';} ?>>Divorced</option>
            <option value="Widowed" <?php if($edit['marital'] == "Widowed"){ echo 'selected="selected"';} ?>>Widowed</option>
          </select>
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Nationality  :</span></p></td>
                      <td colspan="2"><label for="textfield17"></label>
                        <input type="text" required name="nationality" id="textfield17" class="txtformbox"  value="<?php echo $edit['nationality']; ?>"/ />
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Region  :</span></p></td>
                      <td colspan="2"><label for="textfield18"></label>
                        <input type="text" required name="religion" id="textfield18" class="txtformbox" value="<?php echo $edit['religion']; ?>"/ />
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">City   :</span></p></td>
                      <td colspan="2"><label for="textfield19"></label>
                        <input type="text" required name="city_of_birth" id="textfield19" class="txtformbox" value="<?php echo $edit['city_of_birth']; ?>" />
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Country   :</span></p></td>
                      <td colspan="2"><label for="textfield20"></label>
                        
                        <select name="country_id"  required class=" checklisttxt-l"  id="country_id" style="width:80%">
    	 <option value=""></option>
      <?php foreach($country as $listcountry){?>
      <option value="<?php echo $listcountry['country_id']; ?>" <?php if($listcountry['country_id'] == $edit['country_id']){ echo 'selected="selected"';} ?> ><?php echo $listcountry['country_name']; ?></option>
      <?php } ?>
    </select>
                        <span class="labelimportant">*</span></td>
                    </tr>
                    <tr>
                      <td><p align="right"><span class="labeltxt">Passport ID</span></p></td>
                      <td width="238"><input name="passport_id" type="text" class="txtformbox" value="<?php echo $edit['passport_id']; ?>" required  /></td>
                      <td width="662"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="200" align="left" valign="top"><p align="right"><span class="labeltxt">Passport expire</span></p></td>
                          <td align="left" valign="top"><input name="expire_passport" type="text" class="txtformbox" id="datepicker1" value="<?php echo $edit['expire_passport']; ?>"  required="required"/></td>
                        </tr>
                      </table></td>
                    </tr>
                  <tr>
                    <td><p align="right"><span class="labeltxt">Expiry Date of Insurance</span></p></td>
                      <td>
                        <input name="expire_insurance" type="text" class="txtformbox" id="datepicker2" value="<?php echo $edit['expire_insurance']; ?>"  required/>
                     </td>
                    <td><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
  <tr>
    <td width="200" ><p align="right"><span class="labeltxt">Expiry Date of VISA</span></p></td>
    <td ><strong>
    
      <input name="expire_visa" type="text" class="txtformbox" id="datepicker3" value="<?php echo $edit['expire_visa']; ?>" required="required" />
    </strong></td>
  </tr>
</table></td>
                  </tr>
                  </table></td>
              </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                      <td class="tdgray">Home location </td>
                      <td class="tdgray" width="50%">Work location</td>
                    </tr>
                    <tr>
                      <td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="180" height="9"></td>
                            <td height="9"></td>
                          </tr>
                          <tr>
                            <td width="175"><p align="right"><span class="labeltxt">Address :</span></p></td>
                            <td><label for="fullname"></label>
                              <textarea name="home_address" cols="42" rows="5" class="checklisttxt-area" id="home_address"><?php echo $edit['home_address']; ?></textarea>
                              <span class="labelimportant">*</span></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td><span class="subtextdetail">(Please complete this section as clear as possible, <br />
                              information will be used for travel arrangement.)</span></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Telephone :</span></p></td>
                            <td><label for="emergency_name"></label>
                              <input type="text" required name="home_tel" id="textfield6" class="txtformbox" value="<?php echo $edit['home_tel']; ?>"/>
                              <span class="labelimportant">*</span></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Fax :</span></p></td>
                            <td><label for="emergency_telephone"></label>
                              <input type="text" required name="home_fax" id="textfield7" class="txtformbox"  value="<?php echo $edit['home_fax']; ?>" />
                              <span class="labelimportant">*</span></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">International Airport : &nbsp; <br />
                                City for departture </span></p></td>
                            <td><label for="home_tel"></label>
                              <input type="text" required name="departure" id="departure" class="txtformbox" value="<?php echo $edit['departure']; ?>"  />
                              <span class="labelimportant">*</span></td>
                          </tr>
                        </table></td>
                      <td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="175" height="9"></td>
                            <td height="9"></td>
                          </tr>
                          <tr>
                            <td width="175"><p align="right"><span class="labeltxt">Address :</span></p></td>
                            <td><label for="nationality"></label>
                              <textarea name="work_address" cols="42" rows="5" class="checklisttxt-area" id="work_address"><?php echo $edit['work_address']; ?></textarea>
                              <span class="labelimportant">*</span></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td><span class="subtextdetail">(Please complete this section as clear as possible, <br />
                              information will be used for travel arrangement.)</span></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Telephone :</span></p></td>
                            <td><label for="city_of_birth"></label>
                              <input name="telephone" type="text" required class="txtformbox" id="textfield10" value="<?php echo $edit['telephone']; ?>" />
                              <span class="labelimportant">*</span></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Fax :</span></p></td>
                            <td><label for="textfield11"></label>
                              <input name="fax" type="text" required class="txtformbox" id="textfield11" value="<?php echo $edit['fax']; ?>" />
                              <span class="labelimportant">*</span></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Email :</span></p></td>
                            <td><label for="textfield12"></label>
                              <input name="email" type="text" required class="txtformbox" id="textfield12" value="<?php echo $edit['email']; ?>" />
                              <span class="labelimportant">*</span></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                      <td class="tdgray">Name and address of person to be notified in case of emergency </td>
                      <td class="tdgray" width="50%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="180" height="9"></td>
                            <td height="9"></td>
                          </tr>
                          <tr>
                            <td width="165"><p align="right"><span class="labeltxt">Name :</span></p></td>
                            <td><label for="telephone"></label>
                              <input name="emergency_name" type="text" required class="txtformbox" id="textfield5" value="<?php echo $edit['emergency_name']; ?>" />
                              <span class="labelimportant">*</span></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Relationship  :</span></p></td>
                            <td><input name="emergency_relation" type="text" required class="txtformbox" id="emergency_relation" value="<?php echo $edit['emergency_relation']; ?>" />
                              <span class="labelimportant">*</span></td>
                          </tr>
                          <tr>
                            <td><p align="right"><span class="labeltxt">Telephone  :</span></p></td>
                            <td><input name="emergency_telephone" type="text" required class="txtformbox" id="emergency_telephone" value="<?php echo $edit['emergency_telephone']; ?>" />
                              <span class="labelimportant">*</span></td>
                          </tr>
                        </table></td>
                      <td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="175" height="9"></td>
                            <td height="9"></td>
                          </tr>
                          <tr>
                            <td width="175"><p align="right"><span class="labeltxt">Address :</span></p></td>
                            <td><textarea name="emergency_address" cols="42" rows="5" class="checklisttxt-area" id="emergency_address"><?php echo $edit['emergency_address']; ?></textarea>
                              <span class="labelimportant">*</span></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr>
                      <td style="border-top:1px solid #e5e5e5;"><p style="padding-left:15px; font-weight:bold; color:#00549d; font-style:italic;">Page 1 of 3 Pages
                        <input name="status" type="hidden" id="status" value="1" />
                        <input name="comment" type="hidden" id="comment" value="-" />
                        <input name="memberid" type="hidden" id="memberid" value="<?php echo $this->session->userdata('memberid'); ?>" />
                        <input name="recid" type="hidden" id="recid" value="<?php echo $edit['rec_id']; ?>" />
                      </p></td>
                      <td width="50%" style="border-top:1px solid #e5e5e5;"><p align="right" style="padding:15px;"> 
                        <input type="image" name="imageField" id="imageField" src="<?php echo base_url(); ?>assets/welcome/images/btnnext.png">
                     </p></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
</form>
