 <style>
#form1 select{width:100%}
</style>

  <form id="form1" name="form1" method="post" action="<?php echo site_url(); ?>recipient/admin/addrecipient">
  <script type="text/javascript">
$(document).ready(function()
{
/////get Project
$("#plan").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
//alert($(this).val());
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/getprojectlistbyplanid/",
data: dataString,
cache: false,
success: function(html)
{
$("#getproject").fadeOut(100).html(html).fadeIn(500);
} 
});
});
///end get project
	




});
</script>
<p>
<select name="plan" id="plan" class="required chosen-select" data-placeholder="Choose Plan Name..." >
    <option value=""></option>
    <?php foreach($plan as $listplan){ ?>
    <option value="<?php echo $listplan['plan_id']; ?>"><?php echo $listplan['plan_name']; ?></option>
    <?php } ?>
    </select>
    </p>
    <p>
    <span id="getproject">
    <select name="project" id="project" required class="chosen-select" data-placeholder="Choose Project Name..."  >
	  <option value=""></option>
  </select>
  </span>
  </p>
  <p>
           <span id="getactivity">
         <select name="activity" id="activity" required class="chosen-select" data-placeholder="Choose Activity Name..."  >
	  <option value="" ></option>
         </select>   
         </span>   
    </p>
         <p>  
 <span id="getaid">
<select name="aid_id" id="aid_id" class="chosen-select required"  data-placeholder="Choose Aid Name..."  >
        <option></option>
    </select>
      </span>
      </p>

  </p>
  </form>
  <table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
  <tr>
    <th width="84">No.</th>
    <th width="357">Name</th>
    <th width="223">Country</th>
    <th width="177">Aid</th>
    <th width="212">Status</th>
    </tr>
  </thead>
  <tbody id="resultTable">
  <?php
  $count =1;
  foreach($all as $list){
  ?>

    <tr>
      <td align="center"><?php echo $count++; ?></td>
      <td><?php echo $list['prefix_id']."&nbsp;".$list['fullname']; ?></td>
      <td><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
      <td><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <td align="center">
        <?php if($list['status'] == 0){ ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/exclamation.png" title="Wait" width="16" height="16" />
        <?php }else if($list['status'] == 1){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/tick_circle.png" title="Approve" width="16" height="16" />
        <?php }else if($list['status'] == 2){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/cross.png" title="Reject" width="16" height="16" />
        <?php } ?>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
