 <style type="text/css">
	label 
	{
		width:auto;		
	}
</style>
  <div id="resultTable">
<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
  <tr>
    <th width="54">No.</th>
    <th width="222">Name</th>
    <th width="282">Course</th>
    <th width="191">Aid</th>
    <th width="135">Country</th>
    <th width="142">Expire Passport</th>
    <th width="79">Status</th>
    <?php  if( !empty( $menu ) ) { ?>
          <th>&nbsp;</th>
<?php } ?>
    </tr>
  </thead>

  <tbody>
    <?php
  $count =1;
  foreach($all as $list){
  ?>
    <tr>
      <td align="center"><?php echo $count++; ?></td>
      <td><?php echo $list['prefix_id']."&nbsp;".$list['fullname']; ?></td>
      <td><?php echo $list['course']; ?></td>
      <td><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <td><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
      <td align="center"><span style="color:#FF0000"><?php echo $list['expire_passport']; ?></span></td>
      <td align="center">
        <?php if($list['status'] == 1){ ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/exclamation.png" title="Wait" width="16" height="16" />
        <?php }else if($list['status'] == 2){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/tick_circle.png" title="Approve" width="16" height="16" />
        <?php }else if($list['status'] == 3){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/cross.png" title="Reject" width="16" height="16" />
        <?php } ?>
      </td>
     <?php
 
               	$q = $this->db->query("SELECT activity.project_id FROM activity left join aid on aid.activity_id = activity.activity_id where aid.aid_id = '".$list['aid_id']."' ");
		$rowq = $q->row_array(); 
		$project_id = $rowq['project_id'];
                
	      	$project_assistant = '';
                $project_owner = '';
                $this->db->where('project_id',$project_id);
                $Q = $this->db->get('project_assistant'); 
                foreach($Q->result_array() as $listp){
			$project_assistant .= $listp['user_id'].",";  
		}
                
                $this->db->where('project_id',$project_id);
                $Q = $this->db->get('project'); 
                foreach($Q->result_array() as $listp){
			$project_owner = $listp['project_owner'];  
		}
		
		$auser = $project_assistant.$project_owner;
 
		  $ex = explode(",",$auser);
		 $Q->free_result();
		  ?>
          <td width="120" align="right">
                <?php  for($i=0;$i<count($ex);$i++){ 
		  if($ex[$i] == $this->session->userdata('userid')){ 
		?> 
                    <a href="<?php echo site_url(); ?>recipient/admin/addrecipient/<?php echo $list['rec_id']; ?>" class="button blue">Edit</a><a href="<?php echo site_url(); ?>recipient/admin/deleterecipient/<?php echo $list['rec_id']; ?>"  onclick="return confirm('Are you sure you want to delete Recipient Name <?php echo $list['fullname']; ?> ?')" class="button red"><?php echo $this->lang->line('delete'); ?></a>
		<?php }}?>
          </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
