<style>
label{
	margin-left:10px;
	font-weight:normal;
}
label.error{
	margin-left:10px;
	font-weight:normal;
	float:right;
}
input[type="text"]{

}


</style>
<form action="<?php echo base_url(); ?>recipient/admin/insert" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table width="1000" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="175" valign="top" bgcolor="#FFFFFF"><strong>Aid name </strong></td>
      <td colspan="2" valign="top" bgcolor="#FFFFFF"><?php echo $aid['aid_name']; ?>
      <input type="hidden" name="aid_id" id="aid_id"  value="<?php echo $aid['aid_id']; ?>"/></td>
      <td width="218" rowspan="8" valign="middle" bgcolor="#FFFFFF">
        <input type="file" name="imageprofile" id="imageprofile"  onchange="readURL(this);"  class="required"/><img id="blah" src="#" alt="Recipient image" /></td>
    </tr>
    <script>
  function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Fullname&nbsp;&nbsp;</strong></td>
      <td bgcolor="#FFFFFF"><select  id="prefix_id" name="prefix_id" class="chosen-select" >
        <option value="Ms.">Ms.</option>
        <option value="Mrs.">Mrs.</option>
        <option value="Mr.">Mr.</option>
        </select>
        <input name="fullname" type="text" class="text-input medium-input" id="fullname" required="required" />
        <label for="lastname"></label></td>
      <td bgcolor="#FFFFFF"><input type="text" name="lastname" id="lastname" class="text-input medium-input"  /></td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Given name</strong></td>
      <td colspan="2" bgcolor="#FFFFFF"><strong>
        <input name="givenname" type="text" class="text-input medium-input" value="" />
      </strong></td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Sex</strong></td>
      <td width="273" bgcolor="#FFFFFF"><strong>
        <select name="sex_id2" id="sex_id2" class="chosen-select" data-placeholder="Choose Sex..." style="width:150px">
          <option value=""></option>
          <option value="Female">Female</option>
          <option value="Male">Male</option>
        </select>
      </strong></td>
      <td width="334" rowspan="3" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><strong>Marital</strong></td>
          <td><strong>
            <select name="marital" class="chosen-select" data-placeholder="Choose Marital..." >
              <option value=""></option>
              <option value="Single">Single</option>
              <option value="Married">Married</option>
              <option value="Separated">Separated</option>
              <option value="Divorced">Divorced</option>
              <option value="Widowed">Widowed</option>
            </select>
          </strong></td>
        </tr>
        <tr>
          <td><strong>Age</strong></td>
          <td><strong>
            <input name="age" type="text" class="text-input small-input" required="required" id="age" />
          </strong></td>
        </tr>
        <tr>
          <td><strong>Course</strong></td>
          <td><strong>
            <input name="course" type="text" class="text-input medium-input" id="course" required="required" />
          </strong></td>
        </tr>
      </table></td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Birthday</strong></td>
      <td bgcolor="#FFFFFF"><strong>
        <input name="birthday" type="text" class="text-input small-input" id="date_receipion_birth" required="required" />
        &nbsp;&nbsp;&nbsp;</strong></td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Student ID</strong></td>
      <td bgcolor="#FFFFFF"><strong>
        <input name="student_id" type="text" class="text-input medium-input" required="required" />
      </strong></td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Study status</strong></td>
      <td colspan="2" bgcolor="#FFFFFF"><strong>
        <input type="radio" name="study_status" id="radio" value="Graduate" />
        Graduate
  <input type="radio" name="study_status" id="radio" value="Education" />
        Education
  <input type="radio" name="study_status" id="radio" value="No graduation" />
        No graduation </strong></td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Passport ID</strong></td>
      <td bgcolor="#FFFFFF"><strong>
        <input name="passport_id" type="text" class="text-input medium-input" required="required" />
      </strong></td>
      <td bgcolor="#FFFFFF"><strong>Passport expire 
        <input name="expire_passport" type="text" class="text-input medium-input"  id="datepicker1" required="required" />
      </strong></td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Expiry Date of Insurance</strong></td>
      <td bgcolor="#FFFFFF"><strong>
        <input name="expire_insurance" type="text" class="text-input medium-input" id="datepicker2" />
      </strong></td>
      <td bgcolor="#FFFFFF"><strong>Expiry Date of VISA</strong><strong>
        <input name="expire_visa" type="text" class="text-input medium-input" id="datepicker3" />
      </strong></td>
      <td width="32%" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Departure</strong></td>
      <td bgcolor="#FFFFFF"><strong>
        <input name="departure" type="text" class="text-input medium-input" />
      </strong></td>
      <td bgcolor="#FFFFFF"><strong>Email
          <input name="email" type="text" class="text-input medium-input" required="required" />
      </strong></td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Mobile</strong></td>
      <td bgcolor="#FFFFFF"><strong>
        <input name="mobile" type="text" class="text-input medium-input" />
      </strong></td>
      <td bgcolor="#FFFFFF"><strong>Telephone
          <input name="telephone" type="text" class="text-input medium-input" />
      </strong></td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Fax</strong></td>
      <td colspan="2" bgcolor="#FFFFFF"><strong>
        <input name="fax" type="text" class="text-input medium-input" />
      </strong></td>
      <td width="218" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr valign="top">
      <td valign="top" bgcolor="#FFFFFF"><strong>Home Address</strong></td>
      <td colspan="2" bgcolor="#FFFFFF"><strong>
        <textarea name="home_address" id="home_address" cols="45" rows="5"></textarea>
      </strong></td>
      <td width="218" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr valign="top">
      <td valign="top" bgcolor="#FFFFFF"><strong>Work Address</strong></td>
      <td colspan="2" bgcolor="#FFFFFF"><strong>
        <textarea name="work_address" id="work_address" cols="45" rows="5"></textarea>
      </strong></td>
      <td width="218" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Religion</strong></td>
      <td colspan="2" bgcolor="#FFFFFF"><strong>
        <input name="religion" type="text" class="text-input medium-input" />
      </strong></td>
      <td width="218" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>Nationality</strong></td>
      <td colspan="2" bgcolor="#FFFFFF"><strong>
        <input name="nationality" type="text" class="text-input medium-input" />
      </strong></td>
      <td width="218" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr valign="top">
      <td bgcolor="#FFFFFF"><strong>City Of Birth</strong></td>
      <td colspan="2" bgcolor="#FFFFFF"><strong>
        <input name="city_of_birth" type="text" class="text-input medium-input" />
      </strong></td>
      <td width="218" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
  </table>
     
  <p><label>Institute</label>
      <select name="institute_id" class="chosen-select" data-placeholder="Choose Institute Name..." >
    							  <option value=""></option>
      <?php foreach($in as $listin){?>
      <option value="<?php echo $listin['institute_id']; ?>"><?php echo $listin['institute_name']; ?></option>
      <?php } ?>
      </select>

  </p>
  <p><label>	Position</label>
  <input name="position" type="text" class="text-input medium-input" id="position">
  </p> 
  <p><label>Country</label>
  <select name="country_id" class="chosen-select" data-placeholder="Choose Country..." >
    							  <option value=""></option>
      <?php foreach($country as $listcountry){?>
      <option value="<?php echo $listcountry['country_id']; ?>"><?php echo $listcountry['country_name']; ?></option>
      <?php } ?>
      </select>
  </p> 

  <p><label>Emergency Name</label>
  <input name="emergency_name" type="text" class="text-input medium-input" id="emergency_name">
  </p>
  <p><label>Emergency Relation</label>
  <input name="emergency_relation" type="text" class="text-input medium-input" id="emergency_relation">
  </p>
    <p><label>Emergency Address</label>
  <input name="emergency_address" type="text" class="text-input medium-input" id="emergency_address">
  </p>
  <p><label>Emergency telephone</label>
  <input name="emergency_telephone" type="text" class="text-input medium-input" id="emergency_telephone">
  </p>
        <p><label>Name organisation</label>
  <input name="gov_organisation" type="text" class="text-input medium-input" id="gov_organisation">
  </p>
 
  <p><label>Goverment Title</label>
  <input name="gov_title" type="text" class="text-input medium-input" id="gov_title">
  </p> 
      <p><label>Goverment Date</label>
  <input name="gov_date" type="text" class="text-input medium-input">
  </p> 
      <p><label>Goverment Address</label>
  <input name="gov_address" type="text" class="text-input medium-input">
  </p> 


      <p><label>Gover duties</label>
  <input name="gov_duties" type="text" class="text-input medium-input">
  </p> 
      <p><label>Goverment post title</label>
  <input name="gov_post_title" type="text" class="text-input medium-input">
  </p>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#F0F8FF">

     <p><label>Previous Type</label>
  <input name="previous_type" type="text" class="text-input medium-input">
  </p> 
          <p><label>Previous Title</label>
  <input name="previous_title" type="text" class="text-input medium-input">
  </p> 
        <p><label>Previous Name</label>
  <input name="previous_name" type="text" class="text-input medium-input">
  </p> 

        <p><label>Previous Date Start</label>
  <input name="previous_datestart" type="text" class="text-input medium-input" id="datepicker5">
  </p> 
        <p><label>Previous Date End</label>
  <input name="previous_dateend" type="text" class="text-input medium-input" id="datepicker6">
  </p> 
       <p><label>Previous Detail</label>
  <input name="previous_detail" type="text" class="text-input medium-input">
  </p>       <p><label>Previous Address</label>
    <textarea name="previous_address" id="previous_address" cols="45" rows="5"></textarea>
  </p> 
  </td>
    <td bgcolor="#D5F3FF">         <p><label>Present Type</label>
  <input name="present_type" type="text" class="text-input medium-input" id="present_type">
  </p>  
            <p><label>Present Title</label>
  <input name="present_title" type="text" class="text-input medium-input">
  </p>      <p><label>Present Name</label>
  <input name="present_name" type="text" class="text-input medium-input">
  </p> 
      <p>
        <label>Present Date Start</label>
  <input name="present_datestart" type="text" class="text-input medium-input" id="datepicker7">
  </p> 
        <p><label>Present Date End</label>
  <input name="present_dateend" type="text" class="text-input medium-input" id="datepicker8">
  </p> 
       <p><label>Present Detail</label>
  <input name="present_detail" type="text" class="text-input medium-input">
  </p>       <p><label>Present Address</label>
    <textarea name="present_address" id="present_address" cols="45" rows="5"></textarea>
  </p> 
  </td>
  </tr>
</table>


        <p><label>Candidate</label>
  <input name="candidate" type="text" class="text-input medium-input">
  </p> 
        <p>
  <label>Have you been aid?</label>
  <input name="before_come" type="radio" value="1"> Yes  <input name="before_come" type="radio" value="0">No
  </p> 
         

  <p>
              <input name="is_ielts" type="checkbox" id="is_ielts" value="1">
              <strong>              IELTS
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Score
<input name="ielts_score" type="text" class="text-input small-input" id="ielts_score">
  </strong></p> 
              <p><strong>
                <input name="is_toefl" type="checkbox" value="1" id="is_toefl">
                TOEFL    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Score
<input name="toefl_score" type="text" class="text-input small-input" id="toefl_score">
              </strong></p>
                <p><strong>
                  <input name="is_orther" type="checkbox" value="1">
                  Other    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Score
                  <input name="orther_score" type="text" class="text-input small-input" id="orther_score">
                </strong> </p>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#EAF4FF">  <p><label>Mather Tongue</label>
  <input name="mother_tongue" type="text" class="text-input medium-input" id="	mother_tongue">
  </p>
    <p><label>English Speak</label>
      <input type="radio" name="eng_speak" id="radio4" value="3">
      Good 
      <input type="radio" name="eng_speak" id="radio5" value="2">
      Poor 
    <input type="radio" name="eng_speak" id="radio6" value="1">Fair</p>
    <p><label>English Write</label>
    <input type="radio" name="eng_write" id="radio4" value="3">
     Good 
      <input type="radio" name="eng_write" id="radio5" value="2">
      Poor 
      <input type="radio" name="eng_write" id="radio6" value="1">Fair
  </p>
  <p><label>English Read</label>
  <input type="radio" name="eng_read" id="radio4" value="3">
      Good 
      <input type="radio" name="eng_read" id="radio5" value="2">
    Poor 
    <input type="radio" name="eng_read" id="radio6" value="1">Fair
  </p></td>
    <td bgcolor="#DDF4FF">
  <p><label>Other Language</label>
  <input name="other_lang" type="text" class="text-input medium-input" id="other_lang">
  </p>
  <p><label>Other Speak</label>
    <input type="radio" name="other_speak" id="radio4" value="3">
      Good 
      <input type="radio" name="other_speak" id="radio5" value="2">
      Poor 
      <input type="radio" name="other_speak" id="radio6" value="1">Fair
  </p>
  <p><label>Other Write</label>
    <input type="radio" name="other_write" id="radio4" value="3">
      Good 
      <input type="radio" name="other_write" id="radio5" value="2">
    Poor 
    <input type="radio" name="other_write" id="radio6" value="1">Fair
  </p>
  <p><label>Other Read</label>
    <input type="radio" name="other_read" id="radio4" value="3">
      Good 
      <input type="radio" name="other_read" id="radio5" value="2">
    Poor 
    <input type="radio" name="other_read" id="radio6" value="1">Fair
  </p></td>
  </tr>
</table>


  
  <p><label>Expect</label>
            <textarea name="expect" id="textarea" cols="45" rows="5"></textarea>
  </p>
            <p><label>Signature</label>
              <input type="file" name="signature" id="signature" />
  </p>
              <p><label>Printed</label>
             <input name="printed" type="text" class="text-input small-input" id="printed">
             <input type="hidden" name="age_hide" id="age_real" value="">
  </p>
  <p>
    <label>Status</label>
        
    <input name="status" type="radio" id="radio7" value="0" checked="checked" />
    Wait&nbsp;&nbsp;&nbsp;
<input type="radio" name="status" id="radio8" value="2" />
Reject
    <input type="radio" name="status" id="radio9" value="1" />

  Approve</p>
  <p><label>Comment</label>

             <textarea name="comment" id="comment" cols="45" rows="5"></textarea>
  </p>
  
  <p><label>Approve By</label>
  <select name="approve_by" class="chosen-select" data-placeholder="Choose User Name..."  style=" width:250px;">
    							  <option value=""></option>
  <?php foreach($user as $listuser) { ?>
  <option value="<?php echo $listuser['user_id']; ?>"><?php echo $listuser['user_fullname']; ?></option>
  <?php } ?>
  </select>
  </p> 
  <p><label>Approve status </label>
    <input type="radio" name="approve_status" id="radio2" value="1">
  Active 
  <input type="radio" name="approve_status" id="radio3" value="0">
  None Active</p> 

  

  <p>
                                  <input type="submit" name="button" id="button" value="Save" class="button green">
  </p>
  </form>
 

