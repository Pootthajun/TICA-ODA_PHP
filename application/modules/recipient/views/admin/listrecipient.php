<style>
#form1 select{width:100%}
</style>
  <script type="text/javascript">
$(document).ready(function()
{
/////get Project
$("#plan").change(function()
{
var id=$(this).val();
var project=$('#project').val();
var status=$('#status').val();
var dataString = 'id='+ id;
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>recipient/admin/getprojectlistbyplanid/",
data: dataString,
cache: false,
success: function(html)
{
$("#getproject").fadeOut(100).html(html).fadeIn(500);
} 
});
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>recipient/admin/getrecipientlistbyplanid/",
data: {'id':id,'status':status},
cache: false,
success: function(html)
{
$("#resultTable").fadeOut(100).html(html).fadeIn(500);
} 
});

});
//status change
$("#status").change(function()
{
	var plan=$('#plan').val();
	var project=$('#project').val();
	var activity=$('#activity').val();
	var aid=$('#aid').val();
	var status=$('#status').val();

$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>recipient/admin/getrecipientbystatus/",
data: {"status": status, "plan": plan,"project":project,"activity":activity,"aid":aid},
//data: {"status": status},
cache: false,
success: function(html)
{
$("#resultTable").fadeOut(100).html(html).fadeIn(500);
} 
});
});

});

</script>


  <form id="form1" name="form1" method="post" action="<?php echo site_url(); ?>recipient/admin/addrecipient">

<p>
<select name="plan" id="plan" class="required chosen-select" data-placeholder="Choose Plan Name..." >
    <option value=""></option>
    <?php foreach($plan as $listplan){ ?>
    <option value="<?php echo $listplan['plan_id']; ?>"><?php echo $listplan['plan_name']; ?></option>
    <?php } ?>
    </select>
    </p>
    <p>
    <span id="getproject">
    <select name="project" id="project" required class="chosen-select" data-placeholder="Choose Project Name..."  >
	  <option value=""></option>
  </select>
  </span>
  </p>
  <p>
           <span id="getactivity">
         <select name="activity" id="activity" required class="chosen-select" data-placeholder="Choose Activity Name..."  >
	  <option value="" ></option>
         </select>   
         </span>   
    </p>
         <p>  
 <span id="getaid">
<select name="aid_id" id="aid_id" class="chosen-select required"  data-placeholder="Choose Aid Name..."  >
        <option></option>
    </select>
      </span>
     <p>
       <span id="getstatus">
<select name="status" id="status" class="chosen-select"  data-placeholder="Choose Status..."  >
  <option ></option>
  <option value="1">Wait</option>
  <option value="2">Approve</option>
  <option value="3">Reject</option>
</select>
      </span>
      </p>
      
      </p>
  <div class="addbutton"><input type="submit" name="button" id="button" value="Add" class="button green" /></div>
<p>

  </p>
  </form>
  <div id="resultTable">
<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
  <tr>
    <th width="84">No.</th>
    <th width="179">Name</th>
    <th width="282">Course</th>
    <th width="191">Aid</th>
    <th width="135">Country</th>
    <th width="136">Status</th>
    <?php  if( !empty( $menu ) ) { ?>
        <?php } ?>
    </tr>
  </thead>

  <tbody>
    <?php
  $count =1;
  foreach($all as $list){
  ?>
  	  	  <?php
	  $sql = "select aid_id,fullname from recipient where aid_id = ".$list['aid_id']." and fullname = '".$list['fullname']."' ";
	  $QRow = $this->db->query($sql);
	  $num = $QRow->num_rows();  
	  ?>
    <tr   <?php
	  if($num > 1){ ?> style="background-color:#FFFFE1"  <?php } ?>>
      <td align="center"><?php echo $count++; ?></td>
      <td>
        
        
        
        
        <?php echo $list['prefix_id']."&nbsp;".$list['fullname']; ?>
        
        
      </td>
      <td><?php echo $list['course']; ?></td>
      <td><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <td><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
      <td align="center">
        <?php 
		
		if($list['status'] == 1){ ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/exclamation.png" title="Wait" width="16" height="16" />
        <?php }else if($list['status'] == 2){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/tick_circle.png" title="Approve" width="16" height="16" />
        <?php }else if($list['status'] == 3){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/cross.png" title="Reject" width="16" height="16" />
        <?php } ?>
      </td>
     <!-- <td width="130" align="right">-->
 
<?php
 
               	$q = $this->db->query("SELECT activity.project_id FROM activity left join aid on aid.activity_id = activity.activity_id where aid.aid_id = '".$list['aid_id']."' ");
		$rowq = $q->row_array(); 
		$project_id = $rowq['project_id'];
                
	      	$project_assistant = '';
                $project_owner = '';
                $this->db->where('project_id',$project_id);
                $Q = $this->db->get('project_assistant'); 
                foreach($Q->result_array() as $listp){
			$project_assistant .= $listp['user_id'].",";  
		}
                
                $this->db->where('project_id',$project_id);
                $Q = $this->db->get('project'); 
                foreach($Q->result_array() as $listp){
			$project_owner = $listp['project_owner'];  
		}
		
		$auser = $project_assistant.$project_owner;
 
		  $ex = explode(",",$auser);
		 $Q->free_result();
		  ?>
        <!--      </td>-->
      
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
