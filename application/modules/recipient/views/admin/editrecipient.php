<style>


td{
	vertical-align:top;
	}


</style>
<?php echo validation_errors(); ?>
<form action="<?php echo base_url(); ?>recipient/admin/update" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21%" valign="top" bgcolor="#FFFFFF"><strong>Aid name</strong></td>
    <td colspan="2" valign="top" bgcolor="#FFFFFF">
      <?php echo $aid['aid_name']; ?><input type="hidden" name="aid_id" id="aid_id"  value="<?php echo $aid['aid_id']; ?>"/></td>
    <td width="29%" rowspan="8" align="center" valign="top" bgcolor="#FFFFFF">
        
      
  <?php if($edit['imageprofile'] != ""){ ?>
  <input type="file" name="imageprofile" id="imageprofile"  onchange="readURL(this);" ><br />
  <img src="<?php echo base_url(); ?>upload/profile/<?php echo $edit['imageprofile']; ?>"  width="150" height="200" id="blah" />
  <?php }else{ ?>
  <input type="file" name="imageprofile" id="imageprofile"  onchange="readURL(this);"   class="text-input"><br />
  <img id="blah" src="#" alt="Recipient image" />
  <?php } ?><strong>&nbsp;&nbsp;&nbsp;</strong></td>
    </tr>
  <script>
  function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
  </script>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Course</strong></td>
    <td colspan="2" bgcolor="#FFFFFF"><input name="course" type="text" required="required" id="course" class="text-input large-input" value="<?php echo $edit['course']; ?>" /></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Full name</strong></td>
    <td colspan="2" bgcolor="#FFFFFF"><select  id="prefix_id" name="prefix_id" class="chosen-select" style="width:100px;">
      <option value="Ms." <?php if($edit['prefix_id'] == "Ms."){ echo "selected=\"selected\"";} ?>>Ms.</option>
      <option value="Mrs." <?php if($edit['prefix_id'] == "Mrs."){ echo  "selected=\"selected\"";} ?>>Mrs.</option>
      <option value="Mr." <?php if($edit['prefix_id'] == "Mr."){ echo  "selected=\"selected\"";} ?>>Mr.</option>
      </select>
      <input name="fullname" type="text" required class="text-input " id="fullname" value="<?php echo $edit['fullname']; ?>" size="50" />
</td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Given name</strong></td>
    <td colspan="2" bgcolor="#FFFFFF"><strong>
      <input name="givenname" type="text" class="text-input" value="<?php echo $edit['givenname']; ?>"  />
    </strong></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Sex</strong></td>
    <td width="19%" bgcolor="#FFFFFF"><strong>
      <select name="sex_id" id="sex_id" class="chosen-select" style="width:160px" required>
        <option value="Female" <?php if($edit['sex_id'] == "Female"){ echo 'selected="selected"';} ?>>Female</option>
        <option value="Male" <?php if($edit['sex_id'] == "Male"){ echo 'selected="selected"';} ?>>Male</option>
        </select></strong></td>
    <td width="31%" rowspan="3" bgcolor="#FFFFFF"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" valign="top"><strong>Marital</strong></td>
        <td align="left" valign="top"><strong>
          <select name="marital" class="chosen-select" style="width:160px;">
            <option value="Single" <?php if($edit['marital'] == "Single"){ echo 'selected="selected"';} ?>>Single</option>
            <option value="Married" <?php if($edit['marital'] == "Married"){ echo 'selected="selected"';} ?>>Married</option>
            <option value="Separated" <?php if($edit['marital'] == "Separated"){ echo 'selected="selected"';} ?>>Separated</option>
            <option value="Divorced" <?php if($edit['marital'] == "Divorced"){ echo 'selected="selected"';} ?>>Divorced</option>
            <option value="Widowed" <?php if($edit['marital'] == "Widowed"){ echo 'selected="selected"';} ?>>Widowed</option>
          </select>
        </strong></td>
      </tr>
      <tr>
        <td align="left" valign="top"><strong>Age</strong></td>
        <td align="left" valign="top"><strong>
          <input name="age" type="text" class="text-input" value="<?php echo $agecal;?>" id="age" />
          <input type="hidden" name="age_hide" id="age_real" value="" />
        </strong></td>
      </tr>
      <tr>
        <td align="left" valign="top"><strong>Nationality</strong></td>
        <td align="left" valign="top"><strong>
          <input name="nationality" type="text" class="text-input" value="<?php echo $edit['nationality']; ?>"/>
        </strong></td>
      </tr>
    </table></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Birthday</strong></td>
    <td bgcolor="#FFFFFF"><strong>
      <input name="birthday" type="text" class="text-input" id="date_receipion_birth" value="<?php echo $edit['birthday']; ?>"  required/>
      &nbsp;&nbsp;&nbsp;</strong></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Religion</strong></td>
    <td bgcolor="#FFFFFF"><input name="religion" type="text" class="text-input" value="<?php echo $edit['religion']; ?>"/></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>City Of Birth</strong></td>
    <td colspan="2" bgcolor="#FFFFFF"><strong>
      <input name="city_of_birth" type="text" class="text-input" value="<?php echo $edit['city_of_birth']; ?>"/>
    </strong></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Home Address</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><textarea name="home_address" id="home_address" cols="45" rows="5"><?php echo $edit['home_address']; ?></textarea></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Home Telphone</strong></td>
    <td bgcolor="#FFFFFF"><strong>
      <input name="home_tel" type="text" class="text-input" id="home_tel" value="<?php echo $edit['home_tel']; ?>" />
    </strong></td>
    <td colspan="2" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
          <td width="150"><strong>Home Fax</strong></td>
          <td>
            <input name="home_fax" type="text" class="text-input" id="home_fax" value="<?php echo $edit['home_fax']; ?>" />
          </td>
        </tr>
    </table></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Student ID</strong></td>
    <td bgcolor="#FFFFFF"><strong>
      <input name="student_id" type="text" class="text-input" value="<?php echo $edit['student_id']; ?>" />
      </strong></td>
    <td colspan="2" bgcolor="#FFFFFF"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td width="150" align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      </table></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Study status</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><strong>
      <input type="radio" name="study_status" id="radio" value="Graduate" <?php if($edit['study_status'] == "Graduate"){ echo 'checked="checked"';} ?> />
      Graduate
      <input type="radio" name="study_status" id="radio" value="Education" <?php if($edit['study_status'] == "Education"){ echo 'checked="checked"';} ?> />
      Study
      <input type="radio" name="study_status" id="radio" value="No graduation" <?php if($edit['study_status'] == "No graduation"){ echo 'checked="checked"';} ?> />
      No graduation </strong></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Passport ID</strong></td>
    <td bgcolor="#FFFFFF"><strong>
      <input name="passport_id" type="text" class="text-input" value="<?php echo $edit['passport_id']; ?>" required  />
      </strong>&nbsp;&nbsp; </td>
    <td colspan="2" rowspan="2" bgcolor="#FFFFFF"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td width="150" align="left" valign="top"><strong>Passport expire</strong></td>
        <td align="left" valign="top"><input name="expire_passport" type="text" class="text-input"  id="datepicker1" value="<?php echo $edit['expire_passport']; ?>"  required/></td>
      </tr>
      <tr>
        <td align="left" valign="top"><strong>Expiry Date of VISA</strong></td>
        <td align="left" valign="top"><strong>
          <input name="expire_visa" type="text" class="text-input" id="datepicker3" value="<?php echo $edit['expire_visa']; ?>" required />
      
          </strong></td>
      </tr>
      </table></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Expiry Date of Insurance</strong></td>
    <td bgcolor="#FFFFFF"><strong>
      <input name="expire_insurance" type="text" class="required text-input" id="datepicker2" value="<?php echo $edit['expire_insurance']; ?>" />
    </strong>&nbsp;&nbsp;&nbsp;</td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Work Address</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><textarea name="work_address" id="work_address" cols="45" rows="5"><?php echo $edit['work_address']; ?></textarea></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Departure</strong></td>
    <td bgcolor="#FFFFFF"><strong>
      <input name="departure" type="text" class="required text-input" value="<?php echo $edit['departure']; ?>" />
    </strong></td>
    <td bgcolor="#FFFFFF"><strong>Position
        <input name="position" type="text" class="required text-input" id="position" value="<?php echo $edit['position']; ?>"/>
    </strong></td>
    <td bgcolor="#FFFFFF"><strong>Telephone
        <input name="telephone" type="text" class="required text-input" value="<?php echo $edit['telephone']; ?>"/>
    </strong></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Email</strong></td>
    <td bgcolor="#FFFFFF"><strong>
      <input name="email" type="text" class="email text-input" value="<?php echo $edit['email']; ?>" />
      </strong></td>
    <td bgcolor="#FFFFFF"><strong>
      Mobile
      <input name="mobile" type="text" class="required text-input" value="<?php echo $edit['mobile']; ?>"/>
      </strong></td>
    <td bgcolor="#FFFFFF"><strong>Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input name="fax" type="text" class="text-input" value="<?php echo $edit['fax']; ?>"/>
      </strong></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Institute</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><select name="institute_id" class="required chosen-select" data-placeholder="Choose Institute Name..."  style="width:80%;">
      <option value=""></option>
      <?php foreach($in as $listin){?>
      <option value="<?php echo $listin['institute_id']; ?>" <?php if($listin['institute_id'] == $edit['institute_id']){ echo 'selected="selected"';} ?>><?php echo $listin['institute_name']; ?></option>
      <?php } ?>
      </select></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Country</strong></td>
    <td colspan="3" bgcolor="#FFFFFF">
    <select name="country_id" class="required chosen-select" data-placeholder="Choose Country..."  id="country_id" style="width:80%">
    	 <option value=""></option>
      <?php foreach($country as $listcountry){?>
      <option value="<?php echo $listcountry['country_id']; ?>" <?php if($listcountry['country_id'] == $edit['country_id']){ echo 'selected="selected"';} ?>><?php echo $listcountry['country_name']; ?></option>
      <?php } ?>
    </select></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Emergency Name</strong></td>
    <td bgcolor="#FFFFFF"><input name="emergency_name" type="text" class="text-input" id="emergency_name" value="<?php echo $edit['emergency_name']; ?>"/></td>
    <td colspan="2" bgcolor="#FFFFFF"><strong>Emergency Relation</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      <input name="emergency_relation" type="text" class="text-input" id="emergency_relation" value="<?php echo $edit['emergency_relation']; ?>"/>
      </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Emergency telephone</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td bgcolor="#FFFFFF"><input name="emergency_telephone" type="text" class="text-input" id="emergency_telephone" value="<?php echo $edit['emergency_telephone']; ?>"/></td>
    <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Emergency Address</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><textarea name="emergency_address" class="text-input" id="emergency_address"><?php echo $edit['emergency_address']; ?></textarea>      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Name organisation</strong></td>
    <td bgcolor="#FFFFFF"><input name="gov_organisation" type="text" class="text-input" id="gov_organisation" value="<?php echo $edit['gov_organisation']; ?>"/></td>
    <td colspan="2" bgcolor="#FFFFFF"><strong>Goverment Title</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input name="gov_title" type="text" class="text-input" id="gov_title" value="<?php echo $edit['gov_title']; ?>"/>
    </td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Goverment Date</strong></td>
    <td bgcolor="#FFFFFF"><input name="gov_date" type="text" class="text-input" value="<?php echo $edit['gov_date']; ?>" id="datepicker4"/></td>
    <td colspan="2" bgcolor="#FFFFFF"><strong>Gover duties</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input name="gov_duties" type="text" class="text-input" value="<?php echo $edit['gov_duties']; ?>"/>
      </td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Goverment post title</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><input name="gov_post_title" type="text" class="text-input" value="<?php echo $edit['gov_post_title']; ?>"/></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Goverment Address</strong></td>
    <td colspan="3" bgcolor="#FFFFFF">
      
      <textarea name="gov_address" id="gov_address" cols="45" rows="5"><?php echo $edit['gov_address']; ?></textarea></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Previous Type</strong></td>
    <td bgcolor="#FFFFFF"><input name="previous_type" type="text" class="text-input" value="<?php echo $edit['previous_type']; ?>"/></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Previous Title</strong></td>
    <td bgcolor="#FFFFFF"><strong>
      <input name="previous_title" type="text" class="text-input" value="<?php echo $edit['previous_title']; ?>"/>
    </strong></td>
    <td colspan="2" bgcolor="#FFFFFF"><strong>Previous Name</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;
<input name="previous_name" type="text" class="text-input" value="<?php echo $edit['previous_name']; ?>"/></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Previous Date Start</strong></td>
    <td bgcolor="#FFFFFF"><input name="previous_datestart" type="text" class="text-input" id="datepicker9" value="<?php echo $edit['previous_datestart']; ?>"/></td>
    <td colspan="2" bgcolor="#FFFFFF"><strong>Previous Date End&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input name="previous_dateend" type="text" class="text-input" id="datepicker10" value="<?php echo $edit['previous_dateend']; ?>"/>
    </strong></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Previous Detail</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><input name="previous_detail" type="text" class="text-input large-input" value="<?php echo $edit['previous_detail']; ?>"/></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Previous Address</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><textarea name="previous_address" id="previous_address" cols="45" rows="5"><?php echo $edit['previous_address']; ?></textarea></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Present Type</strong></td>
    <td bgcolor="#FFFFFF"><input name="present_type" type="text" class="text-input" id="present_type" value="<?php echo $edit['present_type']; ?>"/></td>
    <td bgcolor="#FFFFFF"><strong>Present Title</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input name="present_title" type="text" class="text-input"  value="<?php echo $edit['present_title']; ?>"/></td>
    <td bgcolor="#FFFFFF"><strong>Present Name
      <input name="present_name" type="text" class="text-input"  value="<?php echo $edit['present_name']; ?>"/>
    </strong></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Present Date Start</strong></td>
    <td bgcolor="#FFFFFF"><input name="present_datestart" type="text" class="text-input" id="datepicker7"  value="<?php echo $edit['present_datestart']; ?>"/></td>
    <td bgcolor="#FFFFFF"><strong>Present Date End</strong>
      <input name="present_dateend" type="text" class="text-input" id="datepicker8"  value="<?php echo $edit['present_dateend']; ?>"/></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Present Detail</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><input name="present_detail" type="text" class="text-input large-input"  value="<?php echo $edit['present_detail']; ?>"/></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Present Address</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><textarea name="present_address" id="present_address" cols="45" rows="5"> <?php echo $edit['present_address']; ?></textarea></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Candidate</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><input name="candidate" type="text" class="text-input"  value="<?php echo $edit['candidate']; ?>"/>
&nbsp;&nbsp;&nbsp;&nbsp;      <strong>Have you been Aid?</strong>
      <input name="before_come" type="radio" value="1" <?php if($edit['before_come'] == "1"){ echo 'checked="checked"';} ?> />
      <strong>Yes
<input name="before_come" type="radio" value="0" <?php if($edit['before_come'] == "0"){ echo 'checked="checked"';} ?> />
No </strong></td>
    </tr>
  <tr valign="top">
    <td colspan="4" bgcolor="#FFFFFF"><input name="is_ielts" type="checkbox" id="is_ielts" value="1" <?php if($edit['is_ielts'] == "1"){ echo 'checked="checked"';} ?>/>
      <strong>IELTS</strong> <strong>Score
      <input name="ielts_score" type="text" class="text-input " id="ielts_score" value="<?php echo $edit['ielts_score']; ?>" size="5"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <input name="is_toefl" type="checkbox" value="1" id="is_toefl" <?php if($edit['is_toefl'] == "1"){ echo 'checked="checked"';} ?> />
  TOEFL    Score
  <input name="toefl_score" type="text" class="text-input" id="toefl_score"  value="<?php echo $edit['toefl_score']; ?>" size="5"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <input name="is_orther" type="checkbox" value="1" <?php if($edit['is_orther'] == "1"){ echo 'checked="checked"';} ?> />
Other    Score
<input name="orther_score" type="text" class="text-input " id="orther_score" value="<?php echo $edit['orther_score']; ?>" size="5"/>
  </strong></td>
    </tr>
  <tr valign="top">
    <td colspan="4" bgcolor="#FFFFFF"><!--<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td bgcolor="#F3F3F3"><p><strong>Mather Tongue</strong>
          <input name="mother_tongue" type="text" class="text-input" id="mother_tongue" value="<?php echo $edit['mother_tongue']; ?>"/>
        </p>
          <p><strong>English Speak</strong>
            &nbsp;&nbsp;
            <input type="radio" name="eng_speak" id="radio4" value="3" <?php if($edit['eng_speak'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="eng_speak" id="radio5" value="2" <?php if($edit['eng_speak'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="eng_speak" id="radio6" value="1" <?php if($edit['eng_speak'] == "1"){ echo 'checked="checked"';} ?> />
            Fair</p>
          <p><strong>English Write</strong>
            &nbsp;&nbsp;&nbsp;
            <input type="radio" name="eng_write" id="radio4" value="3" <?php if($edit['eng_write'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="eng_write" id="radio5" value="2" <?php if($edit['eng_write'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="eng_write" id="radio6" value="1" <?php if($edit['eng_write'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p>
          <p><strong>English Read</strong>
            &nbsp;&nbsp;&nbsp; 
            <input type="radio" name="eng_read" id="radio4" value="3" <?php if($edit['eng_read'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="eng_read" id="radio5" value="2" <?php if($edit['eng_read'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="eng_read" id="radio6" value="1" <?php if($edit['eng_read'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p></td>
        <td bgcolor="#F8F8F8"><p><strong>Other Language</strong>
          <input name="other_lang" type="text" class="text-input" id="other_lang" value="<?php echo $edit['other_lang']; ?>"/>
        </p>
          <p><strong>Other Speak</strong>
            &nbsp;&nbsp;
            <input type="radio" name="other_speak" id="radio4" value="3" <?php if($edit['other_speak'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="other_speak" id="radio5" value="2" <?php if($edit['other_speak'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="other_speak" id="radio6" value="1" <?php if($edit['other_speak'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p>
          <p><strong>Other Write</strong>
            &nbsp;&nbsp;&nbsp;
            <input type="radio" name="other_write" id="radio4" value="3" <?php if($edit['other_write'] == "3"){ echo 'checked="checked"';} ?>  />
            <span class="header">Excellent</span>
            <input type="radio" name="other_write" id="radio5" value="2" <?php if($edit['other_write'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="other_write" id="radio6" value="1" <?php if($edit['other_write'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p>
          <p><strong>Other Read</strong>
            &nbsp;&nbsp;&nbsp;
            <input type="radio" name="other_read" id="radio4" value="3" <?php if($edit['other_read'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="other_read" id="radio5" value="2" <?php if($edit['other_read'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="other_read" id="radio6" value="1" <?php if($edit['other_read'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p></td>
      </tr>
    </table>--></td>
    </tr>
  <tr valign="top">
    <td colspan="4" bgcolor="#FFFFFF">
    <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#929292">
      <tr>
        <td rowspan="2" align="center" bgcolor="#FFFFFF" class="header"><strong>Languages </strong></td>
        <td colspan="3" align="center" bgcolor="#FFFFFF" class="header"><strong>Read</strong></td>
        <td colspan="3" align="center" bgcolor="#FFFFFF" class="header"><strong>Write</strong></td>
        <td colspan="3" align="center" bgcolor="#FFFFFF" class="header"><strong>Speak</strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#FFFFFF" class="header"><strong>Excellent</strong></td>
        <td align="center" bgcolor="#FFFFFF" class="header"><strong>Good</strong></td>
        <td align="center" bgcolor="#FFFFFF" class="header"><strong>Fair</strong></td>
        <td align="center" bgcolor="#FFFFFF" class="header"><strong>Excellent</strong></td>
        <td align="center" bgcolor="#FFFFFF" class="header"><strong>Good</strong></td>
        <td align="center" bgcolor="#FFFFFF" class="header"><strong>Fair</strong></td>
        <td align="center" bgcolor="#FFFFFF" class="header"><strong>Excellent</strong></td>
        <td align="center" bgcolor="#FFFFFF" class="header"><strong>Good</strong></td>
        <td align="center" bgcolor="#FFFFFF" class="header"><strong>Fair</strong></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><strong><span  class="labeltxt">Mother tongue :</span>
          <input name="mother_tongue" type="text" class="text-input" id="mother_tongue" value="<?php echo $edit['mother_tongue']; ?>"/>
        </strong></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="mother_read" id="radio" value="3" <?php if($edit['mother_read'] == "3"){ echo 'checked="checked"';} ?> />
        </p>
          <label for="mother_speak"></label></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="mother_read" id="radio3" value="2" <?php if($edit['mother_read'] == "2"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="mother_read" id="radio10" value="1" <?php if($edit['mother_read'] == "1"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="mother_write" id="radio17" value="3" <?php if($edit['mother_write'] == "3"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="mother_write" id="radio18" value="2" <?php if($edit['mother_write'] == "2"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="mother_write" id="radio19" value="1" <?php if($edit['mother_write'] == "1"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="mother_speak" id="radio19" value="3" <?php if($edit['mother_speak'] == "3"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="mother_speak" id="radio20" value="2" <?php if($edit['mother_speak'] == "2"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="mother_speak" id="radio21" value="1" <?php if($edit['mother_speak'] == "1"){ echo 'checked="checked"';} ?> />
        </p></td>
        </tr>
      <tr>
        <td bgcolor="#FFFFFF"><strong><span class="labeltxt">English : </span></strong></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="eng_read" id="radio21" value="3" <?php if($edit['eng_read'] == "3"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="eng_read" id="radio23" value="2" <?php if($edit['eng_read'] == "2"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="eng_read" id="radio25" value="1" <?php if($edit['eng_read'] == "1"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="eng_write" id="radio17" value="3" <?php if($edit['eng_write'] == "3"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="eng_write" id="radio18" value="2" <?php if($edit['eng_write'] == "2"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="eng_write" id="radio19" value="1" <?php if($edit['eng_write'] == "1"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="eng_speak" id="radio19" value="3" <?php if($edit['eng_speak'] == "3"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="eng_speak" id="radio20" value="2" <?php if($edit['eng_speak'] == "2"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="eng_speak" id="radio21" value="1" <?php if($edit['eng_speak'] == "1"){ echo 'checked="checked"';} ?> />
        </p></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><strong><span class="labeltxt">Other :</span>
          <input name="other_lang" type="text" class="text-input" id="other_lang" value="<?php echo $edit['other_lang']; ?>"/>
        </strong></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="other_read" id="radio22" value="3" <?php if($edit['other_read'] == "3"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="other_read" id="radio23" value="2" <?php if($edit['other_read'] == "2"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="other_read" id="radio27" value="1" <?php if($edit['other_read'] == "1"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="other_write" id="radio24" value="3" <?php if($edit['other_write'] == "3"){ echo 'checked="checked"';} ?>  />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="other_write" id="radio25" value="2" <?php if($edit['other_write'] == "2"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="other_write" id="radio26" value="1" <?php if($edit['other_write'] == "1"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="other_speak" id="radio27" value="3" <?php if($edit['other_speak'] == "3"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="other_speak" id="radio28" value="2" <?php if($edit['other_speak'] == "2"){ echo 'checked="checked"';} ?> />
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <input type="radio" name="other_speak" id="radio29" value="1" <?php if($edit['other_speak'] == "1"){ echo 'checked="checked"';} ?> />
        </p></td>
      </tr>
    </table></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Expect</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><textarea name="expect" id="textarea" cols="45" rows="5"><?php echo $edit['expect']; ?></textarea></td>
  </tr>
  <tr valign="top">
    <td colspan="4" bgcolor="#FFFFFF"><strong>Signature</strong>
      <input type="file" name="signature" id="signature" />
     <?php if(!empty($edit['signature'])){?> <img src="<?php echo base_url(); ?>upload/signature/<?php echo $edit['signature']; ?>" width="200"><?php } ?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Printed</strong>
      <input name="printed" type="text" class="text-input" id="printed" value="<?php echo $edit['printed']; ?>" /></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Comment</strong></td>
    <td colspan="3" bgcolor="#FFFFFF"><textarea name="comment" id="comment" cols="45" rows="5"><?php echo $edit['comment']; ?></textarea></td>
  </tr>
  <tr valign="top">
    <td colspan="4" bgcolor="#FFFFFF"><strong>Approve Status</strong>
      <input name="status" type="radio" id="status" value="1" <?php if(($edit['status'] == "1") || ($this->uri->segment(4) == "")){ echo 'checked="checked"';} ?> required="required" />
      Wait&nbsp;&nbsp;&nbsp;
  <input type="radio" name="status" id="status" value="3" <?php if($edit['status'] == "3"){ echo 'checked="checked"';} ?>  required="required"  />
      Reject
  <input type="radio" name="status" id="status" value="2" <?php if($edit['status'] == "2"){ echo 'checked="checked"';} ?>   required="required" />
      Approve</td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Approve By</strong></td>
    <td bgcolor="#FFFFFF"><select name="approve_by" class="required chosen-select" data-placeholder="Choose User Name..."  style="width:250px;">
      <option value=""></option>
      <?php 
	  if(empty($edit['approve_by'])){
	  $userapprove = $this->session->userdata('userid');
	  }else{
		$userapprove =   $edit['approve_by'];
	  }
	  foreach($user as $listuser) { ?>
      <option value="<?php echo $listuser['user_id']; ?>" <?php if($userapprove == $listuser['user_id']){echo "selected='selected'";} ?>><?php echo $listuser['user_fullname']; ?></option>
      <?php } ?>
    </select></td>
    <td bgcolor="#FFFFFF"><strong>Dates capital</strong></td>
    <td bgcolor="#FFFFFF"><input name="date_capital" type="text" class="text-input" id="datepicker11"  value="<?php echo $edit['date_capital']; ?>" required="required"/></td>
    </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong> Start Course</strong></td>
    <td bgcolor="#FFFFFF">
      <input name="date_funded" type="text"  class="text-input datefunded" id="date_funded" value="<?php echo $edit['date_funded']; ?>"/></td>
    <td bgcolor="#FFFFFF"><strong>End Course</strong></td>
    <td bgcolor="#FFFFFF"><input name="end_date_funded" type="text" class="text-input datefunded" id="end_date_funded" value="<?php echo $edit['end_date_funded']; ?>" /></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><strong>Start Course Extend</strong></td>
    <td bgcolor="#FFFFFF">
      <input name="start_date_extend" type="text" class=" text-input datefunded" id="start_date_extend" value="<?php echo $edit['start_date_extend']; ?>"/></td>
    <td bgcolor="#FFFFFF"><strong>End Course Extend</strong></td>
    <td bgcolor="#FFFFFF"><input name="end_date_extend" type="text" class="text-input datefunded" id="end_date_extend" value="<?php echo $edit['end_date_extend']; ?>" /></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF"><input type="submit" name="button" id="button" value="Submit" class="button green" />
      <input name="recid" type="hidden" value="<?php echo $edit['rec_id']; ?>" />
      <input name="memberid" type="hidden" id="memberid" value="<?php echo $edit['member_id']; ?>" /></td>
    <td colspan="3" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  </table>

  </form>


