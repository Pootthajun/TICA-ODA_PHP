<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MRegion extends CI_Model{
    
    function insertregion(){
        $data = array(
            'region_name'=>$this->input->post('region_name'),          
        );
        $this->db->insert('region',$data);
        
    }
    
    function listallregion(){
        $data = array();
        $this->db->order_by('region_id',"ASC");
        $Q = $this->db->get('region');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getregiontypebyid($regionid){
        //$data = array();getregiontypebyid
        $this->db->where('region_id',$regionid);
        $Q = $this->db->get('region');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updteregion(){
        $data = array(
            'region_name'=>$this->input->post('region_name'),         
        );
        $this->db->where('region_id',$this->input->post('regionid'));
        $this->db->update('region',$data);
    }
    
    function deleteregion($regionid){              
           $this->db->where('region_id',$regionid);
           $this->db->delete('region');       
    }
    

}