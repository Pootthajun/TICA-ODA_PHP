<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Region Table";
        $data['all'] = $this->MRegion->listallregion();
    	$data['menu'] = $this->MUser->getMenubyUser();      
	$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addregion(){
        $data['header'] = "Add region name";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addregion';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertregion(){
        $this->MRegion->insertregion();
        flashMsg('success',$this->lang->line('insert_success'));
	    redirect( 'region/admin/','refresh');
    }
    
    function editregion($regionid){
        $data['header'] = "Edit Region table";
        $data['edit'] = $this->MRegion->getregiontypebyid($regionid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editregion';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateregion(){
        $this->MRegion->updteregion();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'region/admin/','refresh');;
    }
    
    function deleteregion($regionid){
        $query = $this->db->get_where('country', array('region_id' => $regionid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'region/admin/','refresh');            
        }else{
            $this->MRegion->deleteregion($regionid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'region/admin/','refresh');
        }
    }
}