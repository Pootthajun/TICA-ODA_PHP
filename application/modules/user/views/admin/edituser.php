                  <form name="form1" method="post" action="<?php echo site_url(); ?>user/admin/updateuser/" id="form1">				  <p>
									<label>Username</label>
									<input name="user_username" type="text" required class="text-input medium-input" id="user_username" value="<?php echo $user['user_username']; ?>"> 
								</p>
                                 <p>
									<label>User Password</label>
									<input type="password" name="user_password" id="user_password" class="text-input medium-input" > 
								</p>
                                <p>
									<label>User Fullname</label>
									<input type="text" name="user_fullname" id="user_fullname" class="text-input medium-input" required value="<?php echo $user['user_fullname']; ?>"> 
								</p>
                                
                                 <p>
									<label>User Email</label>
									<input type="text" name="user_email" id="user_email" class="text-input medium-input required email" value="<?php echo $user['user_email']; ?>"> 
								</p>
                                 <p>
									<label>User level</label>
                                    <?php if($this->session->userdata('level') == 1) { ?>
									<select name="user_level" id="user_level"  class="chosen-select" data-placeholder="Choose User level..." >
    							  <option value=""></option>
									  <option value="1" <?php if($user['user_level'] == 1){ echo "selected='selected' ";} ?>>Administrator</option>
									  <option value="2" <?php if($user['user_level'] == 2){ echo "selected='selected' ";} ?>>เจ้าหน้าที่ TICA</option>
                                      <option value="3" <?php if($user['user_level'] == 3){ echo "selected='selected' ";} ?>>เจ้าหน้าที่การเงิน สพร.</option>
									  <option value="4" <?php if($user['user_level'] == 4){ echo "selected='selected' ";} ?>> เจ้าหน้าที่อำนวยสิทธิ์</option>
                                      <option value="5" <?php if($user['user_level'] == 5){ echo "selected='selected' ";} ?>> หน่วยงานภายนอก</option>
									  <option value="6" <?php if($user['user_level'] == 6){ echo "selected='selected' ";} ?>>อื่นๆ</option>
						           </select>
                                   <?php }else{ ?>
                                
                                      <?php if($user['user_level'] == 1){ echo "Administrator ";} ?>
									 <?php if($user['user_level'] == 2){ echo "เจ้าหน้าที่ TICA ";} ?>
                                     <?php if($user['user_level'] == 3){ echo "เจ้าหน้าที่การเงิน สพร.";} ?>
									 <?php if($user['user_level'] == 4){ echo "เจ้าหน้าที่อำนวยสิทธิ์ ";} ?>
                                    <?php if($user['user_level'] == 5){ echo "หน่วยงานภายนอก ";} ?>
									  <?php if($user['user_level'] == 6){ echo "อื่นๆ ";} ?>
                                   
                                    <input type="hidden" name="user_level" id="user_level"  value="<?php echo $user['user_level']; ?>"> 
                                   <?php } ?>
                                </p>
                                <p>
									<label>Tel</label>
									<input type="text" name="user_tel" id="user_tel" class="text-input medium-input " value="<?php echo $user['user_tel']; ?>"> 
								</p>
                    <p>
									<label>Fax</label>
									<input type="text" name="user_fax" id="user_fax" class="text-input medium-input " value="<?php echo $user['user_fax']; ?>"> 
								</p>
                                <p>
                                  <input type="submit" name="button" id="button" value="Update" class="button green">
                                  <input name="editid" type="hidden" id="editid" value="<?php echo $user['user_id']; ?>" />
                                </p>
                  </form>
