 <style type="text/css">
	label 
	{
		width:auto;		
	}
</style>
                 <p class="addbutton"> 
                <a href="<?php echo site_url(); ?>user/admin/adduser/" class="button green">Add</a>
                </p>
                
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Username</th>
                      <th>Name Lastname</th>
                      <th>User Email</th>
                      <th>User Level</th>
                      <th width="23%">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
					$count = 1;
					foreach($user as $list){ ?>
                    <tr>
                      <td><?php echo $count++; ?></td>
                      <td><?php echo $list['user_username']; ?></td>
                      <td><?php echo $list['user_fullname']; ?></td>
                      <td><?php echo $list['user_email']; ?></td>
                      <td>
					  <?php if($list['user_level'] == "1"){
							echo "Administrator";
					  }else if($list['user_level'] == "2"){
						  echo "เจ้าหน้าที่ TICA";
					  }else if($list['user_level'] == "3"){
						  echo "เจ้าหน้าที่การเงิน สพร.";
					 }else if($list['user_level'] == "4"){
						  echo " เจ้าหน้าที่อำนวยสิทธิ์";
				     }else if($list['user_level'] == "5"){
						  echo "หน่วยงานภายนอก";
					 }else if($list['user_level'] == "6"){
						  echo "อื่นๆ";
					 }
					 ?>
                      </td>
                      <td align="right">
                      <?php if(($list['user_id'] == $this->session->userdata('userid')) ||  ($this->session->userdata('level') == 1)){ ?>
                      <a href="<?php echo site_url(); ?>user/admin/edituser/<?php echo $list['user_id']; ?>" class="button blue">Edit</a>
                        <?php } ?>
                      
                      <?php if($this->session->userdata('level') == 1) { ?>
                      <a href="<?php echo site_url(); ?>user/admin/deleteuser/<?php echo $list['user_id']; ?>"  onclick="return confirm('Are you sure you want to delete <?php echo $list['user_fullname']; ?>?')" class="button red"><?php echo $this->lang->line('delete'); ?></a>
                      <?php } ?>
                      </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>

