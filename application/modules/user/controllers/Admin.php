<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Admin extends Admin_Controller{

	function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));
       // $this->bep_site->set_crumb($this->lang->line('user'),'user/admin'); 
	
    }
	
	function welcome(){
        $data['header'] = "User Table";
        $data['user'] = $this->MUser->listalluser();
        $data['menu'] = $this->MUser->getMenubyUser();      
	$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data);     
	}
        
        function adduser(){
            $data['header'] ="Add User";
            $data['page'] = $this->config->item('wconfig_template_admin').'adduser';
            $this->load->view($this->_container_admin,$data);            
        }
        
        function insertuser(){
            $this->db->where('user_username',$this->input->post('user_username'));
            $Q = $this->db->get('user');
		if($Q->num_rows() > 0){
                    flashMsg('warning',"Can not add more because the username already funded. ");
                    redirect( 'user/admin/welcome','refresh');
                }else{
                    $this->MUser->inseruser();
                    flashMsg('success',$this->lang->line('insert_success'));
                    redirect( 'user/admin/welcome','refresh');
                }
                $Q->free_result();
        return $data;
        }
        
        function deleteuser($userid){
            $this->MUser->deleteuser($userid);
            flashMsg('success',$this->lang->line('delete_success'));
	    redirect( 'user/admin/welcome','refresh');
            
        }
        
        function edituser($userid){
            $data['header'] ="Edit User";
            $data['user'] = $this->MUser->getUserById($userid);
            $data['page'] = $this->config->item('wconfig_template_admin').'edituser';
            $this->load->view($this->_container_admin,$data);
        }
        
        function updateuser(){
        /*    $this->db->where('user_username',$this->input->post('user_username'));
            $Q = $this->db->get('user');
		if($Q->num_rows() > 0){
                    flashMsg('warning',"Can not add more because the username already funded. ");
                    redirect( 'user/admin/welcome','refresh');
                }else{*/
                    $this->MUser->updateuser();
                    flashMsg('success',$this->lang->line('update_success'));
                    redirect( 'user/admin/welcome','refresh');
           /*    }
                $Q->free_result();
        return $data;*/
        }
        
        function userauthen(){
            $data['header'] ="User Authen";
            $data['user'] = $this->MUser->listalluser();
            $data['page'] = $this->config->item('wconfig_template_admin').'userauthen';
            $this->load->view($this->_container_admin,$data);
            
        }
        
        function userauthenedit($userid){
            $name = $this->MUser->getUserById($userid);
            $data['header'] ="User  Authen&nbsp;&nbsp;&nbsp;&nbsp;".$name['user_username'];
            $data['user'] = $name;
            $data['page'] = $this->config->item('wconfig_template_admin').'userauthenedit';
            $this->load->view($this->_container_admin,$data);
        }

        function updateauthen(){
            $this->MUser->updateauthen();        
            flashMsg('success',$this->lang->line('update_success'));
            redirect( 'user/admin/userauthen','refresh');
        }
        
}