<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Aid Table";
        $data['ac'] = $this->MActivity->listallActivity();
        $data['project'] = $this->MProject->listallproject();
        $data['all'] = $this->MAid->listallAidadminforaidmenu();
    	$data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function aidtype(){
        $data['header'] = "Aid type table";
        $data['all'] = $this->MAid->listallAidtype();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidtype';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addaidtype(){
        $data['header'] = "Aid type table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addaidtype';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function insertaidtype(){
        $this->MAid->insertaidtype();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'aid/admin/aidtype','refresh');
    }
    
    function editaidtype($aidtypeid){
        $data['header'] = "Edit Aid type table";
        $data['edit'] = $this->MAid->getAidtypebyid($aidtypeid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editaidtype';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateaidtype(){
        $this->MAid->updteaidtype();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'aid/admin/aidtype','refresh');;
    }
    
    function deleteaidtype($aidtypeid){
        $query = $this->db->get_where('aid', array('aid_type_id' => $aidtypeid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'aid/admin/aidtype','refresh');            
        }else{
            $this->MAid->deleteaidtype($aidtypeid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'aid/admin/aidtype','refresh');
        }
    }
    
    function addaid(){
        $data['header'] = "Add Aid Name";
        $data['all'] = $this->MAidtype->listallAidtype();
        $data['allactivity'] = $this->MActivity->listallActivity();
    	$data['page'] = $this->config->item('wconfig_template_admin').'addaid';
    	$this->load->view($this->_container_admin,$data);
    }

    function insertaid(){
        $this->MAid->insertaid();
        flashMsg('success',$this->lang->line('insert_success'));
        redirect( 'aid/admin/','refresh');
    }

    function editaid($aidid){
        $data['header'] = "Edit Aid Name";
        $data['all'] = $this->MAidtype->listallAidtype();
        $data['allactivity'] = $this->MActivity->listallActivity();
        $data['edit'] = $this->MAid->getAidbyid($aidid);
        $data['page'] = $this->config->item('wconfig_template_admin').'editaid';
        $this->load->view($this->_container_admin,$data);
    }

    function updateaid(){
        $this->MAid->updateaid();
        flashMsg('success',$this->lang->line('update_success'));
        redirect( 'aid/admin/','refresh');
    }

    function deleteaid($aidid){
        $this->MAid->deleteaid($aidid);
        flashMsg('success',$this->lang->line('delete_success'));
        redirect( 'aid/admin/','refresh');
    }
	
	function searchdata(){	
		   $id=$this->input->post('id');
           $Q=$this->db->query("select aid.*,ac.activity_name,aidtype.aid_type_name from aid as aid 
		   inner join activity as ac on aid.activity_id = ac.activity_id
		   inner join aid_type as aidtype on aidtype.aid_type_id = aid.aid_type_id
		    where  aid.activity_id='$id'");      
			$data['all'] = $Q->result_array();	    
			$data['menu'] = $this->MUser->getMenubyUser();   
			$data['page'] = $this->config->item('wconfig_template_admin').'resultsearch';      
			$this->load->view($this->_view_subsector,$data); 
	}
	
	
		function getActivitybyprojectid(){
		   $id=$this->input->post('id');
           $Q=$this->db->query("select * from activity where  project_id='$id'"); 
		   echo "
		   	   	   <script>
				  $(document).ready(function(){  
				  $(\".chosen-select\").chosen({allow_single_deselect:true,no_results_text:'Oops, nothing found!'});
				  
  $(\"#activity\").change(function()
	{	
	var id=$(this).val();
	var dataString = 'id='+ id;
	$(\"#resultTable\").html('<center><br><br><img src=\"".base_url()."assets/welcome/images/ajax-loader.gif\" /></center>');
	$.ajax
	({
	type: \"POST\",
	url: '".base_url()."aid/admin/searchdata/',
	data: dataString,
	cache: false,
	success: function(html)
	{
	$(\"#resultTable\").fadeOut(100).html(html).fadeIn(500);
	} 
	});
		});
				});
				</script>
		   <select name=\"activity\" id=\"activity\" class=\"chosen-select\"  data-placeholder=\"Choose Activity Name..\" >";        
			foreach($Q->result_array() as$value){
             	echo '  <option value=""></option>';
                echo'<option value="'.$value['activity_id'].'">'.$value['activity_name'].'</option>';              
			}
			echo '  </select> ';
	}
 
        
        function getuserproject(){
		$id=$this->input->post('id');
		$user = $this->session->userdata('userid');                
		$project_assistant = '';
                $project_owner = '';
                $this->db->where('project_id',$id);
                $Q = $this->db->get('project_assistant'); 
                foreach($Q->result_array() as $listp){
			$project_assistant .= $listp['user_id'].",";  
		}
                
                $this->db->where('project_id',$id);
                $Q = $this->db->get('project'); 
                foreach($Q->result_array() as $listp){
			$project_owner = $listp['project_owner'];  
		}
		
		$auser = $project_assistant.$project_owner;
 
		  $ex = explode(",",$auser);
			 for($i=0;$i<count($ex);$i++){ 
					  if($ex[$i] == $this->session->userdata('userid')){ 
					
							echo '<input type="submit" name="button" id="button" value="Add" class="button green" />';
					}
			 }
	}

}