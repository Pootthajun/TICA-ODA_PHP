<script type="text/javascript">
$(document).ready(function(){
	$('#table_id').DataTable();
	$('tbody tr:even').addClass("alt-row");
});
</script>

       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="52">Id</th>
          <th width="292">Aid  Name</th>
          <th width="273">Aid Type</th>
          <th width="305">Activity Name</th>
          
          
        <?php  if( !empty( $menu ) ) { ?>
          <th>&nbsp;</th>
<?php } ?>
    
        </tr>
      </thead>
      <tbody>
  

<?php 
$count = 1;
foreach($all as $list){ ?>
        <tr>
          <td><?php echo $count++; ?></td>
          <td><?php echo $list['aid_name']; ?></td>
          
          
          <td><?php 
		  $this->db->where('aid_type_id',$list['aid_type_id']);
			$QT = $this->db->get('aid_type');
			$typename = $QT->row_array();
			echo $typename['aid_type_name']
		    ?></td>
		    <td><?php
		  $this->db->where('activity_id',$list['activity_id']);
			$QA = $this->db->get('activity');
			$acname = $QA->row_array();
			echo $acname['activity_name']
          
          ?></td>
          
          
          <?php  if( !empty( $menu ) ) { ?>
          <td width="131" align="right"><a href="<?php echo site_url(); ?>aid/admin/editaid/<?php echo $list['aid_id']; ?>" class="button blue">Edit</a><a href="<?php echo site_url(); ?>aid/admin/deleteaid/<?php echo $list['aid_id']; ?>"  onclick="return confirm('Are you sure you want to delete aid  name <?php echo $list['aid_name']; ?> ?')" class="button red"><?php echo $this->lang->line('delete'); ?></a></td>
       <?php } ?>
        </tr>
        <?php } ?>
      </tbody>
    </table>