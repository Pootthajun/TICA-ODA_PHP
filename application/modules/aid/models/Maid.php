<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MAid extends CI_Model{
    
    

    
    function getAidByActivityid($activityid){
        //$data = array();
        $this->db->where('activity_id',$activityid);
        $Q = $this->db->get('aid');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;        
    }
    

    
    function getAidByAidtype($aidtypeid){
        $this->db->where('aid_type_id',$aidtypeid);
        $Q = $this->db->get('aid');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;   
    }
    function listallAidadmin(){
        $data = array();
        $this->db->where('aid_status',"1");
        $this->db->order_by('aid_id',"ASC");
        $Q = $this->db->get('aid');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function listallAidadminForBudget(){
        $data = array(); 
        $this->db->order_by('aid_id',"ASC");
        $Q = $this->db->get('aid');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function listallAidadminforaidmenu(){
        $data = array();
        //$this->db->where('aid_status',"1");
        $this->db->order_by('aid_id',"ASC");
        $Q = $this->db->get('aid');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function listallAid(){
        $data = array();
        $this->db->where('aid_status',"1");
        $this->db->order_by('aid_id',"ASC");
        $Q = $this->db->get('aid');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    function listallAidforDashboard(){
        $data = array(); 
        $this->db->order_by('aid_id',"ASC");
        $Q = $this->db->get('aid');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    function insertaid(){
        $data = array(
        'activity_id'=>$this->input->post('activity_id'),
        'aid_type_id'=>$this->input->post('aid_type_id'),
        'aid_name'=>$this->input->post('aid_name'),
        'aid_budget'=>str_replace(",","",$this->input->post('aid_budget')),
        'aid_status'=>$this->input->post('aid_status'),
	'aid_pay'=>$this->input->post('aid_pay'),
        'aid_recipient'=>$this->input->post('aid_recipient'),
        'aid_by'=>$this->session->userdata('userid'),
        'aid_create'=>date('Y-m-d')
        );
        $this->db->insert('aid',$data);
    }

    function getAidbyid($aid){
        $this->db->where('aid_id',$aid);
        $Q = $this->db->get('aid');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }

    function updateaid(){
        $data = array(
        'activity_id'=>$this->input->post('activity_id'),
        'aid_type_id'=>$this->input->post('aid_type_id'),
        'aid_name'=>$this->input->post('aid_name'),
        'aid_budget'=>str_replace(",","",$this->input->post('aid_budget')),
        'aid_status'=>$this->input->post('aid_status'),
	'aid_pay'=>$this->input->post('aid_pay'),
        'aid_recipient'=>$this->input->post('aid_recipient'),
        'aid_by'=>$this->session->userdata('userid'),
        'aid_create'=>date('Y-m-d')
        );
        $this->db->where('aid_id',$this->input->post('aidid'));
        $this->db->update('aid',$data);
    }

    function deleteaid($aidid){
        $this->db->where('aid_id',$aidid);
        $this->db->delete('aid');       
    }
    
    function getAidJoin($aid){
        $sql = "select a.*, p.plan_name, project.project_id,project.project_name, activity.activity_name 
                                from aid as a 
                                inner join activity as activity on activity.activity_id=a.activity_id 
                                inner join project as project on project.project_id=activity.project_id 
                                inner join plan as p on p.plan_id=project.plan_id 
                                WHERE a.aid_id = '$aid'
			   ";
        $Q=$this->db->query($sql);
             $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
    
    function getAidbYprojectid($project){
        $Q=$this->db->query("select a.*,p.project_name, activity.activity_id
                                from aid as a 
                                inner join activity as activity on activity.activity_id=a.activity_id 
                                inner join project as p on p.project_id=activity.project_id 
                                WHERE p.project_id = '$project'
			   ");
             $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
    
    function getnone($groupid,$projecttype){
        $data = array();
           $sql = "select a.*,at.aid_type_group, p.plan_name, project.project_name,project.project_type_id, activity.activity_name 
                                from aid as a
                                inner join aid_type as at on a.aid_type_id = at.aid_type_id 
                                inner join activity as activity on activity.activity_id=a.activity_id 
                                inner join project as project on project.project_id=activity.project_id 
                                inner join plan as p on p.plan_id=project.plan_id 
                                WHERE at.aid_type_group = '$groupid'
                                and project.project_type_id = '$projecttype'
                                and a.aid_recipient = 1
                    ";
             $Q=$this->db->query($sql);
             foreach($Q->result_array() as $row){
            $data[] = $row;
            }
        $Q->free_result();
        return $data;
    }
    
    

}