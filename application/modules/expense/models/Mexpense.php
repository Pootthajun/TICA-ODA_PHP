<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MExpense extends CI_Model{
    

    function insertexpense(){
        $data = array(
            'expense_name'=>$this->input->post('expense_name'), 
            'expense_by'=>$this->session->userdata('userid'), 
            'expense_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('expense',$data);
        
    }
    
    function listallexpense(){
        $data = array();
        $this->db->order_by('expense_id',"ASC");
        $Q = $this->db->get('expense');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getexpensebyid($expense){
        //$data = array();
        $this->db->where('expense_id',$expense);
        $Q = $this->db->get('expense');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updateexpense(){
        $data = array(
            'expense_name'=>$this->input->post('expense_name'),   
            'expense_by'=>$this->session->userdata('userid'), 
            'expense_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('expense_id',$this->input->post('expenseid'));
        $this->db->update('expense',$data);
    }
    
    function deleteexpense($expense){              
           $this->db->where('expense_id',$expense);
           $this->db->delete('expense');       
    }
    
}