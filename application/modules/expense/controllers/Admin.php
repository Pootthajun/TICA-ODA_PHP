<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));
       // $this->bep_site->set_crumb($this->lang->line('user'),'user/admin'); 
	
    }
    
    function index(){
        $data['header'] = "Expense Table";
        $data['ex'] = $this->MExpense->listallexpense();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }   
            
    function addpay(){
        $data['header'] = "Add Expense";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addexpends';
    	$this->load->view($this->_container_admin,$data);
    }

    function insertexpense(){
        $this->MExpense->insertexpense();
        flashMsg('success',$this->lang->line('insert_success'));
        redirect( 'expense/admin/','refresh');
    }
    
    function editexpense($expenseid){
        $data['header'] = "Edit expense table";
        $data['region'] = $this->MRegion->listallregion();
        $data['edit'] = $this->MExpense->getexpensebyid($expenseid);
        $data['page'] = $this->config->item('wconfig_template_admin').'editexpense';
        $this->load->view($this->_container_admin,$data);
    }
    
    function updateexpense(){
        $this->MExpense->updateexpense();
        flashMsg('success',$this->lang->line('update_success'));
    redirect( 'expense/admin/','refresh');;
    }
    
    function deleteexpense($expenseid){
        $query = $this->db->get_where('project', array('expense_id' => $expenseid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'expense/admin/','refresh');            
        }else{
            $this->MExpense->deleteexpense($expenseid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'expense/admin/','refresh');
        }
    }
}
