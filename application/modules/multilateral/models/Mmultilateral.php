<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MMultilateral extends CI_Model{
    
    function insertmultilateral(){
        $data = array(
            'multilateral_name'=>$this->input->post('multilateral_name'), 
			'multilateral_detail'=>$this->input->post('multilateral_detail'), 
            'multilateral_by'=>$this->session->userdata('userid'), 
            'multilateral_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('multilateral',$data);
        
    }
    
    function listallmultilateral(){
        $data = array();
        $this->db->order_by('multilateral_id',"ASC");
        $Q = $this->db->get('multilateral');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function listmultilateral(){
        $data = array();
        $this->db->where('country_group_id',"3");
        $this->db->order_by('country_id',"ASC");
        $Q = $this->db->get('country');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getmultilateralbyid($multilateral){
        //$data = array();
        $this->db->where('multilateral_id',$multilateral);
        $Q = $this->db->get('multilateral');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    
    function getmultilateralbyCountry($multilateral){
        //$data = array();
        $this->db->where('country_id',$multilateral);
        $Q = $this->db->get('country');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updatemultilateral(){
        $data = array(
            'multilateral_name'=>$this->input->post('multilateral_name'),  
			'multilateral_detail'=>$this->input->post('multilateral_detail'),  
            'multilateral_by'=>$this->session->userdata('userid'), 
            'multilateral_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('multilateral_id',$this->input->post('multilateral'));
        $this->db->update('multilateral',$data);
    }
    
    function deletemultilateral($multilateral){              
           $this->db->where('multilateral_id',$multilateral);
           $this->db->delete('multilateral');       
    }
    

}