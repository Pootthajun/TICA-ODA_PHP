 <p class="addbutton"> 
  <?php  if( !empty( $menu ) ) { ?>
  <a href="<?php echo site_url(); ?>multilateral/admin/addmultilateral" class="button green">Add</a>  
  <?php } ?>
  </p>
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="7%">Id</th>
          <th width="39%">Multilateral  Name</th>
          <th width="39%">Multilateral  Detail</th>
          <?php  if( !empty( $menu ) ) { ?>
          <th>&nbsp;</th>
<?php } ?>
        </tr>
      </thead>
      <tbody>
<?php 
$count = 1;
foreach($all as $list){ ?>
        <tr>
          <td><?php echo $count++; ?></td>
          <td><?php echo $list['multilateral_name']; ?></td>
          <td><?php echo $list['multilateral_detail']; ?></td>
            <?php  if( !empty( $menu ) ) { ?>
          <td width="120" align="right"><a href="<?php echo site_url(); ?>multilateral/admin/editmultilateral/<?php echo $list['multilateral_id']; ?>" class="button blue">Edit</a><a href="<?php echo site_url(); ?>multilateral/admin/deletemultilateral/<?php echo $list['multilateral_id']; ?>"  onclick="return confirm('Are you sure you want to delete Multilateral name <?php echo $list['multilateral_name']; ?> ?')" class="button red"><?php echo $this->lang->line('delete'); ?></a></td>
        <?php } ?>
        </tr>
        <?php } ?>
      </tbody>
    </table>
