<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Multilateral table";
        $data['all'] = $this->MMultilateral->listallmultilateral();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addMultilateral(){
        $data['header'] = "Multilateral table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addmultilateral';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertMultilateral(){
        $this->MMultilateral->insertmultilateral();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'multilateral/admin/','refresh');
    }
    
    function editmultilateral($multilateralid){
        $data['header'] = "Edit Multilateral";
        $data['edit'] = $this->MMultilateral->getmultilateralbyid($multilateralid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editmultilateral';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateMultilateral(){
        $this->MMultilateral->updateMultilateral();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'multilateral/admin/','refresh');
    }
    
    function deleteMultilateral($Multilateralid){
        $query = $this->db->get_where('project', array('multilateral_id' => $Multilateralid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'multilateral/admin/','refresh');            
        }else{
            $this->MMultilateral->deleteMultilateral($Multilateralid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'multilateral/admin/','refresh');
        }
    }
}