<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    
    function index(){
        $data['header'] = "Aid type Table";
        $data['all'] = $this->MAidtype->listallAidtype();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addaidtype(){
        $data['header'] = "Aid type table";
		$data['group'] = $this->MAidtype->getallgroup();
    	$data['page'] = $this->config->item('wconfig_template_admin').'addaidtype';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function insertaidtype(){
        $this->MAidtype->insertaidtype();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'aidtype/admin/','refresh');
    }
    
    function editaidtype($aidtypeid){
        $data['header'] = "Edit Aid type table";
        $data['edit'] = $this->MAidtype->getAidtypebyid($aidtypeid);
		$data['group'] = $this->MAidtype->getallgroup();
    	$data['page'] = $this->config->item('wconfig_template_admin').'editaidtype';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateaidtype(){
        $this->MAidtype->updteaidtype();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'aidtype/admin/','refresh');;
    }
    
    function deleteaidtype($aidtypeid){
        $query = $this->db->get_where('aid', array('aid_type_id' => $aidtypeid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'aidtype/admin/','refresh');            
        }else{
            $this->MAidtype->deleteaidtype($aidtypeid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'aidtype/admin/','refresh');
        }
    }
    
  }