<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MAidtype extends CI_Model{
    
    function insertaidtype(){
        $data = array(
            'aid_type_name'=>$this->input->post('aid_type_name'),
            'aid_type_engname'=>$this->input->post('aid_type_engname'),
            'aid_type_group'=>$this->input->post('aid_type_group'),
            'aid_type_by'=>$this->session->userdata('userid'),
            'aid_type_create'=>date('Y-m-d')            
        );
        $this->db->insert('aid_type',$data);
        
    }
    
    function listallAidtype(){
        $data = array();
      /*  $this->db->order_by('aid_type_id',"ASC");
        $Q = $this->db->get('aid_type');*/
		$Q = $this->db->query("select * from aid_type 
									   left join aid_type_group on 
									   aid_type.aid_type_group =  aid_type_group.tgroup_id
									   order by aid_type.aid_type_id asc
		");
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
	
	function getallgroup(){
        $data = array();
        $this->db->order_by('tgroup_id',"ASC");
        $Q = $this->db->get('aid_type_group');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getAidtypebyid($aidtypeid){
        //$data = array();
        $this->db->where('aid_type_id',$aidtypeid);
        $Q = $this->db->get('aid_type');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;        
    }
    
    function gettypegroupbyid($groupid){
        $data = array();
        $this->db->where('tgroup_id',$groupid);
        $Q = $this->db->get('aid_type_group');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;
    }
            
    function updteaidtype(){
        $data = array(
            'aid_type_name'=>$this->input->post('aid_type_name'),
            'aid_type_engname'=>$this->input->post('aid_type_engname'),
            'aid_type_group'=>$this->input->post('aid_type_group'),
            'aid_type_by'=>$this->session->userdata('userid'),
            'aid_type_update'=>date('Y-m-d')            
        );
        $this->db->where('aid_type_id',$this->input->post('aidtype'));
        $this->db->update('aid_type',$data);
    }
    
    function deleteaidtype($aidtypeid){              
           $this->db->where('aid_type_id',$aidtypeid);
           $this->db->delete('aid_type');       
    }
    
    function getAidByAidtype($aidtypeid){
        $this->db->where('aid_type_id',$aidtypeid);
        $Q = $this->db->get('aid');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;   
    }
    
   
}