<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Country Group Table";
        $data['all'] = $this->MCountrygroup->listallcountrygroup();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
        
    function insertcountrygroup(){
        $this->MCountrygroup->insertcountrygroup();
        flashMsg('success',$this->lang->line('insert_success'));
	    redirect( 'countrygroup/admin/','refresh');
    }
    
    function editcountrygroup($countrygroupid = 0){
        $data['header'] = "Edit Country Group";
        $data['edit'] = $this->MCountrygroup->getcountrygrouptypebyid($countrygroupid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editcountrygroup';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updatecountrygroup(){
		if($this->input->post('countrygroupid') == 0){
			$this->insertcountrygroup();
		}else{
        $this->MCountrygroup->updtecountrygroup();
    	flashMsg('success',$this->lang->line('update_success'));
		redirect( 'countrygroup/admin/','refresh');
		}
    }
    
    function deletecountrygroup($countrygroupid){
        $query = $this->db->get_where('country', array('country_group_id' => $countrygroupid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'countrygroup/admin/','refresh');            
        }else{
            $this->MCountrygroup->deletecountrygroup($countrygroupid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'countrygroup/admin/','refresh');
        }
    }
}