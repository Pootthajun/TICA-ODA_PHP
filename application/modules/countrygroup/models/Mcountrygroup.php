<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MCountrygroup extends CI_Model{
    
    function insertcountrygroup(){
        $data = array(
            'country_group_name'=>$this->input->post('country_group_name'),          
        );
        $this->db->insert('country_group',$data);
        
    }
    
    function listallcountrygroup(){
        $data = array();
        $this->db->order_by('country_group_id',"ASC");
        $Q = $this->db->get('country_group');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getcountrygrouptypebyid($countrygroupid){
        //$data = array();getcountrygrouptypebyid
        $this->db->where('country_group_id',$countrygroupid);
        $Q = $this->db->get('country_group');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updtecountrygroup(){
        $data = array(
            'country_group_name'=>$this->input->post('country_group_name'),         
        );
        $this->db->where('country_group_id',$this->input->post('countrygroupid'));
        $this->db->update('country_group',$data);
    }
    
    function deletecountrygroup($countrygroupid){              
           $this->db->where('country_group_id',$countrygroupid);
           $this->db->delete('country_group');       
    }
    

}