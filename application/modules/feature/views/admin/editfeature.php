<form action="<?php echo base_url(); ?>feature/admin/update" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><strong>Feature title</strong></td>
      <td><label for="feature_title"></label>
      <input name="feature_title" type="text" id="feature_title" value="<?php echo $edit['feature_title']; ?>"  class="text-input large-input" required="required" /></td>
    </tr>
    <tr>
      <td valign="top"><strong>Image</strong></td>
      <td>
        <?php if($edit['feature_thumnail'] != ""){ ?>
  <input type="file" name="feature_thumnail" id="feature_thumnail"  onchange="readURL(this);" ><br />
    <img src="<?php echo base_url(); ?>upload/feature/<?php echo $edit['feature_thumnail']; ?>"  width="200px" id="blah" />
  <?php }else{ ?>
  <input type="file" name="feature_thumnail" id="feature_thumnail"  onchange="readURL(this);"><br />
  <img id="blah" src="#" alt="image" />
  <?php } ?>
   
  <script>
  function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
  </script>
    </td>
    </tr>
    <tr>
      <td><strong>Detail</strong></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><label for="feature_detail"></label>
      <textarea name="feature_detail" id="feature_detail" cols="45" rows="5"><?php echo $edit['feature_detail']; ?></textarea></td>
    </tr>
    <tr>
      <td><input type="submit" name="button" id="button" value="Save"  class="button green"/>
      <input name="featureid" type="hidden" id="featureid" value="<?php echo $edit['feature_id']; ?>" /></td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script>
CKEDITOR.replace( 'feature_detail', {});
</script>
