<?php


class Admin extends Admin_Controller{
	function __construct(){
	parent::__construct(); 
        $this->module=  basename(dirname(dirname(__FILE__)));
	
    }
	function index(){
		$data['header'] = "Feature";
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
		 $data['menu'] = $this->MUser->getMenubyUser();
		$data['n'] = $this->MFeature->getAllfeature();
		$this->load->view($this->_container_admin,$data); 
	}


	function insertfeature(){
	    $this->MFeature->insertfeature();
	    flashMsg('success',$this->lang->line('insertsuccess'));
	    redirect('feature/admin/index','refresh');
	}

	function addfeature($featureid = 0){
	$data['header'] = "Feature";
	$data['edit'] = $this->MFeature->getFeaturebyid($featureid);
        $data['page'] = $this->config->item('wconfig_template_admin').'editfeature';
        $this->load->view($this->_container_admin,$data);
	}

	function update(){
	/*	if(!empty($_FILES['feature_images_th']['name'])){
        	$featureid = $_POST['featureid'];
        	$m =  $this->MFeature->getFeaturebyid($featureid);
	        $clipdel = "./upload/".$m['feature_images_th'];
	        @unlink($clipdel);
        }*/
            if($this->input->post('featureid') == 0){
            $this->insertfeature();    
            }else{
	    $this->MFeature->updatefeature();
	    flashMsg('success',$this->lang->line('insert_success'));
	    redirect('feature/admin/index','refresh');
            }
	}

	function deletefeature($featureid){
		$this->MFeature->deletefeature($featureid);
        flashMsg('success',$this->lang->line('deletesuccess'));
        redirect('feature/admin/index','refresh');
	}

	function deleteimage($imageid,$featureid){
				$this->MFeature->deleteimage($imageid);
		        flashMsg('success',$this->lang->line('deletesuccess'));
		        redirect('feature/admin/editfeature/'.$featureid,'refresh');
	}

	
}