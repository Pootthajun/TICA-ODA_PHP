<?php

class Feature extends Welcome_Controller {

	function Feature()
    {
        parent::__construct();
        $this->module=  basename(dirname(dirname(__FILE__)));

        $this->load->library("pagination");
         $this->load->helper("thumb_helper");
	}

        function index(){
        $data['header'] = "Feature";
        $config = array();
        $config["base_url"] = base_url() . $this->uri->segment('1') ."/feature/index/";
        $config["total_rows"] = count($this->MFeature->getAllfeature());
        $config["per_page"] = 12;
        $config["uri_segment"] = 4; 
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["results"] = $this->MFeature->fetch_Feature($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
    	$data['page'] = $this->config->item('wconfig_template_welcome').'featurelist';
    	$data['module'] = $this->module;
    	$this->load->view($this->_container_contact,$data); 
    	}

        function featuredetail($featureid){
         $data['header'] = "Feature Detail";
        $this->MFeature->updatefeatureview($featureid);
        $data['t'] = $this->MFeature->getFeaturebyid($featureid);
        $data['page'] = $this->config->item('wconfig_template_welcome').'featuredetail';
        $data['module'] = $this->module;
        $this->load->view($this->_container_page,$data); 
        }
		
		
        function listfeature(){
         $data['header'] = "Feature";
        $config = array();
        $config["base_url"] = base_url() . $this->uri->segment('1') ."/feature/listfeature/";
        $config["total_rows"] = count($this->MFeature->getAllfeature());
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
 
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["results"] = $this->MFeature->fetch_Feature($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        $data['page'] = $this->config->item('wconfig_template_welcome').'featurelist';
        $data['module'] = $this->module;
        $this->load->view($this->_container_page,$data); 
        }

}
?>