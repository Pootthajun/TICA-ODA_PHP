<?php

class MFeature extends CI_Model{

	function getAllfeature(){
		$data = array();
		$this->db->order_by('feature_id','DESC');
		$Q = $this->db->get('feature');
		if($Q->num_rows() > 0){
			foreach ($Q->result_array() as $row) {
				# code...
				$data[] = $row;
			}
		}
		$Q->free_result();
		return $data;
	}

    function getAllfeatureIndex(){
        $data = array();
        $this->db->order_by('feature_id','DESC');
        $this->db->limit('6');
        $Q = $this->db->get('feature');
        if($Q->num_rows() > 0){
            foreach ($Q->result_array() as $row) {
                # code...
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

	function insertfeature(){
		$data = $this->_uploadFile();
		$this->db->insert('feature',$data);
	}

	function _uploadFile(){
        $data = array(
            'feature_title' => $_POST['feature_title'],
            'feature_detail' => $_POST['feature_detail'],
            'feature_by'=>$this->session->userdata('userid'),
            'feature_create'=>date('Y-m-d'),
        );
            $filename_th = date('YmdHis');
        	$config['upload_path'] ='./upload/feature/';
		$config['allowed_types'] = 'png|jpg|gif|JPEG';
		$config['remove_spaces']= true;
		$config['overwrite']=false;
                $config['file_name'] = $filename_th;
		$this->load->library('upload', $config);
                $this->upload->initialize($config);
		if (strlen($_FILES['feature_thumnail']['name'])){
                    if(!$this->upload->do_upload('feature_thumnail')){
                        $this->upload->display_errors();
			exit("Cannot upload");
                    }
		$image = $this->upload->data();
		if ($image['file_name']){
                    $data['feature_thumnail'] = $image['file_name'];					
		}
	}          
	return $data;
    }




    function getFeaturebyid($featureid){
    	$this->db->where('feature_id',$featureid);
        $Q = $this->db->get('feature');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }

    function updatefeature(){
    	$data = $this->_uploadFile();
    	$this->db->where('feature_id',$_POST['featureid']);
	$this->db->update('feature',$data);        
    }

    
    function deletefeature($featureid){
    	$t =  $this->getFeaturebyid($featureid);
         $del = "./upload/feature/".$t['feature_thumnail'];
         @unlink($del);
         $this->db->where('feature_id',$featureid);
         $this->db->delete('feature');
    }


    public function fetch_Feature($limit, $start) {
    	$data = array();
      	$this->db->order_by('feature_id','ASC');
        $this->db->limit($limit, $start);
        $query = $this->db->get("feature");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        $query->free_result();
		return $data;
   }

   function updatefeatureview($featureid){
     	$v = $this->getFeaturebyid($featureid);
     	$view = $v['feature_counter']+1;
     	$data = array(
     		'feature_counter'=>$view,
     		);
     	$this->db->where('feature_id',$featureid);
     	$this->db->update('feature',$data);
     }





     

}

?>