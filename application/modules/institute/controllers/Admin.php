<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Institute Table";
        $data['all'] = $this->MInstitute->listallInstitute();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addInstitute(){
        $data['header'] = "Institute table";
        $data['funding'] = $this->MFunding->listallFunding();
    	$data['page'] = $this->config->item('wconfig_template_admin').'addinstitute';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertInstitute(){
        $this->MInstitute->insertinstitute();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'institute/admin/','refresh');
    }
    
    function editinstitute($instituteid){
        $data['header'] = "Edit Institute name";
        $data['edit'] = $this->MInstitute->getinstitutebyid($instituteid);
        $data['funding'] = $this->MFunding->listallFunding();
    	$data['page'] = $this->config->item('wconfig_template_admin').'editinstitute';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateInstitute(){
        $this->MInstitute->updateInstitute();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'institute/admin/','refresh');;
    }
    
    function deleteInstitute($Instituteid){
        $query = $this->db->get_where('institute', array('institute_id' => $Instituteid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'institute/admin/','refresh');            
        }else{
            $this->MInstitute->deleteInstitute($Instituteid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'institute/admin/','refresh');
        }
    }
}