<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MInstitute extends CI_Model{
    
    function insertinstitute(){
        $data = array(
            'institute_name'=>$this->input->post('institute_name'), 
			'funding_id'=>$this->input->post('funding_id'), 
            'institute_by'=>$this->session->userdata('userid'), 
            'institute_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('institute',$data);
        
    }
    
    function listallInstitute(){
        $data = array();
        $this->db->order_by('institute_id',"ASC");
        $Q = $this->db->get('institute');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getinstitutebyid($institute){
        //$data = array();
        $this->db->where('institute_id',$institute);
        $Q = $this->db->get('institute');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updateinstitute(){
        $data = array(
            'institute_name'=>$this->input->post('institute_name'),   
			'funding_id'=>$this->input->post('funding_id'), 
            'institute_by'=>$this->session->userdata('userid'), 
            'institute_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('institute_id',$this->input->post('institute'));
        $this->db->update('institute',$data);
    }
    
    function deleteinstitute($institute){              
           $this->db->where('institute_id',$institute);
           $this->db->delete('institute');       
    }
    

}