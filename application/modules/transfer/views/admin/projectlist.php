<script type="text/javascript">
$(document).ready(function(){
	$('#table_id').DataTable();
	$('tbody tr:even').addClass("alt-row");	
	///  setting menu
    $('.checkall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.projectid').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.projectid').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });

	

    
});
</script>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="63"><input type="checkbox" name="checkbox" id="checkbox" class="checkall"></th>
          <th width="63">No.</th>
          <th width="322">Project Name</th>
          <th width="122">Start</th>
          <th width="126">End</th>
          <th width="126">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
<?php 
$count = 1;
foreach($project as $list){ 
 $this->db->where('project_id',$list['project_id']);
$Q = $this->db->get('project');
$row = $Q->row_array();
$Q->free_result();

?>
        <tr>
          <td><center><input type="checkbox" name="project_id[]" id="project_id" class="projectid" value="<?php echo $list['project_id']; ?>"></center></td>
          <td><?php echo $count++; ?></td>
          <td><?php echo $row['project_name']; ?></td>
          <td><?php echo $row['project_start']; ?></td>
          <td><?php echo $row['project_end']; ?></td>
          <td><?php if($row['project_owner'] ==$list['project_owner']){ echo "Project officer";}else{ echo "Project Assistant";} ?></td>
          </tr>
          <?php } ?>
      </tbody>
    </table>