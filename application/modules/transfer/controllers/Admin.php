<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    
     function index(){
         
        $data['header'] = "Select Project Owner";
        $data['user'] = $this->MUser->listalluser();
        $data['project'] = $this->MProject->listallproject();
    	$data['menu'] = $this->MUser->getMenubyUser();      
	$data['page'] = $this->config->item('wconfig_template_admin').'index';      
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function getprojectbyowner(){
        $id=$this->input->post('id');
        /*$Q=$this->db->query("select p.*,s.user_id from project as p
            inner join project_assistant as s on s.project_id =p.project_id 
            where s.user_id = '$id' or p.project_owner = '$id'
			   ");*/
        /*echo "select p.*,s.user_id from project as p
            inner join project_assistant as s on s.project_id =p.project_id 
            where s.user_id = '$id' or p.project_owner = '$id'
			   ";*/
        $Q=$this->db->query("SELECT project_id,project_owner FROM project WHERE project_owner='$id' UNION SELECT project_id,user_id FROM project_assistant WHERE user_id ='$id' 
                
        ");
	$data['project'] = $Q->result_array();
	$Q->free_result();
	$data['page'] = $this->config->item('wconfig_template_admin').'projectlist';      
	$this->load->view($this->_view_subsector,$data);
    }
    
    function changetranfer(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('project_owner', 'Project Owner', 'required');
        $this->form_validation->set_rules('project_transfer', 'Project Transfer', 'required');
        $this->form_validation->set_rules('project_id[]', 'Project name', 'required');

                if ($this->form_validation->run() == FALSE)
                {
                        $this->index();
                }
                else
                {
        $this->MTransfer->changetranfer();
        flashMsg('success',$this->lang->line('update_success'));
        redirect( 'transfer/admin/','refresh');
                }
    }
    
}
    