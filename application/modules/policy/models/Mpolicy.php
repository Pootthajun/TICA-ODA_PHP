<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MPolicy extends CI_Model{   
    
    function update(){
        $data = array(
            'policy_detail' => nl2br($this->input->post('policy_detail')),
        );
        $this->db->update('policy',$data);
    }
    
    function getPolicy(){
        $data = array();
        $query = $this->db->get('policy'); 
        $data = $query->row_array();
        $query->free_result();
        return $data;
    }
    
}