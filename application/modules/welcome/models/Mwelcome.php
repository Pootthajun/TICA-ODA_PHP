<?php

class MWelcome extends CI_Model {
    function inseruser(){
        $data = array(
            'member_username'=>$_POST['member_username'],
            'member_password'=>md5($_POST['member_password']),
            'member_fullname'=>$_POST['member_fullname'],
            'member_email'=>$_POST['member_email'],
            'member_level'=>$_POST['member_level'],
            'member_tel'=>$_POST['member_tel'],
            'member_fax'=>$this->input->post('member_fax'),
            'member_update'=>date('Y-m-d H:i:s'),
            'member_create'=>date('Y-m-d H:i:s'),
            'member_status'=>$this->input->post('member_status'),
            'member_by'=>$this->session->userdata('userid')
        );
        $this->db->insert('member',$data);
    }
    
    function changestatus($memberid,$status){
        $data = array('member_status'=>1);
        $this->db->where('member_id',$memberid);
        $this->db->update('member',$data);
    }
    
    function getEmailadmin(){
        $this->db->select('');
    }
    
    function getprofile($member){
        $this->db->where('member_id',$member);
        $Q = $this->db->get('member');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
    
    function updateprofile(){
        if(!empty($_POST['member_password'])){
        $data = array(
            'member_username'=>$_POST['member_username'],
            'member_password'=>md5($_POST['member_password']),
            'member_fullname'=>$_POST['member_fullname'],
            'member_email'=>$_POST['member_email'],
            'member_tel'=>$_POST['member_tel'],
            'member_fax'=>$this->input->post('member_fax'),
            'member_status'=>$this->input->post('member_status'),
        );
        $this->db->where('member_id',$this->session->userdata('memberid'));
        $this->db->update('member',$data);
        }else{
        $data = array(
            'member_username'=>$_POST['member_username'],
            'member_fullname'=>$_POST['member_fullname'],
            'member_email'=>$_POST['member_email'],
            'member_tel'=>$_POST['member_tel'],
            'member_fax'=>$this->input->post('member_fax'),
            'member_status'=>$this->input->post('member_status'),
        );
        $this->db->where('member_id',$this->session->userdata('memberid'));
        $this->db->update('member',$data);    
        }
    }
    
}
