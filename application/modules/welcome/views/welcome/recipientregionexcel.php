<?php

ob_start();
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=recipientregionexcel.xls"); 
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); 
ob_end_flush();
?>
<?php
$piriodtype = $this->input->get('piriodtype'); 
$month = $this->input->get('month'); 
$monthtext = $this->input->get('monthtext'); 
$monthyear = $this->input->get('monthyear'); 
$year = $this->input->get('year'); 
$fiscalyear = $this->input->get('fiscalyear'); 
?>


<?php
 
				
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis_date) =  '".$monthyear."'  and month(dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
				} 
				if($piriodtype ==2){
					$and = "  and year(dis_date) =  '".$year."'  ";
					$yearcat = $year;
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
				}  
				
				$texttitle = $header.' '.$yearcat;
 
?>
<h2 class="titletable"><?php echo $texttitle; ?></h2>

<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="box-table-a">
  <thead>
    <tr>
      <th  style='font-size:10.0pt;font-family:"tahoma"' width="428"><strong>Region (OECD)</strong></th>
      <th  style='font-size:10.0pt;font-family:"tahoma"' width="186">Project</th>
      <th width="186" align="right" style='font-size:10.0pt;font-family:"tahoma"'> 	Disbursement (THB)</th>
    </tr>
  </thead>
    <tbody>
    <?php
	$budget ="";
	$Q = $this->db->query('select * from region group by region_name order by region_id asc');
	foreach($Q->result_array() as $list){
											
	?>
    <tr>
         <td  style='font-size:10.0pt;font-family:"tahoma"'><?php echo $list['region_name']; ?></td>
      <td align="center"  style='font-size:10.0pt;font-family:"tahoma"'>
      <?php 
	   $QProject = $this->db->query('select count(dis.country_id) as count ,sum(dis.dis_budget) as sumbudget,country.region_id from disbureseme as dis
											inner join country as country on dis.country_id = country.country_id
											where country.region_id = '.$list['region_id'].'  '.$and.' ');										
											$sumb =  $QProject->row_array();
											echo $sumb['count'];
	  ?>
      </td>
      <td align="right"  style='font-size:10.0pt;font-family:"tahoma"'>
      <?php
	  echo number_format($sumb['sumbudget'],2,'.',',');  
	  ?>
      </td>
    </tr>

    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
      <td  style='font-size:10.0pt;font-family:"tahoma"'>Total</td>
      <td  style='font-size:10.0pt;font-family:"tahoma"'>&nbsp;</td>
      <td align="right"  style='font-size:10.0pt;font-family:"tahoma"'><?php
		$query = $this->db->query("select sum(dis_budget) as total from disbureseme where dis_id <>''  $and ");
		$sum = $query->row_array();
		echo number_format($sum['total'],2,'.',',');  
	  ?></td>
    </tr>
    </tfoot>
    
</table>

			
 