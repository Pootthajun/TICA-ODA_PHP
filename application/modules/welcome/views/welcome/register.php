<?php echo validation_errors(); ?>
<form action="<?php echo base_url(); ?>welcome/insertregister" method="post" name="form1" id="form1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top"><h2>Register</h2></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" style="border:1px solid #e5e5e5;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="tdblue">&nbsp;</td>
      </tr>
      <tr>
        <td></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="450" height="9" style="border-right:1px solid #e5e5e5"></td>
            <td height="9"></td>
          </tr>
          <tr>
            <td valign="top"  style="border-right:1px solid #e5e5e5"><p style="padding:15px;"><span class="subtextdetail">  Please describe the practical use you will make of this training/study on your return home in relation to the responsibilities you expect to assume and the conditions existing in your country in the field of your training.(give the attached paper, if necessary) </span></p></td>
            <td valign="top"><label for="textfield7"></label>
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:15px;">
                <tr>
                  <td width="167"><p align="right"><span class="labeltxt">Name - Surname</span></p></td>
                  <td><span style="padding-left:8px;">
                    <input type="text" name="member_fullname" id="member_fullname" class="txtformbox"  required/>
                  </span></td>
                </tr>
                <tr>
                  <td><p align="right">E-mail</p></td>
                  <td><span style="padding-left:8px;">
                    <input type="text" name="member_email" id="member_email" class="txtformbox required email"/>
                  </span></td>
                </tr>
                <tr>
                  <td><p align="right">Telephone</p></td>
                  <td><span style="padding-left:8px;">
                    <input type="text" name="member_tel" id="member_tel" class="txtformbox"  />
                  </span></td>
                </tr>
                <tr>
                  <td><p align="right"><span class="labeltxt">Username</span></p></td>
                  <td><span style="padding-left:8px;">
                    <input type="text" name="member_username" id="member_username" class="txtformbox" required />
                  </span></td>
                </tr>
                <tr>
                  <td><p align="right"><span class="labeltxt">Password</span></p></td>
                  <td><span style="padding-left:8px;">
                    <input type="password" name="member_password" id="member_password" class="txtformbox" required />
                  </span></td>
                </tr>
                <tr>
                  <td><p align="right"><span class="labeltxt">Confirm Password</span></p></td>
                  <td><span style="padding-left:8px;">
                    <input type="password" name="confirmpassword" id="confirmpassword" class="txtformbox" required/>
                  </span></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><span style="padding-left:8px;"><?php echo $capchar; ?></span></td>
                </tr>
                <tr>
                  <td height="8"></td>
                  <td height="8"></td>
                </tr>
                <tr>
                  <td><p align="right"><span class="labeltxt">Veriy Code</span></p></td>
                  <td><span style="padding-left:8px;">
                    <input type="text" name="captcha" id="captcha" class="txtformbox" required/>
                    <input name="member_level" type="hidden" id="member_level" value="6">
                    <input name="member_status" type="hidden" id="member_status" value="0" />
                  </span></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                    <input type="image" name="imageField" id="imageField" src="<?php echo base_url(); ?>assets/welcome/images/btn-register.png"></td>
                </tr>
              </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

</form>
