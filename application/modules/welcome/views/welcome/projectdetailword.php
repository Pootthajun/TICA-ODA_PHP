<?php
$projectid = $this->input->get('progectid');
$detail = $this->MProject->getProjectByid($projectid);
?>
<?php 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=projectdetailword.doc");
?>
<style>
<!--
 /* Style Definitions */
@page Section1{
            size: 29.7cm 21cm;
            margin: 2cm 2cm 2cm 2cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
        div.Section1 { page:Section1;}
-->
</style>
<div class="Section1">

<h2 class="titletable" style='font-size:16.0pt;font-family:"tahoma"' >Project Code ID&nbsp;<?php echo $projectid; ?></h2>
<h4 style='font-size:12.0pt;font-family:"tahoma"' >Start Date : <?php echo $detail['project_start']; ?>   Enddate :  <?php echo $detail['project_end']; ?></h4>


  <table width="1000" border="1" cellspacing="0" cellpadding="0"  bgcolor="#666666" id="box-table-b">

    <tr>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="2%" rowspan="3" bgcolor="#FBFBFB" nowrap >No</th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="27%" rowspan="2" nowrap bgcolor="#FBFBFB"><strong>รายละเอียด</strong></th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="14%" rowspan="2" nowrap bgcolor="#FBFBFB"><strong>องค์กรพหุภาคี</strong></th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  colspan="5" nowrap bgcolor="#FBFBFB"><strong>จำนวนเงิน ( บาท )</strong></th>
    </tr>
    <tr>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="13%" bgcolor="#FBFBFB" nowrap><strong>งบประมาณ</strong></th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="13%" bgcolor="#FBFBFB" nowrap><strong>การเบิกจ่ายจริง</strong></th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="8%" bgcolor="#FBFBFB" nowrap> 	ตามพันธกรณี</th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="11%" bgcolor="#FBFBFB" nowrap> 	ตามความสมัครใจ</th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="12%" bgcolor="#FBFBFB" nowrap><strong>สาขา</strong></th>
    </tr>
    <tr>
      <th style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FBFBFB" nowrap><strong>Description</strong></th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FBFBFB" nowrap><strong>(Multilateral <br>
Oraganizations)</strong></th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FBFBFB" nowrap><strong>(Commitment/<br>
Budget) </strong></th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FBFBFB" nowrap> 	
(Disbursement)</th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FBFBFB" nowrap>(Mandary)</th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FBFBFB" nowrap> 	
(Voluntary)</th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FBFBFB" nowrap><strong>(Sector)</strong></th>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'  align="center" bgcolor="#FFFFFF" nowrap>1</td>
      <td style='font-size:10.0pt;font-family:"tahoma"' nowrap bgcolor="#FFFFFF"><?php echo $detail['project_name']; ?></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  align="center" nowrap bgcolor="#FFFFFF">
	  <?php
	$sql ="select executing_name
						from 
								executing 
						where 
								executing_id = ".$detail['executing_id']."
								
 ";
	  	$Q = $this->db->query($sql);
	    $c = $Q->row_array();
		echo $c['executing_name'];
	  ?>
	  
	  </td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  align="right" nowrap bgcolor="#FFFFFF"><?php echo number_format($detail['commitment'],2,'.',',');  ?></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  align="right" nowrap bgcolor="#FFFFFF">          <?php
	  $sql2 ="select 
				sum(dis.dis_budget) as sumbudget,dis.project_id,project.project_name
						from 
								disbureseme as dis 
								left join project on project.project_id = dis.project_id
						where 
								project.project_id = ".$detail['project_id']."
 ";
 		$QLoan= $this->db->query($sql2);
		$sumloan = $QLoan->row_array();
		//$totalloan += $sumloan['sumbudget'];
	  echo number_format($sumloan['sumbudget'],2,'.',',');  
	  ?></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  align="center" nowrap bgcolor="#FFFFFF"><?php if($detail['pay_type'] == 0){?>
      <img src="<?php echo base_url(); ?>assets/admin/images/check.png" width="16" height="15">
      <?php } ?>
      </td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  align="center" nowrap bgcolor="#FFFFFF"><?php if($detail['pay_type'] == 1){?>
      <img src="<?php echo base_url(); ?>assets/admin/images/check.png" width="16" height="15">
      <?php } ?></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  align="center" nowrap bgcolor="#FFFFFF">
	  	  <?php
	$sql ="select sector_name
						from 
								sector 
						where 
								sector_id = ".$detail['sector_id']."
								
 ";
	  	$Q = $this->db->query($sql);
	    $c = $Q->row_array();
		echo $c['sector_name'];
	  ?>
	  
	 </td>
    </tr>
  </table>
  
</div>
