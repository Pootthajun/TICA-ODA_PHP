<?php
$piriodtype = $this->input->get('piriodtype'); 
$month = $this->input->get('month'); 
$monthtext = $this->input->get('monthtext'); 
$monthyear = $this->input->get('monthyear'); 
$year = $this->input->get('year'); 
$fiscalyear = $this->input->get('fiscalyear'); 
$totalgrant = "";
$totalcon = "";
$totalloan="";
?>

<?php
 
				
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis.dis_date) =  '".$monthyear."'  and month(dis.dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
					$link = "&month=".$month."&monthyear=".$monthyear."";
				} 
				if($piriodtype ==2){
					$and = "  and year(dis.dis_date) =  '".$year."'  ";
					$yearcat = $year;
					$link = "&year=".$year."";
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis.dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
					$link = "&startyear=01/10/".$startyear."&fiscalyear=30/09/".$fiscalyear."";
				}  
				
				$texttitle = $header .' '.$yearcat ;
 
?>
<h2 class="titletable"><?php echo $texttitle; ?></h2>

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#505050">
  <thead>
    <tr>
      <th width="172" bgcolor="#F7F7F7"><strong>Project Code</strong></th>
      <th width="519" align="center" bgcolor="#F7F7F7">Project Title</th>
      <th width="209" align="center" bgcolor="#F7F7F7">Recipient country</th>
      <th width="211" align="right" bgcolor="#F7F7F7">Disbursement (THB)</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$budget ="";

 $sql ="select 
				dis.project_id,project.project_name,project.project_type_id
						from 
								disbureseme as dis 
								left join project on project.project_id = dis.project_id
						where 
								project.project_type_id = '4'
								and
								project.executing_id = ".$this->input->get('exid')."	".$and."
								group by project.project_id
 ";
	$Q = $this->db->query($sql);
	foreach($Q->result_array() as $list){
	?>
    <tr>
      <td bgcolor="#FFFFFF"><?php echo $list['project_id']; ?></td>
      <td height="40" align="left" bgcolor="#FFFFFF"><?php echo $list['project_name']; ?></td>
      <td align="center" bgcolor="#FFFFFF">
      <?php
	$sql ="select  count(dis.country_id) as countcountry,dis.project_id,project.project_name
						from 
								disbureseme as dis 
								left join project on project.project_id = dis.project_id
						where 
								project.project_id = ".$list['project_id']."	".$and."
								
 ";
	  	$Q = $this->db->query($sql);
	    $c = $Q->row_array();
		echo $c['countcountry'];
	  ?>
      </td>
      <td align="right" bgcolor="#FFFFFF">
          <?php
	  $sql2 ="select 
				sum(dis.dis_budget) as sumbudget,dis.project_id,project.project_name
						from 
								disbureseme as dis 
								left join project on project.project_id = dis.project_id
						where 
								project.project_id = ".$list['project_id']."	".$and."
 ";
 		$QLoan= $this->db->query($sql2);
		$sumloan = $QLoan->row_array();
		$totalloan += $sumloan['sumbudget'];
	  echo number_format($sumloan['sumbudget'],2,'.',',');  
	  ?>
      </td>
      </tr>

    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
      <td bgcolor="#F7F7F7">Total</td>
      <td align="center" bgcolor="#F7F7F7">&nbsp;</td>
      <td align="center" bgcolor="#F7F7F7">&nbsp;</td>
      <td align="right" bgcolor="#F7F7F7"><?php
		echo @number_format($totalloan,2,'.',',');  
		  ?></td>
      </tr>
    </tfoot>
    
  </table>


