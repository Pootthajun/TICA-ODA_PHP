<?php
$piriodtype = $this->input->get('piriodtype'); 
$month = $this->input->get('month'); 
$monthtext = $this->input->get('monthtext'); 
$monthyear = $this->input->get('monthyear'); 
$year = $this->input->get('year'); 
$fiscalyear = $this->input->get('fiscalyear'); 
$totalgrant = "";
$totalcon = "";
$totalloan="";
				
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis.dis_date) =  '".$monthyear."'  and month(dis.dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
					$link = "&month=".$month."&monthyear=".$monthyear."";
				} 
				if($piriodtype ==2){
					$and = "  and year(dis.dis_date) =  '".$year."'  ";
					$yearcat = $year;
					$link = "&year=".$year."";
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis.dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
					$link = "&startyear=01/10/".$startyear."&fiscalyear=30/09/".$fiscalyear."";
				}  
?>





<?php
 

				
				$texttitle = $header .' '.$yearcat ;
 
?>
<h2 class="titletable"><?php echo $texttitle; ?></h2>
<div id="getdata">
<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#4F4F4F">
  <thead>
    <tr>
      <th width="428" bgcolor="#F8F8F8"><strong>Executing Agency</strong></th>
      <th width="186" align="right" bgcolor="#F8F8F8"> Grant/Technical Cooperation</th>
      <th width="186" align="right" bgcolor="#F8F8F8">Contributions to International Org.</th>
      <th width="186" align="right" bgcolor="#F8F8F8">Loans</th>
      <th width="186" align="right" bgcolor="#F8F8F8">Total (THB)</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$budget ="";

 $sql ="select 
				dis.project_id,executing.executing_id,executing.executing_name 
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing on executing.executing_id = project.executing_id
		where 
		        project.project_type_id = '4'
			    and
				project.funding_id =".$this->input->get('fundingid')."	".$and."
				group by executing.executing_id
 ";
	$Q = $this->db->query($sql);
	foreach($Q->result_array() as $list){
	?>
    <tr>
      <td height="40" bgcolor="#FFFFFF"><?php echo $list['executing_name']; ?></td>
      <td align="right" bgcolor="#FFFFFF">
	  <?php
		 $sql1 ="select 
				sum(dis.dis_budget) as sumbudget,ex.executing_id
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing  as ex on ex.executing_id = project.executing_id
		where 
				project.executing_id = ".$list['executing_id']."  and dis.title = '1' 	".$and."
 ";
 		$QB= $this->db->query($sql1);
		$sumb = $QB->row_array();
		$totalgrant += $sumb['sumbudget'];
	  echo number_format($sumb['sumbudget'],2,'.',',');  
	  ?></td>
      <td align="right" bgcolor="#FFFFFF"><?php
		 $sql3 ="select 
				sum(dis.dis_budget) as sumbudget,ex.executing_id
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing  as ex on ex.executing_id = project.executing_id
		where 
				project.executing_id = ".$list['executing_id']."  and dis.title >= 3  	".$and."
 ";
 		$Qcon= $this->db->query($sql3);
		$sumcon = $Qcon->row_array();
		$totalcon += $sumcon['sumbudget'];
	  echo number_format($sumcon['sumbudget'],2,'.',',');  
	  ?></td>
      <td align="right" bgcolor="#FFFFFF">
          <?php
		  $sql2 ="select 
				sum(dis.dis_budget) as sumbudget,ex.executing_id
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing  as ex on ex.executing_id = project.executing_id
		where 
				project.executing_id = ".$list['executing_id']."  and dis.title = '2' 	".$and."
 ";
 		$QLoan= $this->db->query($sql2);
		$sumloan = $QLoan->row_array();
		$totalloan += $sumloan['sumbudget'];
	  echo number_format($sumloan['sumbudget'],2,'.',',');  
	  ?>
      </td>
      <td align="right" bgcolor="#FFFFFF"><?php echo number_format($sumb['sumbudget']+$sumcon['sumbudget']+$sumloan['sumbudget'],2,'.',',');   ?></td>
    </tr>

    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
      <td bgcolor="#F8F8F8">Total</td>
      <td align="right" bgcolor="#F8F8F8"><?php
		echo @number_format($totalgrant,2,'.',',');  
		  ?></td>
      <td align="right" bgcolor="#F8F8F8"><?php
		echo @number_format($totalcon,2,'.',',');  
		  ?></td>
      <td align="right" bgcolor="#F8F8F8"><?php
		echo @number_format($totalloan,2,'.',',');  
		  ?></td>
      <td align="right" bgcolor="#F8F8F8"><?php
		echo @number_format($totalloan+$totalcon+$totalgrant,2,'.',',');  
		  ?></td>
      </tr>
    </tfoot>
    
  </table>
  </div>

