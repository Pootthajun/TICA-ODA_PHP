<?php
$projectid = $this->input->get('progectid');
$detail = $this->MProject->getProjectByid($projectid);
?>



<div style="position: relative; ">

  <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
      <td width="500" align="right"><table width="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><a href="<?php echo base_url(); ?>welcome/projectdetailpdf?progectid=<?php echo $this->input->get('progectid'); ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/welcome/images/pdf.png" width="24" height="24" /></a></td>
          <td><a href="<?php echo base_url(); ?>welcome/projectdetailword?progectid=<?php echo $this->input->get('progectid'); ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/welcome/images/word.png" width="24" height="24" /></a></td>
          <td><a href="<?php echo base_url(); ?>welcome/projectdetailexcel?progectid=<?php echo $this->input->get('progectid'); ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/welcome/images/xls.png" width="24" height="24" /></a></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td width="500">&nbsp;</td>
    </tr>
    </table>

</div>
<link href="<?php echo base_url(); ?>assets/welcome/css/table.css" rel="stylesheet" type="text/css" />
<h2 class="titletable">Project Code ID&nbsp;<?php echo $projectid; ?></h2><br>
<h4>Start Date : <?php echo $detail['project_start']; ?>   Enddate :  <?php echo $detail['project_end']; ?></h4>

<div id="getdata">
  <table width="1064" border="0" cellspacing="1" cellpadding="1"  bgcolor="#666666" id="box-table-b">
    <tr>
      <th width="2%" rowspan="3" bgcolor="#FBFBFB" nowrap >No</th>
      <th width="27%" rowspan="2" nowrap bgcolor="#FBFBFB"><strong>รายละเอียด</strong></th>
      <th width="14%" rowspan="2" nowrap bgcolor="#FBFBFB"><strong>องค์กรพหุภาคี</strong></th>
      <th colspan="5" nowrap bgcolor="#FBFBFB"><strong>จำนวนเงิน ( บาท )</strong></th>
    </tr>
    <tr>
      <th width="13%" bgcolor="#FBFBFB" nowrap><strong>งบประมาณ</strong></th>
      <th width="13%" bgcolor="#FBFBFB" nowrap><strong>การเบิกจ่ายจริง</strong></th>
      <th width="8%" bgcolor="#FBFBFB" nowrap> 	ตามพันธกรณี</th>
      <th width="11%" bgcolor="#FBFBFB" nowrap> 	ตามความสมัครใจ</th>
      <th width="12%" bgcolor="#FBFBFB" nowrap><strong>สาขา</strong></th>
    </tr>
    <tr>
      <th bgcolor="#FBFBFB" nowrap><strong>Description</strong></th>
      <th bgcolor="#FBFBFB" nowrap><strong>(Multilateral <br>
Oraganizations)</strong></th>
      <th bgcolor="#FBFBFB" nowrap><strong>(Commitment/<br>
Budget) </strong></th>
      <th bgcolor="#FBFBFB" nowrap> 	
(Disbursement)</th>
      <th bgcolor="#FBFBFB" nowrap>(Mandary)</th>
      <th bgcolor="#FBFBFB" nowrap> 	
(Voluntary)</th>
      <th bgcolor="#FBFBFB" nowrap><strong>(Sector)</strong></th>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFFF" nowrap>1</td>
      <td height="40" nowrap bgcolor="#FFFFFF"><?php echo $detail['project_name']; ?></td>
      <td align="center" nowrap bgcolor="#FFFFFF">
	  <?php
	$sql ="select executing_name
						from 
								executing 
						where 
								executing_id = ".$detail['executing_id']."
								
 ";
	  	$Q = $this->db->query($sql);
	    $c = $Q->row_array();
		echo $c['executing_name'];
	  ?>
	  
	  </td>
      <td align="right" nowrap bgcolor="#FFFFFF"><?php echo number_format($detail['commitment'],2,'.',',');  ?></td>
      <td align="right" nowrap bgcolor="#FFFFFF">          <?php
	  $sql2 ="select 
				sum(dis.dis_budget) as sumbudget,dis.project_id,project.project_name
						from 
								disbureseme as dis 
								left join project on project.project_id = dis.project_id
						where 
								project.project_id = ".$detail['project_id']."
 ";
 		$QLoan= $this->db->query($sql2);
		$sumloan = $QLoan->row_array();
		//$totalloan += $sumloan['sumbudget'];
	  echo number_format($sumloan['sumbudget'],2,'.',',');  
	  ?></td>
      <td align="center" nowrap bgcolor="#FFFFFF"><?php if($detail['pay_type'] == 0){?>
      <img src="<?php echo base_url(); ?>assets/admin/images/check.png" width="16" height="15">
      <?php } ?>
      </td>
      <td align="center" nowrap bgcolor="#FFFFFF"><?php if($detail['pay_type'] == 1){?>
      <img src="<?php echo base_url(); ?>assets/admin/images/check.png" width="16" height="15">
      <?php } ?></td>
      <td align="center" nowrap bgcolor="#FFFFFF">
	  	  <?php
	$sql ="select sector_name
						from 
								sector 
						where 
								sector_id = ".$detail['sector_id']."
								
 ";
	  	$Q = $this->db->query($sql);
	    $c = $Q->row_array();
		echo $c['sector_name'];
	  ?>
	  
	 </td>
    </tr>
  </table>
</div>