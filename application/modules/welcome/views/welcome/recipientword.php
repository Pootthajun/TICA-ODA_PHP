<?php
$piriodtype = $this->input->get('piriodtype'); 
$month = $this->input->get('month'); 
$monthtext = $this->input->get('monthtext'); 
$monthyear = $this->input->get('monthyear'); 
$year = $this->input->get('year'); 
$fiscalyear = $this->input->get('fiscalyear'); 
?>
<?php
 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=recipientcountryword.doc");
?>
<style>
<!--
 /* Style Definitions */
@page Section1{
            size: 29.7cm 21cm;
            margin: 2cm 2cm 2cm 2cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
        div.Section1 { page:Section1;}
-->
</style>
<div class="Section1">


<?php
 
				$texttitle = 'Recipient Country ';  $and = "";
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis_date) =  '".$monthyear."'  and month(dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
				} 
				if($piriodtype ==2){
					$and = "  and year(dis_date) =  '".$year."'  ";
					$yearcat = $year;
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
				}  
 
?>
<h2 class="titletable"><?php echo $texttitle; ?></h2>

<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" >
  <thead>
    <tr>
      <th style='font-size:10.0pt;font-family:"tahoma"' width="428" bgcolor="#FCFCFC"><strong>Country</strong></th>
      <th style='font-size:10.0pt;font-family:"tahoma"' width="186" bgcolor="#FCFCFC">Project</th>
      <th style='font-size:10.0pt;font-family:"tahoma"' width="186" bgcolor="#FCFCFC"> 	Disbursement (THB)</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$budget ="";
	$Q = $this->db->query("select dis.country_id,country.country_name from disbureseme as dis
											left join country as country on dis.country_id = country.country_id
											where dis.dis_id <>'' ".$and."
											group by dis.country_id ");
	foreach($Q->result_array() as $list){
	?>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"' bgcolor="#FFFFFF"><?php echo $list['country_name']; ?></td>
      <td style='font-size:10.0pt;font-family:"tahoma"' bgcolor="#FFFFFF">
      <?php 
	  $sql = "SELECT * FROM disbureseme where country_id = ".$list['country_id']."  ".$and." ";
	  $QProject = $this->db->query($sql);
	   echo $num = $QProject->num_rows(); 
	  ?>
      </td>
      <td style='font-size:10.0pt;font-family:"tahoma"' align="right" bgcolor="#FFFFFF">
      <?php
	  $sqlsum = "SELECT SUM(dis_budget)  AS sumbudget FROM disbureseme where country_id = ".$list['country_id']."  ".$and." ";
	  $QSum= $this->db->query($sqlsum);
	  $rowsum = $QSum->row_array();
	  echo number_format($rowsum['sumbudget'],2,'.',',');  
	  ?>
      </td>
    </tr>

    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"' bgcolor="#FDFDFD">Total</td>
      <td style='font-size:10.0pt;font-family:"tahoma"' bgcolor="#FDFDFD">&nbsp;</td>
      <td style='font-size:10.0pt;font-family:"tahoma"' align="right" bgcolor="#FDFDFD"><?php
		$query = $this->db->query("select sum(dis_budget) as total from disbureseme where dis_id <>''  $and ");
		$sum = $query->row_array();
		echo number_format($sum['total'],2,'.',',');  
	  ?></td>
    </tr>
    </tfoot>
    
  </table>
</div>
			
 