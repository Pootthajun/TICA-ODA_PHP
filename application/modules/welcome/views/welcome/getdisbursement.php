<?php
$piriodtype = $this->input->post('piriodtype'); 
$type = $this->input->post('type'); 
if(empty($type)){ $type = 3; } 
$month = $this->input->post('month'); 
$monthtext = $this->input->post('monthtext'); 
$monthyear = $this->input->post('monthyear'); 
$year = $this->input->post('year'); 
$fiscalyear = $this->input->post('fiscalyear'); 
?>
<script src="<?php echo base_url(); ?>assets/highcharts/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/exporting.js"></script>

<?php

if($type==1){    
				$texttitle = 'Disbursement';  $and = "";
				
					
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis_date) =  '".$monthyear."'  and month(dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
				} 
				if($piriodtype ==2){
					$and = "  and year(dis_date) =  '".$year."'  ";
					$yearcat = $year;
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
				}  
				 //grant
				$QL = $this->db->query("select sum(dis_budget) as total from disbureseme where title = 1  $and ");
				$grantdata = $QL->row_array();
				if(empty($grantdata['total'])){ $grantdata['total'] = 0;}
				$grant = $grantdata['total']; 
				
				//loan
				$QL = $this->db->query("select sum(dis_budget) as total  from disbureseme where title = 2  $and ");
				$loandata = $QL->row_array();
				if(empty($loandata['total'])){ $loandata['total'] = 0;}
				$loan = $loandata['total']; 
				//contribution
				$QL = $this->db->query("select sum(dis_budget) as total  from disbureseme where title > 2   $and ");
				$contributiondata = $QL->row_array();
				if(empty($contributiondata['total'])){ $contributiondata['total'] = 0;}
				$contribution =  $contributiondata['total'];

				// Show Count Project
				// grant
				$QL = $this->db->query("select count(project_id) as total from disbureseme where title = 1  $and group by project_id ");
				$grantdatapp = $QL->row_array();
				if(empty($grantdatapp['total'])){ $grantdatapp['total'] = 0;}
				$granpp = $grantdatapp['total'];
				//loan
				$QL = $this->db->query("select count(project_id) as total  from disbureseme where title = 2  $and group by project_id ");
				$loandatapp = $QL->row_array();
				if(empty($loandatapp['total'])){ $loandatapp['total'] = 0;}
				$loanpp = $loandatapp['total']; 
				//contribution
				$QL = $this->db->query("select count(project_id) as total  from disbureseme where title > 2   $and group by project_id ");
				$contributiondatapp = $QL->row_array();
				if(empty($contributiondatapp['total'])){ $contributiondatapp['total'] = 0;}
				$contributionpp =  $contributiondatapp['total'];
				//total
				

				
				
 
?>

				<script type="text/javascript">
                $(function () {
				$('#gp-project').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Type of AID (<?php echo $texttitle;?>)'
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: [ '<?php echo $yearcat;?>' ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Rainfall (mm)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.,.2f} mm</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [  
                         {
                            name: 'Grant',
                            data: [<?php echo $grant;?>] 
                        }, {
                            name: 'Loan',
                            data: [<?php echo $loan;?>] 
                        }, {
                            name: 'Contributions',
                            data: [<?php echo $contribution;?>] 
                        } 
                        ]
                    });
                }); 
		</script>
		
<?php }  


if($type==2){   
		$texttitle = 'Projects';  $and = '';
		//total year
				if($piriodtype ==1){
					$and = "  and year(dis_date) =  '".$monthyear."'  and month(dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
				} 
				if($piriodtype ==2){
					$and = "  and year(dis_date) =  '".$year."'  ";
					$yearcat = $year;
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
				}  
				 //grant
				$QL = $this->db->query("select count(project_id) as total from disbureseme where title = 1  $and group by project_id ");
				$grantdata = $QL->row_array();
				$grant = $grantdata['total'];
				
				
				//loan
				$QL = $this->db->query("select count(project_id) as total  from disbureseme where title = 2  $and group by project_id ");
				$loandata = $QL->row_array();
				$loan = $loandata['total']; 
				//contribution
				$QL = $this->db->query("select count(project_id) as total  from disbureseme where title > 2   $and group by project_id ");
				$contributiondata = $QL->row_array();
				$contribution =  $contributiondata['total'];  
				
				//*************** show Count project *******************
				  $QL = $this->db->query("select count(project_id) as total from disbureseme where title = 1  $and group by project_id ");
				$grantdatapp = $QL->row_array();
				if(empty($grantdatapp['total'])){ $grantdatapp['total'] = 0;}
				$granpp = $grantdatapp['total'];
				//loan
				$QL = $this->db->query("select count(project_id) as total  from disbureseme where title = 2  $and group by project_id ");
				$loandatapp = $QL->row_array();
				if(empty($loandatapp['total'])){ $loandatapp['total'] = 0;}
				$loanpp = $loandatapp['total']; 
				//contribution
				$QL = $this->db->query("select count(project_id) as total  from disbureseme where title > 2   $and group by project_id ");
				$contributiondatapp = $QL->row_array();
				if(empty($contributiondatapp['total'])){ $contributiondatapp['total'] = 0;}
				$contributionpp =  $contributiondatapp['total'];
				//total
				
				//*************** Show disburesement disburesementClick *******************
				 //grant
				$QL = $this->db->query("select sum(dis_budget) as total from disbureseme where title = 1  $and ");
				$grantdatap = $QL->row_array();
				if(empty($grantdatap['total'])){ $grantdatap['total'] = 0;}
				$grantp = $grantdatap['total']; 
				
				//loan
				$QL = $this->db->query("select sum(dis_budget) as total  from disbureseme where title = 2  $and ");
				$loandatap = $QL->row_array();
				if(empty($loandatap['total'])){ $loandatap['total'] = 0;}
				$loanp = $loandatap['total']; 
				
				//contribution
				$QL = $this->db->query("select sum(dis_budget) as total  from disbureseme where title > 2   $and ");
				$contributiondatap = $QL->row_array();
				if(empty($contributiondatap['total'])){ $contributiondatap['total'] = 0;}
				$contributionp =  $contributiondatap['total'];  
				
				//*************** Show disburesement projectClick *******************
				 //grant
				$QL = $this->db->query("select sum(dis_budget) as total from disbureseme where title = 1  $and ");
				$grantdatapPClick = $QL->row_array();
				if(empty($grantdatapPClick['total'])){ $grantdatapPClick['total'] = 0;}
				$grantpPClick = $grantdatapPClick['total']; 
				
				//loan
				$QL = $this->db->query("select sum(dis_budget) as total  from disbureseme where title = 2  $and ");
				$loandatapPClick = $QL->row_array();
				if(empty($loandatapPClick['total'])){ $loandatapPClick['total'] = 0;}
				$loanpPClick = $loandatapPClick['total']; 
				
				//contribution
				$QL = $this->db->query("select sum(dis_budget) as total  from disbureseme where title > 2   $and ");
				$contributiondatapPClick = $QL->row_array();
				if(empty($contributiondatapPClick['total'])){ $contributiondatapPClick['total'] = 0;}
				$contributionpPClick =  $contributiondatapPClick['total'];
				
?>
				<script type="text/javascript">
                $(function () {
                    $('#gp-project').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Type of AID (<?php echo $texttitle;?>)'
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: [ '<?php echo $yearcat;?>' ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Rainfall (Project)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.,.1f} Project</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [  
                         {
                            name: 'Grant',
                            data: [<?php echo $grant;?>] 
                        }, {
                            name: 'Loan',
                            data: [<?php echo $loan;?>] 
                        }, {
                            name: 'Contributions',
                            data: [<?php echo $contribution;?>] 
                        } 
                        ]
                    });
                }); 
		</script>
<?php }  


if($type==3){
		
		$texttitle = 'Comparative 5 Years'; 
		//total year
		$yearnow = date('Y',time());
		
		//loop year
		$years = '';$datagrant = "";$dataloan = "";$contribution = "";
		for($i=($yearnow-4); $i<=$yearnow; $i++){
 
			 $years .=  $i.",";   
			 //grant
				$QL = $this->db->query("select sum(dis_budget) as total from disbureseme where title = 1  and year(dis_date) =  '".$i."' ");
				$grantdata = $QL->row_array();
				if(empty($grantdata['total'])){ $grantdata['total'] = 0;}
				$datagrant .= $grantdata['total'].","; 
				//loan
				$QL = $this->db->query("select sum(dis_budget) as total  from disbureseme where title = 2  and year(dis_date) =  '".$i."' ");
				$loandata = $QL->row_array();
				if(empty($loandata['total'])){ $loandata['total'] = 0;}
				$dataloan .= $loandata['total'].","; 
				//contribution
				$QL = $this->db->query("select sum(dis_budget) as total  from disbureseme where title > 2  and year(dis_date) =  '".$i."' ");
				$contributiondata = $QL->row_array();
				if(empty($contributiondata['total'])){ $contributiondata['total'] = 0;}
				$contribution .=  $contributiondata['total'].",";  
			} 
		$grant = substr($datagrant,0, -1);
		$loan = substr($dataloan,0, -1);
		$contribution = substr($contribution,0, -1);
		$years = substr($years,0, -1);
?>
<style type="text/css">
.dontshow {
    display:none;
}
</style>
				<script type="text/javascript">
                $(function () {
					    Highcharts.setOptions({
						lang: {
							thousandsSep: ','
						}
					});
                    $('#gp-project').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Type of AID (<?php echo $texttitle;?>)'
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: [ <?php  echo $years ; ?> ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Rainfall (mm)'
                            }
                        },
/*                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.2f} mm</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },*/
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [  
                         {
                            name: 'Grant',
                            data: [<?php echo $grant;?>] 
                        }, {
                            name: 'Loan',
                            data: [<?php echo $loan;?>] 
                        }, {
                            name: 'Contributions',
                            data: [<?php echo $contribution;?>] 
                        } 
                        ]
                    });
                }); 
		</script>
<?php } 
?>
<script>

</script>
<div>
 <div class="row">
  <h3 style="" class="dontshow"><b>Type of Aid&nbsp;(<?php if($piriodtype ==1){
  if($month==1){echo "January";
  }
  if($month==2){echo "February";
  }
  if($month==3){echo "March";
  }
  if($month==4){echo "April";
  }
  if($month==5){echo "May";
  }
  if($month==6){echo "June";
  }
  if($month==7){echo "July";
  }
  if($month==8){echo "August";
  }
  if($month==9){echo "September";
  }
  if($month==10){echo "October";
  }
  if($month==11){echo "November";
  }
  if($month==12){echo "December";
  }
  echo " - ";
  echo $monthyear;}
  if($piriodtype ==2){echo $year;}
  if($piriodtype==3){echo "Fiscal Year : "; echo $fiscalyear;} ?>)</b></h3>
  </div>
  <?php if($type==1){$sumpro = $grantdatapp['total'] +  $loandatapp['total'] +$contributiondatapp['total'];
  $sumdisburse = $grantdata['total'] + $loandata['total'] + $contributiondata['total'];}
  if($type==2){$sumpro = $grantdatapp['total'] +  $loandatapp['total'] +$contributiondatapp['total'];$sumdbp = $grantdatapPClick['total'] + $loandatapPClick['total'] + $contributiondatapPClick['total'];}
  if($type == 3){}
  ?>

<br/>

<table width="100%" id="tbDataPro" class="dontshow" style="background:#ABC7EC">
		<thead
			<tr >
				<td align="left"><h4>Descriptions</h4><td>
				<td align="center"><h4>Project</h4><td>
				<td align="right"><h4>Disbursement (THB)</h4><td>
			</tr>
		</thead>
		<tbody>
		<tr style="background:#F8F8F8">
				<td align="left">Grant<td>
				<td align="center"> <?php echo  $grantdatapp['total'] ?> <td>
				<td align="right"> <?php if($type==1){echo number_format($grantdata['total'],2);} if($type==2){echo number_format($grantdatap['total'],2);}if($type==3){}?> <td>
		</tr>
		<tr style="background:##ABC7EC">
				<td align="left">Loan<td>
				<td align="center"> <?php echo $loandatapp['total']?> <td>
				<td align="right"><?php echo number_format($loandata['total'],2) ?><td>
		</tr>
		<tr style="background:#F8F8F8">
				<td align="left">contribution<td>
				<td align="center"> <?php echo $contributiondatapp['total']?> <td>
				<td align="right"><?php echo number_format($contributiondata['total'],2) ?><td>
		</tr>
		</tbody>
		<tfoot>
		<tr >
				<td align="left"><b>Total</b><td>
				<td align="center"> <?php echo $sumpro?> <td>
				<td align="right"> <?php if($type==1){echo number_format($sumdisburse,2);}if($type==2){echo number_format($sumdbp,2);}?> <td>
	    </tr>
		</tfoot>
  </table>
 </div>
<br/>
<div id="gp-project" style="min-width: 310px; height: 450px; margin: 0 auto; position: relative; padding-bottom:20px; ">
</div>

 