<p>Dear Member,<br>
  You recently added xxxx@gmail.com as a new rescue email address for your member. To verify this email address belongs to you, click the link below and then sign in using your username and password.</p>
<p>Verify now &gt; <?php echo $site_url; ?>welcome/confirmregister/<?php echo $memberid; ?>/1/</p>
<p>Why you received this email.<br>
  Apple requests verification whenever an email address is added to an username. Your email address cannot be used without verification.</p>
<p>ODA Support</p>
