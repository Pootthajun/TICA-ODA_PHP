<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top"><h2>Contact us</h2></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3873.2450248421937!2d100.56401647483933!3d13.884292074314374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2832567e56109%3A0x580edb98def14f92!2z4Lio4Li54LiZ4Lii4LmM4Lij4Liy4LiK4LiB4Liy4Lij4LmA4LiJ4Lil4Li04Lih4Lie4Lij4Liw4LmA4LiB4Li14Lii4Lij4LiV4Li0IOC5mOC5kCDguJ7guKPguKPguKnguLIg4LmVIOC4mOC4seC4meC4p-C4suC4hOC4oSDguZLguZXguZXguZA!5e0!3m2!1sen!2sth!4v1452494902226" width="1080" height="415" frameborder="0" style="border:0" allowfullscreen></iframe></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" style="border:1px solid #e5e5e5;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="50%" valign="top" style="border-right:1px #e5e5e5 solid"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:15px;">
              <tr>
                <td width="50"><img src="<?php echo base_url(); ?>assets/welcome/images/icon-building.png" width="18" height="23" align="absmiddle" /></td>
                <td><span class="labeltxt">The Government Complex</span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>Building B (South Zone), 8th Floor, Laksi District, BKK 10210</td>
              </tr>
              <tr>
                <td height="32"></td>
                <td height="32"></td>
              </tr>
              <tr>
                <td><img src="<?php echo base_url(); ?>assets/welcome/images/icon-tell.png" width="24" height="21" align="absmiddle" /></td>
                <td><span class="labeltxt">เบอร์โทรศัพท์</span></td>
              </tr>
              <tr>
                <td height="23">&nbsp;</td>
                <td>0-2203-5000 ext. 42011 , 40502</td>
              </tr>
              <tr>
                <td height="32"></td>
                <td height="32"></td>
              </tr>
              <tr>
                <td><img src="<?php echo base_url(); ?>assets/welcome/images/icon_Fax.png" width="24" height="24" align="absmiddle" /></td>
                <td><span class="labeltxt">เบอร์แฟกซ์</span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>0-2143-9327</td>
              </tr>
              <tr>
                <td height="32"></td>
                <td height="32"></td>
              </tr>
              <tr>
                <td><img src="<?php echo base_url(); ?>assets/welcome/images/icon-mail.png" width="22" height="13" align="absmiddle" /></td>
                <td><span class="labeltxt">อีเมล์</span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><a href="mailto:tica@mfa.go.th" style="font-size:18px;">tica@mfa.go.th</a></td>
              </tr>
            </table></td>
            <td valign="top"><label for="textfield7"></label>
              <form id="form1" name="form1" method="post" action="<?php echo base_url(); ?>welcome/sendcontact">
                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:15px;">
                  <tr>
                    <td width="50"><span class="labeltxt">ส่งข้อมูล</span></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><span style="padding-left:8px;">
                      <input type="text" name="textfield4" id="textfield2" class="txtformbox"placeholder="ชื่อผู้ติดต่อ"  required="required"/>
                    </span></td>
                  </tr>
                  <tr>
                    <td><span style="padding-left:8px;">
                      <input type="text" name="textfield" id="textfield" class="txtformbox" placeholder="เบอร์โทรศัพท์"  required="required" />
                    </span></td>
                  </tr>
                  <tr>
                    <td><span style="padding-left:8px;">
                      <textarea name="textarea" cols="42" rows="5" class="checklisttxt-area2"placeholder="รายละเอียด" required="required" ></textarea>
                    </span></td>
                  </tr>
                  <tr>
                    <td>
                     <span style="padding-left:8px;"> <?php echo $capchar; ?></span></td>
                  </tr>
                  <tr>
                    <td height="5"></td>
                  </tr>
                  <tr>
                    <td><span style="padding-left:8px;">
                      <input type="text" name="captcha" id="textfield3" class="txtformbox" placeholder="กรอกอักษรตามภาพด้านบน" required="required" />
                    </span></td>
                  </tr>
                  <tr>
                    <td><input type="image" name="imageField" id="imageField" src="<?php echo base_url(); ?>assets/welcome/images/btn-send.png" /></td>
                  </tr>
                </table>
              </form></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
