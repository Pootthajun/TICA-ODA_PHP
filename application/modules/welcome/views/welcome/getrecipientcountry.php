<?php
$piriodtype = $this->input->post('piriodtype'); 
$month = $this->input->post('month'); 
$monthtext = $this->input->post('monthtext'); 
$monthyear = $this->input->post('monthyear'); 
$year = $this->input->post('year'); 
$fiscalyear = $this->input->post('fiscalyear'); 
?>

<?php
 
				$texttitle = 'Disbursement';  $and = "";
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis_date) =  '".$monthyear."'  and month(dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
				} 
				if($piriodtype ==2){
					$and = "  and year(dis_date) =  '".$year."'  ";
					$yearcat = $year;
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
				}  
				 //grant
/*				 $QL = $this->db->query("select sum(dis_budget) as total from disbureseme where title = 1  $and ");
				$grantdata = $QL->row_array();
				if(empty($grantdata['total'])){ $grantdata['total'] = 0;}
				$grant = $grantdata['total']; 
				//loan
				$QL = $this->db->query("select sum(dis_budget) as total  from disbureseme where title = 2  $and ");
				$loandata = $QL->row_array();
				if(empty($loandata['total'])){ $loandata['total'] = 0;}
				$loan = $loandata['total']; */
 
?>
 <div class="row">
 <br/>
  <h3 style="" class="dontshow"><b>Recipient of Country&nbsp;(<?php if($piriodtype ==1){
  if($month==1){echo "January";
  }
  if($month==2){echo "February";
  }
  if($month==3){echo "March";
  }
  if($month==4){echo "April";
  }
  if($month==5){echo "May";
  }
  if($month==6){echo "June";
  }
  if($month==7){echo "July";
  }
  if($month==8){echo "August";
  }
  if($month==9){echo "September";
  }
  if($month==10){echo "October";
  }
  if($month==11){echo "November";
  }
  if($month==12){echo "December";
  }
  echo " - ";
  echo $monthyear;}
  if($piriodtype ==2){echo $year;}
  if($piriodtype==3){echo "Fiscal Year : "; echo $fiscalyear;} ?>)</b></h3>
  </div>
<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" id="box-table-a">
 
  <thead>
    <tr>
      <th width="428" align="left"><strong>Country</strong></th>
      <th width="186" align="left">Project</th>
      <th width="186" align="right"> 	Disbursement (THB)</th>
    </tr>
  </thead>
    <tbody>
    <?php
	$budget ="";
	$Q = $this->db->query("select dis.country_id,country.country_name from disbureseme as dis
											inner join country as country on dis.country_id = country.country_id
											where country.countrytype ='1' ".$and."
											group by dis.country_id ");
											
	$sumProject = 0;
	$sumDisbursement = 0.0;
	foreach($Q->result_array() as $list){
	?>
    <tr>
      <td><?php echo $list['country_name']?></td>
      <td>
      <?php 
	   $sql = "SELECT * FROM disbureseme where country_id = ".$list['country_id']."  ".$and." ";
	   $QProject = $this->db->query($sql);
	   echo $num = $QProject->num_rows(); 
	   
	   $sumProject += $num;
	  ?>
      </td>
      <td align="right">
      <?php
	  $sqlsum = "SELECT SUM(dis_budget)  AS sumbudget FROM disbureseme where country_id = ".$list['country_id']."  ".$and." ";
	  $QSum= $this->db->query($sqlsum);
	  $rowsum = $QSum->row_array();
	  echo number_format($rowsum['sumbudget'],2,'.',',');  
	  
	  $sumDisbursement += $rowsum['sumbudget'];
	  ?>
      </td>
    </tr>

    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
      <td>Total</td>
      <td>
		<?php
			//$query = $this->db->query("SELECT count(dis_id) as count_dis FROM disbureseme where 1=1  ".$and." ");
			//$sum = $query->row_array();
			//echo number_format($sum['count_dis'],2,'.',',');  
			echo number_format($sumProject);
		?>
	  </td>
      <td align="right"><?php
		//$query = $this->db->query("select sum(dis_budget) as total from disbureseme where dis_id <>''  $and ");
		//$sum = $query->row_array();
		//echo number_format($sum['total'],2,'.',',');  
		echo number_format($sumDisbursement,2,'.',',');  
	  ?></td>
    </tr>
    </tfoot>
    
</table>
			
 