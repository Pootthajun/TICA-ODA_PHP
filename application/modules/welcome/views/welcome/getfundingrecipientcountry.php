<link href="<?php echo base_url(); ?>assets/welcome/css/table.css" rel="stylesheet" type="text/css" />
<?php
$piriodtype = $this->input->post('piriodtype'); 
$month = $this->input->post('month'); 
$monthtext = $this->input->post('monthtext'); 
$monthyear = $this->input->post('monthyear'); 
$year = $this->input->post('year'); 
$fiscalyear = $this->input->post('fiscalyear'); 
?>

<?php
 
				
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis.dis_date) =  '".$monthyear."'  and month(dis.dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
				} 
				if($piriodtype ==2){
					$and = "  and year(dis.dis_date) =  '".$year."'  ";
					$yearcat = $year;
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis.dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
				}  
				
				$texttitle = $header .' '.$yearcat ;
 
?>
<h2 class="titletable"><?php echo $texttitle; ?></h2>
<?php
$sql ="select 
				dis.project_id,funding.funding_id,funding.funding_name 
		from 
				disbureseme as dis 
				inner join project on project.project_id = dis.project_id
				inner join funding on funding.funding_id = project.funding_id
		where 
				project.project_id <> ''  	".$and."
				group by funding.funding_id
 ";
	$Q = $this->db->query($sql);
	if($Q->num_rows() > 0){
	

$country = $this->db->query("
	select 
		country.country_name,dis.* from disbureseme  as dis
	inner join 
		country on country.country_id = dis.country_id
		where dis.title < '3'  
			".$and."
		group by dis.country_id
	");
	$numcountry = $country->num_rows();
	?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="box-table-a">
  <thead>
    <tr>
      <th width="271">Ministry</th>
      <th width="586" colspan="<?php echo $numcountry; ?>" align="center">Recipient Country (Grant Aid and Loan)</th>
      <th width="117" align="center">Contribution</th>
      <th width="125" align="center">Total (THB)</th>
    </tr>

        <tr>
      <td>&nbsp;</td>
          <?php 
	foreach($country->result_array() as $listcountry){
		$arraycountryid[] = $listcountry['country_id'];
		?>
	

      <td align="right" style="border-left:#aabcfe thin solid; border-right:thin #aabcfe solid"><?php echo $listcountry['country_name']; ?></td>
      <?php } ?>
     <td align="right" style="border-left:#aabcfe thin solid; border-right:thin #aabcfe solid">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    </thead>
    <tbody>
    <?php
	$budget ="";


	foreach($Q->result_array() as $list){
	?>

    <tr>
      <td colspan="<?php echo $numcountry; ?>"><h5><?php echo $list['funding_name']; ?> </h5></td>
      <td align="right">&nbsp;</td>
      <td align="right"></td>
      <td>&nbsp;</td>
      </tr>
      <?php $ex = $sql ="select 
				dis.project_id,executing.executing_id,executing.executing_name 
		from 
				disbureseme as dis 
				inner join project on project.project_id = dis.project_id
				inner join executing on executing.executing_id = project.executing_id
		where 
				project.funding_id =".$list['funding_id']."	".$and."
				group by executing.executing_id
 ";
	  
	  $QEX =$this->db->query($ex);
	  				if($QEX->num_rows() > 0){
	  			  foreach($QEX->result_array() as $listexname){	
				  ?>
              <tr>
          <td>    - <?php echo $listexname['executing_name']; ?></td>
                <?php
	  for($i = 0;$i < $numcountry;$i++){
		  ?>

          <td align="right"  style="border-left:#aabcfe thin solid; border-right:thin #aabcfe solid">
          <?php
		 $sql1 ="select 
				sum(dis.dis_budget) as sumbudget,executing.executing_id
		from 
				disbureseme as dis 
				inner join project on project.project_id = dis.project_id
				inner join executing on executing.executing_id = project.executing_id
		where 
				dis.country_id = '". $arraycountryid[$i]."' 
				and
				project.executing_id = ".$listexname['executing_id']."  and dis.title < '3' 	".$and."
 ";
 		$QB= $this->db->query($sql1);
		$sumb = $QB->row_array();
		@$total[$i] += $sumb['sumbudget'];
	  echo number_format($sumb['sumbudget'],2,'.',',');  
	  ?>
          </td>
          <?php } ?>
          <td align="right" style="border-left:#aabcfe thin solid; border-right:thin #aabcfe solid"><?php
		 $sqlcon ="select 
				sum(dis.dis_budget) as sumbudget
		from 
				disbureseme as dis 
				inner join project on project.project_id = dis.project_id	
		where 
				project.executing_id = ".$listexname['executing_id']."  and dis.title >= '3' 	".$and."
 ";
 		$QCon= $this->db->query($sqlcon);
		$sumcon = $QCon->row_array();
		@$totalcon += $sumcon['sumbudget'];
	  echo number_format($sumcon['sumbudget'],2,'.',',');  
	  ?></td>
          <td align="right"><?php 
		 $sql3 ="select 
				sum(dis.dis_budget) as sumbudgettotal,executing.executing_id
		from 
				disbureseme as dis 
				inner join project on project.project_id = dis.project_id
				inner join executing on executing.executing_id = project.executing_id
		where 
				project.executing_id = ".$listexname['executing_id']."  and dis.title < '3' 	".$and."
 ";
 		$QT= $this->db->query($sql3);
		$sumt = $QT->row_array();
		@$grandtotal += $sumt['sumbudgettotal']+$sumcon['sumbudget'];
		 echo number_format($sumt['sumbudgettotal']+$sumcon['sumbudget'],2,'.',',');  

		  ?></td>
            </tr>

		<?php }} ?>

    <?php } ?>
    </tbody>
    <tfoot>
              <tr>
                <td>Total</td>
                <?php 
                  for($i = 0;$i < $numcountry;$i++){
		  ?>
                <td align="right"  style="border-left:#aabcfe thin solid; border-right:thin #aabcfe solid"><?php echo number_format($total[$i],2,'.',',');   ?></td>
                <?php } ?>
                <td align="right" style="border-left:#aabcfe thin solid; border-right:thin #aabcfe solid"><?php echo number_format($totalcon,2,'.',',');   ?></td>
                <td align="right"><?php echo number_format($grandtotal,2,'.',',');   ?></td>
              </tr>
    </tfoot>
    
  </table>
  <?php }else{ ?>
  <div><center><h3  style="color:red">data not found</h3></center></div>
  <?php } ?>