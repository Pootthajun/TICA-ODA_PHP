<script type="text/javascript">
$(document).ready(function()
{
	/////get Project
	$("#mpyear").change(function()
	{
			var mpyear=$('#mpyear').val();
			$('#year').val(mpyear);
	 		$( "#frm" ).submit();
	});
});
</script>
<form action="" name="frm" id="frm" method="post">
<p>
  <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
    <td width="92">Choose year</td>
    <td width="408"><select name="mpyear" id="mpyear">
    
      <?php
//Year
$sqlyear = $this->db->query("select DISTINCT mp_year from money_paid order by mp_year desc");
foreach($sqlyear->result_array() as $listyear){ ?>
<option value="<?php echo  $listyear['mp_year']; ?>" <?php if($listyear['mp_year'] == $this->input->post('year')){echo "selected='selected'";}?> ><?php echo  $listyear['mp_year']; ?></option>
<?php } ?>
    </select>
      <input type="hidden" name="year" id="year" value=""></td>
  </tr>
</table>
</p>
</form>
<?php
if(!empty($year)){
	$year = $this->input->post('year');
}else{
	$sqlyear = $this->db->query("select DISTINCT mp_year from money_paid order by mp_year desc limit 0,1");
	$yeardata = $sqlyear->row_array();
	$year = $yeardata['mp_year'];
}
if(!empty($year)){
	
//query country
$Q = $this->db->query("select r.country_id,c.country_name from 
											money_paid as mp 
											left join recipient as r on r.rec_id = mp.rec_id
											left join country as c on c.country_id = r.country_id
											where mp.mp_year = '".$year."'  group by r.country_id ");

//loop year
$country = ''; $pay = "";
foreach($Q->result_array() as $row){ 
		if(!empty($row['country_id'])){
				$country .=  '"'. $row['country_name'].'",';   
 
				//paid
				$QL = $this->db->query("select sum(mp.mp_amount) as total   from 
											money_paid as mp 
											left join recipient as r on r.rec_id = mp.rec_id
											left join country as c on c.country_id = r.country_id
											where mp.mp_year = '".$year."' and r.country_id = '".$row['country_id']."' ");
				$paydata = $QL->row_array();
				$pay .= $paydata['total'].",";  
		}
}
$country = substr($country,0, -1);
$pay = substr($pay,0, -1);
?>
<script type="text/javascript">
$(function () {
    $('#gp-country').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Cooperation of Country'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
              <?php  echo $country; ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [ 
		 {
            name: 'Cooperation',
            data: [<?php echo $pay;?>] 
        } 
		]
    });
});
		</script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="gp-country" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br>

<?php
}
?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>


