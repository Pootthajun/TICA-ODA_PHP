<?php
$piriodtype = $this->input->post('piriodtype'); 
$month = $this->input->post('month'); 
$monthtext = $this->input->post('monthtext'); 
$monthyear = $this->input->post('monthyear'); 
$year = $this->input->post('year'); 
$fiscalyear = $this->input->post('fiscalyear'); 
?>

<script src="<?php echo base_url(); ?>assets/highcharts/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/exporting.js"></script>
  <script type="text/javascript">
$(document).ready(function()
{
	var month=$('#month').val(); 
	var monthtext=$('#month  :selected').text();
	var monthyear=$('#monthyear').val();
	var year=$('#year').val();
	var fiscalyear =$('#fiscalyear').val();
	var piriodtype = $('input[name=piriodtype]:checked').val();
	$.ajax
	({
			type: "POST",
			url: "<?php echo base_url(); ?>welcome/getrecipientregional/",
			data: {"month":month,"monthyear":monthyear,"year":year,"fiscalyear":fiscalyear},
			cache: false,
			success: function(html)
			{
					$("#getdata").html(html);
			} 
	});
$("#submit").click(function()
{
	$("#getdata").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
		var month=$('#month').val();
		var monthtext=$('#month  :selected').text();
		var monthyear=$('#monthyear').val();
		var year=$('#year').val();
		var fiscalyear =$('#fiscalyear').val();
		var piriodtype = $('input[name=piriodtype]:checked').val();
			$.ajax
			({
					type: "POST",
					url: "<?php echo base_url(); ?>welcome/getrecipientregional/",
					data: {"month":month,"monthyear":monthyear,"year":year,"fiscalyear":fiscalyear,"piriodtype":piriodtype,"type":1},
					cache: false,
					success: function(html)
					{
							$("#getdata").html(html);
					} 
			});
});  
 
 
 $('#genpdf').click(function (event){
    var month=$('#month').val(); 
	var monthtext=$('#month  :selected').text();
	var monthyear=$('#monthyear').val();
	var year=$('#year').val();
	var fiscalyear =$('#fiscalyear').val();
	var piriodtype = $('input[name=piriodtype]:checked').val();
	var strWindowFeatures  = "?month="+month+"&monthtext="+monthtext+"&monthyear="+monthyear+"&year="+year+"&fiscalyear="+fiscalyear+"&piriodtype="+piriodtype+"";
    var url = $(this).attr("href")+strWindowFeatures;
    var windowName = "popUp";$(this).attr("name");
 	window.open(url, windowName);
 	event.preventDefault(); 
     });
	 
	  $('#genword').click(function (event){
    var month=$('#month').val(); 
	var monthtext=$('#month  :selected').text();
	var monthyear=$('#monthyear').val();
	var year=$('#year').val();
	var fiscalyear =$('#fiscalyear').val();
	var piriodtype = $('input[name=piriodtype]:checked').val();
	var strWindowFeatures  = "?month="+month+"&monthtext="+monthtext+"&monthyear="+monthyear+"&year="+year+"&fiscalyear="+fiscalyear+"&piriodtype="+piriodtype+"";
    var url = $(this).attr("href")+strWindowFeatures;
    var windowName = "popUp";//$(this).attr("name");
 	window.open(url, windowName);
 	event.preventDefault(); 
     });
	 
	  $('#genexcel').click(function (event){
    var month=$('#month').val(); 
	var monthtext=$('#month  :selected').text();
	var monthyear=$('#monthyear').val();
	var year=$('#year').val();
	var fiscalyear =$('#fiscalyear').val();
	var piriodtype = $('input[name=piriodtype]:checked').val();
	var strWindowFeatures  = "?month="+month+"&monthtext="+monthtext+"&monthyear="+monthyear+"&year="+year+"&fiscalyear="+fiscalyear+"&piriodtype="+piriodtype+"";
    var url = $(this).attr("href")+strWindowFeatures;
    var windowName = "popUp";//$(this).attr("name");
 	window.open(url, windowName);
 	event.preventDefault(); 
     });

});// end function



</script>
<link href="<?php echo base_url(); ?>assets/welcome/css/table.css" rel="stylesheet" type="text/css" />



<div style="position: relative; ">
<form action="" method="post">

  <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
      <td><u>Filter By Period</u></td>
      <td width="30"><input name="piriodtype" type="radio"  value="1" checked="checked" />
      <label for="filter"></label></td>
      <td>By Month</td>
      <td align="center"><label for="select"></label>
        <select name="month" id="month">
							<option></option>
														<option value="1" <?php if(date('m') == 1){ echo 'selected="selected" '; } ?> >January</option>
														<option value="2" <?php if(date('m') == 2){ echo 'selected="selected"'; } ?>>February</option>
														<option value="3" <?php if(date('m') == 3){ echo 'selected="selected"'; } ?>>March</option>
														<option value="4" <?php if(date('m') == 4){ echo 'selected="selected"'; } ?>>April</option>
														<option value="5" <?php if(date('m') == 5){ echo 'selected="selected"'; } ?>>May</option>
														<option value="6" <?php if(date('m') == 6){ echo 'selected="selected"'; } ?>>June</option>
														<option value="7" <?php if(date('m') == 7){ echo 'selected="selected"'; } ?>>July</option>
														<option value="8" <?php if(date('m') == 8){ echo 'selected="selected"'; } ?>>August</option>
														<option value="9" <?php if(date('m') == 9){ echo 'selected="selected"'; } ?>>September</option>
														<option value="10" <?php if(date('m') == 10){ echo 'selected="selected"'; } ?>>October</option>
														<option value="11" <?php if(date('m') == 11){ echo 'selected="selected"'; } ?>>November</option>
														<option value="12" <?php if(date('m') == 12){ echo 'selected="selected"'; } ?>>December</option>
	    </select></td>
      <td align="center"><select name="monthyear" id="monthyear">
              <?php  
		for($i=2010; $i<=date('Y',time()); $i++){
			if(date('Y') == $i){ $yearselected =  'selected="selected"'; } else { $yearselected = '';}
			echo '<option value="'.$i.'" '.$yearselected.' >'.$i.'</option>';
		}
			?>
      </select></td>
      <td align="right">&nbsp;</td>
      <td width="500" align="right"><table width="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><a href="<?php echo base_url(); ?>welcome/recipientregionpdf" id="genpdf"><img src="<?php echo base_url(); ?>assets/welcome/images/pdf.png" width="24" height="24" /></a></td>
          <td><a href="<?php echo base_url(); ?>welcome/recipienregiontword" id="genword"><img src="<?php echo base_url(); ?>assets/welcome/images/word.png" width="24" height="24" /></a></td>
          <td><a href="<?php echo base_url(); ?>welcome/recipientregionexcel" id="genexcel"><img src="<?php echo base_url(); ?>assets/welcome/images/xls.png" width="24" height="24" /></a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="radio" name="piriodtype" value="2" /></td>
      <td>By Year</td>
      <td align="center">&nbsp;</td>
      <td align="center"><select name="year" id="year">
        <?php  
		for($i=2010; $i<=date('Y',time()); $i++){
						if(date('Y') == $i){ $yearselected =  'selected="selected"'; } else { $yearselected = '';}
			echo '<option value="'.$i.'" '.$yearselected.' >'.$i.'</option>';
		}
			?>
      </select></td>
      <td align="right">&nbsp;</td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="radio" name="piriodtype"   value="3" /></td>
      <td>By Fiscal Year</td>
      <td align="center"><label for="select"></label></td>
      <td align="center">
      <select name="fiscalyear" id="fiscalyear">
        <?php  
		for($i=2010; $i<=date('Y',time()); $i++){
						if(date('Y') == $i){ $yearselected =  'selected="selected"'; } else { $yearselected = '';}
			echo '<option value="'.$i.'" '.$yearselected.' >'.$i.'</option>';
		}
			?>
      </select></td>
      <td align="right"><input type="button" name="button" id="submit" value="Search" /></td>
    
	  
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td width="150">&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100">&nbsp;</td>
      <td width="500">&nbsp;</td>
    </tr>
    </table>
	
</form>
</div>

<div>
  <h3 style="" class="dontshow"><b>Recipient Regional&nbsp;<?php if($piriodtype ==1){
  if($month==1){echo "January";
  }
  if($month==2){echo "February";
  }
  if($month==3){echo "March";
  }
  if($month==4){echo "April";
  }
  if($month==5){echo "May";
  }
  if($month==6){echo "June";
  }
  if($month==7){echo "July";
  }
  if($month==8){echo "August";
  }
  if($month==9){echo "September";
  }
  if($month==10){echo "October";
  }
  if($month==11){echo "November";
  }
  if($month==12){echo "December";
  }
  echo " - ";
  echo $monthyear;}
  if($piriodtype ==2){echo $year;}
  if($piriodtype==3){echo "Fiscal Year : "; echo $fiscalyear;} ?></b></h3>
</div>
<br/>

<div id="gp-project" style="min-width: 310px; height: 450px; margin: 0 auto; position: relative; padding-bottom:20px; ">
</div>
<div id="getdata">

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="box-table-a">
  <thead>
    <tr>
      <th width="428"><strong>Region (OECD)</strong></th>
      <th width="186">Project</th>
      <th width="186" align="right"> 	Disbursement (THB)</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$budget ="";
	$Q = $this->db->query('select * from region group by region_name order by region_id asc');
	foreach($Q->result_array() as $list){
											
	?>
    <tr>
      <td><?php echo $list['region_name']; ?></td>
      <td>
      <?php 
	  $QProject = $this->db->query('select count(dis.country_id) as count ,sum(dis.dis_budget) as sumbudget,country.region_id from disbureseme as dis
											inner join country as country on dis.country_id = country.country_id
											where country.region_id = '.$list['region_id'].' ');										
											$sumb =  $QProject->row_array();
											echo $sumb['count'];
											
      $QProjecta = $this->db->query('select count(dis.country_id) as count ,sum(dis.dis_budget) as sumbudget,country.region_id from disbureseme as dis
											inner join country as country on dis.country_id = country.country_id
											where country.region_id =1  ');										
											$sumba =  $QProjecta->row_array();
											$sumba['sumbudget'];
											
	$QProjectab = $this->db->query('select count(dis.country_id) as count ,sum(dis.dis_budget) as sumbudget,country.region_id from disbureseme as dis
											inner join country as country on dis.country_id = country.country_id
											where country.region_id =2  ');										
											$sumbb =  $QProjectab->row_array();
											$sumbb['sumbudget'];
	
	$QProjectac = $this->db->query('select count(dis.country_id) as count ,sum(dis.dis_budget) as sumbudget,country.region_id from disbureseme as dis
											inner join country as country on dis.country_id = country.country_id
											where country.region_id =3  ');										
											$sumbc =  $QProjectac->row_array();
											$sumbc['sumbudget'];
											
	$QProjectad = $this->db->query('select count(dis.country_id) as count ,sum(dis.dis_budget) as sumbudget,country.region_id from disbureseme as dis
											inner join country as country on dis.country_id = country.country_id
											where country.region_id =4  ');										
											$sumbd =  $QProjectad->row_array();
											$sumbd['sumbudget'];
											
	$QProjectae = $this->db->query('select count(dis.country_id) as count ,sum(dis.dis_budget) as sumbudget,country.region_id from disbureseme as dis
											inner join country as country on dis.country_id = country.country_id
											where country.region_id =5  ');										
											$sumbe =  $QProjectae->row_array();
											$sumbe['sumbudget'];
											
											
	  ?>
      </td>
      <td align="right">
      <?php
	 	  echo number_format($sumb['sumbudget'],2,'.',',');  
	  ?>
      </td>
    </tr>

    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
      <td>Total</td>
      <td>&nbsp;</td>
      <td align="right">
	  <?php
      $this->db->select_sum('dis_budget');
		$query = $this->db->get('disbureseme'); 
		$sum = $query->row_array();
		echo number_format($sum['dis_budget'],2,'.',',');  
	  ?>
      </td>
    </tr>
    </tfoot>
  </table>
  </table>
  <script type="text/javascript">
                $(function () {
				$('#gp-project').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Recipient Regional'
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: [ '<?php ?>' ],
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Disbursement (THB)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.,.2f} mm</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [  
                         {
                            name: 'Europe',
                            data: [<?php  echo $sumba['sumbudget'];  ?>] 
                        },{
                            name: 'Africa',
                            data: [<?php  echo $sumbb['sumbudget'];  ?>] 
                        },
						{
                            name: 'Latin America',
                            data: [<?php  echo $sumbc['sumbudget'];  ?>] 
                        },{
                            name: 'Asia',
                            data: [<?php  echo $sumbd['sumbudget'];  ?>] 
                        },{
                            name: 'Oceania',
                            data: [<?php  echo $sumbe['sumbudget'];  ?>] 
                        }
                        ]
                    });
                }); 
		</script>
</div>

