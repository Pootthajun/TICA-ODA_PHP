<?php
ob_start();
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=fundingprojectexecutingexcel.xls"); 
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); 
ob_end_flush();
?>
<?php
$piriodtype = $this->input->get('piriodtype'); 
$month = $this->input->get('month'); 
$monthtext = $this->input->get('monthtext'); 
$monthyear = $this->input->get('monthyear'); 
$year = $this->input->get('year'); 
$fiscalyear = $this->input->get('fiscalyear'); 
$totalgrant = "";
$totalcon = "";
$totalloan="";
				
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis.dis_date) =  '".$monthyear."'  and month(dis.dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
					$link = "&month=".$month."&monthyear=".$monthyear."";
				} 
				if($piriodtype ==2){
					$and = "  and year(dis.dis_date) =  '".$year."'  ";
					$yearcat = $year;
					$link = "&year=".$year."";
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis.dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
					$link = "&startyear=01/10/".$startyear."&fiscalyear=30/09/".$fiscalyear."";
				}  
?>





<?php
 

				
				$texttitle = $header .' '.$yearcat ;
 
?>
<h2 class="titletable"><?php echo $texttitle; ?></h2>
<div id="getdata">
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="box-table-a">
  <thead>
    <tr>
      <th width="428"><strong>Executing Agency</strong></th>
      <th width="186" align="right"> Grant/Technical Cooperation</th>
      <th width="186" align="right">Contributions to International Org.</th>
      <th width="186" align="right">Loans</th>
      <th width="186" align="right">Total (THB)</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$budget ="";

 $sql ="select 
				dis.project_id,executing.executing_id,executing.executing_name 
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing on executing.executing_id = project.executing_id
		where 
		        project.project_type_id = '4'
			    and
				project.funding_id =".$this->input->get('fundingid')."	".$and."
				group by executing.executing_id
 ";
	$Q = $this->db->query($sql);
	foreach($Q->result_array() as $list){
	?>
    <tr>
      <td height="40"><?php echo $list['executing_name']; ?></td>
      <td align="right">
	  <?php
		 $sql1 ="select 
				sum(dis.dis_budget) as sumbudget,ex.executing_id
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing  as ex on ex.executing_id = project.executing_id
		where 
				project.executing_id = ".$list['executing_id']."  and dis.title = '1' 	".$and."
 ";
 		$QB= $this->db->query($sql1);
		$sumb = $QB->row_array();
		$totalgrant += $sumb['sumbudget'];
	  echo number_format($sumb['sumbudget'],2,'.',',');  
	  ?></td>
      <td align="right"><?php
		 $sql3 ="select 
				sum(dis.dis_budget) as sumbudget,ex.executing_id
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing  as ex on ex.executing_id = project.executing_id
		where 
				project.executing_id = ".$list['executing_id']."  and dis.title >= 3  	".$and."
 ";
 		$Qcon= $this->db->query($sql3);
		$sumcon = $Qcon->row_array();
		$totalcon += $sumcon['sumbudget'];
	  echo number_format($sumcon['sumbudget'],2,'.',',');  
	  ?></td>
      <td align="right">
          <?php
		  $sql2 ="select 
				sum(dis.dis_budget) as sumbudget,ex.executing_id
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing  as ex on ex.executing_id = project.executing_id
		where 
				project.executing_id = ".$list['executing_id']."  and dis.title = '2' 	".$and."
 ";
 		$QLoan= $this->db->query($sql2);
		$sumloan = $QLoan->row_array();
		$totalloan += $sumloan['sumbudget'];
	  echo number_format($sumloan['sumbudget'],2,'.',',');  
	  ?>
      </td>
      <td align="right"><?php echo number_format($sumb['sumbudget']+$sumcon['sumbudget']+$sumloan['sumbudget'],2,'.',',');   ?></td>
    </tr>

    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
      <td><strong>Total</strong></td>
      <td align="right"><strong>
        <?php
		echo @number_format($totalgrant,2,'.',',');  
		  ?>
      </strong></td>
      <td align="right"><strong>
        <?php
		echo @number_format($totalcon,2,'.',',');  
		  ?>
      </strong></td>
      <td align="right"><strong>
        <?php
		echo @number_format($totalloan,2,'.',',');  
		  ?>
      </strong></td>
      <td align="right"><strong>
        <?php
		echo @number_format($totalloan+$totalcon+$totalgrant,2,'.',',');  
		  ?>
      </strong></td>
      </tr>
    </tfoot>
    
  </table>
  </div>

