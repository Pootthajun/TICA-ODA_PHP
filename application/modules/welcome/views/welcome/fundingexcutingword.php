<?php 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=fundingecutingword.doc");
?>
<style>
<!--
 /* Style Definitions */
@page Section1{
            size: 29.7cm 21cm;
            margin: 2cm 2cm 2cm 2cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
        div.Section1 { page:Section1;}
-->
</style>
<div class="Section1">
<?php
$piriodtype = $this->input->get('piriodtype'); 
$month = $this->input->get('month'); 
$monthtext = $this->input->get('monthtext'); 
$monthyear = $this->input->get('monthyear'); 
$year = $this->input->get('year'); 
$fiscalyear = $this->input->get('fiscalyear'); 
?>

<?php
 
				
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis.dis_date) =  '".$monthyear."'  and month(dis.dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
				} 
				if($piriodtype ==2){
					$and = "  and year(dis.dis_date) =  '".$year."'  ";
					$yearcat = $year;
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis.dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
				}  
				
				$texttitle = $header .' '.$yearcat ;
 
?>
<h2 class="titletable"><?php echo $texttitle; ?></h2>
<?php
$sql ="select 
				dis.project_id,funding.funding_id,funding.funding_name 
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join funding on funding.funding_id = project.funding_id
		where 
				project.project_id <> ''  	".$and."
				group by funding.funding_id
 ";
	$Q = $this->db->query($sql);
	if($Q->num_rows() > 0){
	

$country = $this->db->query("
	select 
		country.country_name,dis.* from disbureseme  as dis
	left join 
		country on country.country_id = dis.country_id
		where dis.title < '3'  
			".$and."
		group by dis.country_id
	");
	$numcountry = $country->num_rows();
	?>
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="box-table-a">
  <thead>
    <tr>
      <th style='font-size:10.0pt;font-family:"tahoma"' width="271">Ministry</th>
      <th  style='font-size:10.0pt;font-family:"tahoma"' width="586" colspan="<?php echo $numcountry; ?>" align="center">Recipient Country (Grant Aid and Loan)</th>
      <th style='font-size:10.0pt;font-family:"tahoma"' width="117" align="center">Contribution</th>
      <th style='font-size:10.0pt;font-family:"tahoma"' width="125" align="center">Total (THB)</th>
      <th style='font-size:10.0pt;font-family:"tahoma"' width="125" align="center" >Type of Cooperation</th>
    </tr>

        <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"' >&nbsp;</td>
          <?php 
	foreach($country->result_array() as $listcountry){
		$arraycountryid[] = $listcountry['country_id'];
		?>
	

      <td style='font-size:10.0pt;font-family:"tahoma"'  align="right" ><?php echo $listcountry['country_name']; ?></td>
      <?php } ?>
     <td style='font-size:10.0pt;font-family:"tahoma"'  align="right" >&nbsp;</td>
      <td style='font-size:10.0pt;font-family:"tahoma"' >&nbsp;</td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  >&nbsp;</td>
    </tr>
    </thead>
    <tbody>
    <?php
	$budget ="";


	foreach($Q->result_array() as $list){
	?>

    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'  ><h5><?php echo $list['funding_name']; ?> </h5></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  align="right" colspan="<?php echo $numcountry; ?>">&nbsp;</td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  align="right"></td>
      <td style='font-size:10.0pt;font-family:"tahoma"' >&nbsp;</td>
      <td style='font-size:10.0pt;font-family:"tahoma"'  >&nbsp;</td>
      </tr>
      <?php $ex = $sql ="select 
				dis.project_id,executing.executing_id,executing.executing_name 
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing on executing.executing_id = project.executing_id
		where 
				project.funding_id =".$list['funding_id']."	".$and."
				group by executing.executing_id
 ";
	  
	  $QEX =$this->db->query($ex);
	  				if($QEX->num_rows() > 0){
	  			  foreach($QEX->result_array() as $listexname){	
				  ?>
              <tr>
          <td style='font-size:10.0pt;font-family:"tahoma"' >    <?php echo $listexname['executing_name']; ?></td>


          <td style='font-size:10.0pt;font-family:"tahoma"'  align="right"  colspan="<?php echo $numcountry; ?>" >&nbsp;</td>
    
          <td style='font-size:10.0pt;font-family:"tahoma"'  align="right" >&nbsp;</td>
          <td style='font-size:10.0pt;font-family:"tahoma"'  align="right">&nbsp;</td>
          <td style='font-size:10.0pt;font-family:"tahoma"'  align="right">&nbsp;</td>
      </tr>
 <!-- List project-->
 				 <?php $project = $sqlproject ="select 
								dis.project_id,project.project_name,project.cooperation_type_id,cooperation_type.cooperation_type_name
						from 
								disbureseme as dis 
								left join project on project.project_id = dis.project_id
								left join cooperation_type on cooperation_type.cooperation_type_id = project.cooperation_type_id
						where 
								project.executing_id = ".$listexname['executing_id']."	".$and."
								group by project.executing_id 
				 ";
					  
					  $QP =$this->db->query($project);
								  foreach($QP->result_array() as $listproject){	
								  ?>
              <tr>
                <td style='font-size:10.0pt;font-family:"tahoma"' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <?php echo $listproject['project_name']; ?></td>
                <?php
				for($i = 0;$i < $numcountry;$i++){
		  ?>
                <td style='font-size:10.0pt;font-family:"tahoma"'  align="right"  ><?php
		 $sql1 ="select 
				sum(dis.dis_budget) as sumbudget,executing.executing_id
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing on executing.executing_id = project.executing_id
		where 
				dis.country_id = '". $arraycountryid[$i]."' 
				and
				project.project_id = ".$listproject['project_id']."  and dis.title < '3' 	".$and."
 ";
 		$QB= $this->db->query($sql1);
		$sumb = $QB->row_array();
		@$total[$i] += $sumb['sumbudget'];
	  echo number_format($sumb['sumbudget'],2,'.',',');  
	  ?></td>
                <?php } ?>
                <td style='font-size:10.0pt;font-family:"tahoma"'  align="right" ><?php
		 $sqlcon ="select 
				sum(dis.dis_budget) as sumbudget
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id	
		where 
				project.project_id = ".$listproject['project_id']."   and dis.title >= '3' 	".$and."
 ";
 		$QCon= $this->db->query($sqlcon);
		$sumcon = $QCon->row_array();
		@$totalcon += $sumcon['sumbudget'];
	  echo number_format($sumcon['sumbudget'],2,'.',',');  
	  ?></td>
                <td style='font-size:10.0pt;font-family:"tahoma"'  align="right"><?php 
		 $sql3 ="select 
				sum(dis.dis_budget) as sumbudgettotal,executing.executing_id
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join executing on executing.executing_id = project.executing_id
		where 
				project.project_id = ".$listproject['project_id']."   and dis.title < '3' 	".$and."
 ";
 		$QT= $this->db->query($sql3);
		$sumt = $QT->row_array();
		@$grandtotal += $sumt['sumbudgettotal']+$sumcon['sumbudget'];
		 echo number_format($sumt['sumbudgettotal']+$sumcon['sumbudget'],2,'.',',');  

		  ?></td>
                <td style='font-size:10.0pt;font-family:"tahoma"'  align="center" ><?php echo $listproject['cooperation_type_name']; ?></td>
                </tr>
                		<?php } ?>
      
        <?php }}} ?>
    </tbody>
    <tfoot>
      
              <tr>
                <td style='font-size:10.0pt;font-family:"tahoma"' >Total</td>
                <?php 
                  for($i = 0;$i < $numcountry;$i++){
		  ?>
                <td style='font-size:10.0pt;font-family:"tahoma"'  align="right"  ><?php echo number_format($total[$i],2,'.',',');   ?></td>
                <?php } ?>
                <td style='font-size:10.0pt;font-family:"tahoma"'  align="right" ><?php echo number_format($totalcon,2,'.',',');   ?></td>
                <td style='font-size:10.0pt;font-family:"tahoma"'  align="right"><?php echo number_format($grandtotal,2,'.',',');   ?></td>
                <td style='font-size:10.0pt;font-family:"tahoma"'  align="right" >&nbsp;</td>
              </tr>
    </tfoot>
    
  </table>
  <?php }else{ ?>
<div><center><h3  style="color:red">data not found</h3></center></div>
  <?php } ?>
  </div>