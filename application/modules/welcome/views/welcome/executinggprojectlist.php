<link href="<?php echo base_url(); ?>assets/welcome/css/table.css" rel="stylesheet" type="text/css" />
<?php
$piriodtype = $this->input->get('piriodtype'); 
$month = $this->input->get('month'); 
$monthtext = $this->input->get('monthtext'); 
$monthyear = $this->input->get('monthyear'); 
$year = $this->input->get('year'); 
$fiscalyear = $this->input->get('fiscalyear'); 
$totalgrant = "";
$totalcon = "";
$totalloan="";
?>

<?php
 
				
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis.dis_date) =  '".$monthyear."'  and month(dis.dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
					$link = "&month=".$month."&monthyear=".$monthyear."";
				} 
				if($piriodtype ==2){
					$and = "  and year(dis.dis_date) =  '".$year."'  ";
					$yearcat = $year;
					$link = "&year=".$year."";
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis.dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
					$link = "&startyear=".$startyear."&fiscalyear=".$fiscalyear."";
				}  
				
				$texttitle = $header .' '.$yearcat ;
 
?>
<div style="position: relative; ">

  <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
      <td width="500" align="right"><table width="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><a href="<?php echo base_url(); ?>welcome/executinggprojectlistpdf?exid=<?php echo $this->input->get('exid')."&piriodtype=".$piriodtype."".$link; ?>"  target="_blank"><img src="<?php echo base_url(); ?>assets/welcome/images/pdf.png" width="24" height="24" /></a></td>
          <td><a href="<?php echo base_url(); ?>welcome/executinggprojectlistword?exid=<?php echo $this->input->get('exid')."&piriodtype=".$piriodtype."".$link; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/welcome/images/word.png" width="24" height="24" /></a></td>
          <td><a href="<?php echo base_url(); ?>welcome/executinggprojectlistexcel?exid=<?php echo $this->input->get('exid')."&piriodtype=".$piriodtype."".$link; ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/welcome/images/xls.png" width="24" height="24" /></a></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td width="500">&nbsp;</td>
    </tr>
    </table>

</div>


<h2 class="titletable"><?php echo $texttitle; ?></h2>
<div id="getdata">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="box-table-a">
  <thead>
    <tr>
      <th width="172"><strong>Project Code</strong></th>
      <th width="519" align="center">Project Title</th>
      <th width="209" align="center">Recipient country</th>
      <th width="211" align="right">Disbursement (THB)</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$budget ="";

 $sql ="select 
				dis.project_id,project.project_name,project.project_type_id
						from 
								disbureseme as dis 
								left join project on project.project_id = dis.project_id
						where 
								project.project_type_id = '4'
								and
								project.executing_id = ".$this->input->get('exid')."	".$and."
								group by project.project_id
 ";
	$Q = $this->db->query($sql);
	foreach($Q->result_array() as $list){
	?>
    <tr>
      <td><?php echo $list['project_id']; ?></td>
      <td align="left"><a href="<?php echo site_url(); ?>welcome/projectdetail?progectid=<?php echo $list['project_id']; ?>" class="linktable"><?php echo $list['project_name']; ?></a></td>
      <td align="center">
      <?php
	$sql ="select  count(dis.country_id) as countcountry,dis.project_id,project.project_name
						from 
								disbureseme as dis 
								left join project on project.project_id = dis.project_id
						where 
								project.project_id = ".$list['project_id']."	".$and."
								
 ";
	  	$Q = $this->db->query($sql);
	    $c = $Q->row_array();
		echo $c['countcountry'];
	  ?>
      </td>
      <td align="right">
          <?php
	  $sql2 ="select 
				sum(dis.dis_budget) as sumbudget,dis.project_id,project.project_name
						from 
								disbureseme as dis 
								left join project on project.project_id = dis.project_id
						where 
								project.project_id = ".$list['project_id']."	".$and."
 ";
 		$QLoan= $this->db->query($sql2);
		$sumloan = $QLoan->row_array();
		$totalloan += $sumloan['sumbudget'];
	  echo number_format($sumloan['sumbudget'],2,'.',',');  
	  ?>
      </td>
      </tr>

    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
      <td>Total</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="right"><?php
		echo @number_format($totalloan,2,'.',',');  
		  ?></td>
      </tr>
    </tfoot>
    
  </table>
  </div>

