
<?php
$piriodtype = $this->input->get('piriodtype'); 
$month = $this->input->get('month'); 
$monthtext = $this->input->get('monthtext'); 
$monthyear = $this->input->get('monthyear'); 
$year = $this->input->get('year'); 
$fiscalyear = $this->input->get('fiscalyear'); 
?>
<?php
 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=sectorword.doc");
?>
<style>
<!--
 /* Style Definitions */
@page Section1{
            size: 29.7cm 21cm;
            margin: 2cm 2cm 2cm 2cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
        div.Section1 { page:Section1;}
-->
</style>
<div class="Section1">
<?php
 
				
				//total year
				if($piriodtype ==1){
					$and = "  and year(dis.dis_date) =  '".$monthyear."'  and month(dis.dis_date) =  '".$month."'";
					$yearcat = $monthtext.' '.$monthyear;
				} 
				if($piriodtype ==2){
					$and = "  and year(dis.dis_date) =  '".$year."'  ";
					$yearcat = $year;
				} 
				if($piriodtype ==3){ 
					$startyear = $fiscalyear-1;
					$and = "  and dis.dis_date between '".$startyear."-10-01'  and  '".$fiscalyear."-09-30' ";
					$yearcat =  '01/10/'.$startyear.' - 30/09/'.$fiscalyear;
				}  
				
				$texttitle = $header .' '.$yearcat ;
 
?>
<h2 class="titletable"><?php echo $texttitle; ?></h2>
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="box-table-a">
  <thead>
    <tr>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="428" bgcolor="#FAFAFA"><strong>Sector/Purpose </strong></th>
      <th style='font-size:10.0pt;font-family:"tahoma"'  width="186" align="center" bgcolor="#FAFAFA">Project</th>
      <th  style='font-size:10.0pt;font-family:"tahoma"'  width="186" align="right" bgcolor="#FAFAFA"> 	Disbursement (THB)</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$budget ="";

$sql ="select 
				dis.project_id,funding.funding_id,funding.funding_name 
		from 
				disbureseme as dis 
				left join project on project.project_id = dis.project_id
				left join funding on funding.funding_id = project.funding_id
		where 
				project.funding_id <> '' 	".$and."
 ";
	$Q = $this->db->query($sql);
	foreach($Q->result_array() as $list){
	?>
    <tr><td style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FFFFFF"><?php echo $list['funding_name']; ?></td><td style='font-size:10.0pt;font-family:"tahoma"'  align="center" bgcolor="#FFFFFF">
<?php

$c =  $this->db->query('select count(project_id) as count  from project 	where funding_id = '.$list['funding_id'].'
											  ');	
											  $count =  $c->row_array();
											echo $count['count'];		
											
	 $sql1 = 'select count(project.project_id) as count ,sum(dis.dis_budget) as sumbudget,project.funding_id from disbureseme as dis
											inner join project as project on dis.project_id = project.project_id
											where project.funding_id = '.$list['funding_id'].'
											
											'.$and.'
											  ';
	  $QProject = $this->db->query($sql1 );										
											$sumb =  $QProject->row_array();
											//echo $sumb['count'];
	  ?>

      </td><td style='font-size:10.0pt;font-family:"tahoma"'  align="right" bgcolor="#FFFFFF">
      <?php
$budget += $sumb['sumbudget'];
	  echo number_format($sumb['sumbudget'],2,'.',',');  
	  ?>
      </td>
    </tr>

    <?php } ?>
    </tbody>
    <tfoot>
        <tr><td style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FDFDFD">Total</td><td style='font-size:10.0pt;font-family:"tahoma"'  bgcolor="#FDFDFD">&nbsp;</td><td style='font-size:10.0pt;font-family:"tahoma"'  align="right" bgcolor="#FDFDFD"><?php
		echo @number_format($budget,2,'.',',');  
		  ?></td>
    </tr>
    </tfoot>
    
    </table>