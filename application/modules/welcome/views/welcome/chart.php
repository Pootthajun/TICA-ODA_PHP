<?php
$piriodtype = $this->input->post('piriodtype'); 
$type = $this->input->post('type'); 
if(empty($type)){ $type = 3; } 
$month = $this->input->post('month'); 
$monthtext = $this->input->post('monthtext'); 
$monthyear = $this->input->post('monthyear'); 
$year = $this->input->post('year'); 
$fiscalyear = $this->input->post('fiscalyear');

?>

<script type="text/javascript">
$(document).ready(function()
{
	var month=$('#month').val();	
	var monthyear=$('#monthyear').val();
	var year=$('#year').val();
	var fiscalyear =$('#fiscalyear').val();
	var piriodtype = $('input[name=piriodtype]:checked').val();
	$.ajax
	({
			type: "POST",
			url: "<?php echo base_url(); ?>welcome/getdisbursement/",
			data: {"month":month,"monthyear":monthyear,"year":year,"fiscalyear":fiscalyear},
			cache: false,
			success: function(html)
			{
					$("#getdata").html(html);
			} 
	});
$("#submit").click(function()
{
	$("#getdata").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
	var month=$('#month').val();
	var monthyear=$('#monthyear').val();
	var year=$('#year').val();
	var fiscalyear =$('#fiscalyear').val();
	var piriodtype = $('input[name=piriodtype]:checked').val();
			$.ajax
			({
					type: "POST",
					url: "<?php echo base_url(); ?>welcome/getdisbursement/",
					data: {"month":month,"monthyear":monthyear,"year":year,"fiscalyear":fiscalyear,"piriodtype":piriodtype,"type":1},
					cache: false,
					success: function(html)
					{
							$("#getdata").html(html);
					} 
			});
}); // end disbursement click
$("#disbursement").click(function()
{
	 //$(#b).hide();
	 $("#getdata").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
		var month=$('#month').val();
		var monthtext=$('#month  :selected').text();
	var monthyear=$('#monthyear').val();
	var year=$('#year').val();
	var fiscalyear =$('#fiscalyear').val();
	var piriodtype = $('input[name=piriodtype]:checked').val();
	
			$.ajax
			({
					type: "POST",
					url: "<?php echo base_url(); ?>welcome/getdisbursement/",
					data: {"month":month,"monthyear":monthyear,"year":year,"fiscalyear":fiscalyear,"piriodtype":piriodtype,"monthtext":monthtext,"type":1},
					cache: false,
					success: function(html)
					{
							$("#getdata").html(html);
					} 
			});
}); // end disbursement click
///start project click
$("#projects").click(function()
{
	
        
    
	$("#getdata").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
			var month=$('#month').val();
			var monthtext=$('#month  :selected').text();
			var monthyear=$('#monthyear').val();
			var year=$('#year').val();
			var fiscalyear =$('#fiscalyear').val();
			var piriodtype = $('input[name=piriodtype]:checked').val();
			$.ajax
			({
							type: "POST",
							url: "<?php echo base_url(); ?>welcome/getdisbursement/",
							data: {"month":month,"monthyear":monthyear,"year":year,"fiscalyear":fiscalyear,"monthtext":monthtext,"piriodtype":piriodtype,"type":2},
							cache: false,
							success: function(html)
							{
										$("#getdata").html(html);
							} 
			});
			
			
				
				
			
});//// end project click

///start  comparative click
$("#comparative").click(function()
{ 
$("#TTypeodAid").hide();
$("#getdata").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>welcome/getdisbursement/",
data: {"month":month,"monthyear":monthyear,"year":year,"fiscalyear":fiscalyear,"piriodtype":piriodtype,"type":3},
cache: false,
success: function(html)
{
$("#getdata").html(html);
} 
});
});//// end comparative click

});// end function

</script>


<div style="position: relative; height: 110px;">
<form action="" method="post">
  <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
      <td width="540"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="1"><input type="button" name="button" id="disbursement" value="Disbursement" /> </td>
          <td width="1"><input type="button" name="button" id="projects" value="Projects" /> </td>
          <td width="1"><input type="button" name="button" id="comparative" value="Comparative 5 Years" /> </td>
          <td width="150">&nbsp;</td>
        </tr>
      </table></td>
      <td><u>Filter By Period</u></td>
      <td width="30"><input name="piriodtype" type="radio"  value="1" checked="checked" />
      <label for="filter"></label></td>
      <td>By Month</td>
      <td align="center"><label for="select"></label>
        <select name="month" id="month">
							<option></option>
														<option value="1" <?php if(date('m') == 1){ echo 'selected="selected" '; } ?> >January</option>
														<option value="2" <?php if(date('m') == 2){ echo 'selected="selected"'; } ?>>February</option>
														<option value="3" <?php if(date('m') == 3){ echo 'selected="selected"'; } ?>>March</option>
														<option value="4" <?php if(date('m') == 4){ echo 'selected="selected"'; } ?>>April</option>
														<option value="5" <?php if(date('m') == 5){ echo 'selected="selected"'; } ?>>May</option>
														<option value="6" <?php if(date('m') == 6){ echo 'selected="selected"'; } ?>>June</option>
														<option value="7" <?php if(date('m') == 7){ echo 'selected="selected"'; } ?>>July</option>
														<option value="8" <?php if(date('m') == 8){ echo 'selected="selected"'; } ?>>August</option>
														<option value="9" <?php if(date('m') == 9){ echo 'selected="selected"'; } ?>>September</option>
														<option value="10" <?php if(date('m') == 10){ echo 'selected="selected"'; } ?>>October</option>
														<option value="11" <?php if(date('m') == 11){ echo 'selected="selected"'; } ?>>November</option>
														<option value="12" <?php if(date('m') == 12){ echo 'selected="selected"'; } ?>>December</option>
													</select></td>
      <td align="center"><select name="monthyear" id="monthyear">
              <?php  
		for($i=2010; $i<=date('Y',time()); $i++){
			if(date('Y') == $i){ $yearselected =  'selected="selected"'; } else { $yearselected = '';}
			echo '<option value="'.$i.'" '.$yearselected.' >'.$i.'</option>';
		}
			?>
      </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="radio" name="piriodtype" value="2" /></td>
      <td>By Year</td>
      <td align="center">&nbsp;</td>
      <td align="center"><select name="year" id="year">
        <?php  
		for($i=2010; $i<=date('Y',time()); $i++){
						if(date('Y') == $i){ $yearselected =  'selected="selected"'; } else { $yearselected = '';}
			echo '<option value="'.$i.'" '.$yearselected.' >'.$i.'</option>';
		}
			?>
      </select></td>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="radio" name="piriodtype"   value="3" /></td>
      <td>By Fiscal Year</td>
      <td align="center"><label for="select"></label></td>
      <td align="center">
      <select name="fiscalyear" id="fiscalyear">
        <?php  
		for($i=2010; $i<=date('Y',time()); $i++){
						if(date('Y') == $i){ $yearselected =  'selected="selected"'; } else { $yearselected = '';}
			echo '<option value="'.$i.'" '.$yearselected.' >'.$i.'</option>';
		}
			?>
      </select></td>
      <td align="right"><label for="select2">
        <input type="button" name="button" id="submit" value="Search" />
      </label></td>
    </tr>
    <tr>
      <td width="150">&nbsp;</td>
      <td width="150">&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="100">&nbsp;</td>
    </tr>
  </table>
  </div>
 

 
  
</form>

<br/>
<br/>
<br/>
<div id="getdata"></div>

