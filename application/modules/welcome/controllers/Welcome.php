<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends Welcome_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));
        $this->load->helper('string');
        $this->load->library('email');
	
    }
    
    function index(){
        $data['header'] = "xxxxx"; 
        $data['about'] = $this->MAbout->getAbout();
        $data['module'] = $this->module;
        $data['news']= $this->MNews->getAllnewsIndex();
		//get country index
		$data['country']= $this->MCountry->getAllCountryIndex();
		
		$data['page'] = $this->config->item('wconfig_template_welcome').'index';      
    	$this->load->view($this->_container_index,$data); 
    }
 
		
    function contact(){
        $this->load->helper('captcha');
        $data['header'] = "Contact Us";
        $vals = array(
        'font_size'	=> 18,
        'word_length'	=> 3,
        'pool'		=> '0123456789',
        'img_path'      => './captcha/',
        'img_width'	=> '150',
	'img_height'	=> '40',
        'img_url'       => base_url().'captcha/',
        'font_path'       => './system/fonts/texa.ttf',
            'colors'	=> array(
				'background'	=> array(0,0,0),
				'border'	=> array(0,0,0),
				'text'		=> array(255, 255, 255 ),
				'grid'		=> array(0,0,0)
			)
        );

        $cap = create_captcha($vals);
        $data = array(
                'captcha_time'  => $cap['time'],
                'ip_address'    => $this->input->ip_address(),
                'session_id'  => session_id(),
                'word'          => $cap['word']
        );
        $query = $this->db->insert_string('captcha', $data);
        $this->db->query($query);
        $data['capchar'] = $cap['image'];
        $data['header'] = "Contact Us"; 
        $data['module'] = $this->module;
		$data['page'] = $this->config->item('wconfig_template_welcome').'contact';      
    	$this->load->view($this->_container_contact,$data); 
        
        // First, delete old captchas
        $expiration = time() - 7200; // Two hour limit
        $this->db->where('captcha_time < ', $expiration)
                ->delete('captcha'); 

    }
    
    function sendcontact(){
        // Then see if a captcha exists:
        $expiration = time() - 7200; // Two hour limit
        $captcha = $this->input->post('captcha');
        $ip = $this->input->ip_address();
        $session = session_id();
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = '.$captcha.' AND session_id = '."'$session'".' AND captcha_time > '.$expiration.'';
        //$binds = array($_POST['captcha'], $expiration);
        $query = $this->db->query($sql);
        $row = $query->row();

        if ($row->count == 0)
        {
            flashMsg('warning','You must submit the word that appears in the image.');
            redirect( 'welcome/contact/','refresh');    
        }else{
            flashMsg('sussess','You massage has been send');
            redirect( 'welcome/contact/','refresh'); 
        }
        
    }
    
    function register(){
        $this->load->helper('captcha');
        $data['header'] = "Contact Us";
        $vals = array(
        'font_size'	=> 18,
        'word_length'	=> 3,
        'pool'		=> '0123456789',
        'img_path'      => './captcha/',
        'img_width'	=> '150',
	'img_height'	=> '40',
        'img_url'       => base_url().'captcha/',
        'font_path'       => './system/fonts/texa.ttf',
            'colors'	=> array(
				'background'	=> array(0,0,0),
				'border'	=> array(0,0,0),
				'text'		=> array(255, 255, 255 ),
				'grid'		=> array(0,0,0)
			)
        );

        $cap = create_captcha($vals);
        $data = array(
                'captcha_time'  => $cap['time'],
                'ip_address'    => $this->input->ip_address(),
                'session_id'  => session_id(),
                'word'          => $cap['word']
        );
        $query = $this->db->insert_string('captcha', $data);
        $this->db->query($query);
        $data['capchar'] = $cap['image'];
        $data['header'] = "Contact Us"; 
        $data['module'] = $this->module;
	$data['page'] = $this->config->item('wconfig_template_welcome').'register';      
    	$this->load->view($this->_container_contact,$data); 
        
        // First, delete old captchas
        $expiration = time() + 1200; // Two hour limit
        $this->db->where('captcha_time > ', $expiration)
                ->delete('captcha'); 
    }
    
    function insertregister(){
        //check captcha
         // Then see if a captcha exists:
        $expiration = time() - 7200; // Two hour limit
        $captcha = $this->input->post('captcha');
        $ip = $this->input->ip_address();
        $session = session_id();
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = '.$captcha.' AND session_id = '."'$session'".' AND captcha_time > '.$expiration.'';
        //$binds = array($_POST['captcha'], $expiration);
        $query = $this->db->query($sql);
        $row = $query->row();

        if ($row->count == 0)
        {
            flashMsg('warning','You must submit the word that appears in the image.');
            redirect( 'welcome/register/','refresh');    
        }
            
        
                $this->load->library('form_validation');
                $this->form_validation->set_rules('member_password', 'Password', 'required');
                $this->form_validation->set_rules('confirmpassword', 'Password Confirmation', 'required|matches[member_password]');

                if ($this->form_validation->run() == FALSE)
                {
                        $this->register();
                }
                else
                {
                   
                    $this->db->where('member_username',$this->input->post('member_username'));
                    $Q = $this->db->get('member');
                    if($Q->num_rows() > 0){
                    flashMsg('warning',"Can not add more because the username already funded. ");
                    redirect( 'welcome/register/','refresh');
                    }else{
                        
                        $this->MWelcome->inseruser();
                         
                        //select Max Member
                        $Q = $this->db->query('select * from member order by member_id desc limit 1');
                        $memberid = $Q->row_array();
                        // Send mail
$html = "
Dear Member,
You recently added ".$memberid['member_email']." as a new rescue email address for your member. To verify this email address belongs to you, click the link below and then sign in using your username and password.
Verify now ".site_url()."welcome/confirmregister/". $memberid['member_id']."/1/
Why you received this email.
Apple requests verification whenever an email address is added to an username. Your email address cannot be used without verification.
ODA Support
";
                                
                        $admin = $this->MGeneral->getgenbyid();
                        
                        $this->email->from('tica@mfa.go.th','Thailand International Cooperation Agency (TICA)');
                        $this->email->to($this->input->post('member_email'));
                        $this->email->subject('Verify email for register new account.');
                        $data['module'] = $this->module;
                        $this->email->message($html);
                        $this->email->send();
                    flashMsg('success','You have been successfully registered and Verified you account in email address (Don\'t for get Junk Folder)');
                    redirect( 'welcome/register/','refresh');
                        
                    }
                }
        //flashMsg('sussess','You massage has been send');
        //redirect( 'welcome/register/','refresh'); 
        //}
        //end check captcha
    }
    
    function checklogin(){
            $this->db->where('member_username',$this->input->post('username'));
            $this->db->where('member_password', md5($this->input->post('password')));
            $this->db->where('member_status',1);
            $Q = $this->db->get('member');
            $row = $Q->row();
            
            if (isset($row))
            {                
                //set session
                $this->session->set_userdata('memberid', $row->member_id);
                $this->session->set_userdata('userwelcome',TRUE);               
                //update last login
                $data = array(
                    'member_update'=>(date('Y-m-d H:i:s'))
                );
                $this->db->where('member_id',$this->session->userdata('memberidmemberid'));
                $this->db->update('member',$data);
                redirect( 'welcome/index','refresh');
            }else{
                flashMsg('warning','Username is invalid.');
	     	redirect( 'welcome/index','refresh');
            }
    }
    
    function logout(){
        $this->session->sess_destroy();
        redirect( 'welcome/','refresh');
    }
    
    function cannotregister(){
        $data['header'] = "Unable to enter Please Login";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'cannotregister';      
    	$this->load->view($this->_container_contact,$data); 
    }
    
    function confirmregister($memberid,$status){
        //$data['header'] = "Confirmregister";
        //$data['module'] = $this->module;
        $this->MWelcome->changestatus($memberid,$status);
        flashMsg('success','Successfully  confirmed your email subscription');
	redirect( 'welcome/index','refresh');
        //$data['page'] = $this->config->item('wconfig_template_welcome').'confirmregister';      
    	//$this->load->view($this->_container_contact,$data);
    }
    
    function typeofaid(){
        $data['header'] = "Type of Aid";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'chart';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function cooperationofcountry(){
        $data['header'] = "Cooperation of Country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'cooperationofcountry';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    
    function recipientcountry(){
        $data['header'] = "Recipient of Country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'recipientcountry';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function getrecipientcountry(){
        $data['header'] = "Recipient of Country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getrecipientcountry';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function recipientpdf(){
        $data['header'] = "Recipient of Country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'recipienpdf';         
        ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('', 'A4', 0, '', 12, 12, 10, 10, 5, 5);
        $pdf = new mPDF('th');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function recipientword(){
        $data['header'] = "Recipient of Country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'recipientword';
        $this->load->view($this->_container_result, $data);
    }
    
    function recipientexcel(){
        $data['header'] = "Recipient of Country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'recipientexcel';
        $this->load->view($this->_container_result, $data);
    }
            
    
    function getdisbursement(){
        $data['header'] = "Type of Aid";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getdisbursement';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function getproject(){
        $data['header'] = "Type of Aid";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getproject';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function getcomparative(){
        $data['header'] = "Type of Aid";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getcomparative';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function recipientregional(){
        $data['header'] = "Recipient Regional";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'recipientregional';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function recipientregionpdf(){
        $data['header'] = "Recipient Region";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'recipientregionpdf';      
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('', 'A4', 0, '', 12, 12, 10, 10, 5, 5);
        $pdf = new mPDF('th');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        
    }
    function recipienregiontword(){
        $data['header'] = "Recipient Region";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'recipientregionword';      
    	$this->load->view($this->_container_result,$data);        
    }
    
    function recipientregionexcel(){
        $data['header'] = "Recipient Region";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'recipientregionexcel';      
    	$this->load->view($this->_container_result,$data);
        
    }
    function getrecipientregional(){
        $data['header'] = "Recipient Region";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getrecipientregion';      
    	$this->load->view($this->_container_result,$data);
        
    }            
    
    function sector(){
        $data['header'] = "Sector/Purpose ";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'sector';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function getsector(){
        $data['header'] = "Sector/Purpose ";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getsectordisbursement';      
    	$this->load->view($this->_container_result,$data);
    } 
    
    function sectorpdf(){
        $data['header'] = "Sector/Purpose ";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'sectorpdf';      
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('', 'A4', 0, '', 12, 12, 10, 10, 5, 5);
        $pdf = new mPDF('th');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function sectorword(){
        $data['header'] = "Sector/Purpose";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'sectorword';      
    	$this->load->view($this->_container_result,$data);        
    }
    function sectorexcel(){
        $data['header'] = "Sector/Purpose";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'sectorexcel';      
    	$this->load->view($this->_container_result,$data);        
    }
            
    function fundingproject(){
        $data['header'] = "Funding Project";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingproject';      
    	$this->load->view($this->_container_contact,$data);
    }
    function getfundingproject(){
        $data['header'] =  "Funding Project";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getfundingproject';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function fundingprojectexecuting(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingprojectexecuting';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function projectdetail(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'projectdetail';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function projectdetailpdf(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'projectdetailpdf';      
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function projectdetailword(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'projectdetailword';      
        $this->load->view($this->_container_result, $data);       
    }
    
    function projectdetailexcel(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'projectdetailexcel';      
        $this->load->view($this->_container_result, $data);
    }
            
    function fundingprojectexecutingpdf(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingprojectexecutingpdf';      
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function fundingprojectexecutingword(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingprojectexecutingword';      
        $this->load->view($this->_container_result, $data); 
    }
    function fundingprojectexecutingexcel(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingprojectexecutingexcel';      
        $this->load->view($this->_container_result, $data); 
    }
            
    function executinggprojectlist(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'executinggprojectlist';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function executinggprojectlistpdf(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'executinggprojectlistpdf';      
        ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function executinggprojectlistword(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'executinggprojectlistword';      
        $this->load->view($this->_container_result, $data);       
    }
    function executinggprojectlistexcel(){
        $data['header'] = "Executing Agency Fiscal year";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'executinggprojectlistexcel';      
        $this->load->view($this->_container_result, $data);       
    }
            
    function fundingpdf(){
        $data['header'] =  "Funding Project";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingpdf';      
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    function fundingword(){
        $data['header'] =  "Funding Project";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingword';      
        $this->load->view($this->_container_result, $data);       
    }
    function fundingexcel(){
        $data['header'] = "Funding Project";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingexcel';      
    	$this->load->view($this->_container_result,$data);
    }
            
    function fundingsector(){
        $data['header'] = "Funding Sector";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingsector';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    
    function getfundingsector(){
        $data['header'] = "Funding Sector";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getfundingsector';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function fundingsectorpdf(){
        $data['header'] = "Funding Sector";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingsectorpdf';      
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function fundingsectorword(){
        $data['header'] = "Funding Sector";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingsectorword';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function fundingsectorexcel(){
        $data['header'] = "Funding Sector";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingsectorexcel';      
    	$this->load->view($this->_container_result,$data);
    }
            
    function fundingexcuting(){
        $data['header'] = "Funding Excuting";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingexcuting';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function fundingexcutingpdf(){
        $data['header'] = "Funding Excuting";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingexcutingpdf';      
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function fundingexcutingword(){
        $data['header'] = "Funding Excuting";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingexcutingword';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function fundingexcutingexcel(){
        $data['header'] = "Funding Excuting";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingexcutingexcel';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function getfundingexcuting(){
        $data['header'] = "Funding Excuting";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getfundingexcuting';      
    	$this->load->view($this->_container_result,$data);
    }
            
    function fundrecipientcountry(){
        $data['header'] = "Recipient country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundrecipientcountry';      
    	$this->load->view($this->_container_contact,$data);
    }
    
    function getfundingrecipientcountry(){
        $data['header'] = "Recipient country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'getfundingrecipientcountry';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function fundingrecipientpdf(){
        $data['header'] = "Recipient country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingrecipientpdf';      
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_result, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function fundingrecipientword(){
        $data['header'] = "Recipient country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingrecipientword';      
    	$this->load->view($this->_container_result,$data);
    }
    
    function fundingrecipientexcel(){
        $data['header'] = "Recipient country";
        $data['module'] = $this->module;
        $data['page'] = $this->config->item('wconfig_template_welcome').'fundingrecipientexcel';      
    	$this->load->view($this->_container_result,$data);
    }
	
	function policy(){
		 $data['header'] = "Terms of use privacy policy";
		 $data['policy'] = $this->MPolicy->getPolicy();
        $data['module'] = $this->module;		
		$data['page'] = $this->config->item('wconfig_template_welcome').'policy';      
    	$this->load->view($this->_container_contact,$data); 
	}
    

 
}