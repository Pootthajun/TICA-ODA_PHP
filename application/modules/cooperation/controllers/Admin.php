<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Cooperation Framework  Table";
        $data['all'] = $this->MCooperation->listallCooperation();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addcooperation(){
        $data['header'] = "Cooperation table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addcooperation';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertcooperation(){
        $this->MCooperation->insertcooperation();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'cooperation/admin/','refresh');
    }
    
    function editcooperation($cooperationid){
        $data['header'] = "Edit Cooperation Framwork";
        $data['edit'] = $this->MCooperation->getcooperationbyid($cooperationid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editcooperation';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updatecooperation(){
        $this->MCooperation->updatecooperation();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'cooperation/admin/','refresh');;
    }
    
    function deletecooperation($cooperationid){
        $query = $this->db->get_where('project', array('cooperation_id' => $cooperationid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'cooperation/admin/','refresh');            
        }else{
            $this->MCooperation->deletecooperation($cooperationid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'cooperation/admin/','refresh');
        }
    }
}