<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MCooperation extends CI_Model{
    
    function insertCooperation(){
        $data = array(
            'cooperation_name'=>$this->input->post('cooperation_name'),
            'cooperation_detail'=>$this->input->post('cooperation_detail'),  
            'cooperation_by'=>$this->session->userdata('userid'), 
            'cooperation_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('cooperation',$data);
        
    }
    
    function listallCooperation(){
        $data = array();
        $this->db->order_by('cooperation_id',"ASC");
        $Q = $this->db->get('cooperation');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getCooperationbyid($cooperation){
        //$data = array();
        $this->db->where('cooperation_id',$cooperation);
        $Q = $this->db->get('cooperation');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updateCooperation(){
        $data = array(
            'cooperation_name'=>$this->input->post('cooperation_name'),  
            'cooperation_detail'=>$this->input->post('cooperation_detail'),  
            'cooperation_by'=>$this->session->userdata('userid'), 
            'cooperation_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('cooperation_id',$this->input->post('cooperation'));
        $this->db->update('cooperation',$data);
    }
    
    function deleteCooperation($cooperation){              
           $this->db->where('cooperation_id',$cooperation);
           $this->db->delete('cooperation');       
    }
    

}