<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// ---------------------------------------------------------------------------

$lang['status_type_info'] = "Info";
$lang['status_type_warning'] = "Warning";
$lang['status_type_error'] = "Error";
$lang['status_type_success'] = "Success";

$lang['insert_success'] = "Successfully";
$lang['delete_success'] = "Delete data success";
$lang['login_success'] = "Welcome to Admintrator";
$lang['user_cannotfound'] = "Can't save or use saved usernames and passwords";
$lang['cannot_delete'] = "Can't Delete Data";
$lang['update_success'] = "Update data success";

/* End of file status_lang.php */
/* Location: ./modules/status/language/english/status_lang.php */