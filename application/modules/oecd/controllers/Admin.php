<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Oecd Table";
        $data['all'] = $this->MOecd->listallOecd();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addOecd(){
        $data['header'] = "Oecd table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addoecd';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertOecd(){
        $this->MOecd->insertoecd();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'oecd/admin/','refresh');
    }
    
    function editoecd($oecdid){
        $data['header'] = "Edit Oecd name";
        $data['edit'] = $this->MOecd->getoecdbyid($oecdid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editoecd';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateOecd(){
        $this->MOecd->updateOecd();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'oecd/admin/','refresh');;
    }
    
    function deleteOecd($Oecdid){
        $query = $this->db->get_where('project', array('oecd_id' => $Oecdid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'oecd/admin/','refresh');            
        }else{
            $this->MOecd->deleteOecd($Oecdid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'oecd/admin/','refresh');
        }
    }
}