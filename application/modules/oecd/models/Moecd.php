<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MOecd extends CI_Model{
    
    function insertoecd(){
        $data = array(
            'oecd_name'=>$this->input->post('oecd_name'), 
            'oecd_by'=>$this->session->userdata('userid'), 
            'oecd_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('oecd',$data);
        
    }
    
    function listalloecd(){
        $data = array();
        $this->db->order_by('oecd_id',"ASC");
        $Q = $this->db->get('oecd');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getoecdbyid($oecd){
        //$data = array();
        $this->db->where('oecd_id',$oecd);
        $Q = $this->db->get('oecd');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updateoecd(){
        $data = array(
            'oecd_name'=>$this->input->post('oecd_name'),   
            'oecd_by'=>$this->session->userdata('userid'), 
            'oecd_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('oecd_id',$this->input->post('oecd'));
        $this->db->update('oecd',$data);
    }
    
    function deleteoecd($oecd){              
           $this->db->where('oecd_id',$oecd);
           $this->db->delete('oecd');       
    }
    

}