<?php  
class Admin extends Admin_Controller{
	function __construct(){
	parent::__construct(); 
        $this->module=  basename(dirname(dirname(__FILE__)));
	
    }
	function index(){
		$data['header'] = "pages";
		$data['edit'] = $this->MPage->getpagesbyid();
        $data['page'] = $this->config->item('wconfig_template_admin').'editpage';
        $this->load->view($this->_container_admin,$data); 
	}

	function updatepage(){
		$this->MPage->updatepage();
	    flashMsg('success',$this->lang->line('updatesuccess'));
	    redirect('pages/admin/index','refresh');
	}

}