<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MSubsector extends CI_Model{
    
    function insertsubsector(){
        $data = array(
            'subsector_name'=>$this->input->post('subsector_name'), 
			'sector_id'=>$this->input->post('sector_id'), 
			'subsector_dac5code'=>$this->input->post('subsector_dac5code'), 
			'subsector_crscode'=>$this->input->post('subsector_crscode'), 
			'subsector_detail'=>$this->input->post('subsector_detail'), 
            'subsector_by'=>$this->session->userdata('userid'), 
            'subsector_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('subsector',$data);
        
    }
    
    function listallsubsector(){
        $data = array();
        $this->db->order_by('subsector_id',"ASC");
        $Q = $this->db->get('subsector');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getsubsectorbyid($subsector){
        //$data = array();
        $this->db->where('subsector_id',$subsector);
        $Q = $this->db->get('subsector');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updatesubsector(){
        $data = array(
            'subsector_name'=>$this->input->post('subsector_name'),   
			'sector_id'=>$this->input->post('sector_id'), 
			'subsector_dac5code'=>$this->input->post('subsector_dac5code'), 
			'subsector_crscode'=>$this->input->post('subsector_crscode'), 
			'subsector_detail'=>$this->input->post('subsector_detail'), 
            'subsector_by'=>$this->session->userdata('userid'), 
            'subsector_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('subsector_id',$this->input->post('subsector'));
        $this->db->update('subsector',$data);
    }
    
    function deletesubsector($subsector){              
           $this->db->where('subsector_id',$subsector);
           $this->db->delete('subsector');       
    }
    

}