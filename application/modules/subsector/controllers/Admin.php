<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Subsector Table";
        $data['all'] = $this->MSubsector->listallsubsector();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addSubsector(){
        $data['header'] = "Subsector table";
		$data['all'] = $this->MSector->listallSector();
    	$data['page'] = $this->config->item('wconfig_template_admin').'addsubsector';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertSubsector(){
        $this->MSubsector->insertsubsector();
        flashMsg('success',$this->lang->line('insert_success'));
	    redirect( 'subsector/admin/','refresh');
    }
    
    function editsubsector($subsectorid){
        $data['header'] = "Edit Subsector name";
		$data['all'] = $this->MSector->listallSector();
        $data['edit'] = $this->MSubsector->getsubsectorbyid($subsectorid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editsubsector';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateSubsector(){
        $this->MSubsector->updateSubsector();
    	flashMsg('success',$this->lang->line('update_success'));
	    redirect( 'subsector/admin/','refresh');
    }
    
    function deleteSubsector($Subsectorid){
        $query = $this->db->get_where('project', array('subsector_id' => $Subsectorid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'subsector/admin/','refresh');            
        }else{
            $this->MSubsector->deleteSubsector($Subsectorid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'subsector/admin/','refresh');
        }
    }
}