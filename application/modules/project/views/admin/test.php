<div class="container" role="main">
    <!-- Contents of the popover associated with the task name text input -->
    
    <form class="form-horizontal" method="post" action="" id="taskForm">
        <div class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <label for="taskName" class="control-label col-md-1">Name</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control taskNameValidation" id="taskName" name="taskName" autofocus required data-toggle="popover">
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <label for="taskDataset" class="col-md-1 control-label">Dataset</label>
                    <div class="col-md-11">
                        <select class="form-control chosen-select taskDatasetValidation" data-placeholder="Choose a dataset" name="taskDataset" id="taskDataset" required>
                            <option value=""></option>
                            <option value="runnableDataset_Id1">Dataset 1</option>
                            <option value="runnableDataset_Id2">Dataset 2</option>
                            <option value="runnableDataset_Id3">Dataset 3</option>
                            <option value="runnableDataset_Id4">Dataset 4</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="form-group">
                    <label for="taskDescription" class="col-md-1 control-label">Description</label>
                    <div class="col-md-11">
                        <textarea class="form-control ignoreEmpty" name="taskDescription" id="taskDescription" maxlength="1000" rows="4"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="pull-right top-margin">
                    <input type="submit" class="btn btn-primary btn-sm" value="Submit" name="taskSetUpSubmit">
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </form>
  <script>
			jQuery(document).ready(function() {  
    //Bootstrap popover
/*$('[data-toggle="popover"]').popover({
    trigger: "hover focus",
    container: "body",
    placement: "bottom",
    html: true,
    title: "Name Tips",
    content: function () {
        return $('#namePopoverContent').html();
    }
});*/

//Chosen select plugin
$(function () {
    $('.chosen-select').chosen();
    $('.chosen-select-deselect').chosen({
        allow_single_deselect: true
    });
});

//JQuery validate plugin
$(function () {
    $.validator.setDefaults({
       // errorClass: 'text-danger',
        ignore: ':hidden:not(.chosen-select)',
        errorPlacement: function (error, element) {
            if (element.hasClass('chosen-select')) error.insertAfter(element.siblings(".chosen-container"));
            else {
                error.insertAfter(element);
            }
        }
    });

    //rules and messages objects
    $("#taskForm").validate({
        highlight: function (element) {
            if ($(element).hasClass('chosen-select')) {
                $(element).siblings('.chosen-container').removeClass('green-select').addClass('red-select');
            }
            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            }
        },
        unhighlight: function (element) {
            if ($(element).hasClass('chosen-select')) {
                $(element).siblings('.chosen-container').removeClass('re-select').addClass('green-select');
            }
            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            }
        }
    });

    $('.taskNameValidation').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Provide a space-separated name"
            }
        });
    });

    $('.taskDatasetValidation').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Choose a dataset"
            }
        });
    });

    $('.chosen-select').on('change', function () {
        $(this).valid();
    });

});

});
</script>