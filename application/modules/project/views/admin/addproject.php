<style>
#country_id{
	width:410px;
	margin-left: 5px;
}
#kind_id{
	width:440px;
	padding-left: 5px;
}
select{
	width:420px;
}
</style>


  <form id="form1" name="form1" method="post" action="<?php echo site_url(); ?>project/admin/insertproject/">
    <p>
	  <label>Plan name</label>
	 <?php echo $planname['plan_name']; ?>
	 <input type="hidden" name="planid" id="planid" value="<?php echo $planname['plan_id']; ?>" />
         </p>
      <p>
        <label> Project type</label>
   
<?php echo $ptname['project_type_name']; ?>
    <input type="hidden" name="projecttype" id="projecttype"  value="<?php echo $ptname['project_type_id']; ?>"/>
    </p>
    
    <script>
  $(document).ready(function(){
    $("#addbt").click(function(){ 
	  $.ajax({
       url: "<?php echo base_url(); ?>project/admin/listbtadd/",
       success: function(html) {
          $("#Tbudget").append(html);
       }
    });	  
    });    
    $("#Tbudget").on('click', '.rebt', function(){
        $(this).parent().parent().remove();
    });
    
});
</script> 
<div style="background:#F9F9F9; padding-left:5px; padding-top:5px;" > 
<label>Budget Type</label>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="Tbudget">
      <tr>
        <td width="60%"><strong>Budget Type Name</strong> &nbsp;
         <select name="bt_id[]" id="bt_id[]" class="chosen-select " data-placeholder="Choose Budget Type...">
      <option value=""></option>
       <?php foreach($bgtype as $listbgtype){ ?>
      <option value="<?php echo $listbgtype['bt_id']; ?>"><?php echo $listbgtype['bt_name']; ?></option>
      <?php } ?>
      </select></td>
        <td width="32%"><strong>Budget</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="text" name="budget_money[]" id="budget_money[]" class="text-input m-input number" ></td>
        <td width="8%"><input type="button" name="addbt" id="addbt" value="add" class="button green" /></td>
      </tr>
    </table>
    </div>
         <p>
           <label for="project_name">Project name</label>
           <input type="text" name="project_name" id="project_name" class="text-input large-input" required>
         </p>
         
         
      <p>
           <label for="project_object">Project Objective</label>
        <textarea name="project_object" cols="45" rows="5" required="required"  id="project_object"></textarea>
         </p>
      <p>
        <label for="project_detail">Project Detail</label>
        <label for="project_detail"></label>
        <textarea name="project_detail" id="project_detail" cols="45" rows="5"></textarea>
         </p>
         <p>
           <label>Project Officer</label>
           <select name="project_owner" id="project_owner" data-placeholder="Choose Project Owner..." class="chosen-select"  required>
           <option value=""></option>
           <?php foreach($user as $listuser){?>
             <option value="<?php echo $listuser['user_id']; ?>"><?php echo $listuser['user_fullname']; ?></option>
             <?php } ?>
           </select>
    </p>
    
    <script type="text/javascript">
$(document).ready(function()
{
$("#project_owner").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/listprojectassistant/",
data: dataString,
cache: false,
success: function(html)
{
$("#getprojectassistant").html(html);
} 
});
});
});
</script>
    
    
    
             <p>
           <label>Project  Assistant</label>
           <div id="getprojectassistant">
           <select name="project_assistant[]" id="project_assistant" data-placeholder="Choose a Project Assistant..." class="chosen-select required"   multiple>
            </select>
            
    
           </div>
    </p>

         <div style="width:50%; float:left; ">
         <p>
           <label for="project_start">Project Start Date</label>
           <input type="text" name="project_start" id="datepicker1" class="text-input medium-input" required>
           </p>
         </div>
       
            <div style="width:50%; float:right;">
            <p>
           <label for="project_end">Project End Date</label>
           <input type="text" name="project_end" id="datepicker2" class="text-input medium-input" required>
           </p>
           </div>
    

           <p>
           <label for="project_budget">Project Budget</label>
           <input type="text" name="project_budget" id="project_budget" class="text-input medium-input required number">
         </p>
           <p>
           <label for="project_file">Project File</label>
           <input name="project_file" type="file" class="text-input medium-input" id="project_file">
          
         </p>

         
                    <p>
           <label for="project_comment">Project Comment</label>
               <input type="text" name="project_comment" id="project_budget" class="text-input medium-input" >
          
         </p>
  
    <p>
      <label for="sector_id">Sector</label>
      <select name="sector_id" id="sector_id"  class="required chosen-select" data-placeholder="Choose Sector..." >
      <option value=""></option>
      <?php foreach($sector as $listsector){ ?>
      <option value="<?php echo $listsector['sector_id']; ?>"><?php echo $listsector['sector_name']; ?></option>
      <?php } ?>
      </select>        
      </p>
    <script type="text/javascript">
$(document).ready(function()
{
$("#sector_id").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
//alert($(this).val());
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/listsubsector/",
data: dataString,
cache: false,
success: function(html)
{
$("#getsubsector").html(html);
} 

});

});


});
</script>
    <p>
    <!--<div id="subsector_id"></div>-->
      <label for="subsector_id">Sub Sector</label>
         <div id="getsubsector">
           <select name="subsector_id" id="subsector_id" class="chosen-select required" data-placeholder="Choose Sub Sector..."  >
         </select>   
         </div>         
      </p>

    <p>
      <label for="cooperation_id">Cooperation Framework</label>
      <select name="cooperation_id" id="cooperation_id" class="chosen-select" data-placeholder="Choose Cooperation Framework..." >
      <option value=""></option>
       <?php foreach($coop as $listcoop){ ?>
      <option value="<?php echo $listcoop['cooperation_id']; ?>"><?php echo $listcoop['cooperation_name']; ?></option>
      <?php } ?>
      </select>

    </p>
    <p>
      <label for="cooperation_type_id">Cooperation Type</label>
      <select name="cooperation_type_id" id="cooperation_type_id" class="chosen-select" data-placeholder="Choose Cooperation Type..." >
      <option value=""></option>
       <?php foreach($cooptype as $listcooptype){ ?>
      <option value="<?php echo $listcooptype['cooperation_type_id']; ?>"><?php echo $listcooptype['cooperation_type_name']; ?></option>
      <?php } ?>
      </select>
    </p>
    <p>
      <label for="oecd_id">OECD Aid Type</label>
      <select name="oecd_id" id="oecd_id" class="chosen-select" data-placeholder="Choose OECD Aid Type.." >
      <option value=""></option>
       <?php foreach($oecd as $listoecd){ ?>
      <option value="<?php echo $listoecd['oecd_id']; ?>"><?php echo $listoecd['oecd_name']; ?></option>
      <?php } ?>
      </select>
    </p>
    <script>
  $(document).ready(function(){
    $("#addkind").click(function(){ 
	  $.ajax({
       url: "<?php echo base_url(); ?>project/admin/listkind/",
       success: function(html) {
          $("#Taddkind").append(html);
       }
    });	  
    });    
    $("#Taddkind").on('click', '.rekind', function(){
        $(this).parent().parent().remove();
    });
    
});
</script><script type="text/javascript">
$(document).ready(function()
{
$("#funding_id").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
//alert($(this).val());
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/getExecuting/",
data: dataString,
cache: false,
success: function(html)
{
$("#getexecuting").html(html);
} 
});


});
});
</script>

  <p>
    <label>Funding Agency</label>
               <select name="funding_id" id="funding_id" class="chosen-select" data-placeholder="Choose Funding Agency..." >
      <option value=""></option>
       <?php foreach($funding as $listfunding){ ?>
      <option value="<?php echo $listfunding['funding_id']; ?>"><?php echo $listfunding['funding_name']; ?></option>
      <?php } ?>
      </select>   
         </p>
           <p>
           <label for="executing_id">Executing Agency</label>
           
           <div id="getexecuting">
                 <select name="executing_id" id="executing_id" class="chosen-select" data-placeholder="Choose Executing Agency.." >
                  <option value=""></option>
                  </select>   
           </div>
     
         </p>
         
    <p>
           <label>Implementing Agency</label>
                      <div id="getimplement">
                            <select name="implement_id" id="implement_id" class="chosen-select" data-placeholder="Choose Implementing Agency.." >
                          <option value=""></option>
                            <?php    foreach($im as $listex){
   					echo "<option value=\"".$listex['implement_id']."\">".$listex['implement_name']."</option>";
              }
			  ?>
                          </select> 
           </div>

      </p>


           
              
  <script>
  $(document).ready(function(){
    $("#addCF").click(function(){ 
	  $.ajax({
       url: "<?php echo base_url(); ?>project/admin/listcountry/",
       success: function(html) {
          $("#customFields").append(html);
       }
    });	  
    });    
    $("#customFields").on('click', '.remCF', function(){
        $(this).parent().parent().remove();
    });
    
});
</script>     
<div style="background:#F9F9F9; padding-left:5px; padding-top:5px;" >
<label>Co-Funding</label>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="customFields">
  <tr>
    <td width="57%"><strong>Country</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <select name="country_id[]" id="country_id" class="chosen-select" data-placeholder="Choose a Conutry Name...">
        <option value=""></option>
  <?php   foreach($country as $list){ ?>
  <option value="<?php echo $list['country_id']; ?>"><?php echo $list['country_name']; ?></option>
  
  <?php } ?>
</select></td>
    <td width="38%"> <strong>Budget</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="text" name="co_budget[]" id="project_budget" class="text-input small-input number" ></td>
    <td width="5%"><input type="button" name="button2" id="addCF" value="add" class="button green" /></td>
  </tr>

</table>
</div>



    <script>
  $(document).ready(function(){
	  var id = 0;
    $("#addDis").click(function(){ 
	dataString =  "add="+id++;
	  $.ajax({
	  type: "POST",
       url: "<?php echo base_url(); ?>project/admin/listDisbureseme/",
	   data: dataString,
       success: function(html) {
          $("#Tdis").append(html);
       }
    });	  
    });    
    $("#Tdis").on('click', '#redis', function(){
        $(this).parent().parent().remove();
    });
	
   $( ".datedis" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
});
</script>
<div style="background:#F1F1F1; padding-left:5px; padding-top:5px;">
<label>Disbureseme</label>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="Tdis">
  <tr>
    <td width="22%"><strong> Titile </strong>
      <select name="title[]" id="title" class="chosen-select" data-placeholder="Choose a Title..." style="width:150px;">
       <option value=""></option>
        <option value="1" selected="selected">Grant</option>
        
      </select></td>
    <td width="30%"><strong>Country </strong>
      <select name="discountry[]" id="discountry[]" class="chosen-select" data-placeholder="Choose a Conutry Name..." style="width:200px;">
        <option value=""></option>
        <?php   foreach($country as $list){ ?>
        <option value="<?php echo $list['country_id']; ?>"><?php echo $list['country_name']; ?></option>
        <?php } ?>
        </select></td>
    <td width="25%"><strong>Budget</strong>
      <input type="text" name="dis_budget[]" id="dis_budget[]" class="text-input number" style="width:70%" /></td>
    <td width="20%"><strong>Date </strong>
      <input type="text" name="disdate[]" id="datepicker8" class="text-input"  style="width:70%" /></td>
    <td width="3%" bgcolor="#F1F1F1"><input type="button" name="addDis" id="addDis" value="add" class="button green" /></td>
  </tr>
  </table>
</div>

        
         
    <p>
      <label>Contact Person</label>
        <input type="text" name="contact_name" id="project_budget" class="text-input medium-input" >          
      </p>
    <p>
      <label>Contact Position </label>
        <input type="text" name="contact_position" id="project_budget" class="text-input medium-input" >          
      </p>
    <p>
      <label >Contact Email </label>
      <input type="text" name="contact_email" id="project_budget" class="text-input medium-input" >          
      </p>
    <p>
      <label >Contact Tel </label>
      <input type="text" name="contact_tel" id="project_budget" class="text-input medium-input" >          
      </p>
    <p>
      <label>Contact Fax </label>
      <input type="text" name="contact_fax" id="project_budget" class="text-input medium-input" >          
      </p>
      
          <p>
      <label>Project Status </label>
 
      <input name="project_status" type="radio" id="project_status" value="0" checked="checked" />
      Running
      <input name="project_status" type="radio" id="project_status" value="1" />
      Complete</p>
         
           <p>
                                  <input type="submit" name="submit" id="submit" value="Save" class="button green">
    </p>
         
  </form>


