<style>
#country_id{
	width:410px;
	margin-left: 5px;
}
#kind_id{
	width:440px;
	padding-left: 5px;
}
select{
	width:420px;
}

</style>
<style type="text/css">
#main-content 	 table td,
#main-content table th {
                padding: 5px;
                line-height: 1.3em;
                }
</style>
  <form action="<?php echo site_url(); ?>project/admin/updateproject/" method="post" enctype="multipart/form-data" name="form1" id="form1" class="form1">

    <p>
	  <label>Plan&nbsp;Name: &nbsp;&nbsp;<?php echo $planname['plan_name']; ?></label>

	 <input type="hidden" name="planid" id="planid" value="<?php echo $planname['plan_id']; ?>" />
         </p>
      <p>
        <label> Project&nbsp;type: &nbsp;&nbsp;<?php echo $ptname['project_type_name']; ?></label>


    <input type="hidden" name="projecttype" id="projecttype"  value="<?php echo $ptname['project_type_id']; ?>"/>
    </p>
    <script>
  $(document).ready(function(){
    $("#addbt").click(function(){
	  $.ajax({
       url: "<?php echo base_url(); ?>project/admin/listbtadd/",
       success: function(html) {
          $("#Tbudget").append(html);
       }
    });
    });

   $("#addkt").click(function(){
	  $.ajax({
       url: "<?php echo base_url(); ?>project/admin/listktadd/",
       success: function(html) {
          $("#Tbudget").append(html);
       }
    });
    });

    $("#Tbudget").on('click', '.rebt', function(){
        $(this).parent().parent().remove();
    });

	$(".trashbt").click(function(){

   var del_id = $(this).attr('id');
   var dataString = 'id='+ del_id;
   var rowElement = $(this).parent().parent(); //grab the row
  $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>project/admin/delbudgettype/",
            data: dataString,
			success: function(){
			//	 $(this).parent().parent().remove();

			}
	});
			$(this).parents(".recordbt").animate({ backgroundColor: "#fbc7c7" }, "fast")
			.animate({ opacity: "hide" }, "slow");
});
	$(".trashbtt").click(function(){

   var del_id = $(this).attr('id');
   var dataString = 'id='+ del_id;
   var rowElement = $(this).parent().parent(); //grab the row
  $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>project/admin/deletekindproject/",
            data: dataString,
			success: function(){
				// $(this).parent().parent().remove();

			}
	});
			$(this).parents(".recordbt").animate({ backgroundColor: "#fbc7c7" }, "fast")
			.animate({ opacity: "hide" }, "slow");
});

});
</script>
<div style="background:#F9F9F9; padding-left:5px; padding-top:5px;" >
<label>Budget&nbsp;Type</label>

<?php /*$_GET['bid'];
		echo $_GET['bid'];
	*/?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="Tbudget">
    <?php foreach($bt as $listbt){?>
      <tr class="recordbt">
        <td><strong>Budget Type Name</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php
		 $this->db->where('bt_id',$listbt['bt_id']);
		 $QB=$this->db->get('budget_type');
		 $nametb = $QB->row();
		 echo $nametb->bt_name;
		 ?></td>
        <td><strong>Budget </strong></td>
        <td align="right"><?php echo number_format($listbt['budget_money'],2,'.',','); ?>&nbsp;&nbsp;&nbsp;THB.</td>
        <td>&nbsp;<a href="javascript:void(0);" class="button red trashbt" id="<?php echo $listbt['b_id']; ?>" onclick="confirm('Sure you want to delete this update? There is NO undo! ')">Del</a></td>
      </tr>
      <?php } ?>
      <tr>
        <td width="60%"><strong>Budget Type Name</strong> &nbsp;
          <select name="bt_id[]" id="bt_id[]" class="chosen-select" data-placeholder="Choose budget Type...">
            <option value=""></option>
            <?php foreach($bgtype as $listbgtype){ ?>
            <option value="<?php echo $listbgtype['bt_id']; ?>"><?php echo $listbgtype['bt_name']; ?></option>
            <?php } ?>
        </select></td>
        <td width="6%"><strong>Budget</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td width="26%" align="right"><input type="text" name="budget_money[]" id="budget_money[]" class="text-input m-input number" />THB.</td>
        <td width="8%"><input type="button" name="addbt" id="addbt" value="add" class="button green" /></td>
      </tr>
    </table>

</div>

	<br />
	<br />
<div style="background:#F9F9F9; padding-left:5px; padding-top:5px;" >
	<label>Kind&nbsp;Type  </label>
<table width="100%" border="100" cellspacing="0" cellpadding="0" id="Tbudget">
    <?php foreach($kt as $listkt){?>
      <tr class="recordbt">
        <td><strong>Kind Type Name</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php
		 $this->db->where('kind_type_id',$listkt['inkind_id']);
		 $QB=$this->db->get('kind_type');
		 $nametb = $QB->row();
		 echo $nametb->kind_type_name;
		// echo $nametb->kind_type_id;

		 ?></td>
        <td><strong>Budget</strong></td>
        <td align="right"><?php echo number_format($listkt['kind_estimated'],2,'.',','); ?>&nbsp;&nbsp;&nbsp;THB.</td>
        <td>&nbsp;<a href="javascript:void(0);" class="button red trashbtt" id="<?php echo $listkt['id']; ?>" onclick="confirm('Sure you want to delete this update? There is NO undo! ')">Del</a></td>
        <td> </td>
		<tr> <label>Kind Description</label>  
        <textarea name="description" id="description" cols="45" rows="5"><?php echo $listkt['description']; ?></textarea></tr>
      </tr>
	  <tr>
	
	  </tr>
      <?php 
	  
	  
	  } ?>
      <tr>
	  
        <td width="60%"><strong>Kind Type Name</strong> &nbsp;
          <select name="kind_type_id[]" id="kind_type_id[]" class="chosen-select" data-placeholder="Choose kind Type...">
            <option value=""></option>
            <?php foreach($kindtype as $listkindtype){ ?>
            <option value="<?php echo $listkindtype['kind_type_id']; ?>"><?php echo $listkindtype['kind_type_name']; ?></option>
            <?php } ?>
        </select></td>
        <td width="6%"><strong>Budget</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td width="26%" align="right"><input type="text" name="kindtype_money[]" id="kindtype_money[]" class="text-input m-input number" />
&nbsp;&nbsp;          THB.</td>
        <td width="8%"><input type="button" name="addkt" id="addkt" value="add" class="button green" /></td>
      </tr>
	  <tr>
			<td>
			<label>Kind Description</label>  
        <textarea name="description" id="description" cols="45" rows="5"><?php  ?></textarea>
			</td>
	  </tr>
	  <tr>
	 
	  </tr>
</table>

</div>


         <p>
           <label for="">Project&nbsp;title</label>  <!-- projectname -->
           <input name="project_name" type="text" required class="text-input large-input" id="project_name" value="<?php echo $edit['project_name']; ?>">
         </p>


      <p>
           <label for="">Objectives</label>  <!-- Project Objective -->
        <textarea name="project_object" cols="45" rows="5" required="required"  id="project_object"><?php echo $edit['project_object']; ?></textarea>
         </p>
      <p>
        <label for="">Description</label>  <!-- Project Detail -->
        <label for="project_detail"></label>
        <textarea name="project_detail" id="project_detail" cols="45" rows="5"><?php echo $edit['project_detail']; ?></textarea>
         </p>
         <p>
       <label>Project&nbsp;Officer</label>

           <select name="project_owner" id="project_owner" class="required chosen-select"  style="width:520px;" data-placeholder="Choose Project owner...">
           <option value=""></option>
           <?php

		   foreach($user as $listuser){?>


             <option value="<?php echo $listuser['user_id']; ?>"
            		 <?php
                      // for($i=0;$i<count($ex);$i++){ if($listuser['user_id'] ==$edit['']){echo "selected ='selected '";} }
					  if($listuser['user_id'] ==$edit['project_owner']){echo "selected ='selected '";} ?>
             >
			 <?php echo $listuser['user_fullname']; ?>
             </option>
             <?php } ?>
           </select>
    </p>
<script type="text/javascript">
$(document).ready(function()
{
$("#project_owner").change(function()
{
var id=$(this).val();
//var dataString = 'id='+ id;
var assistant='<?php echo $edit['project_assistant'];?>';
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/listprojectassistantedit/",
//data: dataString,
data:  {"owner": id, "assistant": assistant},
cache: false,
success: function(html)
{
$("#getprojectassistant").html(html);
}
});
});
});
</script>



             <p>
           <label>ผู้ช่วยโปรเจค</label>  <!-- Project Assistant -->
           <div id="getprojectassistant">
           <?php //$ex = explode(",",$edit['project_assistant']); ?>
           <select name="project_assistant[]" id="project_assistant" data-placeholder="Choose a Project Assistant..." class="required chosen-select"   multiple>
           <?php
		$Q = $this->db->query("select * from project_assistant
		inner join user on project_assistant.user_id = user.user_id
		where project_assistant.project_id = '".$edit['project_id']."'
		");

        //$this->db->where('project_id',$edit['project_id']);
       // $Q = $this->db->get('project_assitant');
        foreach($projectassistant as $listuseras){
			   ?>
			   <option value="<?php echo $listuseras['user_id']; ?>"

			   <?php  foreach($Q->result_array() as $ex){ if($listuseras['user_id'] ==$ex['user_id']){echo "selected ='selected '";} } ?>

               ><?php echo $listuseras['user_fullname']; ?></option>
             <?php } ?>


            </select>
           </div>
    </p>
<div style="width:50%; float:left; ">
         <p>
           <label for="">Project&nbsp;Start&nbsp;Date</label>
           <input name="project_start" type="text" required class="text-input medium-input" id="datepicker1" value="<?php echo $edit['project_start']; ?>">
         </p>
         </div>
          <div style="width:50%; float:right;">
           <p>
           <label for="">Project&nbsp;End&nbsp;Date</label>
           <input name="project_end" type="text" required class="text-input medium-input" id="datepicker2" value="<?php echo $edit['project_end']; ?>">
         </p>
         </div>
           <p>
           <label for="">Allocated&nbsp;Budget</label>  <!--Project budget -->
           <input name="project_budget" type="text"  class="text-input medium-input number" id="project_budget" value="<?php echo $edit['project_budget']; ?>">
         THB.      </p>
<p>
  <script>
  $(document).ready(function(){
    $("#addF").click(function(){
	  $.ajax({
       url: "<?php echo base_url(); ?>project/admin/fileuploadlist/",
       success: function(html) {
          $("#Fdis").append(html);
       }
    });
    });
    $("#Fdis").on('click', '#reF', function(){
        $(this).parent().parent().remove();
    });

  $(".delfile").click(function(){
   var del_id = $(this).attr('id');
   var dataString = 'id='+ del_id;
   var rowElement = $(this).parent().parent(); //grab the row
//alert(del_id );
  $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>project/admin/delfileproject/",
            data: dataString,
			success: function(){
			//	 $(this).parent().parent().remove();
			}
		});
			$(this).parents(".removefile").animate({ backgroundColor: "#fbc7c7" }, "fast")
			.animate({ opacity: "hide" }, "slow");
	    });

});
</script>
<label>Attached&nbsp;Files</label> <!--Attached Files-->
           <table width="500" border="0" cellspacing="0" cellpadding="0"  id="Fdis">
           <?php
		   $count =1;
		   foreach($file as $listfile){
		   ?>
              <tr class="removefile">
               <td><?php echo $count++; ?>.&nbsp;&nbsp;<a href="<?php echo base_url(); ?>upload/project/<?php echo $listfile['project_file']; ?>" target="_blank"><?php echo $listfile['project_file']; ?></a></td>
               <td><a href="javascript:void(0);" class="button red delfile" id="<?php echo $listfile['file_id']; ?>" onclick="confirm('Sure you want to delete this update? There is NO undo! ')">Del</a></td>
			   <td></td
			 </tr>
             <?php } ?>
               <tr >
               <td width="283"><input name="files[]" type="file"  id="files[]" multiple="multiple"></td>
               <td width="51"><input type="button" name="button" id="addF" value="add" class="button green" /></td>
             </tr>
           </table>
           </p>


                    <p>
           <label for="">Project&nbsp;Comment</label>
               <input name="project_comment" type="text" class="text-input medium-input" id="project_comment" value="<?php echo $edit['project_comment']; ?>" >

    </p>





    <p>
      <label for="sector_id">Sector</label>
      <select name="sector_id" id="sector_id"  class="chosen-select" data-placeholder="Choose Sector..." >
          <option value="" ></option>
      <?php foreach($sector as $listsector){ ?>
      <option value="<?php echo $listsector['sector_id']; ?>" <?php if($listsector['sector_id'] == $edit['sector_id']){ echo 'selected="selected"';} ?>><?php echo $listsector['sector_name']; ?></option>
      <?php } ?>
      </select>
      </p>
    <script type="text/javascript">
$(document).ready(function()
{
$("#sector_id").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
//alert($(this).val());
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/listsubsector/",
data: dataString,
cache: false,
success: function(html)
{
$("#getsubsector").html(html);
}

});

});


});
</script>
    <p>
      <label for="subsector_id">Sub&nbsp;Sector</label>
        <div id="getsubsector">
         <select name="subsector_id" id="subsector_id"  class="chosen-select" data-placeholder="Choose Sub Sector..."  >
             <option value="" ></option>
         <?php
        $this->db->order_by('sector_id',$edit['sector_id']);
        $Q = $this->db->get('subsector');
        foreach($Q->result_array() as $row){
		 ?>
         <option value="<?php echo $row['subsector_id']; ?>" <?php if($row['subsector_id'] = $edit['subsector_id']){ echo 'selected="selected"';} ?>><?php echo $row['subsector_name']; ?></option>
         <?php } ?>
         </select>

      </div>
     </p>
    <p>
      <label for="">Cooperation&nbsp;Framework</label>
      <select name="cooperation_id" id="cooperation_id" class="chosen-select" data-placeholder="Choose Cooperation Framework..." >
      <option value=""></option>
       <?php foreach($coop as $listcoop){ ?>
      <option value="<?php echo $listcoop['cooperation_id']; ?>" <?php if($listcoop['cooperation_id'] == $edit['cooperation_id']){ echo 'selected="selected"';} ?>><?php echo $listcoop['cooperation_name']; ?></option>
      <?php } ?>
      </select>
      </select>
    </p>
    <p>
      <label for="">Cooperation&nbsp;Type</label>
      <select name="cooperation_type_id" id="cooperation_type_id" class="chosen-select" data-placeholder="Choose Cooperation Type..." >
      <option value=""></option>
       <?php foreach($cooptype as $listcooptype){ ?>
      <option value="<?php echo $listcooptype['cooperation_type_id']; ?>" <?php if($listcooptype['cooperation_type_id'] == $edit['cooperation_type_id']){ echo 'selected="selected"';} ?>><?php echo $listcooptype['cooperation_type_name']; ?></option>
      <?php } ?>
      </select>
    </p>
    <p>
      <label for="">OECD&nbsp;Aid&nbsp;Type</label>
      <select name="oecd_id" id="oecd_id" class="chosen-select" data-placeholder="Choose OECD Aid Type.." >
      <option value=""></option>
       <?php foreach($oecd as $listoecd){ ?>
      <option value="<?php echo $listoecd['oecd_id']; ?>" <?php if($listoecd['oecd_id'] == $edit['oecd_id']){ echo 'selected="selected"';} ?>><?php echo $listoecd['oecd_name']; ?></option>
      <?php } ?>
      </select>
    </p>

  <p>
      <script type="text/javascript">
$(document).ready(function()
{
$("#funding_id").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
//alert($(this).val());
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/getExecuting/",
data: dataString,
cache: false,
success: function(html)
{
$("#getexecuting").html(html);
}
});

$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/getimplement/",
data: dataString,
cache: false,
success: function(html)
{
$("#getimplement").html(html);
}
});

});
});
</script>

    <label for="">Funding&nbsp;Agency</label>
               <select name="funding_id" id="funding_id" class="chosen-select" data-placeholder="Choose Funding Agency..." >
      <option value=""></option>
       <?php foreach($funding as $listfunding){ ?>
      <option value="<?php echo $listfunding['funding_id']; ?>"  <?php if($listfunding['funding_id'] == $edit['funding_id']){ echo 'selected="selected"';} ?>><?php echo $listfunding['funding_name']; ?></option>
      <?php } ?>
      </select>
         </p>
           <p>
      <label for="">Executing&nbsp;Agency</label>
       <div id="getexecuting">
      <select name="executing_id" id="executing_id" class="chosen-select" data-placeholder="Choose Executing Agency.." >
      <option value=""></option>
       <?php foreach($exa as $listex){ ?>
      <option value="<?php echo $listex['executing_id']; ?>"  <?php if($listex['executing_id'] == $edit['executing_id']){ echo 'selected="selected"';} ?>><?php echo $listex['executing_name']; ?></option>
      <?php } ?>
      </select>
      </div>
    </p>

    <p>
           <label for="implement_id">Implementing&nbsp;Agency</label>
           <div id="getimplement">
                     <select name="implement_id" id="implement_id" class="chosen-select" data-placeholder="Choose Implementing Agency.." >
      <option value=""></option>
       <?php foreach($im as $listim){ ?>
      <option value="<?php echo $listim['implement_id']; ?>" <?php if($listim['implement_id'] == $edit['implement_id']){ echo 'selected="selected"';} ?>><?php echo $listim['implement_name']; ?></option>
      <?php } ?>
      </select>
      </div>

      </p>





  <script>
  $(document).ready(function(){
    $("#addCF").click(function(){
	  $.ajax({
       url: "<?php echo base_url(); ?>project/admin/listcountry/",
       success: function(html) {
          $("#customFields").append(html);
       }
    });
    });
    $("#customFields").on('click', '.remCF', function(){
        $(this).parent().parent().remove();
    });

  $(".trashcountry").click(function(){
   var del_id = $(this).attr('id');
   var dataString = 'id='+ del_id;
   var rowElement = $(this).parent().parent(); //grab the row
//alert(del_id );
  $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>project/admin/delcountry/",
            data: dataString,
          /*  success:function(data) {
                if(data == "YES") {
                   rowElement.fadeOut(500).remove();
                }
            }*/

			success: function(){
			//	 $(this).parent().parent().remove();
			}
});
			$(this).parents(".recordcountry").animate({ backgroundColor: "#fbc7c7" }, "fast")
			.animate({ opacity: "hide" }, "slow");

  		//  });
	    });

});
</script>
<div style="background:#F9F9F9; padding-left:5px; padding-top:5px;" >
<label>Co&nbsp;-&nbsp;Funding</label>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="customFields">
    <?php foreach($co as $listco){ ?>
  <tr class="recordcountry">
    <td width="64%"><strong>Country</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?php
		 $this->db->where('country_id',$listco['country_id']);
		 $QC=$this->db->get('country');
		 $namecountry = $QC->row();
		 echo $namecountry->country_name;
		 ?></td>
    <td width="4%"><strong>Budget</strong></td>
    <td width="27%" align="right"><?php echo number_format($listco['co_budget'],2,'.',','); ?>&nbsp;&nbsp;&nbsp;THB.</td>
    <td width="5%"><a href="javascript:void(0);" class="button red trashcountry" id="<?php echo $listco['co_id']; ?>" onclick="confirm('Sure you want to delete this update? There is NO undo! ')">Del</a></td>
  </tr>
  <?php } ?>
  <tr>
    <td><strong>Country</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <select name="country_id[]" id="country_id" class="chosen-select" data-placeholder="Choose a Conutry Name...">
        <option value="" selected="selected"></option>
  <?php   foreach($country as $list){ ?>
  <option value="<?php echo $list['country_id']; ?>"><?php echo $list['country_name']; ?></option>

  <?php } ?>
</select></td>
    <td><strong>Budget</strong></td>
    <td align="right"><input type="text" name="co_budget[]" id="co_budget" class="text-input small-input number" />
&nbsp;&nbsp;&nbsp;THB.</td>
    <td><input type="button" name="button2" id="addCF" value="add" class="button green" /></td>
  </tr>
</table>
</div>

<br/>
              <div style="background:#F1F1F1; padding-left:5px; padding-top:5px;">
                  <script>
  $(document).ready(function(){
	  var id = 0;
    $("#addDis").click(function(){
	dataString =  "add="+id++;
	  $.ajax({
	  type: "POST",
       url: "<?php echo base_url(); ?>project/admin/listDisbureseme/",
	   data: dataString,
       success: function(html) {
          $("#Tdis").append(html);
       }
    });
    });
    $("#Tdis").on('click', '#redis', function(){
        $(this).parent().parent().remove();


    });

	  $(".trashdis").click(function(){
   var del_id = $(this).attr('id');
   var dataString = 'id='+ del_id;
   var rowElement = $(this).parent().parent();
  $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>project/admin/deldisbureseme/",
            data: dataString,
			success: function(){
			}
		});
			$(this).parents(".recorddis").animate({ backgroundColor: "#fbc7c7" }, "fast")
			.animate({ opacity: "hide" }, "slow");
});

  $( ".datedis" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});

     $('input[name=\"dis_budget[]\"]').click(function(){

				
				var val = 0;
			   var total = $('#res').val();
			   $('input[name=\"dis_budget[]\"]').each(function() {
							val = (Number($(this).val())+(Number(total)));
						});
				$('input[name=\"res\"]').val(val);
   


			});

});
</script>
<br />
<label>Disbursement</label>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="Tdis">
<?php foreach($dis as $listdis){ ?>
  <tr class="recorddis">
    <td><strong> Titile </strong>&nbsp;&nbsp;      <?php if($listdis['title']==1){echo "Grant"; }else{echo "Loan"; } ?></td>
    <td><strong>Country </strong>&nbsp;&nbsp;      <?php
		 $this->db->where('country_id',$listdis['country_id']);
		 $QC=$this->db->get('country');
		 if($QC->num_rows() > 0){
		 $namecountry = $QC->row_array();
		 echo $namecountry['country_name'];
		 }
		 ?></td>
    <td><strong>Budget</strong></td>
    <td align="right"><?php
	@$total += $listdis['dis_budget'];
	echo number_format($listdis['dis_budget'],2,'.',','); ?>
  &nbsp;&nbsp;&nbsp;&nbsp;THB.</td>

    <td><strong>Date </strong>&nbsp;&nbsp;<?php echo $listdis['dis_date']; ?></td>
    <td bgcolor="#F1F1F1"><a href="javascript:void(0);" class="button red trashdis" id="<?php echo $listdis['dis_id']; ?>" onclick="confirm('Sure you want to delete this update? There is NO undo! ')">Del</a></td>
  </tr>
  <?php } ?>


  <tr>
    <td width="22%"><strong> Titile </strong>
      <select name="title[]" id="title" class="chosen-select" data-placeholder="Choose a Title..." style="width:100px;">
       <option value=""></option>
        <option value="1"  selected="selected">Grant</option>

      </select></td>
    <td width="30%"><strong>Country </strong>
      <select name="discountry[]" id="discountry[]" class="chosen-select" data-placeholder="Choose a Conutry Name..." style="width:180px;">
        <option value=""></option>
        <?php   foreach($country as $list){ ?>
        <option value="<?php echo $list['country_id']; ?>"><?php echo $list['country_name']; ?></option>
        <?php } ?>
      </select></td>
    <td width="5%"><strong>Budget</strong></td>
    <td width="21%" align="right"><input type="text" name="dis_budget[]" id="dis_budget[]" class="text-input  number"  style="width:100px;" />
      THB.</td>

    <td width="20%"><strong>Date </strong>
      <input type="text" name="disdate[]" id="disdate" class="text-input datedis"  style="width:70px" /></td>
    <td width="3%" bgcolor="#F1F1F1"><input type="button" name="addDis" id="addDis" value="add" class="button green" /></td>
  </tr>
  </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td width="22%">&nbsp;</td>
    <td width="28%" align="center">&nbsp;</td>
    <td width="6%"><strong>Total</strong></td>
    <td width="18%" align="right"><input type="text" name="res" id="res"  disabled="disabled" class="text-input number" style="width:100px;" value="<?php echo @number_format($total,2,'.',','); ?>" />
      THB.</td>
    <td width="23%">&nbsp;</td>
    <td width="2%" bgcolor="#F1F1F1">&nbsp;</td>
  </tr>
</table>
    </div>
<br/>
	
<!--  <script>
 $(document).ready(function(){
    $("#btn1").click(function(){
		alert("fff");
       $("p").hide();
    });
	if($("p").hide();){
		$("p").show();
	}

});
</script>  -->
    <p>
      <label>Contact&nbsp;Person</label>
        <input name="contact_name" type="text" class="text-input medium-input" id="contact_name" value="<?php echo $edit['contact_name']; ?>" >
      </p>
    <p>
      <label>Contact&nbsp;Position </label>
        <input name="contact_position" type="text" class="text-input medium-input" id="contact_position" value="<?php echo $edit['contact_position']; ?>" >
      </p>
    <p>
      <label>Contact&nbsp;Email </label>
      <input name="contact_email" type="text" class="text-input medium-input" id="contact_email" value="<?php echo $edit['contact_email']; ?>" >
      </p>
    <p>
      <label>Contact&nbsp;Telephone </label>
      <input name="contact_tel" type="text" class="text-input medium-input" id="contact_tel" value="<?php echo $edit['contact_tel']; ?>" >
      </p>
    <p>
      <label for="">Contact&nbsp;Fax </label>
      <input name="contact_fax" type="text" class="text-input medium-input" id="contact_fax" value="<?php echo $edit['contact_fax']; ?>" >
      </p>
    <p>
      <label>Project&nbsp;Status </label>

      <input name="project_status" type="radio" id="project_status" value="0"  <?php if($edit['project_status'] == 0){ echo 'checked="checked"';} ?> />
      Running
      <input name="project_status" type="radio" id="project_status" value="1"  <?php if($edit['project_status'] == 1){ echo 'checked="checked"';} ?>/>
      Complete</p>

           <p>
                      <input type="submit" name="button" id="button" value="Save" class="button green">
             <input name="projectid" type="hidden" id="projectid" value="<?php echo $edit['project_id']; ?>" />
             
    </p>


  </form>
