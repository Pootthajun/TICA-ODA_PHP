<style>
.chosen-select{
width:250px;	
}

</style>
 <style type="text/css">
	label 
	{
		width:auto;		
	}
</style>
<script type="text/javascript">
$(document).ready(function()
{
	
/////get Activity	
$("#plan").change(function()
{
	var plan=$(this).val();
	var projecttype = $('#project_type').val();
 
	//alert($(this).val());
	$("#resultTable").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
	$.ajax
	({
	type: "POST",
	url: "<?php echo base_url(); ?>project/admin/getprojectbyplan/",
	data: {"projecttype":projecttype,"plan":plan},
	cache: false,
	success: function(html)
	{
	$("#resultTable").fadeOut(100).html(html).fadeIn(500);
	} 
	});
});
///end get activity

/////get project_type	
$("#project_type").change(function()
{
	var projecttype=$(this).val();
	var plan = $('#plan').val();
 
		var dataString = 'projecttype='+ projecttype;
		$("#resultTable").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
		$.ajax
		({
		type: "POST",
		url: "<?php echo base_url(); ?>project/admin/getprojectbyprojecttype/",
		data: {"projecttype":projecttype,"plan":plan},
		cache: false,
		success: function(html)
		{
		$("#resultTable").fadeOut(100).html(html).fadeIn(500);
		} 
		});
	
});
///end get activity



});
</script>
    <p>
    <form id="form1" name="form1" method="post" action="<?php echo site_url(); ?>project/admin/editproject/">
<table cellpadding="0" cellspacing="0" width="100%" border=1>
<tr>
<td width="10"><strong>Plan</strong>&nbsp; </td>
<td width="45%">
<select name="plan" id="plan" class="required chosen-select" data-placeholder="Choose Plan..."  style=" width: 100%;">
    <option value=""></option>
    <?php foreach($plan as $listplan){ ?>
    <option value="<?php echo $listplan['plan_id']; ?>"><?php echo $listplan['plan_name']; ?></option>
    <?php } ?>
    </select></td>
<td width="20"><strong>Type</strong>&nbsp; </td>    
<td width="45%"> 
<select name="project_type" id="project_type" class="chosen-select" data-placeholder="Choose Project Type..." required  style=" width: 100%;">
    <option value=""></option>
    <?php foreach($projecttype as $listpt){ ?>
    <option value="<?php echo $listpt['project_type_id']; ?>"><?php echo $listpt['project_type_name']; ?></option>
    <?php } ?>
    </select></td>
    <td width="100"><?php // if( !empty( $menu ) ) { ?>
<input type="submit" name="button" id="button" value="Add"  class="button green"/>
<?php //} ?></td>
</tr>
</table>
   

    </form>
       
    </p>
    <div id="resultTable">
    
   
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="63">Id</th>
          <th width="168">Plan</th>
          <th width="147">Project Type</th>
          <th width="322">Project Name</th>
          <th width="122">Start</th>
          <th width="126">End</th>
           <?php  //if( !empty( $menu ) ) { ?>
          <th>&nbsp;</th>
          <?php //} ?>
        </tr>
      </thead>
      <tbody>
<?php 
$count = 1;
foreach($project as $list){ ?>
        <tr>
          <td><?php echo $count++; ?></td>
          <td>
		  <?php
		  	$this->db->where('plan_id',$list['plan_id']);
			$QP = $this->db->get('plan');
			$planname = $QP->row_array();
			echo $planname['plan_name'];
		  
		  ?></td>
          <td><?php 
		  $this->db->where('project_type_id'='1',$list['project_type_id']);
			$QT = $this->db->get('project_type');
			$typename = $QT->row_array();
			echo $typename['project_type_name']
		  ?></td>
          <td><?php echo $list['project_name']; ?></td>
          <td><?php echo $list['project_start']; ?></td>
          <td><?php echo $list['project_end']; ?></td>
           <?php // if( !empty( $menu ) ) { ?>
          <td width="120" align="right">
          <?php
	    $projectassistant = '';
        $this->db->where('project_id',$list['project_id']);
        $Q = $this->db->get('project_assistant');
        foreach($Q->result_array() as $listp){
			$projectassistant .= $listp['user_id'].","; 
		}
		
		 $canedit = $projectassistant."".$list['project_owner'];
		  $ex = explode(",",$canedit);
		 $Q->free_result();
		  ?>
          <?php  for($i=0;$i<count($ex);$i++){ 
		  if($ex[$i] == $this->session->userdata('userid')){ 
		  ?>
            <a href="<?php echo site_url(); ?>project/admin/editproject/<?php echo $list['project_id']; ?>" class="button blue">Edit</a>

            <a href="<?php echo site_url(); ?>project/admin/deleteproject/<?php echo $list['project_id']; ?>"  onclick="return confirm('Are you sure you want to delete?')" class="button red"><?php echo $this->lang->line('delete'); ?></a>

		<?php }}?>


          </td>
          <?php } ?>
          </tr>
          <?php //} ?>
      </tbody>
    </table>
 </div>