<style>
#country_id{
	width:410px;
	margin-left: 5px;
}
#kind_id{
	width:440px;
	padding-left: 5px;
}
select{
	width:420px;
}
</style>
<form id="form1" name="form1" method="post" action="<?php echo site_url(); ?>project/admin/insertprojectcontibution">
    <p>
	  <label>Plan name</label>
	 <?php echo $planname['plan_name']; ?>
	 <input type="hidden" name="planid" id="planid" value="<?php echo $planname['plan_id']; ?>" />
  </p>
      <p>
        <label> Project type</label>
   
<?php echo $ptname['project_type_name']; ?>
    <input type="hidden" name="projecttype" id="projecttype"  value="<?php echo $ptname['project_type_id']; ?>"/>
    </p>
    <script>
  $(document).ready(function(){
    $("#addbt").click(function(){ 
	  $.ajax({
       url: "<?php echo base_url(); ?>project/admin/listbtadd/",
       success: function(html) {
          $("#Tbudget").append(html);
       }
    });	  
    });    
    $("#Tbudget").on('click', '.rebt', function(){
        $(this).parent().parent().remove();
    });
    
});
</script>  
<div style="background:#F9F9F9; padding-left:5px; padding-top:5px;" > 
<label>Budget Type</label>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="Tbudget">
      <tr>
        <td width="60%"><strong>Budget Type Name</strong> &nbsp;
         <select name="bt_id[]" id="bt_id[]" class="chosen-select" data-placeholder="Choose Budjet Type...">
      <option value=""></option>
       <?php foreach($bgtype as $listbgtype){ ?>
      <option value="<?php echo $listbgtype['bt_id']; ?>"><?php echo $listbgtype['bt_name']; ?></option>
      <?php } ?>
      </select></td>
        <td width="32%"><strong>Budget</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="text" name="budget_money[]" id="budget_money[]" class="text-input m-input number"  ></td>
        <td width="8%"><input type="button" name="addbt" id="addbt" value="add" class="button green" /></td>
      </tr>
    </table>
  </div>
  <p>
           <label for="">Project name</label>
           <input type="text" name="project_name" id="project_name" class="text-input large-input" required>
  </p>
  <p>
    <label>Project Officer</label>
           <select name="project_owner" id="project_owner" data-placeholder="Choose a Project Owner..." class="required chosen-select"  style="width:520px;">
           <option value=""></option>
           <?php foreach($user as $listuser){?>
             <option value="<?php echo $listuser['user_id']; ?>"><?php echo $listuser['user_fullname']; ?></option>
             <?php } ?>
           </select>
    </p>
    <script type="text/javascript">
$(document).ready(function()
{
$("#project_owner").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/listprojectassistant/",
data: dataString,
cache: false,
success: function(html)
{
$("#getprojectassistant").html(html);
} 
});
});
});
</script>
    
    
    
             <p>
           <label>Project  Assistant</label>
           <div id="getprojectassistant">
           <select name="project_assistant[]" id="project_assistant" data-placeholder="Choose a Project Assistant..." class="required chosen-select"   multiple>
            </select>
           </div>
    </p>
         <div style="width:50%; float:left; ">
         <p>
           <label for="project_start">Project Start Date</label>
           <input type="text" name="project_start" id="datepicker1" class="text-input medium-input" required>
           </p>
         </div>
       
            <div style="width:50%; float:right;">
            <p>
           <label for="project_end">Project End Date</label>
           <input type="text" name="project_end" id="datepicker2" class="text-input medium-input" required>
           </p>
           </div>

  <p>
      <label for="sector_id">Sector</label>
      <select name="sector_id" id="sector_id"  class="required chosen-select" data-placeholder="Choose Sector..." >
      <option value=""></option>
      <?php foreach($sector as $listsector){ ?>
      <option value="<?php echo $listsector['sector_id']; ?>"><?php echo $listsector['sector_name']; ?></option>
      <?php } ?>
      </select>        
  </p>
     <script type="text/javascript">
$(document).ready(function()
{
$("#sector_id").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
//alert($(this).val());
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/listsubsector/",
data: dataString,
cache: false,
success: function(html)
{
$("#getsubsector").html(html);
} 

});

});


});
</script>
    <p>
    <!--<div id="subsector_id"></div>-->
      <label for="subsector_id">Sub Sector</label>
         <div id="getsubsector">
           <select name="subsector_id" id="subsector_id" class="required chosen-select" data-placeholder="Choose Sub Sector..." >
         </select>   
         </div>         
      </p>

  <p>
    <script type="text/javascript">
$(document).ready(function()
{
$("#funding_id").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
//alert($(this).val());
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>project/admin/getExecuting/",
data: dataString,
cache: false,
success: function(html)
{
$("#getexecuting").html(html);
} 
});



});
});
</script>
    <label>Funding Agency</label>
               <select name="funding_id" id="funding_id" class="chosen-select" data-placeholder="Choose Funding Agency..." >
      <option value=""></option>
       <?php foreach($funding as $listfunding){ ?>
      <option value="<?php echo $listfunding['funding_id']; ?>"><?php echo $listfunding['funding_name']; ?></option>
      <?php } ?>
      </select>   
  </p>
           <p>
           <label for="executing_id">Executing Agency</label>
           
  <div id="getexecuting">
                 <select name="executing_id" id="executing_id" class="chosen-select" data-placeholder="Choose Executing Agency.." >
                  <option value=""></option>
    </select>   
  </div>
     
         </p>
         
    <p>
           <label>Implementing Agency</label>
  <div id="getimplement">
                            <select name="implement_id" id="implement_id" class="chosen-select" data-placeholder="Choose Implementing Agency.." >
                          <option value=""></option>
                      <?php    foreach($im as $listex){
   					echo "<option value=\"".$listex['implement_id']."\">".$listex['implement_name']."</option>";
              }
			  ?>
                          </select> 
  </div>

      </p>
  <p>
           <label for="">Multilateral Organizations</label>
                <select name="multilateral_id" id="mutilateral_id" class="chosen-select" data-placeholder="Choose Multiteral Organization ..." >
      <option value=""></option>
       <?php foreach($mu as $listmu){ ?>
      <option value="<?php echo $listmu['country_id']; ?>" ><?php echo $listmu['country_name']; ?></option>
      <?php } ?>
      </select>              
  </p>
  <script>
  $(document).ready(function(){
	  var id = 0;
    $("#addDis").click(function(){ 
	dataString =  "add="+id++;
	  $.ajax({
	  type: "POST",
       url: "<?php echo base_url(); ?>project/admin/listDisburesemecontribution/",
	   data: dataString,
       success: function(html) {
          $("#Tdis").append(html);
       }
    });	  
    });    
    $("#Tdis").on('click', '#redis', function(){
        $(this).parent().parent().remove();
    });
	$( "#disdate" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});    
   
});
</script>       

    <div style="background:#F1F1F1; padding-left:5px; padding-top:5px;">
<label>Disbureseme</label>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="Tdis">
  <tr>
    <td width="22%"><strong> Titile </strong>
      <select name="title[]" id="title" class="chosen-select" data-placeholder="Choose a Title..." style="width:150px;">
       <option value=""></option>
        <option value="3">ค่าบำรุงสมาชิก </option>
        <option value="4">เงินสมทบกองทุน</option>
        <option value="5">เงินอุดหนุน</option>
      </select></td>
    <td width="30%"><strong>Country </strong>
      <select name="discountry[]" id="discountry[]" class="chosen-select" data-placeholder="Choose a Conutry Name..." style="width:200px;">
        <option value=""></option>
        <?php   foreach($country as $list){ ?>
        <option value="<?php echo $list['country_id']; ?>"><?php echo $list['country_name']; ?></option>
        <?php } ?>
        </select></td>
    <td width="25%"><strong>Budget</strong>
      <input type="text" name="dis_budget[]" id="dis_budget[]" class="text-input number" style="width:70%" /></td>
    <td width="20%"><strong>Date </strong>
      <input type="text" name="disdate[]" id="disdate" class="text-input"  style="width:70%" /></td>
    <td width="3%" bgcolor="#F1F1F1"><input type="button" name="addDis" id="addDis" value="add" class="button green" /></td>
  </tr>
  </table>
</div>

      
         
  <p>
      <label for="">Contact Person</label>
        <input type="text" name="contact_name" id="project_budget" class="text-input medium-input" >          
  </p>
    <p>
      <label for="">Contact Position </label>
        <input type="text" name="contact_position" id="project_budget" class="text-input medium-input" >          
  </p>
    <p>
      <label for="">Contact Email </label>
      <input type="text" name="contact_email" id="project_budget" class="text-input medium-input" >          
  </p>
    <p>
      <label for="">Contact Tel </label>
      <input type="text" name="contact_tel" id="project_budget" class="text-input medium-input" >          
  </p>
    <p>
      <label for="">Contact Fax </label>
      <input type="text" name="contact_fax" id="project_budget" class="text-input medium-input" >          
  </p>
  <p>
    <label>Project Status </label>
 
      <input name="project_status" type="radio" id="project_status" value="0" checked="checked" />
    Running
    <input name="project_status" type="radio" id="project_status" value="1" />
    Complete</p>
         
           <p>
                                  <input type="submit" name="button" id="button" value="Save" class="button green">
    </p>
         
</form>



  
