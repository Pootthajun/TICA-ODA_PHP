<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MProject extends CI_Model{

    function insertproject(){
    	//$data = $this->_uploadproject();
            $data = array(
            'plan_id' => $_POST['planid'] ,
            'project_type_id' => $_POST['projecttype'] ,
            'project_name' => $_POST['project_name'] ,
            'project_object'=> $_POST['project_object'] ,
            'project_detail'=> $_POST['project_detail'] ,
            'project_owner'=> $_POST['project_owner'] ,
            'project_start'=> $_POST['project_start'] ,
            'project_end'=> $_POST['project_end'] ,
            'project_budget'=> str_replace(",","",$_POST['project_budget']) ,
            'project_comment'=> $_POST['project_comment'] ,
            'sector_id'=> $_POST['sector_id'] ,
            'subsector_id'=> $this->input->post('subsector_id') ,
            'cooperation_id'=> $_POST['cooperation_id'] ,
            'cooperation_type_id'=> $_POST['cooperation_type_id'] ,
            'oecd_id'=> $_POST['oecd_id'] ,
            'funding_id'=> $_POST['funding_id'] ,
            'executing_id'=> $_POST['executing_id'] ,
            'implement_id'=> $_POST['implement_id'] ,
            'contact_name'=> $_POST['contact_name'] ,
            'contact_position'=> $_POST['contact_position'] ,
            'contact_email'=> $_POST['contact_email'] ,
            'contact_tel'=> $_POST['contact_tel'] ,
            'contact_fax'=> $_POST['contact_fax'] ,
            'project_status'=> $this->input->post('project_status') ,
            'project_by'=>$this->session->userdata('userid'),
            'project_create'=>date('Y-m-d')
        );
	$this->db->insert('project',$data);
         //// insert Co
        $this->db->select_max('project_id');
        $query = $this->db->get('project');
        $max = $query->row();
        $projectid=$max->project_id;

        $this->insertfield($projectid);





        for($i = 0;$i < count($_POST['country_id']);$i++ ){
            if($_POST['country_id'][$i] != null){
                $data  = array(
                    'country_id' => $_POST['country_id'][$i],
                    'co_budget'=> $_POST['co_budget'][$i],
                    'project_id'=>$max->project_id,
                 );
                $this->db->insert('co_funding',$data);
            }
        }

        for($i = 0;$i < count($_POST['dis_budget']);$i++ ){
            if($_POST['dis_budget'][$i] != null){
                $data  = array(
                    'title' => $_POST['title'][$i],
                    'country_id' => $_POST['discountry'][$i],
                    'dis_budget'=> str_replace(",","",$_POST['dis_budget'][$i]),
		    'dis_date'=> $_POST['disdate'][$i],
                    'project_id'=>$max->project_id,
                 );
                $this->db->insert('disbureseme',$data);
            }
        }

	for($i = 0;$i < count($_POST['bt_id']);$i++ ){
            if($_POST['bt_id'][$i] != null){
                $data  = array(
                    'bt_id' => $_POST['bt_id'][$i],
                    'budget_money'=> str_replace(",","",$_POST['budget_money'][$i]),
                    'project_id'=>$max->project_id,
                 );
                $this->db->insert('budget_project',$data);
            }
        }

        $this->inserprojectassistant($projectid);

                              /* insertkindproject*/
       
for($i = 0;$i < count($_POST['kind_type_id']);$i++ ){
                  if($_POST['kind_type_id'][$i] != null){
                      $data  = array(
                          'project_id'=>$max->project_id,
                          'inkind_id' => $_POST['kind_type_id'][$i],
                          'kind_estimated'=> str_replace(",","",$_POST['kindtype_money'][$i]),
                          'description'=> $_POST['description'],
                          'project_kind_by'=> $this->session->userdata('userid'),
                          'project_kind_createDate'=> date('Y-m-d'),
                       );
                      $this->db->insert('projectkind',$data);
                  }
              }




    }
/* end insertproject*/



    function  inserprojectassistant($projectid){
        for($i = 0;$i < count($this->input->post('project_assistant'));$i++ ){
            if($_POST['project_assistant'][$i] != null){
                $data  = array(
                    'user_id'=> $_POST['project_assistant'][$i],
                    'project_id'=>$projectid,
                 );
                $this->db->insert('project_assistant',$data);
            }
        }
    }

       /* function _uploadproject($projectid){
               $cpt = count($_FILES['files']['tmp_name']);

                    $config['upload_path']          = './upload/project/';
                    $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx|PDF';
                    $config['max_size']='1000000000';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    //foreach($_FILES as $file => $value){
                  //  if( $value['size'] > 0 ){
                    //$file = $_FILES;
                    for($x=0; $x<=$cpt; $x++){
                    $this->upload->do_upload('files');
                    $udata=$this->upload->data();
                        $data = array(
                            'project_id' => $projectid,
                            'project_file'=>$udata[$x]['file_name']
                         );
                    $this->db->insert('project_file',$data);
                   }
                return $data;

            }*/


       function insertfield($projectid){
        //print_r ($_FILES['files']['tmp_name']);
       $count = count($_FILES['files']['tmp_name']);
       $j=0;
        for ($i = 0; $i < $count; $i++) {
            if($_FILES['files']['tmp_name'][$i]  != ""){
                //print_r($_FILES['files']['tmp_name'][$i]);
                $f =  $_FILES['files']['tmp_name'][$i];
                $m = $i;

               }
            }
            $j = @$m+1;
            for ($x = 0; $x < $j; $x++) {
                //$j;
                    $data = $this->_uploadfield($x,$projectid);
                    $this->db->insert('project_file',$data);
           }
        }


        function _uploadfield($x,$projectid){

                 $data = array(
                            'project_id' => $projectid,
                          );


                    $config['upload_path']          = './upload/project/';
                    $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx|PDF';
                    $config['max_size']='1000000000';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

            $pseudo_field_name = '_psuedo_'. 'file' .'_'. $x;
            $_FILES[$pseudo_field_name] = array(
                'name' => $_FILES['files']['name'][$x],
                'size' => $_FILES['files']['size'][$x],
                'type' => $_FILES['files']['type'][$x],
                'tmp_name' => $_FILES['files']['tmp_name'][$x],
                'error' => $_FILES['files']['error'][$x]
            );
            if ( ! $this->upload->do_upload($pseudo_field_name) ) {
                $error = $this->upload->display_errors();
            } else {
                $udata = $this->upload->data();
                $data['project_file'] = $udata['file_name'];
            }

          return $data;
         }




        function listallproject(){
        $data = array();
        $this->db->order_by('project_id',"DESC");
        $Q = $this->db->get('project');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
	}
	function listallProjectByProject(){

        $data = array();
		$this->db->where('project_type_id',"1");
        $this->db->order_by('project_id',"DESC");
        $Q = $this->db->get('project');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
	}
	function listallProjectByNonproject(){

        $data = array();
		$this->db->where('project_type_id',"2");
        $this->db->order_by('project_id',"DESC");
        $Q = $this->db->get('project');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
	}
	function listallProjectByLoan(){

        $data = array();
		$this->db->where('project_type_id',"3");
        $this->db->order_by('project_id',"DESC");
        $Q = $this->db->get('project');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
	}
	function listallProjectByContribuition(){

        $data = array();
		$this->db->where('project_type_id',"4");
        $this->db->order_by('project_id',"DESC");
        $Q = $this->db->get('project');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
	}

    public function insertprojectloan()
    {
        $data = array(
            'plan_id' => $_POST['planid'] ,
            'project_type_id' => $_POST['projecttype'] ,
            'project_name' => $_POST['project_name'] ,
            'project_object'=> $_POST['project_object'] ,
            'project_detail'=> $_POST['project_detail'] ,
            'project_owner'=> $_POST['project_owner'] ,
            'project_start'=> $_POST['project_start'] ,
            'project_end'=> $_POST['project_end'] ,
            'sector_id'=> $_POST['sector_id'] ,
            'subsector_id'=> $_POST['subsector_id'] ,
            'cooperation_type_id'=> $_POST['cooperation_type_id'] ,
            'cooperation_id'=> $_POST['cooperation_id'] ,
            'funding_id'=> $_POST['funding_id'] ,
            'executing_id'=> $_POST['executing_id'] ,
            'implement_id'=> $_POST['implement_id'] ,
            'contact_name'=> $_POST['contact_name'] ,
            'contact_position'=> $_POST['contact_position'] ,
            'contact_email'=> $_POST['contact_email'] ,
            'contact_tel'=> $_POST['contact_tel'] ,
            'contact_fax'=> $_POST['contact_fax'] ,
            'project_status'=> $this->input->post('project_status') ,
        );
        $this->db->insert('project',$data);
	$this->db->select_max('project_id');
        $query = $this->db->get('project');
        $query = $this->db->get('project');
        $max = $query->row();
	$projectid = $max->project_id;
        //// insert Co
       // $this->insertcofunding($projectid );
        if($projectid > 0){
	$this->insertbutgettype($projectid );
	$this->insertdisbureseme($projectid);
     $this->inserprojectassistant($projectid);
        }
		
		         /* insertkindproject*/
       
for($i = 0;$i < count($_POST['kind_type_id']);$i++ ){
                  if($_POST['kind_type_id'][$i] != null){
                      $data  = array(
                          'project_id'=>$max->project_id,
                          'inkind_id' => $_POST['kind_type_id'][$i],
                          'kind_estimated'=> str_replace(",","",$_POST['kindtype_money'][$i]),
                          'description'=> $_POST['description'],
                          'project_kind_by'=> $this->session->userdata('userid'),
                          'project_kind_createDate'=> date('Y-m-d'),
                       );
                      $this->db->insert('projectkind',$data);
                  }
              }

    }

	function insertdisbureseme($projectid){
		for($i = 0;$i < count($_POST['dis_budget']);$i++ ){
            if($_POST['dis_budget'][$i] != null){
                $data  = array(
					'title' => $_POST['title'][$i],
                    'country_id' => $_POST['discountry'][$i],
                    'dis_budget'=> str_replace(",","",$_POST['dis_budget'][$i]),
					'dis_date'=> $_POST['disdate'][$i],
                    'project_id'=>$projectid,
                 );
                $this->db->insert('disbureseme',$data);
			}
				 }
	}

	function insertbutgettype($projectid ){
	 for($i = 0;$i < count($_POST['bt_id']);$i++ ){
            if($_POST['bt_id'][$i] != null){
                $data  = array(
                    'bt_id' => $_POST['bt_id'][$i],
                    'budget_money'=> str_replace(",","",$_POST['budget_money'][$i]),
                    'project_id'=>$projectid,
                 );
                $this->db->insert('budget_project',$data);

            }
        }
	}

    function insertcofunding($projectid ){
        for($i = 0;$i < count($_POST['country_id']);$i++ ){
            if($_POST['country_id'][$i] != null){
                $data  = array(
                    'country_id' => $_POST['country_id'][$i],
                    'co_budget'=> str_replace(",","",$_POST['co_budget'][$i]),
                    'project_id'=>$projectid,
                 );
                $this->db->insert('co_funding',$data);

            }
        }
    }

    function updateprojectloan(){
        $data = array(
            'plan_id' => $_POST['planid'] ,
            'project_type_id' => $_POST['projecttype'] ,
            'project_name' => $_POST['project_name'] ,
            'project_object'=> $_POST['project_object'] ,
            'project_detail'=> $_POST['project_detail'] ,
            'project_owner'=> $_POST['project_owner'] ,
            'project_start'=> $_POST['project_start'] ,
            'project_end'=> $_POST['project_end'] ,
            'sector_id'=> $_POST['sector_id'] ,
            'subsector_id'=> $_POST['subsector_id'] ,
            'cooperation_type_id'=> $_POST['cooperation_type_id'] ,
            'cooperation_id'=> $_POST['cooperation_id'] ,
            'funding_id'=> $_POST['funding_id'] ,
            'executing_id'=> $_POST['executing_id'] ,
            'implement_id'=> $_POST['implement_id'] ,
            'contact_name'=> $_POST['contact_name'] ,
            'contact_position'=> $_POST['contact_position'] ,
            'contact_email'=> $_POST['contact_email'] ,
            'contact_tel'=> $_POST['contact_tel'] ,
            'contact_fax'=> $_POST['contact_fax'] ,
			'project_status'=> $this->input->post('project_status') ,
        );
        $this->db->where('project_id',$this->input->post('projectid'));
        $this->db->update('project',$data);
		$this->updatebudgettype();
		$projectid = $this->input->post('projectid');
		$this->insertdisbureseme($projectid);
                $this->updateprojectassistant($projectid);
    }

    function updatecofunding(){
         for($i = 0;$i < count($_POST['country_id']);$i++ ){
            if($_POST['country_id'][$i] != null){
                $data  = array(
                    'country_id' => $_POST['country_id'][$i],
                    'co_budget'=> str_replace(",","",$_POST['co_budget'][$i]),
                    'project_id'=>$this->input->post('projectid'),
                 );
                $this->db->insert('co_funding',$data);

            }
        }
		}

		function updatebudgettype(){
			 for($i = 0;$i < count($_POST['bt_id']);$i++ ){
            if($_POST['bt_id'][$i] != null){
                $data  = array(
                    'bt_id' => $_POST['bt_id'][$i],
                    'budget_money'=> str_replace(",","",$_POST['budget_money'][$i]),
                    'project_id'=>$this->input->post('projectid'),
                 );
                $this->db->insert('budget_project',$data);

            }
        }
		}



    function insertprojectcontibution(){
         $data = array(
            'plan_id' => $_POST['planid'] ,
            'project_type_id' => $_POST['projecttype'] ,
             'project_name' => $_POST['project_name'] ,
             'project_owner'=> $_POST['project_owner'] ,
            'project_start'=> $_POST['project_start'] ,
            'project_end'=> $_POST['project_end'] ,
            'pay_type'=> $_POST['pay_type'] ,
            'sector_id'=> $_POST['sector_id'] ,
            'subsector_id'=> $_POST['subsector_id'] ,
            'funding_id'=> $_POST['funding_id'] ,
            'executing_id'=> $_POST['executing_id'] ,
            'implement_id'=> $_POST['implement_id'] ,
            'multilateral_id'=>$_POST['multilateral_id'],
             'commitment'=>str_replace(",","",$_POST['commitment']),
            'contact_name'=> $_POST['contact_name'] ,
            'contact_position'=> $_POST['contact_position'] ,
            'contact_email'=> $_POST['contact_email'] ,
            'contact_tel'=> $_POST['contact_tel'] ,
            'contact_fax'=> $_POST['contact_fax'] ,
	    'project_status'=> $this->input->post('project_status') ,
            );
        $this->db->insert('project',$data);

         $this->db->select_max('project_id');
        $query = $this->db->get('project');
        $max = $query->row();
        $projectid = $max->project_id;
        //// insert Co
        //$this->insertcofunding($projectid );
        if($projectid > 0){
        $this->insertbutgettype($projectid );
	$this->insertdisbureseme($projectid);
	$this->inserprojectassistant($projectid);
        }
		
		
		         /* insertkindproject*/
       
for($i = 0;$i < count($_POST['kind_type_id']);$i++ ){
                  if($_POST['kind_type_id'][$i] != null){
                      $data  = array(
                          'project_id'=>$max->project_id,
                          'inkind_id' => $_POST['kind_type_id'][$i],
                          'kind_estimated'=> str_replace(",","",$_POST['kindtype_money'][$i]),
                          'description'=> $_POST['description'],
                          'project_kind_by'=> $this->session->userdata('userid'),
                          'project_kind_createDate'=> date('Y-m-d'),
                       );
                      $this->db->insert('projectkind',$data);
                  }
              }
    }

    function updatecontibution(){
         $data = array(
            'plan_id' => $_POST['planid'] ,
            'project_type_id' => $_POST['projecttype'] ,
            'project_name' => $_POST['project_name'] ,
            'project_owner'=> $_POST['project_owner'] ,
            'project_start'=> $_POST['project_start'] ,
            'project_end'=> $_POST['project_end'] ,
             'pay_type'=> $_POST['pay_type'] ,
            'sector_id'=> $_POST['sector_id'] ,
            'subsector_id'=> $_POST['subsector_id'] ,
            'funding_id'=> $_POST['funding_id'] ,
            'executing_id'=> $_POST['executing_id'] ,
            'implement_id'=> $_POST['implement_id'] ,
             'commitment'=>str_replace(",","",$_POST['commitment']),
            'multilateral_id'=>$_POST['multilateral_id'] ,
            'contact_name'=> $_POST['contact_name'] ,
            'contact_position'=> $_POST['contact_position'] ,
            'contact_email'=> $_POST['contact_email'] ,
            'contact_tel'=> $_POST['contact_tel'] ,
            'contact_fax'=> $_POST['contact_fax'] ,
            'project_status'=> $this->input->post('project_status') ,
            );
        $this->db->where('project_id',$this->input->post('projectid'));
        $this->db->update('project',$data);
        $this->updatebudgettype();
	$projectid = $this->input->post('projectid');
	$this->insertdisbureseme($projectid);
        $this->updateprojectassistant($projectid);
    }

    function getProjectByid($projectid){
        $this->db->where('project_id',$projectid);
        $Q = $this->db->get('project');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }

    function deleteproject($projectid){
        $this->db->where('project_id',$projectid);
        $this->db->delete('project');
		/*delete projectkind when delete project*/
		$this->db->where('project_id',$projectid);
        $this->db->delete('projectkind');
		
    }
	function deletekindproject($id){
        
		
		$this->db->where('id',$id);
        $this->db->delete('projectkind');
		
    }
	
	
	
    function getCofundingByprojectid($projectid){
        $data = array();
        $this->db->where('project_id',$projectid);
        $Q = $this->db->get('co_funding');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }

	function getDisburesemeByprojectid($projectid){
        $data = array();
        $this->db->where('project_id',$projectid);
		$this->db->order_by('dis_id',"ASC");
        $Q = $this->db->get('disbureseme');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;

	}
          
	function getbudgetByprojectid($projectid){
		/*$_GET['bid'];*/
	    $data = array();
        $this->db->where('project_id',$projectid);
        $Q = $this->db->get('budget_project');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();

	   return $data;
	}

		function getkindByprojectid($projectid){
	   $data = array();
        $this->db->where('project_id',$projectid);
        $Q = $this->db->get('projectkind');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();

	   return $data;
	}


    function updateproject(){

            $data = array(
            'plan_id' => $_POST['planid'] ,
            'project_type_id' => $_POST['projecttype'] ,
            'project_name' => $_POST['project_name'] ,
            'project_object'=> $_POST['project_object'] ,
            'project_detail'=> $_POST['project_detail'] ,
            'project_owner'=> $_POST['project_owner'] ,
            'project_start'=> $_POST['project_start'] ,
            'project_end'=> $_POST['project_end'] ,
            'project_budget'=> str_replace(",","",$_POST['project_budget']) ,
            'project_comment'=> $_POST['project_comment'] ,
            'sector_id'=> $_POST['sector_id'] ,
            'subsector_id'=> $this->input->post('subsector_id') ,
            'cooperation_id'=> $_POST['cooperation_id'] ,
            'cooperation_type_id'=> $_POST['cooperation_type_id'] ,
            'oecd_id'=> $_POST['oecd_id'] ,
            'funding_id'=> $_POST['funding_id'] ,
            'executing_id'=> $_POST['executing_id'] ,
            'implement_id'=> $_POST['implement_id'] ,
            'contact_name'=> $_POST['contact_name'] ,
            'contact_position'=> $_POST['contact_position'] ,
            'contact_email'=> $_POST['contact_email'] ,
            'contact_tel'=> $_POST['contact_tel'] ,
            'contact_fax'=> $_POST['contact_fax'] ,
            'project_status'=> $this->input->post('project_status') ,
            'project_by'=>$this->session->userdata('userid'),
            'project_create'=>date('Y-m-d')
        );
        $this->db->where('project_id',$this->input->post('projectid'));
        $this->db->update('project',$data);


		
     




        //// insert Co
        $projectid = $this->input->post('projectid');
        $this->insertfield($projectid);

                    for($i = 0;$i < count($_POST['country_id']);$i++ ){
                        if($_POST['country_id'][$i] != null){
                            $data  = array(
                                'country_id' => $_POST['country_id'][$i],
                                'co_budget'=> str_replace(",","",$_POST['co_budget'][$i]),
                                'project_id'=>$this->input->post('projectid'),
                             );
                            $this->db->insert('co_funding',$data);

                        }
                    }

                    for($i = 0;$i < count($_POST['bt_id']);$i++ ){
                        if($_POST['bt_id'][$i] != null){
                            $data  = array(
                                'bt_id' => $_POST['bt_id'][$i],
                                'budget_money'=> str_replace(",","",$_POST['budget_money'][$i]),
                                'project_id'=>$this->input->post('projectid'),
                             );
                            $this->db->insert('budget_project',$data);

                        }
                    }
					
					 for($i = 0;$i < count($_POST['kind_type_id']);$i++ ){
                  if($_POST['kind_type_id'][$i] != null){
                      $data  = array(
                          'project_id'=>$this->input->post('projectid'),
                          'inkind_id' => $_POST['kind_type_id'][$i],
                          'kind_estimated'=> str_replace(",","",$_POST['kindtype_money'][$i]),
                          'description'=> $_POST['description'],
                          'project_kind_by'=> $this->session->userdata('userid'),
                          'project_kind_createDate'=> date('Y-m-d'),
                       );
                      $this->db->insert('projectkind',$data);
                  }
              }

		$this->insertdisbureseme($projectid);
                $this->updateprojectassistant($projectid);
    }

    function updateprojectassistant($projectid){
        $this->db->where('project_id',$projectid);
        $this->db->delete('project_assistant');
        $this->inserprojectassistant($projectid);
    }


    function getactivitybyprojectid($projectid){
	//$data = array();
        $this->db->where('project_id',$projectid);
        $Q = $this->db->get('activity');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }

    function listallfilebyprojectid($projectid){
        $data = array();
        $this->db->where('project_id',$projectid);
        $this->db->where('project_file !=',"");
        $this->db->order_by('file_id',"ASC");
        $Q = $this->db->get('project_file');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }

        function getfilebyid($fileid){
        $data = array();
        $this->db->where('file_id',$fileid);
        $Q = $this->db->get('project_file');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }

    function listAllprojectstatus(){
        $data = array();
        $this->db->where('project_status',0);
        $this->db->order_by('project_id',"DESC");
        $Q = $this->db->get('project');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }


}
