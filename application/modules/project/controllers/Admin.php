<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{

    function __construct(){
	parent::__construct();
        $this->module =  basename(dirname(dirname(__FILE__)));

    }

    function index(){
        $data['header'] = "Project Table";
        $data['plan'] = $this->MPlan->listAllplan();
        $data['projecttype'] = $this->MProjecttype->listallProjecttype();

        $project_type_id = $this->uri->segment(4);
        if(empty($project_type_id)){
        	$data['project'] = $this->MProject->listallproject();
        }else if(($project_type_id==1)){
        	$data['project'] = $this->MProject->listallProjectByProject($project_type_id);
        }else if(($project_type_id==2)){
        	$data['project'] = $this->MProject->listallProjectByNonproject($project_type_id);
        }
		else if(($project_type_id==3)){
        	$data['project'] = $this->MProject->listallProjectByLoan($project_type_id);
        }
		else if(($project_type_id==4)){
        	$data['project'] = $this->MProject->listallProjectByContribuition($project_type_id);
        }
    	//$data['menu'] = $this->MUser->getMenubyUser();
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data);


    }

    function addproject(){
        $planid = $this->input->post('plan');
        $projecttypeid = $this->input->post('project_type');
        $data['header'] = "Add Project";
        $data['region'] = $this->MRegion->listallregion();
        $data['sector'] = $this->MSector->listallSector();
        $data['cooptype'] = $this->MCooperationtype->listallCooperationtype();
        $data['coop'] = $this->MCooperation->listallCooperation();
        $data['oecd'] = $this->MOecd->listalloecd();
        $data['funding']= $this->MFunding->listallFunding();
        $data['ex']= $this->MExecuting->listallExecuting();
        $data['ac'] = $this->MActivity->listallactivity();
        $data['mu'] = $this->MMultilateral->listmultilateral();
        $data['country'] = $this->MCountry->listallcountry();
        $data['user'] = $this->MUser->listalluser();
        $data['bgtype'] = $this->MBudgettype->listallBudgettype();
		$data['kindtype'] = $this->MKindtype->listallKindtype();
        $data['im'] = $this->MImplement->listallImplement();
        $data['planname'] = $this->MPlan->getPlanbyid($planid);
        $data['ptname'] = $this->MProjecttype->getProjecttypetypebyid($projecttypeid);
        if($_POST['project_type'] == "2"){
    	$data['page'] = $this->config->item('wconfig_template_admin').'addproject';
        }if($_POST['project_type'] == "1"){
        $data['page'] = $this->config->item('wconfig_template_admin').'addproject';
        }if($_POST['project_type'] == "3"){
        $data['page'] = $this->config->item('wconfig_template_admin').'addloan';
        }if($_POST['project_type'] == "4"){
        $data['page'] = $this->config->item('wconfig_template_admin').'addcontribution';
        }
    	$this->load->view($this->_container_admin,$data);
    }


    function listsubsector() {
           $id=$this->input->post('id');
           $Q=$this->db->query("select * from subsector where  sector_id='$id'");
		   echo '
		   <script>
			  $(document).ready(function(){
			    $(\'.chosen-select\').chosen({allow_single_deselect:true,no_results_text:\'Oops, nothing found!\'});
			});
			</script>
		   <select name="subsector_id" id="subsector_id" class="chosen-select"  data-placeholder="Choose Sub Sector..." >';
		   echo "<option></option>";
              foreach($Q->result_array() as $value){
                      echo  '<option value="'.$value['subsector_id'].'">'.$value['subsector_name'].'</option>';
					   }
            echo '</select>';

    }

	function listprojectassistant(){
            $id=$this->input->post('id');
            $Q=$this->db->query("select * from user where  user_id !='$id'");
            echo '
		<script>
		$(document).ready(function(){
		$(\'.chosen-select\').chosen({allow_single_deselect:true,no_results_text:\'Oops, nothing found!\'});
		});
		</script>
		<select name="project_assistant[]" id="project_assistant" class="chosen-select"  data-placeholder="Choose Project Assistant...." multiple >';
		echo "<option></option>";
                foreach($Q->result_array() as $value){
                      echo  '<option value="'.$value['user_id'].'">'.$value['user_fullname'].'</option>';
                 }
                 echo '</select>';
            }

    function listprojectassistantedit(){
            $id=$this->input->post('owner');
            $Q=$this->db->query("select * from user where  user_id !='$id'");
            echo '
		<script>
		$(document).ready(function(){
		$(\'.chosen-select\').chosen({allow_single_deselect:true,no_results_text:\'Oops, nothing found!\'});
		});
		</script>
                ';
                $ex = explode(",",$_POST['assistant']);

		echo '<select name="project_assistant[]" id="project_assistant" class="chosen-select required"  data-placeholder="Choose Project Assistant...." multiple >';
		echo "<option></option>";
                foreach($Q->result_array() as $value){
                      echo  '<option value="'.$value['user_id'].'"';
                      for($i=0;$i<count($ex);$i++){ if($value['user_id'] ==$ex[$i]){echo "selected ='selected '";} }
                      echo '>'.$value['user_fullname'].'</option>';
                 }
                 echo '</select>';
            }


    function insertproject(){
        /*$this->load->library('form_validation');
        $this->form_validation->set_rules('project_owner', 'Project Owner', 'required');
        $this->form_validation->set_rules('project_assistant[]', 'Project Assistant', 'required');
        if ($this->form_validation->run() == FALSE)
                {
                        $this->addproject();
                }
                else
                {    */
                $this->MProject->insertproject();
                flashMsg('success',$this->lang->line('insert_success'));
                redirect( 'project/admin/','refresh');
              //  }
    }



    function deleteproject($projectid){
        $this->MProject->deleteproject($projectid);
        flashMsg('success',$this->lang->line('delete_success'));
        redirect( 'project/admin/','refresh');

    }

    function editproject($projectid = 0){
        $data['header'] = "Manage Project";
        $type = $this->MProject->getProjectByid($projectid);
        if($projectid == 0){
            $planid = $this->input->post('plan');
            $projecttypeid = $this->input->post('project_type');
        }else{
            $planid = $type['plan_id'];
            $projecttypeid = $type['project_type_id'];
        }
        $data['edit'] = $type;
	    $data['activity']= $this->MProject->getactivitybyprojectid($projectid);
        $data['region'] = $this->MRegion->listallregion();
        $data['sector'] = $this->MSector->listallSector();
        $data['cooptype'] = $this->MCooperationtype->listallCooperationtype();
        $data['coop'] = $this->MCooperation->listallCooperation();
        $data['oecd'] = $this->MOecd->listalloecd();
        $data['funding']= $this->MFunding->listallFunding();
	    $funding = $type['funding_id'];
	    $data['exa'] =  $this->MExecuting->getExecutingByfunding($funding);
	    $data['im'] =  $this->MImplement->listallImplement();
        $data['ac'] = $this->MActivity->listallactivity();
        $data['mu'] = $this->MMultilateral->listmultilateral();
        $data['country'] = $this->MCountry->listallcountry();
        $data['user'] = $this->MUser->listalluser();
        $data['file'] = $this->MProject->listallfilebyprojectid($projectid);
	    $userid = $type['project_owner'];
	    $data['projectassistant'] = $this->MUser->getUsernotinProjectowner($userid);
        $data['co'] = $this->MProject->getCofundingByprojectid($projectid);
	    $data['dis'] = $this->MProject->getDisburesemeByprojectid($projectid);
	    $data['bt'] = $this->MProject->getbudgetByprojectid($projectid);
	    $data['kt'] = $this->MProject->getkindByprojectid($projectid);
	    $data['kindtype'] = $this->MKindtype->listallKindtype();
        $data['bgtype'] = $this->MBudgettype->listallBudgettype();
        $data['planname'] = $this->MPlan->getPlanbyid($planid);
        $data['ptname'] = $this->MProjecttype->getProjecttypetypebyid($projecttypeid);





        if($projecttypeid == "2"){
        $data['page'] = $this->config->item('wconfig_template_admin').'editproject';
        }if($projecttypeid == "1"){
        $data['page'] = $this->config->item('wconfig_template_admin').'editproject';
        }if($projecttypeid == "3"){
        $data['page'] = $this->config->item('wconfig_template_admin').'editloan';
        }if($projecttypeid == "4"){
        $data['page'] = $this->config->item('wconfig_template_admin').'editcontribution';
        }
        $this->load->view($this->_container_admin,$data);
    }

    function listcountry(){
       $country = $this->MCountry->listallcountry();
       echo  "

	   <tr><td>
	   	   <script>
  $(document).ready(function(){
  $('.chosen-select').chosen({allow_single_deselect:true,no_results_text:'Oops, nothing found!'});
  $('.number').number( true, 2 );
});
</script>
	   <strong>Country</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <select name=\"country_id[]\" id=\"country_id\" class=\"chosen-select\" data-placeholder=\"Choose a Country Name...\">
       <option value=\"\" selected=\"selected\"></option>";
        foreach($country as $list){
             echo  "<option value=\"".$list['country_id']."\">".$list['country_name']."</option>";
       }
      echo " </select>
       </td><td>
       <strong>Budget</strong> </td>
<td align=\"right\">
       <input type=\"text\" name=\"co_budget[]\" id=\"project_budget\" class=\"text-input small-input number\" > &nbsp;&nbsp;&nbsp;THB.</td>
      <td> <a href=\"javascript:void(0);\" class=\"remCF button red\">Del</a> </td>
        </tr>";
    }

	function listDisbureseme(){
			$country = $this->MCountry->listallcountry();
			echo  "<tr>
			    <script>
  $(document).ready(function(){
  $( \".datedis\" ).datepicker({ dateFormat: \"yy-mm-dd\" ,changeMonth: true, changeYear: true });
  $('.chosen-select').chosen({allow_single_deselect:true,no_results_text:'Oops, nothing found!'});
    $('.number').number( true, 2 );


			$('input[name=\"dis_budget[]\"]').keyup(function(){
			   var val = 0;
			   var total = $('#res').val();
			   $('input[name=\"dis_budget[]\"]').each(function() {
							val += (Number($(this).val()));
						});
				$('input[name=\"res\"]').val(val+(Number(total)));
			});
			});

</script>
    <td ><strong> Titile </strong>
     <select name=\"title[]\" id=\"title\" class=\"chosen-select\" data-placeholder=\"Choose a Title...\" style=\"width:100px;\">
       <option value=\"\"></option>
        <option value=\"1\" selected=\"selected\">Grant</option>
        <option value=\"2\">Loan</option>
      </select></td>
    <td ><strong>Country </strong>
      <select name=\"discountry[]\" id=\"discountry\" class=\"chosen-select\" data-placeholder=\"Choose a Country Name...\" style=\"width:180px;\">
        <option value=\"\"></option>";
         foreach($country as $list){
        echo "<option value=\"".$list['country_id']."\">".$list['country_name']."</option>";
       }
       echo  "</select></td>
    <td><strong>Budget</strong>
    </td>
     <td align=\"right\">
      <input type=\"text\" name=\"dis_budget[]\" id=\"dis_budget\" class=\"text-input  required number\" style=\"width:100px\" />     THB.

</td>
    <td ><strong>Date </strong>
      <input type=\"text\" name=\"disdate[]\" id=\"disdate".$this->input->post('add')."\" class=\"text-input datedis\"  style=\"width:70px\" /></td>
    <td ><input type=\"button\" name=\"redis\" id=\"redis\" value=\"Del\" class=\"button red\" /></td>
  </tr>";
	}

        function listDisburesemecontribution(){
            			$country = $this->MCountry->listallcountry();
			echo  "<tr>
			    <script>
  $(document).ready(function(){
  $( \".datedis\" ).datepicker({ dateFormat: \"yy-mm-dd\" ,changeMonth: true, changeYear: true });
  $('.chosen-select').chosen({allow_single_deselect:true,no_results_text:'Oops, nothing found!'});
    $('.number').number( true, 2 );

                    $('input[name=\"dis_budget[]\"]').keyup(function(){
			   var val = 0;
			   var total = $('#res').val();
			   $('input[name=\"dis_budget[]\"]').each(function() {
							val += (Number($(this).val()));
						});
				$('input[name=\"res\"]').val(val+(Number(total)));
			});

});
</script>
    <td ><strong> Titile </strong>
     <select name=\"title[]\" id=\"title\" class=\"chosen-select\" data-placeholder=\"Choose a Title...\" style=\"width:120px;\">
       <option value=\"\"></option>
        <option value=\"3\">ค่าบำรุงสมาชิก</option>
        <option value=\"4\">เงินสมทบกองทุน</option>
        <option value=\"5\">เงินอุดหนุน</option>
      </select></td>
    <td><strong>Country </strong>
      <select name=\"discountry[]\" id=\"discountry\" class=\"chosen-select\" data-placeholder=\"Choose a Country Name...\" style=\"width:180px;\">
        <option value=\"\"></option>";
         foreach($country as $list){
        echo "<option value=\"".$list['country_id']."\">".$list['country_name']."</option>";
       }
       echo  "</select></td>
    <td ><strong>Budget</strong></td>
     <td  align=\"right\"> <input type=\"text\" name=\"dis_budget[]\" id=\"dis_budget\" class=\"text-input  required number\"  style=\"width:100px;\"/>  THB.</td>
    <td><strong>Date </strong>
      <input type=\"text\" name=\"disdate[]\" id=\"disdate".$this->input->post('add')."\" class=\"text-input datedis\"  style=\"width:70px;\" /></td>
    <td width=\"3%\"><input type=\"button\" name=\"redis\" id=\"redis\" value=\"Del\" class=\"button red\" /></td>
  </tr>";
        }

        /*    function listkind(){
        $kind = $this->MKind->listallKind();
        echo "

        <tr>
				<script>
  $(document).ready(function(){
  $('.chosen-select').chosen({allow_single_deselect:true,no_results_text:'Oops, nothing found!'});
});
</script>
        <td><strong>Kind Name</strong>
          <select name=\"kind_id[]\" id=\"kind_id\" class=\"chosen-select\" data-placeholder=\"Choose a Kind Name...\">";
             echo "<option value=\"\" selected=\"selected\"></option>";
          foreach($kind as $listkind){
             echo "<option value=\"".$listkind['kind_id']."\">".$listkind['kind_name']."</option>";
            }
        echo "</select></td>
        <td><strong>Estimated</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type=\"text\" name=\"kind_estimated[]\" id=\"kind_estimated[]\" class=\"text-input small-input\" >
          </td>
		     <td><a href=\"javascript:void(0);\" class=\"rekind button red\">Del</a></td>
      </tr>";
    }*/

	    function listbtadd(){

        $bt = $this->MBudgettype->listallBudgettype();
        //$kt = $this->MKindtype->listallKindtype();

        echo "
        <tr>
		<script>
		  $(document).ready(function(){
		  $('.chosen-select').chosen({allow_single_deselect:true,no_results_text:'Oops, nothing found!'});
		  $('.number').number( true, 2 );
		});
		</script>
        <td><strong>Budget Type Name</strong>
          <select name=\"bt_id[]\" id=\"bt_id\" class=\"chosen-select\" data-placeholder=\"Choose a Budget Type...\">";
             echo "<option value=\"\" selected=\"selected\"></option>";
          foreach($bt as $listbt){
             echo "<option value=\"".$listbt['bt_name']."\">".$listbt['bt_name']."</option>";
            }
        echo "</select></td>
        <td ><strong>Budget</strong></td>
          <td  align=\"right\"><input type=\"text\" name=\"budget_money[]\" id=\"budget_money[]\" class=\"text-input m-input number\" />
&nbsp;&nbsp;THB.</td>
		     <td><a href=\"javascript:void(0);\" class=\"rebt button red\">Del</a></td>
      </tr>";
    }




/*    function delkind(){
        $id=$this->input->post('id');
        $this->db->where('pk_id',$id);
        $this->db->delete('project_kind');
    }*/

    function delcountry(){
        $id=$this->input->post('id');
        $this->db->where('co_id',$id);
        $this->db->delete('co_funding');
    }

	function delbudgettype(){
		 $id=$this->input->post('id');
        $this->db->where('b_id',$id);
        $this->db->delete('budget_project');
	}
	function delkindproject(){
		 $id=$this->input->post('id');
        $this->db->where('project_id',$id);
        $this->db->delete('projectkind');
	}

	function deldisbureseme(){
	$id=$this->input->post('id');
        $this->db->where('dis_id',$id);
        $this->db->delete('disbureseme');
	}

    function updateproject(){
        if($this->input->post('projectid') ==""){
            $this->MProject->insertproject();
            flashMsg('success insert',$this->lang->line('insert_success'));
            redirect( 'project/admin/','refresh');
        }else{
            $this->MProject->updateproject();
            flashMsg('success update',$this->lang->line('update_success'));
            redirect( 'project/admin/','refresh');
        }
    }

    function updateprojectloan(){
        if($this->input->post('projectid') == ""){
        $this->MProject->insertprojectloan();
        flashMsg('success',$this->lang->line('insert_success'));
        redirect( 'project/admin/','refresh');
        }else{
        $this->MProject->updateprojectloan();
        flashMsg('success',$this->lang->line('update_success'));
        redirect( 'project/admin/','refresh');
         }
    }

    function updatecontibution(){
        if($this->input->post('projectid') == ""){
        $this->MProject->insertprojectcontibution();
        flashMsg('success',$this->lang->line('insert_success'));
        redirect( 'project/admin/','refresh');
        }else{
        $this->MProject->updatecontibution();
        flashMsg('success',$this->lang->line('update_success'));
        redirect( 'project/admin/','refresh');
        }
    }


	function getprojectbyplan(){
				$data['menu'] = $this->MUser->getMenubyUser();

				$id = $this->input->post('projecttype');
				$plan = $this->input->post('plan');
				if(empty($id)){
					$and = "";
				}else{
					$and = "and  project.project_type_id = ".$id."";
				}

               $Q=$this->db->query("select project.*,p.plan_name,pt.project_type_name from project as project
			   inner join plan as p on project.plan_id = p.plan_id
			   inner join project_type as pt on project.project_type_id = pt.project_type_id
			   where project.plan_id = '$plan' $and
			   ");
				$data['project'] = $Q->result_array();
			   $Q->free_result();
			   $data['page'] = $this->config->item('wconfig_template_admin').'projectlist';
				$this->load->view($this->_view_subsector,$data);
			}



	function getprojectbyprojecttype(){
				$id=$this->input->post('projecttype');
				$plan = $this->input->post('plan');
				if(empty($plan)){
					$and = "";
				}else{
					$and = "and  project.plan_id = ".$plan."";
				}

              $Q=$this->db->query("select project.*,p.plan_name,pt.project_type_name from project as project
			   inner join plan as p on project.plan_id = p.plan_id
			   inner join project_type as pt on project.project_type_id = pt.project_type_id
			   where project.project_type_id = '$id' $and
			   ");
             $data['project'] = $Q->result_array();
			 $Q->free_result();
			$data['page'] = $this->config->item('wconfig_template_admin').'projectlist';
			$this->load->view($this->_view_subsector,$data);
	}

	function getExecuting(){
			    $id=$this->input->post('id');
        	   $Q=$this->db->query("select * from executing where  funding_id='$id'");
			   echo "<script>
						  $(document).ready(function(){
						  $('.chosen-select').chosen({allow_single_deselect:true,no_results_text:'Oops, nothing found!'});
						});
						</script>";
			   echo '<select name="executing_id" id="executing_id" class="chosen-select" data-placeholder="Choose Executing Agency.." >';
			   echo '<option value=""></option>';
              foreach($Q->result_array() as $listex){
   					echo "<option value=\"".$listex['executing_id']."\">".$listex['executing_name']."</option>";
              }
			  echo " </select>";
	}

	function getimplement(){
			 $id=$this->input->post('id');
        	   $Q=$this->db->query("select * from implement where  funding_id='$id'");
			   echo "<script>
						  $(document).ready(function(){
						  $('.chosen-select').chosen({allow_single_deselect:true,no_results_text:'Oops, nothing found!'});
						});
						</script>";
			   echo '<select name="implement_id" id="implement_id" class="chosen-select" data-placeholder="Choose Implement Agency.." >';
			   echo '<option value=""></option>';
              foreach($Q->result_array() as $listex){
   					echo "<option value=\"".$listex['implement_id']."\">".$listex['implement_name']."</option>";
              }
			  echo " </select>";

	}

        function projectnotcomplete(){
        //$status = 0;
        $data['header'] = "Project Not Complete";
        $data['plan'] = $this->MPlan->listAllplan();
        $data['projecttype'] = $this->MProjecttype->listallProjecttype();
        $data['project'] = $this->MProject->listAllprojectstatus();
    	//$data['menu'] = $this->MUser->getMenubyUser();
        $data['page'] = $this->config->item('wconfig_template_admin').'projectnotcomplete';
    	$this->load->view($this->_container_admin,$data);
        }

        function test(){
            $data['header'] ="test";
             $data['user'] = $this->MUser->listalluser();
             $data['page'] = $this->config->item('wconfig_template_admin').'test';
    	$this->load->view($this->_container_admin,$data);
        }

        function fileuploadlist(){
            echo '
            <tr>
               <td width="283"><input name="files[]" type="file"  id="files[]" multiple="multiple"></td>
               <td width="51"><input type="button" name="button2" id="reF" value="del" class="button red" /></td>
             </tr>';
        }

        function delfileproject(){
            $fileid=$this->input->post('id');
            $delfile = $this->MProject->getfilebyid($fileid);
            @unlink('./upload/project/'.$delfile['project_file']);


            $this->db->where('file_id',$fileid);
            $this->db->delete('project_file');
        }


}
