<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Budget extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    	function index(){
	    $data['header'] = "Budget Finance";
        $data['all'] = $this->MAid->listallAidadminForBudget();
        $data['plan'] = $this->MPlan->listAllplan();
		$data['project'] = $this->MProject->listallproject();
		$data['menu'] = $this->MUser->getFinanceMenubyUser();      
    	$data['page'] = $this->config->item('wconfig_template_admin').'budget';
    	$this->load->view($this->_container_admin,$data); 
	}
        
        function editbudget($mbid=0){ 
        $aid = $this->uri->segment(4);
        $mbid = $this->uri->segment(5);
        $data['header'] = "Budget Finance";
		$data['ex'] = $this->MExpense->listallexpense();
        $data['detail'] = $this->MAid->getAidJoin($aid);
        $data['edit'] = $this->MBudget->getBudgetbyaid($mbid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editbudget';
    	$this->load->view($this->_container_admin,$data);
        }
        
        function insert(){
            $aid = $this->input->post('aidid');
            if($this->input->post('mbid') == 0){
            $this->MBudget->insert(); 
            $this->db->select_max('mb_id');
            /*$max = $this->db->get('money_budget');
            $maxmp = $max->row_array();*/
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/budget/editbudget/'.$aid,'refresh');
            }else{
            $this->MBudget->update(); 
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/budget/editbudget/'.$aid.'/'.$this->input->post('mbid'),'refresh');
            }
            
        }
        
        function getaidbyplanid(){
            $id=$this->input->post('id');     
             $Q=$this->db->query("select a.*,
                                p.plan_id,project.plan_id,activity.project_id
                                from aid as a
                                inner join activity as activity on activity.activity_id=a.activity_id
                                inner join project as project on project.project_id=activity.project_id
                                inner join plan as p on p.plan_id=project.plan_id
                                where p.plan_id = '$id'
                            ");
		     $data['all'] = $Q->result_array();
		     $data['menu'] = $this->MUser->getMenubyUser();    
		     $data['page'] = $this->config->item('wconfig_template_admin').'resultaid';      
		     $this->load->view($this->_view_subsector,$data);
        }
        
        function getdatabyprojectid(){
                $projectid=$this->input->post('projectid');   
                $planid=$this->input->post('planid'); 
            
             $Q=$this->db->query("select a.*,
             p.plan_id,project.project_id,project.plan_id,activity.project_id
              from aid as a 
              inner join activity as activity on activity.activity_id=a.activity_id
                inner join project as project on project.project_id=activity.project_id
              inner join plan as p on p.plan_id='$planid'
			  where project.project_id='$projectid'
			   ");
			  $data['all'] = $Q->result_array();
			$data['menu'] = $this->MUser->getMenubyUser();  
                        $data['type'] = $this->input->post('type');
			$data['page'] = $this->config->item('wconfig_template_admin').'resultaid';      
			$this->load->view($this->_view_subsector,$data);
	}
        
        function getdatabyActiivityid(){
	    $id=$this->input->post('id');     
            $Q=$this->db->query("select a.*,p.plan_id,project.plan_id,activity.project_id
             from aid as a
             inner join activity as activity on activity.activity_id=a.activity_id
             inner join project as project on project.project_id=activity.project_id
             inner join plan as p on p.plan_id=project.plan_id 
			  where activity.activity_id='$id'
			   ");
			    $data['all'] = $Q->result_array();
				$data['menu'] = $this->MUser->getMenubyUser();   
                                $data['type'] = $this->input->post('type');
				$data['page'] = $this->config->item('wconfig_template_admin').'resultaid';      
				$this->load->view($this->_view_subsector,$data);
	}
        
        function histotybudget(){
            $aid = $this->uri->segment(4);
            $data['header'] = "History";
            $data['detail'] = $this->MAid->getAidJoin($aid);
            $data['page'] = $this->config->item('wconfig_template_admin').'histotybudget';
            $this->load->view($this->_container_admin,$data);
        }
        
        function delbudget(){
            $aid = $this->uri->segment('4');
            $mbid = $this->uri->segment('5');
            $this->MBudget->delbudget($mbid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'finance/budget/histotybudget/'.$aid,'refresh');
        }
        

    
}