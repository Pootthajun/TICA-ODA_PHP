<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pay extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index($groupid,$projecttype){
        $groupname = $this->MAidtype->gettypegroupbyid($groupid);
        $data['header'] = 'ระบบการเงิน '.$groupname['tgroup_name'];
        $data['plan'] = $this->MPlan->listAllplan();
        $data['group'] = $groupname;
        $data['all'] = $this->MRecipient->getrecipientbygrouptype($groupid,$projecttype);   
        $data['non'] =  $this->MAid->getnone($groupid,$projecttype);
		$data['menu'] = $this->getPaybyUser();
    	$data['page'] = $this->config->item('wconfig_template_admin').'pay';
    	$this->load->view($this->_container_admin,$data);       
    }
	
	    function getPaybyUser(){
        $data = array();        
        $this->db->where('user_id',$this->session->userdata('userid'));
        $this->db->where('menu_module',"pay/index/".$this->uri->segment(4)."/".$this->uri->segment(5));
        $QMENU = $this->db->get('userauthen');
        $data = $QMENU->num_rows();
        $QMENU->free_result();
        return $data;
    }
    
    function editpay($mpid = 0){
        $recid = $this->uri->segment(5);
        $mpid = $this->uri->segment(6);
        $data['header'] = "Pay Finance";
	$data['ex'] = $this->MExpense->listallexpense();
        $data['detail'] = $this->MRecipient->getRecipientJoin($recid);
        $data['edit'] = $this->MFinance->getPaybympid($mpid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editpay';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function editpaynon($mpid = 0){
        $aid = $this->uri->segment(4);
        $mpid = $this->uri->segment(5);
        $data['header'] = "Pay Finance";
        $data['country'] = $this->MCountry->listallcountry();
        $data['detail'] = $this->MAid->getAidJoin($aid);
        $data['edit'] = $this->MFinance->getPaynonbympid($mpid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editpaynon';
    	$this->load->view($this->_container_admin,$data);
    }
	

        
        function getprojectlistbyplanid(){
	$id=$this->input->post('id');
        $projecttype=$this->input->post('projecttype');
        $Q=$this->db->query("select * from project where  plan_id='$id' and project_type_id ='$projecttype' "); 
        $data['option'] = $Q->result_array();	          
	$data['page'] = $this->config->item('wconfig_template_admin').'projectlist';      
	$this->load->view($this->_view_subsector,$data); 
	}
        
        function getrecipientbyplanid(){
	     $id=$this->input->post('id'); 
             $groupid = $this->input->post('groupid');
             $projecttype = $this->input->post('projecttype');
             $sql = "select r.*,a.aid_name,a.activity_id,at.aid_type_group,c.country_name,
             p.plan_id,project.plan_id,activity.project_id
              from recipient as r 
              inner join aid as a on r.aid_id = a.aid_id
              inner join aid_type as at on a.aid_type_id = at.aid_type_id 
              inner join country as c on r.country_id = c.country_id
              inner join activity as activity on activity.activity_id=a.activity_id
              inner join project as project on project.project_id=activity.project_id
              inner join plan as p on p.plan_id=project.plan_id
	      where p.plan_id = '$id' AND r.status = 2
              and at.aid_type_group = '$groupid'
              and project.project_type_id = '$projecttype'
			   ";
             $Q=$this->db->query($sql);
		     $data['all'] = $Q->result_array();
                     $data['type'] = $this->input->post('type');
		     $data['menu'] = $this->MUser->getMenubyUser();    
			$data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
			$this->load->view($this->_view_subsector,$data);
	 }
                
        function getActivitylistbyprojectid(){
				 $id=$this->input->post('id');
          		 $Q=$this->db->query("select * from activity where  project_id='$id'"); 
                 $data['option'] = $Q->result_array();	          
				$data['page'] = $this->config->item('wconfig_template_admin').'activityselectlist';      
				$this->load->view($this->_view_subsector,$data);
        }
        
        function getdatabyprojectid(){
			$projectid=$this->input->post('projectid');   
			$planid=$this->input->post('planid');
                        $groupid = $this->input->post('groupid');
                        
            
             $Q=$this->db->query("select r.*,a.aid_name,at.aid_type_group,a.activity_id,c.country_name,
             p.plan_id,project.project_id,project.plan_id,activity.project_id
              from recipient as r 
			  inner join aid as a on r.aid_id = a.aid_id
                          inner join aid_type as at on a.aid_type_id = at.aid_type_id 
              inner join country as c on r.country_id = c.country_id
              inner join activity as activity on activity.activity_id=a.activity_id
			  inner join project as project on project.project_id=activity.project_id
              inner join plan as p on p.plan_id='$planid'
			  where project.project_id='$projectid' AND r.status = 2
                              and at.aid_type_group = '$groupid'
                              
			   ");
			  $data['all'] = $Q->result_array();
			$data['menu'] = $this->MUser->getMenubyUser();  
                        $data['type'] = $this->input->post('type');
			$data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
			$this->load->view($this->_view_subsector,$data);
	}
        
        function getAidlistbyActiivityid(){
				$id=$this->input->post('id');
          		 $Q=$this->db->query("select * from aid where  activity_id='$id'"); 
                $data['option'] = $Q->result_array();	          
				$data['page'] = $this->config->item('wconfig_template_admin').'aidselectlist';      
				$this->load->view($this->_view_subsector,$data); 
	}
	
        function getdatabyActiivityid(){
			$id=$this->input->post('id');    
                        $groupid = $this->input->post('groupid');
$projecttype = $this->input->post('projecttype');
            $Q=$this->db->query("select r.*,a.aid_name,at.aid_type_group,a.activity_id,c.country_name,
             p.plan_id,project.plan_id,activity.project_id
             from recipient as r 
			inner join aid as a on r.aid_id = a.aid_id
                        inner join aid_type as at on a.aid_type_id = at.aid_type_id 
            inner join country as c on r.country_id = c.country_id
           inner join activity as activity on activity.activity_id=a.activity_id
			inner join project as project on project.project_id=activity.project_id
             inner join plan as p on p.plan_id=project.plan_id 
			  where activity.activity_id='$id' AND r.status = 2
                              and at.aid_type_group = '$groupid'
                                and project.project_type_id = '$projecttype'
			   ");
			    $data['all'] = $Q->result_array();
				$data['menu'] = $this->MUser->getMenubyUser();   
                                $data['type'] = $this->input->post('type');
				$data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
				$this->load->view($this->_view_subsector,$data);
	}
        
        function getRecipientbyAid(){	
	     $id=$this->input->post('id');
             $groupid = $this->input->post('groupid');
$projecttype = $this->input->post('projecttype');
             $Q=$this->db->query("select r.*,a.aid_name,at.aid_type_group,a.activity_id,c.country_name,
             p.plan_id,project.plan_id,activity.project_id
             from recipient as r 
			inner join aid as a on r.aid_id = a.aid_id
                        inner join aid_type as at on a.aid_type_id = at.aid_type_id 
            inner join country as c on r.country_id = c.country_id
           inner join activity as activity on activity.activity_id=a.activity_id
			inner join project as project on project.project_id=activity.project_id
             inner join plan as p on p.plan_id=project.plan_id 
			  where r.aid_id = '$id' AND r.status = 2
                              and at.aid_type_group = '$groupid'
                                and project.project_type_id = '$projecttype'
			   ");
	$data['all'] = $Q->result_array();
	$data['menu'] = $this->MUser->getMenubyUser();  
        $data['type'] = $this->input->post('type');
        $data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
	$this->load->view($this->_view_subsector,$data);
	}
        
        
        
        function insertpay(){
            //check rec
            $mpid = $this->input->post('mpid');
            $check = $this->MFinance->getPaybympid($mpid);
            //if($this->input->post('mpid') == 0){
            if($check == TRUE){
            $this->MFinance->updatepay();
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/pay/editpay/'.$this->input->post('aidid').'/'.$this->input->post('recipientid').'/'.$this->input->post('mpid'),'refresh');
            }else{
            $this->MFinance->insertpay(); 
            /*$this->db->select_max('mp_id');
            $max = $this->db->get('money_paid');
            $maxmp = $max->row_array();*/
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/pay/editpay/'.$this->input->post('aidid').'/'.$this->input->post('recipientid') ,'refresh');
            }
            //$this->MFinance->insertpay();
            
        }
        
        function insertpaynon(){
            //check rec
            $mpid = $this->input->post('mpid');
            $check = $this->MFinance->getPaynonbympid($mpid);
            //if($this->input->post('mpid') == 0){
            if($check == TRUE){
            $this->MFinance->updatepaynon();
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/pay/editpaynon/'.$this->input->post('aidid').'/'.$this->input->post('mpid'),'refresh');
            }else{
            $this->MFinance->insertpaynon(); 
            /*$this->db->select_max('mp_id');
            $max = $this->db->get('money_paid');
            $maxmp = $max->row_array();*/
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/pay/editpaynon/'.$this->input->post('aidid') ,'refresh');
            }
            //$this->MFinance->insertpay();
        }
                
        function waitpay(){            
            $data['header']= "Wait for pay";
            $pay = $this->MFinance->getallpay();
            if(count($pay) > 0){
            $data['all'] = $this->MFinance->waitpay();
            }else{
            $this->index();
            }
            $data['menu'] = $this->MUser->getMenubyUser();   
            $data['type'] = $this->input->post('type');
            $data['page'] = $this->config->item('wconfig_template_admin').'waitpay';      
            $this->load->view($this->_container_admin,$data);
        }
        
        function histotypay(){
            $recid = $this->uri->segment(5);
            $data['header'] = "History";
            $data['detail'] = $this->MRecipient->getRecipientJoin($recid);
            $data['page'] = $this->config->item('wconfig_template_admin').'histotypay';
            $this->load->view($this->_container_admin,$data);
        }
        
        function histotypaynon(){
            $aid = $this->uri->segment(4);
            $data['header'] = "History";
            $data['detail'] = $this->MAid->getAidJoin($aid);
            $data['page'] = $this->config->item('wconfig_template_admin').'histotypaynon';
            $this->load->view($this->_container_admin,$data);
        }
                
        function delpay(){
            $aid = $this->uri->segment('4');
            $recid = $this->uri->segment('5');
            $mpid = $this->uri->segment('6');
            $this->MFinance->delpay($mpid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'finance/pay/histotypay/'.$aid.'/'.$recid,'refresh');
        }
        
        function delpaynon(){
            $aid = $this->uri->segment('4');
            $mpid = $this->uri->segment('5');
            $this->MFinance->delpaynon($mpid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'finance/pay/histotypaynon/'.$aid,'refresh');
        }
}