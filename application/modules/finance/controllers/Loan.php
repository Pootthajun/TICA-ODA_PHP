<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loan extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $status = 2;
        $data['header'] = "Loan Finance";
        $data['all'] = $this->MRecipient->listAllrecipientbyStatus($status);
        $data['plan'] = $this->MPlan->listAllplan();
        //$data['non'] =  $this->MAid->getnone($groupid,$projecttype);
		$data['menu'] = $this->MUser->getFinanceMenubyUser();
    	$data['page'] = $this->config->item('wconfig_template_admin').'loan';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function editloan($mlid =0){ 
        $recid = $this->uri->segment(5);
        $mlid = $this->uri->segment(6);
        $data['header'] = "Loan Finance";
        $data['detail'] = $this->MRecipient->getRecipientJoin($recid);
        $data['edit'] = $this->MLoan->getLoanbyrecid($mlid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editloan';
    	$this->load->view($this->_container_admin,$data);
        }
        
        function editloannon(){
        $aid = $this->uri->segment(4);
        $mlid = $this->uri->segment(5);
        $data['header'] = "Loan Finance";
        $data['country'] = $this->MCountry->listallcountry();
        $data['detail'] = $this->MAid->getAidJoin($aid);
        $data['edit'] = $this->MLoan->getLoannonbymlid($mlid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editloannon';
    	$this->load->view($this->_container_admin,$data);   
        }
        function insert(){
            if($this->input->post('mlid') == 0){
            $this->MLoan->insert();
            $this->db->select_max('ml_id');
           /* $max = $this->db->get('money_loan');
            $maxmp = $max->row_array();*/
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/loan/editloan/'.$this->input->post('aid').'/'.$this->input->post('recid'),'refresh');
            }else{
            $this->MLoan->update(); 
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/loan/editloan/'.$this->input->post('aid').'/'.$this->input->post('recid').'/'.$this->input->post('mlid'),'refresh');
            }
            
        }
        
        function insertloannon(){
            if($this->input->post('mlid') == 0){
            $this->MLoan->insertnon();
            $this->db->select_max('ml_id');
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/loan/editloannon/'.$this->input->post('aid'),'refresh');
            }else{
            $this->MLoan->updatenon(); 
            flashMsg('success',$this->lang->line('insert_success'));
            redirect( 'finance/loan/editloannon/'.$this->input->post('aid').'/'.$this->input->post('mlid'),'refresh');
            }
        }
                
        function getrecipientbyplanid(){
			$id=$this->input->post('id');     
             $Q=$this->db->query("select r.*,a.aid_name,a.activity_id,c.country_name,
             p.plan_id,project.plan_id,activity.project_id
              from recipient as r 
			  inner join aid as a on r.aid_id = a.aid_id
              inner join country as c on r.country_id = c.country_id
              inner join activity as activity on activity.activity_id=a.activity_id
              inner join project as project on project.project_id=activity.project_id
              inner join plan as p on p.plan_id=project.plan_id
			  where p.plan_id = '$id' AND r.status = 2
			   ");
		     $data['all'] = $Q->result_array();
                     $data['type'] = $this->input->post('type');
		     $data['menu'] = $this->MUser->getMenubyUser();    
			$data['page'] = $this->config->item('wconfig_template_admin').'resultloan';      
			$this->load->view($this->_view_subsector,$data);
	 }
        
        function getdatabyprojectid(){
                $projectid=$this->input->post('projectid');   
                $planid=$this->input->post('planid'); 
            
              $Q=$this->db->query("select r.*,a.aid_name,a.activity_id,c.country_name,
             p.plan_id,project.project_id,project.plan_id,activity.project_id
              from recipient as r 
			  inner join aid as a on r.aid_id = a.aid_id
              inner join country as c on r.country_id = c.country_id
              inner join activity as activity on activity.activity_id=a.activity_id
			  inner join project as project on project.project_id=activity.project_id
              inner join plan as p on p.plan_id='$planid'
			  where project.project_id='$projectid' AND r.status = 2
			   ");
			  $data['all'] = $Q->result_array();
			$data['menu'] = $this->MUser->getMenubyUser();  
                        $data['type'] = $this->input->post('type');
			$data['page'] = $this->config->item('wconfig_template_admin').'resultloan';      
			$this->load->view($this->_view_subsector,$data);
	}
        
        function getdatabyActiivityid(){
			$id=$this->input->post('id'); 
	    $Q=$this->db->query("select r.*,a.aid_name,a.activity_id,c.country_name,
             p.plan_id,project.plan_id,activity.project_id
             from recipient as r 
			inner join aid as a on r.aid_id = a.aid_id
            inner join country as c on r.country_id = c.country_id
           inner join activity as activity on activity.activity_id=a.activity_id
			inner join project as project on project.project_id=activity.project_id
             inner join plan as p on p.plan_id=project.plan_id 
			  where activity.activity_id='$id' AND r.status = 2
			   ");
			    $data['all'] = $Q->result_array();
				$data['menu'] = $this->MUser->getMenubyUser();   
                                $data['type'] = $this->input->post('type');
				$data['page'] = $this->config->item('wconfig_template_admin').'resultloan';      
				$this->load->view($this->_view_subsector,$data);
	}
	
	function getRecipientbyAid(){	
	     $id=$this->input->post('id');
             $Q=$this->db->query("select r.*,a.aid_name,c.country_name from recipient as r 
			  inner join aid as a on r.aid_id = a.aid_id
              inner join country as c on r.country_id = c.country_id
			  where r.aid_id = '$id' AND r.status = 2
			   ");
	$data['all'] = $Q->result_array();
	$data['menu'] = $this->MUser->getMenubyUser();  
        $data['type'] = $this->input->post('type');
        $data['page'] = $this->config->item('wconfig_template_admin').'resultloan';      
	$this->load->view($this->_view_subsector,$data);
	}
        
         function histotyloan(){
            $recid = $this->uri->segment(5);
            $data['header'] = "History";
            $data['detail'] = $this->MRecipient->getRecipientJoin($recid);
            $data['page'] = $this->config->item('wconfig_template_admin').'histotyloan';
            $this->load->view($this->_container_admin,$data);
        }
        
        function historyloannon(){
            $aid = $this->uri->segment(4);
            $data['header'] = "History";
            $data['detail'] = $this->MAid->getAidJoin($aid);
            $data['page'] = $this->config->item('wconfig_template_admin').'histotyloannon';
            $this->load->view($this->_container_admin,$data);
        }
                
        function delloan(){
            $aid = $this->uri->segment('4');
            $recid = $this->uri->segment('5');
            $mlid = $this->uri->segment('6');
            $this->MLoan->delloan($mlid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'finance/loan/histotyloan/'.$aid.'/'.$recid,'refresh');
        }
}