<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MLoan extends CI_Model{
    
    function insert(){
        $data = array(
            'aid_id'=>$this->input->post('aid'),
            'rec_id'=>$this->input->post('recid'),
            'loan_number'=>$this->input->post('loan_number'),
            'ml_date'=>$this->input->post('pay_date'),
            'ml_amount'=>str_replace(",","",$this->input->post('amount')),
            'ml_year'=>$this->input->post('ml_year'),
            'ml_date_back'=>$this->input->post('pay_date_back'),
            'ml_money_back'=>str_replace(",","",$this->input->post('money_back')),
            'ml_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'ml_create'=>date('Y-m-d H:i:s'),  
        );
        $this->db->insert('money_loan',$data);
    } 
    
        function insertnon(){
            $data = array(
            'aid_id'=>$this->input->post('aid'),
            'country_id'=>$this->input->post('country_id'),
            'ml_no'=>$this->input->post('pay_no'),
            'ml_date'=>$this->input->post('pay_date'),
            'ml_amount'=>str_replace(",","",$this->input->post('amount')),
            'ml_year'=>$this->input->post('ml_year'),
            'ml_title'=>$this->input->post('title'),
            'ml_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'ml_create'=>date('Y-m-d H:i:s'),                 
        );
        $this->db->insert('money_loan_non',$data);
        }
        
        function updatenon(){
            $data = array(
            'aid_id'=>$this->input->post('aid'),
            'country_id'=>$this->input->post('country_id'),
            'ml_no'=>$this->input->post('pay_no'),
            'ml_date'=>$this->input->post('pay_date'),
            'ml_amount'=>str_replace(",","",$this->input->post('amount')),
            'ml_year'=>$this->input->post('ml_year'),
            'ml_title'=>$this->input->post('title'),
            'ml_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'ml_create'=>date('Y-m-d H:i:s'),                 
        );
            $this->db->where('ml_id',$this->input->post('mlid'));
            $this->db->update('money_loan_non',$data);
        }
        
        function getLoanbyrecid($mlid){
        $this->db->where('ml_id',$mlid);
        $Q = $this->db->get('money_loan');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
    
    function getLoannonbymlid($mlid){
        $this->db->where('ml_id',$mlid);
        $Q = $this->db->get('money_loan_non');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
            
    function update(){
        $data = array(
            'rec_id'=>$this->input->post('recid'),
            'loan_number'=>$this->input->post('loan_number'),
            'ml_date'=>$this->input->post('pay_date'),
            'ml_amount'=>str_replace(",","",$this->input->post('amount')),
            'ml_year'=>$this->input->post('ml_year'),
            'ml_date_back'=>$this->input->post('pay_date_back'),
            'ml_money_back'=>str_replace(",","",$this->input->post('money_back')),
            'ml_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'ml_create'=>date('Y-m-d H:i:s'),  
        );
        $this->db->where('ml_id',$this->input->post('mlid'));
        $this->db->update('money_loan',$data);
    }
    
    function delloan($mlid){
        $this->db->where('ml_id',$mlid);
        $this->db->delete('money_loan');
    }
    
}