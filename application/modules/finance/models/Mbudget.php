<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MBudget extends CI_Model{
    
    function insert(){
        $data = array(
            'aid_id'=>$this->input->post('aidid'),
            'budget_type'=>$this->input->post('budget_type'),
            'mb_no'=>$this->input->post('mb_no'),
            'mb_date'=>$this->input->post('pay_date'),
            'mb_amount'=>str_replace(",","",$this->input->post('amount')),
            'mb_year'=>$this->input->post('mb_year'),
            'mb_title'=>$this->input->post('title'),
            'mb_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'mb_create'=>date('Y-m-d H:i:s'),  
        );
        $this->db->insert('money_budget',$data);
        $maxid = $this->selectMaxID();
        $mbid = $maxid->mb_id;       
        $this->insertMoneybudget($mbid);
    }
    
    function selectMaxID(){
        $data = array();
        $this->db->select_max('mb_id');
        $query = $this->db->get('money_budget'); 
        $data = $query->row();
        $query->free_result();
        return $data;
    }   
    
    
    function insertMoneybudget($mbid){
        for($i = 0;$i < count($_POST['expense_budget']);$i++ ){
            if($_POST['expense_budget'][$i] != null){
                $data  = array(
		    'mb_id' => $mbid,
                    'aid_id'=>$this->input->post('aidid'),
                    'expense_id' => $_POST['expense_id'][$i],
                    'expense_budget'=> str_replace(",","",$_POST['expense_budget'][$i]),
                 );
                $this->db->insert('money_budget_expense',$data);
            }
        }
    }   

    function getBudgetbyaid($mbid){
        $this->db->where('mb_id',$mbid);
        $Q = $this->db->get('money_budget');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
            
    function update(){
        $data = array(
            'aid_id'=>$this->input->post('aidid'),
            'mb_date'=>$this->input->post('pay_date'),
            'mb_no'=>$this->input->post('mb_no'),
            'mb_amount'=>str_replace(",","",$this->input->post('amount')),
            'mb_year'=>$this->input->post('mb_year'),
            'mb_title'=>$this->input->post('title'),
            'mb_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'mb_create'=>date('Y-m-d H:i:s'), 
        );
        $this->db->where('mb_id',$this->input->post('mbid'));
        $this->db->update('money_budget',$data);
        
        /*$this->db->where('mb_id',$this->input->post('mbid'));
        $this->db->delete('money_budget_expense');*/
        
        $mbid = $this->input->post('mbid');        
        $this->insertMoneybudget($mbid);
    }
    
    function delbudget($mbid){
        $this->db->where('mb_id',$mbid);
        $this->db->delete('money_budget_expense');
        $this->db->where('mb_id',$mbid);
        $this->db->delete('money_budget');
    }
    
}