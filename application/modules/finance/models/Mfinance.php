<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MFinance extends CI_Model{    
    
    function selectMaxIDpay(){
        $data = array();
        $this->db->select_max('mp_id');
        $query = $this->db->get('money_paid'); 
        $data = $query->row();
        $query->free_result();
        return $data;
    } 
    

    
    function insertpay(){
        $this->insert();
        /*$aid = $this->input->post('aidid');
        $pay = $this->MAid->getAidbyid($aid);
        echo $pay['aid_pay'];
        if($pay['aid_pay'] == "1"){
        $list = $this->MRecipient->getRecipientbyAid($aid);
            foreach ($list as $lists){
                $recid = $lists['rec_id'];
                $this->insert($recid);                
            } 
            
        }else{
            $recid = $this->input->post('recipientid');
            $this->insert($recid);
            
        }*/
        
    }
    
    function insert(){
        $aid = $this->input->post('aidid');
        $list = $this->MRecipient->getRecipientbyAid($aid);
        foreach ($list as $lists){
           //echo  $lists['rec_id'];
           
        $data = array(
            'aid_id'=>$this->input->post('aidid'),
            'rec_id'=>$lists['rec_id'],
            'mp_no'=>$this->input->post('pay_no'),
            'mp_date'=>$this->input->post('pay_date'),
            'mp_amount'=>str_replace(",","",$this->input->post('amount')),
            'mp_year'=>$this->input->post('mp_year'),
            'mp_title'=>$this->input->post('title'),
            'mp_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'mp_create'=>date('Y-m-d H:i:s'),                 
        );
        $this->db->insert('money_paid',$data);
        $aid = $this->input->post('aidid');
        $maxid = $this->selectMaxIDpay();
        $mpid = $maxid->mp_id; 
        $recid = $lists['rec_id'];
        $this->insertMoneypay($mpid,$recid);
        }
    }
    
    function insertpaynon(){
        $data = array(
            'aid_id'=>$this->input->post('aidid'),
            'country_id'=>$this->input->post('country_id'),
            'mp_no'=>$this->input->post('pay_no'),
            'mp_date'=>$this->input->post('pay_date'),
            'mp_amount'=>str_replace(",","",$this->input->post('amount')),
            'mp_year'=>$this->input->post('mp_year'),
            'mp_title'=>$this->input->post('title'),
            'mp_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'mp_create'=>date('Y-m-d H:i:s'),                 
        );
        $this->db->insert('money_paid_non',$data);
    }
    
    function updatepaynon(){
        $data = array(
            'aid_id'=>$this->input->post('aidid'),
            'country_id'=>$this->input->post('country_id'),
            'mp_no'=>$this->input->post('pay_no'),
            'mp_date'=>$this->input->post('pay_date'),
            'mp_amount'=>str_replace(",","",$this->input->post('amount')),
            'mp_year'=>$this->input->post('mp_year'),
            'mp_title'=>$this->input->post('title'),
            'mp_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'mp_create'=>date('Y-m-d H:i:s'),                 
        );
        $this->db->where('mp_id',$this->input->post('mpid'));
        $this->db->update('money_paid_non',$data);
    }
            
   
    
    function insertMoneypay($mpid,$recid){
        //echo count($_POST['expense_budget']);
        for($i = 0;$i < count($this->input->post('expense_budget'));$i++ ){
            if($_POST['expense_budget'][$i] != null){
                $data  = array(
		    'mp_id' => $mpid,
                    'rec_id'=>$recid,
                    'expense_id' => $_POST['expense_id'][$i],
                    'expense_budget'=> str_replace(",","",$_POST['expense_budget'][$i]),
                 );
                $this->db->insert('money_pay_expense',$data);
            }
        }
    }
    
    function getPaybympid($mpid){
    	$this->db->where('mp_id',$mpid);
        $Q = $this->db->get('money_paid');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
    
    function getPaynonbympid($mpid){
        $this->db->where('mp_id',$mpid);
        $Q = $this->db->get('money_paid_non');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }
            
	function getPaybyrecid($recid){
		$this->db->where('rec_id',$recid);
        $Q = $this->db->get('money_paid');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
	}
    
            
    function updatepay(){
        $data = array(
            'aid_id'=>$this->input->post('aidid'),
            'rec_id'=>$this->input->post('recipientid'),
            'mp_no'=>$this->input->post('pay_no'),
            'mp_date'=>$this->input->post('pay_date'),
            'mp_amount'=>str_replace(",","",$this->input->post('amount')),
            'mp_year'=>$this->input->post('mp_year'),
            'mp_title'=>$this->input->post('title'),
            'mp_comment'=>$this->input->post('comment'),
            'user_by'=>$this->session->userdata('userid'),
            'mp_create'=>date('Y-m-d H:i:s'),  
        );
        $this->db->where('mp_id',$this->input->post('mpid'));
        $this->db->update('money_paid',$data);
        
        $this->db->where('mp_id',$this->input->post('mpid'));
        $this->db->delete('money_pay_expense');
        
        $recid = $this->input->post('recipientid');
        $mpid = $this->input->post('mpid');  
        //$this->insert($recid);      
        $this->insertMoneypay($mpid,$recid);
    }
    
    function waitpay(){
        $status = 1;
        $rec = $this->MRecipient->listAllrecipientbyStatus($status);
        $data = array();
        $Q=$this->db->query("select r.*,c.country_name
             from recipient as r 
             inner join country as c on r.country_id = c.country_id 
             where r.status = 1 and r.rec_id not in (SELECT rec_id FROM money_paid)
            ");
 
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
 
        $Q->free_result();
        return $data;
    }
    
    function getallpay(){
        $data = array();
        $this->db->order_by('mp_id',"DESC");
        $Q = $this->db->get('money_paid');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function GetMoneypay(){
        $data = array();
        $sql = "select M.*,R.prefix_id,R.fullname,C.country_name,C.country_id,A.aid_name"
                . " FROM money_paid AS M"
                . " INNER JOIN recipient AS R ON R.rec_id = M.rec_id"
                . " LEFT JOIN country AS C ON R.country_id = C.country_id"
                . " LEFT JOIN aid AS A ON A.aid_id = M.aid_id"
                . " ORDER BY M.mp_id DESC";
        $Q = $this->db->query($sql);
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;        
    }
    
    function delpay($mpid){
        $this->db->where('mp_id',$mpid);
        $this->db->delete('money_pay_expense');
        $this->db->where('mp_id',$mpid);
        $this->db->delete('money_paid');
    }
    
    function delpaynon($mpid){
        $this->db->where('mp_id',$mpid);
        $this->db->delete('money_paid_non');
    }
    
}