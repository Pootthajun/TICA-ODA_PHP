 <style type="text/css">
	label 
	{
		width:auto;		
	}
</style>
<style>
#form1 select{width:100%}
</style>
  <script type="text/javascript">
$(document).ready(function()
{
/////get Project
$("#plan").change(function()
{
var id=$(this).val();
var type = $('#type').val();
var dataString = 'id='+ id;
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>finance/loan/getprojectlistbyplanid/",
data: dataString,
cache: false,
success: function(html)
{
$("#getproject").fadeOut(100).html(html).fadeIn(500);
} 
});

$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>finance/"+type+"/getrecipientbyplanid/",
data: {"id": id,"type": type},
cache: false,
success: function(html)
{
$("#resultTable").fadeOut(100).html(html).fadeIn(500);
} 
});
});
///end get project

/*$("#project").change(function()
{	
var id=$(this).val();
var plan = $('#plan').val();
//var dataString = 'id='+ id;

$.ajax
({
type: "POST",
url: "<?php //echo base_url(); ?>project/admin/getActivitylistbyprojectid/",
data: {"projectid": id, "planid": plan},
cache: false,
success: function(html)
{
$("#resultTable").fadeOut(100).html(html).fadeIn(500);
} 
});
});*/


});
</script>

  <form id="form1" name="form1" method="post" action="#">

<p>
<select name="plan" id="plan" class="required chosen-select" data-placeholder="Choose Plan Name..." >
    <option value=""></option>
    <?php foreach($plan as $listplan){ ?>
    <option value="<?php echo $listplan['plan_id']; ?>"><?php echo $listplan['plan_name']; ?></option>
    <?php } ?>
    </select>
    </p>
    <p>
    <span id="getproject">
    <select name="project" id="project" required class="chosen-select" data-placeholder="Choose Project Name..."  >
	  <option value=""></option>
  </select>
  </span>
  </p>
  <p>
           <span id="getactivity">
         <select name="activity" id="activity" required class="chosen-select" data-placeholder="Choose Activity Name..."  >
	  <option value="" ></option>
         </select>   
         </span>   
    </p>
         <p>  
 <span id="getaid">
<select name="aid_id" id="aid_id" class="chosen-select required"  data-placeholder="Choose Aid Name..."  >
        <option></option>
    </select>
      </span>
      
      <input name="type" type="hidden" id="type" value="loan" />
      </p>
  </form>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css">
  <style>
  #tabs{
	   	  font-size:13px;
  }
  #tabs ul{
	  padding:0;
	 
  }
  #table_id a{
	  color:#e8f0de;

  }
  </style>
  <script>
  $(function() {
    $( "#tabs" ).tabs();
  });
  </script>
  <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Recipient</a></li>
    <li><a href="#tabs-2">Non Recipient</a></li>
  </ul>
  <div id="tabs-1">
    <div id="resultTable">
      <table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
        <thead>
          <tr>
            <th width="82">No.</th>
            <th width="176">Name</th>
            <th width="226">Course</th>
            <th width="227">Aid</th>
            <th width="215">Country</th>
            <?php  if( !empty( $menu ) ) { ?>
            <th>&nbsp;</th>
            
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <?php
  $count =1;
  foreach($all as $list){
  ?>
          <tr>
            <td align="center"><?php echo $count++; ?></td>
            <td><?php echo $list['prefix_id']."&nbsp;".$list['fullname']; ?></td>
            <td><?php echo $list['course']; ?></td>
            <td><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
            <td><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
        <?php  if( !empty( $menu ) ) { ?>
            <td width="168" align="right">
              <a href="<?php echo site_url(); ?>finance/loan/histotyloan/<?php echo $list['aid_id']; ?>/<?php echo $list['rec_id']; ?>" class="button green" target="_blank">Detail</a>
            <a href="<?php echo site_url(); ?>finance/loan/editloan/<?php echo $list['aid_id']; ?>/<?php echo $list['rec_id']; ?>" class="button blue">Pay</a></td>
          
            <?php } ?>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    </div>

  <div id="tabs-2">

<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
    <tr>
      <th width="48">No.</th>
      <th width="291">Aid</th>
        <?php  if( !empty( $menu ) ) { ?>
      <th>&nbsp;</th>
      <?php } ?>
    </tr>
  </thead>
  <tbody>
    <?php
  $count =1;
  $sql = "select a.*,at.aid_type_group, p.plan_name, project.project_name,project.project_type_id, activity.activity_name 
                                from aid as a
                                inner join aid_type as at on a.aid_type_id = at.aid_type_id 
                                inner join activity as activity on activity.activity_id=a.activity_id 
                                inner join project as project on project.project_id=activity.project_id 
                                inner join plan as p on p.plan_id=project.plan_id 
                                WHERE at.aid_type_group in(1,6)
                                and project.project_type_id in(1,2)
								and a.aid_recipient = 1
								order by a.aid_id ASC
								";
								$Q=$this->db->query($sql);
             foreach($Q->result_array() as $listnon){
  ?>
    <tr>
      <td align="center"><?php echo $count++; ?></td>
      <td><?php 
	  $this->db->where('aid_id',$listnon['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <?php  if( !empty( $menu ) ) { ?>
      <td width="149" align="right"><a href="<?php echo site_url(); ?>finance/loan/historyloannon/<?php echo $listnon['aid_id']; ?>/" class="button green" target="_blank">Detail</a> <a href="<?php echo site_url(); ?>finance/loan/editloannon/<?php echo $listnon['aid_id']; ?>/" class="button blue">Pay</a></td>
      <?php } ?>
    </tr>
    <?php } ?>
  </tbody>
</table>


  </div>


</div>


