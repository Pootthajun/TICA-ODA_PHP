<p>
<div id="resultReport">
<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#A9A9A9" >
  <?php
  $t = "";
  $q = "
		select * from 
				money_paid_non
		where
			aid_id = '".$this->uri->segment(4)."' 
			order by mp_id  DESC
	";
  	 $query = $this->db->query($q); 
	 $num = $query->num_rows();
	 if($num > 0){
  ?>  
  

<thead>	  

  <tr>
    <th width="8%" nowrap="nowrap" bgcolor="#F4F4F4"><strong>Date</strong></th>
    <th nowrap="nowrap" bgcolor="#F4F4F4"><strong>Title</strong></th>
    <th nowrap="nowrap" bgcolor="#F4F4F4"><strong>Comment</strong></th>
    <th nowrap="nowrap" bgcolor="#F4F4F4"><strong>Country</strong></th>
    <th width="100" nowrap="nowrap" bgcolor="#F4F4F4"><strong>รวม(THB.)</strong></th>
    <th width="100" bgcolor="#F4F4F4" nowrap="nowrap"></th>
      </tr>
  
  </thead>
  <?php 
		 $this->db->where('aid_id',$this->uri->segment(4));
		  $this->db->order_by('mp_date','DESC');
		 $this->db->limit(3);
		 $Qdate = $this->db->get('money_paid_non');  
		foreach($Qdate->result_array() as $listdate){
		?>
  <tr>
  <td height="40" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mp_date']; ?></td>
    <td width="60" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mp_title']; ?></td>
    <td width="60" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mp_comment']; ?></td>
  <td width="60" bgcolor="#FFFFFF" nowrap="nowrap">
  <?php 
	    $this->db->where('country_id',$listdate['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?>
  </td>
    <td width="100" align="right" nowrap="nowrap" bgcolor="#FFFFFF"><strong>
<?php 
$t += $listdate['mp_amount'];
 echo @number_format($listdate['mp_amount'], 2,'.',','); ?>
    </strong></td>
    <td width="100" align="right" bgcolor="#FFFFFF" nowrap="nowrap"><span style="color:red;font-size:9px;">
      <?php 
	$userid = $listdate['user_by'];
	$username  = $this->MUser->getUserById($userid);
	echo $username['user_fullname']."<br/>".$listdate['mp_create']; ?>
    </span></td>


    </tr>
        <?php } ?>
  <tr>
    <td height="40" colspan="4" nowrap="nowrap" bgcolor="#F4F4F4"><strong>รวม(THB.)</strong></td>
    <td align="right" nowrap="nowrap" bgcolor="#F4F4F4"><strong><?php echo @number_format($t, 2,'.',','); ?>

    </strong></td>
    <td align="right" bgcolor="#F4F4F4" nowrap="nowrap">&nbsp;</td>
    </tr>
      <?php } ?>

</table>
</div>
</p>

<form action="<?php echo base_url(); ?>finance/pay/insertpaynon" method="post" name="form1" id="form1">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="127"><strong>Plan Name</strong></td>
      <td colspan="3"><?php echo $detail['plan_name']; ?>
        
        <input name="aidid" type="hidden" id="aidid" value="<?php echo $detail['aid_id']; ?>" /></td>
    </tr>
    <tr>
      <td><strong>Project Name</strong></td>
      <td colspan="3"><?php echo $detail['project_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Activity Name</strong></td>
      <td colspan="3"><?php echo $detail['activity_name']; ?></td>
    </tr>
    <tr>
      <td><strong>AID Name</strong></td>
      <td colspan="3"><?php echo $detail['aid_name']; ?></td>
    </tr>
    <tr>
      <td><strong>No.</strong></td>
      <td><input name="pay_no" type="text"  required="required" class="text-input " id="textfield4" value="<?php echo $edit['mp_no']; ?>"/></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><strong>Date</strong></td>
      <td width="182"><input name="pay_date" type="text"  required class="text-input" id="datepicker1" value="<?php echo $edit['mp_date']; ?>"/></td>
      <td width="260"><strong>Year
        &nbsp;&nbsp;&nbsp;&nbsp;
        <select name="mp_year" id="mp_year" class="chosen-select"  data-placeholder="Choose Year.." style="width:150px;">
          <option></option>
          <?php 
		  if($edit['mp_year'] == ""){
			 $cyear = date('Y')+543;
		  }else{
			  $cyear = $edit['mp_year'];
		  }
		  for($i = 2550;$i <= 2600;$i++){ 
		  ?>
          <option value="<?php echo $i; ?>"  <?php if($cyear == $i){echo 'selected="selected" ';} ?>><?php echo $i; ?></option>
          <?php } ?>
        </select>
      </strong></td>
      <td width="513"><strong>Amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php
			if($edit['mp_id'] > 0){
				$amount = $edit['mp_amount'];
			}else{
			$sqlsum = "select sum(expense_budget) AS sumbudget from money_pay_expense where rec_id = '".$this->uri->segment(5)."' ";
			$querysum =$this->db->query($sqlsum);
			$sum = $querysum->row_array();
			$amount = $sum['sumbudget'];
			}
	
	?>
        <input name="amount" type="text" required class="text-input medium-small number" id="textfield2" value="<?php echo $amount; ?>" maxlength="15"/>
        THB.</strong></td>
    </tr>
    <tr>
      <td><strong>Country</strong></td>
      <td colspan="3"><label for="country"></label>
        <select name="country_id" id="country_id" class="chosen-select"  data-placeholder="Choose Country.." style="width:400px;" required="required">
          <option></option>
          <?php foreach($country as $listcountry){?>
          <option value="<?php echo $listcountry['country_id']; ?>" <?php if($listcountry['country_id'] == $edit['country_id']){echo 'selected="selected"'; }?>><?php echo $listcountry['country_name']; ?></option>
          <?php } ?>
      </select></td>
    </tr>
    <tr>
      <td><strong>Title</strong></td>
      <td colspan="3"><input type="text" name="title" id="textfield3" class="text-input large-input "  required  value="<?php echo $edit['mp_title']; ?>"/></td>
    </tr>
    <tr>
      <td><strong>Comment</strong></td>
      <td colspan="3"><label for="pay_date2"></label>
        <textarea name="comment" cols="45" rows="5" class="text-input" id="textfield"><?php echo $edit['mp_comment']; ?></textarea></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="3"><input type="submit" value="Save" class="button green"/>
      <input name="mpid" type="hidden" id="mpid" value="<?php echo $edit['mp_id']; ?>" /></td>
    </tr>
  </table>
</form>


