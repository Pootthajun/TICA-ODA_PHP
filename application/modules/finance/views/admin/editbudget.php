<script type="text/javascript">
function plusexpense(){
	var i = 0;
	$(".expense_value").each(function() {
 
        if ($(this).val() > 0)
        {
        	i = parseInt(i) + parseInt($(this).val());
        }
		$("#amount").val(i);
	});
	
}
</script>
<p>
<div id="resultReport">
<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#A9A9A9" >
  <?php
  	$q = "
		select * from 
				money_budget 
				left join money_budget_expense on money_budget_expense.mb_id = money_budget.mb_id
				left join expense on expense.expense_id = money_budget_expense.expense_id 
		where
				money_budget.aid_id = '".$this->uri->segment(4)."' 
		group by  
				expense.expense_id
	";
  	 $query = $this->db->query($q); 
	 $num = $query->num_rows();
	 if($num > 0){
  ?>  
<thead>	  

  <tr>
    <th width="8%" nowrap="nowrap" bgcolor="#F4F4F4"><strong>Date</strong></th>
    <th width="8%" nowrap="nowrap" bgcolor="#F4F4F4"><strong>Title</strong></th>
    <th width="8%" nowrap="nowrap" bgcolor="#F4F4F4"><strong>Comment</strong></th>
    <th width="60" nowrap="nowrap" bgcolor="#F4F4F4"><strong>No.</strong></th>
<?php 	 foreach($query->result_array() as $list){ ?>
    <th nowrap="nowrap"  bgcolor="#F4F4F4">
	  <strong>
	  <?php 
 
	echo $list['expense_name']; 
 	$arrayexpenseid[] = $list['expense_id'];?>
      (THB.)</strong></th>
    <?php } ?>
        <th width="100" nowrap="nowrap"  bgcolor="#F4F4F4" ><strong>รวม(THB.)</strong></th>
        <th width="100" bgcolor="#F4F4F4" nowrap="nowrap"></th>
    </tr>
  </thead>
  <?php 
		 $this->db->where('aid_id',$this->uri->segment(4));
		 $this->db->order_by('mb_id','DESC');
		 $this->db->limit(3);
		 $Qdate = $this->db->get('money_budget');  
	 
		foreach($Qdate->result_array() as $listdate){
			
		?>
  <tr>
  <td height="40" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mb_date']; ?></td>
  <td bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mb_title']; ?></td>
  <td bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mb_comment']; ?></td>
   <td width="60" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mb_no']; ?></td>
  <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td bgcolor="#FFFFFF" align="right" nowrap="nowrap">
    
    <?php
 
	$query =$this->db->query("select * from money_budget_expense where mb_id = ".$listdate['mb_id']." and expense_id = '". $arrayexpenseid[$i]."' ");
  $rowpay = $query->row_array();
   if($query->num_rows() > 0){
	 @$sumb2[$i] += $rowpay['expense_budget'];
	echo @number_format($rowpay['expense_budget'], 2,'.',',');
   }
	?>
	

    </td>
<?php } //end for ?>
    <td width="100" align="right" nowrap="nowrap" bgcolor="#FFFFFF"><strong>
      <?php
 
	$query =$this->db->query("select sum(expense_budget) as sumb2 from money_budget_expense where mb_id = ".$listdate['mb_id']."  ");
  $rowpay = $query->row_array();
  @$gtotal +=$rowpay['sumb2'];
	echo @number_format($rowpay['sumb2'], 2,'.',',');
	
	?>
    </strong></td>
    <td width="100" align="right" bgcolor="#FFFFFF" nowrap="nowrap"><span style="color:red;font-size:9px;">
      <?php 
	$userid = $listdate['user_by'];
	$username  = $this->MUser->getUserById($userid);
	echo $username['user_fullname']."<br/>".$listdate['mb_create']; ?>
    </span></td>
    </tr>
      <?php } ?>
    <tr>
    <td width="100" height="40" nowrap="nowrap" bgcolor="#F4F4F4"><strong>รวม(THB.)</strong></td>
    <td width="100" nowrap="nowrap" bgcolor="#F4F4F4">&nbsp;</td>
    <td width="100" nowrap="nowrap" bgcolor="#F4F4F4">&nbsp;</td>
    <td width="100" nowrap="nowrap" bgcolor="#F4F4F4">&nbsp;</td>
      <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td width="50" align="right" nowrap="nowrap" bgcolor="#F4F4F4">
	  <strong>
	  <?php
	echo @number_format($sumb2[$i], 2,'.',',');
	
	?>
	  </strong></td>
    <?php } ?>
    <td width="100" align="right" nowrap="nowrap" bgcolor="#F4F4F4"><strong><?php 	echo @number_format($gtotal, 2,'.',',');?>
      
    </strong></td>
    <td align="right" bgcolor="#F4F4F4" nowrap="nowrap">&nbsp;</td>
    </tr>
    <?php } ?>
</table>
</div>
</p>
<form action="<?php echo base_url(); ?>finance/budget/insert" method="post" name="form1" id="form1">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="156"><strong>Plan Name</strong></td>
      <td colspan="3"><?php echo $detail['plan_name']; ?>
      <input type="hidden" name="aidid" id="aidid"  value="<?php echo $detail['aid_id']; ?>"/></td>
    </tr>
    <tr>
      <td><strong>Project Name</strong></td>
      <td colspan="3"><?php echo $detail['project_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Activity Name</strong></td>
      <td colspan="3"><?php echo $detail['activity_name']; ?></td>
    </tr>
    <tr>
      <td><strong>AID Name</strong></td>
      <td colspan="3"><?php echo $detail['aid_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Budget Type</strong></td>
      <td colspan="3">
        <select name="budget_type" id="budget_type"  class="chosen-select required" data-placeholder="Choose Budget Type.." style="width:100%;">
        <option> </option>
        <?php
	echo 	$SQL = "select B.* from budget_project as BP
					left join budget_type as B on BP. bt_id = B.bt_id
					where BP.project_id =  ".$detail['project_id']."	";
					$Qb = $this->db->query($SQL);
					foreach($Qb->result_array() as $listbt){
		?>
          <option value="<?php echo $listbt['bt_id']; ?>" <?php if($listbt['bt_id'] ==$edit['budget_type']){echo 'selected="selected"';} ?>><?php echo $listbt['bt_name']; ?></option>
          <?php } ?>
      </select></td>
    </tr>
    <tr>
      <td><strong>No.</strong></td>
      <td><input name="mb_no" type="text"  required="required" class="text-input" id="mb_no" value="<?php echo $edit['mb_no']; ?>"/></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><strong>Date</strong></td>
      <td width="248"><input name="pay_date" type="text"  required class="text-input" id="datepicker1" value="<?php echo $edit['mb_date']; ?>"/></td>
      <td width="303"><strong>Year
        <select name="mb_year" id="mb_year" class="chosen-select"  data-placeholder="Choose Year.." style="width:150px;">
          <option></option>
          <?php 
		   if($edit['mb_year'] == ""){
			 $cyear = date('Y')+543;
		  }else{
			  $cyear = $edit['mb_year'];
		  }
		  for($i = 2550;$i <= 2600;$i++){ ?>
          <option value="<?php echo $i; ?>" <?php if($i ==$cyear){echo 'selected="selected"';} ?>><?php echo $i;  ?></option>
          <?php } ?>
        </select>
      </strong></td>
      <td width="375"><strong>Amount
 
        <input name="amount" type="text" class="text-input medium-small number" id="amount" maxlength="15" required value=""/>
      THB.</strong></td>
    </tr>
    <tr>
      <td><strong>Title</strong></td>
      <td colspan="3"><input type="text" name="title" id="textfield3" class="text-input large-input "  required value="<?php echo $edit['mb_title']; ?>"/></td>
    </tr>
    <tr>
      <td colspan="4">  <?php //for($i=0;$i<count($ex);$i++){ 
  foreach($ex as $listexpense) {
	 $this->db->where('mb_id',$edit['mb_id']);
	 $this->db->where('expense_id',$listexpense['expense_id']);
     $query = $this->db->get('money_budget_expense'); 
     $ex = $query->row_array(); 
  ?>
  <div id="expense">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2"><?php echo $listexpense['expense_name']; ?><input name="expense_id[]" type="hidden" value="<?php echo $listexpense['expense_id']; ?>" /></td>
  </tr>
  <tr>
    <td align="left"><input name="expense_budget[]" onkeyup="plusexpense()" type="text" class="text-input1 number expense_value" id="expense_budget[]" size="15" maxlength="15"  value="<?php echo $ex['expense_budget']; ?>"></td>
    <td align="left">&nbsp;<strong>&nbsp;</strong></td>
  </tr>
</table>

  </div>
  <?php } ?></td>
    </tr>
    <tr>
      <td><strong>Comment</strong></td>
      <td colspan="3"><label for="pay_date"></label>
      <textarea name="comment" cols="45" class="text-input" id="textfield"><?php echo $edit['mb_comment']; ?></textarea>        <input name="mbid" type="hidden" id="mbid" value="<?php echo $edit['mb_id']; ?>" /></td>
    </tr>
  </table>


  <div class="clear"></div> 
    <p><input type="submit" value="Save" class="button green"/></p>
</form>
