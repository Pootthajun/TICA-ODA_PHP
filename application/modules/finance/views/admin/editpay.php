<script type="text/javascript">
function plusexpense(){
	var i = 0;
	$(".expense_value").each(function() {
 
        if ($(this).val() > 0)
        {
        	i = parseInt(i) + parseInt($(this).val());
        }
		$("#amount").val(i);
	});
	
}
</script>

<p>
<div id="resultReport">
<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#A9A9A9" >
  <?php
  $q = "
		select * from 
				money_paid 
				left join money_pay_expense on money_pay_expense.mp_id = money_paid.mp_id
				left join expense on expense.expense_id = money_pay_expense.expense_id 
		where
				money_paid.rec_id = '".$this->uri->segment(5)."'
				and money_paid.aid_id = '".$this->uri->segment(4)."' 

		group by  
				expense.expense_id
						order by  money_pay_expense.mp_id  DESC
	";
  	 $query = $this->db->query($q); 
	 $num = $query->num_rows();
	 if($num > 0){
  ?>  
  

<thead>	  

  <tr>
    <th width="8%" nowrap="nowrap" bgcolor="#F4F4F4"><strong>Date</strong></th>
    <th nowrap="nowrap" bgcolor="#F4F4F4" width="350px"><strong>Title</strong></th>
    <th nowrap="nowrap" bgcolor="#F4F4F4"><strong>Comment</strong></th>
    <th nowrap="nowrap" bgcolor="#F4F4F4"><strong>Country</strong></th>
    <th nowrap="nowrap" bgcolor="#F4F4F4"><strong>NO.</strong></th>
<?php 	 foreach($query->result_array() as $list){ ?>
    <th nowrap="nowrap"  bgcolor="#F4F4F4">
	  <strong>
	  <?php 
 
	echo $list['expense_name']; 
 	$arrayexpenseid[] = $list['expense_id'];?>
      (THB.)</strong></th>
        <?php } ?> 
    <th width="100" nowrap="nowrap" bgcolor="#F4F4F4"><strong>รวม(THB.)</strong></th>
    <th width="100" bgcolor="#F4F4F4" nowrap="nowrap"></th>
      </tr>
  
  </thead>
  <?php 
		 $this->db->where('rec_id',$this->uri->segment(5));
		 $this->db->where('aid_id',$this->uri->segment(4));
		  $this->db->order_by('mp_date','DESC');
		 $this->db->limit(3);
		 $Qdate = $this->db->get('money_paid');  
		foreach($Qdate->result_array() as $listdate){
		?>
  <tr>
  <td height="40" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mp_date']; ?></td>
    <td width="60" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mp_title']; ?></td>
    <td width="60" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mp_comment']; ?></td>
  <td width="60" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $detail['country_name']; ?></td>
   
  <td width="60" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mp_no']; ?></td>
  <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td width="50" align="right" bgcolor="#FFFFFF" nowrap="nowrap">
    
    <?php
$query =$this->db->query("select * from money_pay_expense where mp_id = ".$listdate['mp_id']." and rec_id = '".$listdate['rec_id']."' and expense_id = '". $arrayexpenseid[$i]."' ");
$rowpay = $query->row_array();
 if($query->num_rows() > 0){
	 @$sumb2[$i] += $rowpay['expense_budget'];
echo @number_format($rowpay['expense_budget'], 2,'.',',');
 }
	?>
    </td>
    <?php } //end for ?>
    <td width="100" align="right" nowrap="nowrap" bgcolor="#FFFFFF"><strong>
      <?php
$query =$this->db->query("select sum(expense_budget) AS sumbudget from money_pay_expense where mp_id = ".$listdate['mp_id']." and rec_id = '".$listdate['rec_id']."'  ");
$rowpay = $query->row_array();
@$gtotal += $rowpay['sumbudget'];
echo @number_format($rowpay['sumbudget'], 2,'.',',');
	
	?>
    </strong></td>
    <td width="100" align="right" bgcolor="#FFFFFF" nowrap="nowrap"><span style="color:red;font-size:9px;">
      <?php 
	$userid = $listdate['user_by'];
	$username  = $this->MUser->getUserById($userid);
	echo $username['user_fullname']."<br/>".$listdate['mp_create']; ?>
    </span></td>


    </tr>
        <?php } ?>
  <tr>
    <td height="40" colspan="5" nowrap="nowrap" bgcolor="#F4F4F4"><strong>รวม(THB.)</strong></td>
    <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td align="right" bgcolor="#F4F4F4" nowrap="nowrap">   
      <strong>
      <?php
			echo @number_format($sumb2[$i], 2,'.',',');
	
	?>
      </strong></td>
          <?php } ?>
    <td align="right" nowrap="nowrap" bgcolor="#F4F4F4"><strong><?php echo @number_format($gtotal, 2,'.',','); ?>

    </strong></td>
    <td align="right" bgcolor="#F4F4F4" nowrap="nowrap">&nbsp;</td>
    </tr>
      <?php } ?>

</table>
</div>
</p>

<form action="<?php echo base_url(); ?>finance/pay/insertpay" method="post" name="form1" id="form1">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="127"><strong>Plan Name</strong></td>
      <td colspan="3"><?php echo $detail['plan_name']; ?>
      <input type="hidden" name="recipientid" id="recipientid"  value="<?php echo $detail['rec_id']; ?>"/>
      <input name="aidid" type="hidden" id="aidid" value="<?php echo $detail['aid_id']; ?>" /></td>
    </tr>
    <tr>
      <td><strong>Project Name</strong></td>
      <td colspan="3"><?php echo $detail['project_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Activity Name</strong></td>
      <td colspan="3"><?php echo $detail['activity_name']; ?></td>
    </tr>
    <tr>
      <td><strong>AID Name</strong></td>
      <td colspan="3"><?php echo $detail['aid_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Aid Pay</strong></td>
      <td colspan="3"><?php if($detail['aid_pay']=0){echo "Only One ";}else{echo " All AID";} ?></td>
    </tr>
    <tr>
      <td><strong>Recipient Name</strong></td>
      <td colspan="3"><?php echo $detail['prefix_id']."&nbsp;".$detail['fullname']; ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="3"><strong>Sex</strong>&nbsp;&nbsp; <?php echo $detail['sex_id']; ?> <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Age</strong>&nbsp;&nbsp;&nbsp; <?php echo $detail['age']; ?> <strong>Year Old</strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="3"><strong>Email</strong> &nbsp;&nbsp;<?php echo $detail['email']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Tel</strong> <?php echo $detail['telephone']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Country&nbsp;&nbsp;</strong><?php echo $detail['country_name']; ?></td>
    </tr>
    <tr>
      <td><strong>No.</strong></td>
      <td><input name="pay_no" type="text"  required="required" class="text-input " id="textfield4" value="<?php echo $edit['mp_no']; ?>"/></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><strong>Date</strong></td>
      <td width="182"><input name="pay_date" type="text"  required class="text-input" id="datepicker1" value="<?php echo $edit['mp_date']; ?>"/></td>
      <td width="260"><strong>Year
        &nbsp;&nbsp;&nbsp;&nbsp;
        <select name="mp_year" id="mp_year" class="chosen-select"  data-placeholder="Choose Year.." style="width:150px;">
          <option></option>
          <?php 
		  if($edit['mp_year'] == ""){
			 $cyear = date('Y')+543;
		  }else{
			  $cyear = $edit['mp_year'];
		  }
		  for($i = 2550;$i <= 2600;$i++){ 
		  ?>
          <option value="<?php echo $i; ?>"  <?php if($cyear == $i){echo 'selected="selected" ';} ?>><?php echo $i; ?></option>
          <?php } ?>
        </select>
      </strong></td>
      <td width="513"><strong>Amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 
<input name="amount" type="text" required class="text-input medium-small number" id="amount" value="" maxlength="15"/>
      THB.</strong></td>
    </tr>
    <tr>
      <td><strong>Title</strong></td>
      <td colspan="3"><input type="text" name="title" id="textfield3" class="text-input large-input "  required  value="<?php echo $edit['mp_title']; ?>"/></td>
    </tr>
    <tr>
      <td colspan="4"> 
       <?php 
     foreach($ex as $listexpense) {
	 $this->db->where('mp_id',$edit['mp_id']);
	 $this->db->where('expense_id',$listexpense['expense_id']);
     $query = $this->db->get('money_pay_expense'); 
     $ex = $query->row_array(); 
	  
  ?>
  <div id="expense">
  <table width="150" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="center"><?php echo $listexpense['expense_name']; ?><input  name="expense_id[]" type="hidden" value="<?php echo $listexpense['expense_id']; ?>" /></td>
  </tr>
  <tr>
    <td align="left"><input name="expense_budget[]" class="expense_value" onkeyup="plusexpense()"  type="text" class="text-input number" id="expense_budget[]" size="15" maxlength="15" value="<?php echo $ex['expense_budget']; ?>"></td>
    <td align="left">&nbsp;</td>
  </tr>
</table>

  </div>
  <?php } ?></td>
    </tr>
    <tr>
      <td><strong>Comment</strong></td>
      <td colspan="3"><label for="pay_date"></label>
        <textarea name="comment" cols="45" rows="5" class="text-input" id="textfield"><?php echo $edit['mp_comment']; ?></textarea></td>
    </tr>
  </table>


  <div class="clear"></div> 
    <p><input type="submit" value="Save" class="button green"/>
      <input name="mpid" type="hidden" id="mpid" value="<?php echo $edit['mp_id']; ?>" />
    </p>
</form>
