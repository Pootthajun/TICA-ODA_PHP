<div id="resultReport">
  <?php
  	$q = "
		select * from 
				money_budget 
				left join money_budget_expense on money_budget_expense.mb_id = money_budget.mb_id
				left join expense on expense.expense_id = money_budget_expense.expense_id 
		where
				money_budget.aid_id = '".$this->uri->segment(4)."' 
		group by  
				expense.expense_id
	";
  	 $query = $this->db->query($q); 
	 $num = $query->num_rows();
	 if($num > 0){
	 //$detail = $query->row_array();
  ?>  
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="156"><strong>Plan Name</strong></td>
      <td colspan="3"><?php echo $detail['plan_name']; ?>
      <input type="hidden" name="aidid" id="aidid"  value="<?php echo $detail['aid_id']; ?>"/></td>
    </tr>
    <tr>
      <td><strong>Project Name</strong></td>
      <td colspan="3"><?php echo $detail['project_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Activity Name</strong></td>
      <td colspan="3"><?php echo $detail['activity_name']; ?></td>
    </tr>
    <tr>
      <td><strong>AID Name</strong></td>
      <td colspan="3"><?php echo $detail['aid_name']; ?></td>
    </tr>
    </table>
<table width="800" border="0" cellspacing="1" cellpadding="1" bgcolor="#A9A9A9" >

<thead>	  

  <tr>
    <th width="100" nowrap="nowrap" bgcolor="#F4F4F4"><strong>Date</strong></th>
    <th width="100" nowrap="nowrap" bgcolor="#F4F4F4"><strong>Title</strong></th>
    <th width="100" nowrap="nowrap" bgcolor="#F4F4F4"><strong>Comment</strong></th>
    <th width="60" nowrap="nowrap" bgcolor="#F4F4F4"><strong>No.</strong></th>
<?php 	 foreach($query->result_array() as $list){ ?>
    <th width="50" nowrap="nowrap"  bgcolor="#F4F4F4" >
	  <strong>
	  <?php  
	echo $list['expense_name']; 
 	$arrayexpenseid[] = $list['expense_id'];?>
	  (THB.)      </strong></th>
        <?php } ?>
    <th width="100" nowrap="nowrap"  bgcolor="#F4F4F4" ><strong>รวม(THB.)</strong></th>
    <th width="100" bgcolor="#F4F4F4" nowrap="nowrap"></th>
    <th width="100" nowrap="nowrap"  bgcolor="#F4F4F4" >&nbsp;</th>

  </tr>
  </thead>
  <?php 
 
		 $this->db->where('aid_id',$this->uri->segment(4));
		 $this->db->order_by('mb_date',"DESC");
		 $Qdate = $this->db->get('money_budget');  
		foreach($Qdate->result_array() as $listdate){
		?>
  <tr>
  <td width="100" height="40" nowrap="nowrap" bgcolor="#FFFFFF"><?php echo $listdate['mb_date']; ?></td>
  <td width="100" nowrap="nowrap" bgcolor="#FFFFFF"><?php echo $listdate['mb_title']; ?></td>
  <td width="100" nowrap="nowrap" bgcolor="#FFFFFF"><?php echo $listdate['mb_comment']; ?></td>
   <td width="60" bgcolor="#FFFFFF" nowrap="nowrap"><?php echo $listdate['mb_no']; ?></td>
  <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td width="50" align="right" nowrap="nowrap" bgcolor="#FFFFFF">
    
    <?php
 
	$query =$this->db->query("select * from money_budget_expense where mb_id = ".$listdate['mb_id']." and expense_id = '". $arrayexpenseid[$i]."' ");
   $rowpay = $query->row_array();
   if($query->num_rows() > 0){
	echo @number_format($rowpay['expense_budget'], 2,'.',',');
   }
	?>
    </td>
    <?php } //end for ?>
    <td width="100" align="right" nowrap="nowrap" bgcolor="#FFFFFF"><strong>
      <?php
 
	$query =$this->db->query("select sum(expense_budget) as sumb2 from money_budget_expense where mb_id = ".$listdate['mb_id']."  ");
  $rowpay = $query->row_array();
	echo @number_format($rowpay['sumb2'], 2,'.',',');
	
	?>
    </strong></td>
    <td width="150" align="right" bgcolor="#FFFFFF" nowrap="nowrap"><span style="color:red;font-size:9px;">
      <?php 
	$userid = $listdate['user_by'];
	$username  = $this->MUser->getUserById($userid);
	echo $username['user_fullname']."<br/>".$listdate['mb_create']; ?>
    </span></td>
    <td width="220" align="right" nowrap="nowrap" bgcolor="#FFFFFF">   <a href="<?php echo site_url(); ?>finance/budget/editbudget/<?php echo $listdate['aid_id']; ?>/<?php echo $listdate['mb_id']; ?>/" class="button blue">Edit</a>
        <a href="<?php echo site_url(); ?>finance/budget/delbudget/<?php echo $listdate['aid_id']; ?>/<?php echo $listdate['mb_id']; ?>" class="button red">Del</a></td>



    </tr>
      <?php } ?>
  <tr>
    <td width="100" height="40" nowrap="nowrap" bgcolor="#F4F4F4"><strong>รวม(THB.)</strong></td>
    <td width="100" nowrap="nowrap" bgcolor="#F4F4F4">&nbsp;</td>
    <td width="100" nowrap="nowrap" bgcolor="#F4F4F4">&nbsp;</td>
    <td width="100" nowrap="nowrap" bgcolor="#F4F4F4">&nbsp;</td>
      <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td width="50" align="right" nowrap="nowrap" bgcolor="#F4F4F4">
	  <strong>
	  <?php
 
	$query =$this->db->query("select sum(expense_budget) as sumb1 from money_budget_expense where expense_id = '". $arrayexpenseid[$i]."'  and  aid_id = ".$listdate['aid_id']."  ");
  $rowpay = $query->row_array();
	echo @number_format($rowpay['sumb1'], 2,'.',',');
	
	?>
	  </strong></td>
    <?php } ?>
    <td width="100" align="right" nowrap="nowrap" bgcolor="#F4F4F4"><strong>
      <?php
 
	$query =$this->db->query("select sum(expense_budget) as sumb from money_budget_expense where aid_id = ".$listdate['aid_id']."  ");
  $rowpay = $query->row_array();
	echo @number_format($rowpay['sumb'], 2,'.',',');
	
	?>
    </strong></td>
    <td align="right" bgcolor="#F4F4F4" nowrap="nowrap">&nbsp;</td>
    <td width="100" align="right" nowrap="nowrap" bgcolor="#F4F4F4">&nbsp;</td>
  </tr>

</table>
  <?php } ?>
</div>