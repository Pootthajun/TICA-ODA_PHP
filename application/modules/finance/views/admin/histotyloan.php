  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="127"><strong>Plan Name</strong></td>
      <td colspan="3"><?php echo $detail['plan_name']; ?>
      <input type="hidden" name="recid" id="recid"  value="<?php echo $detail['rec_id']; ?>"/>
      <input type="hidden" name="aid" id="aid"  value="<?php echo $detail['aid_id']; ?>"/>
      </td>
    </tr>
    <tr>
      <td><strong>Project Name</strong></td>
      <td colspan="3"><?php echo $detail['project_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Activity Name</strong></td>
      <td colspan="3"><?php echo $detail['activity_name']; ?></td>
    </tr>
    <tr>
      <td><strong>AID Name</strong></td>
      <td colspan="3"><?php echo $detail['aid_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Recipient Name</strong></td>
      <td colspan="3"><?php echo $detail['prefix_id']."&nbsp;".$detail['fullname']; ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="3"><strong>Sex</strong>&nbsp;&nbsp; <?php echo $detail['sex_id']; ?> <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Age</strong>&nbsp;&nbsp;&nbsp; <?php echo $detail['age']; ?> <strong>Year Old</strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="3"><strong>Email</strong> &nbsp;&nbsp;<?php echo $detail['email']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Tel</strong> <?php echo $detail['telephone']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Country&nbsp;&nbsp;</strong><?php echo $detail['country_name']; ?></td>
    </tr>
    </table>
<div id="resultReport">
<table width="995" border="1" cellspacing="1" cellpadding="1" bgcolor="#7D7D7D">
<thead>
  <tr>
    <td width="9%" bgcolor="#F4F4F4"><strong>Loan No.</strong></td>
    <td width="18%" align="center" bgcolor="#F4F4F4"><strong>Date Pay-Date Pay Back</strong></td>
    <td width="18%" align="center" bgcolor="#F4F4F4"><strong>Comment</strong></td>
    <td width="14%" align="center" bgcolor="#F4F4F4"><strong>Amount Loan(THB.)</strong></td>
    <td width="15%" align="center" bgcolor="#F4F4F4"><strong> 	Amount Pay Back(THB.)</strong></td>
    <td width="16%" align="center" bgcolor="#F4F4F4"><strong>คงเหลือ(THB.)</strong></td>
    <td width="14%" align="center" bgcolor="#F4F4F4">&nbsp;</td>
    <td width="14%" align="center" bgcolor="#F4F4F4">&nbsp;</td>
  </tr>
  </thead>
  <tbody>
  <?php
    $totalpay = "";
	$totalback="";
	 $this->db->where('rec_id',$this->uri->segment(5));
	 $this->db->where('aid_id',$this->uri->segment(4));
     $Q = $this->db->get('money_loan'); 
	 if($Q->num_rows() > 0){
	 foreach($Q->result_array() as $row){
  ?>  
    <tr>
    <td width="9%" bgcolor="#FFFFFF"><?php echo $row['loan_number'];?></td>
    <td width="18%" align="center" bgcolor="#FFFFFF"><?php echo $row['ml_date']."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$row['ml_date_back'];?></td>
    <td width="18%" align="center" bgcolor="#FFFFFF"><?php echo $row['ml_comment'];?></td>
    <td width align="right" bgcolor="#FFFFFF"><?php $totalpay += $row['ml_amount']; ?><?php echo number_format($row['ml_amount'], 2, '.', ',');?></td>
    <td width align="right" bgcolor="#FFFFFF">  <?php $totalback += $row['ml_money_back']; ?> <?php echo number_format($row['ml_money_back'], 2, '.', ','); ?> </td>
    <td width="16%" align="right" bgcolor="#FFFFFF"><?php echo number_format(($row['ml_amount']-$row['ml_money_back']), 2,'.',','); ?></td>
    <td width align="right" bgcolor="#FFFFFF"><span style="color:red;font-size:9px;">
      <?php 
	$userid = $row['user_by'];
	$username  = $this->MUser->getUserById($userid);
	echo $username['user_fullname']."<br/>".$row['ml_create']; ?>
    </span></td>
    <td width align="right" bgcolor="#FFFFFF">
    <a href="<?php echo site_url(); ?>finance/loan/editloan/<?php echo $row['aid_id']; ?>/<?php echo $row['rec_id']; ?>/<?php echo $row['ml_id']; ?>/" class="button blue">Edit</a>
        <a href="<?php echo site_url(); ?>finance/loan/delloan/<?php echo $row['aid_id']; ?>/<?php echo $row['rec_id']; ?>/<?php echo $row['ml_id']; ?>" class="button red">Del</a></td>
    </tr>
    <?php } ?>
        <tr>
      <td colspan="3" bgcolor="#F4F4F4"><strong>รวม(THB.)</strong></td>
      <td align="right" bgcolor="#F4F4F4"><strong><?php echo number_format($totalpay, 2, '.', ','); ?></strong></td>
      <td align="right" bgcolor="#F4F4F4"><strong><?php echo number_format($totalback, 2, '.', ','); ?></strong></td>
      <td align="right" bgcolor="#F4F4F4"><?php echo number_format(($totalpay-$totalback), 2,'.',','); ?></td>
      <td align="center" bgcolor="#F4F4F4">&nbsp;</td>
      <td align="center" bgcolor="#F4F4F4">&nbsp;</td>
      </tr>
      <?php } ?>
    </tbody>
</table>
</div>
