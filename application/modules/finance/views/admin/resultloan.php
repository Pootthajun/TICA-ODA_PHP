<script type="text/javascript">
$(document).ready(function(){
	$('#table_id').DataTable();
	$('tbody tr:even').addClass("alt-row");
});
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
    <tr>
      <th width="84">No.</th>
      <th width="179">Name</th>
      <th width="261">Course</th>
      <th width="291">Aid</th>
      <th width="164">Country</th>
      <th>&nbsp;</th>
      <?php  if( !empty( $menu ) ) { ?>
      <?php } ?>
    </tr>
  </thead>
  <tbody>
    <?php
  $count =1;
  foreach($all as $list){
  ?>
    <tr>
      <td align="center"><?php echo $count++; ?></td>
      <td><?php echo $list['prefix_id']."&nbsp;".$list['fullname']; ?></td>
      <td><?php echo $list['course']; ?></td>
      <td><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <td><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
      <td width="130" align="right"><a href="<?php echo site_url(); ?>finance/loan/histotyloan/<?php echo $list['aid_id']; ?>/<?php echo $list['rec_id']; ?>" class="button green" target="_blank">Detail</a> <a href="<?php echo site_url(); ?>finance/loan/editloan/<?php echo $list['aid_id']; ?>/<?php echo $list['rec_id']; ?>" class="button blue">Pay</a></td>
      <?php  if( !empty( $menu ) ) { ?>
      <?php } ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
