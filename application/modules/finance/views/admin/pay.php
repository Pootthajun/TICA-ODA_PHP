<style>
#form1 select{width:100%}
</style>
<script type="text/javascript">
$(document).ready(function()
{
$("#plan").change(function()
{
$("#resultTable").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
var id=$(this).val();
var projecttype = $('#projecttype').val();
var groupid = $('#groupid').val();
var dataString = 'id='+ id;
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>finance/pay/getprojectlistbyplanid/",
data: {"id": id,"projecttype": projecttype},
cache: false,
success: function(html)
{
$("#getproject").fadeOut(100).html(html).fadeIn(500);
} 
});

$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>finance/pay/getrecipientbyplanid/",
data: {"id": id,"projecttype": projecttype,"groupid": groupid},
cache: false,
success: function(html)
{
$("#resultTable").html(html);
} 
});
});
///end get project

$("#project").change(function()
{	
var id=$(this).val();
var plan = $('#plan').val();
//var dataString = 'id='+ id;

$.ajax
({
type: "POST",
url: "<?php //echo base_url(); ?>project/admin/getActivitylistbyprojectid/",
data: {"projectid": id, "planid": plan},
cache: false,
success: function(html)
{
$("#resultTable").fadeOut(100).html(html).fadeIn(500);
} 
});
});


});
 

 
</script>
<style type="text/css">
	label 
	{
		width:auto;		
	}
</style>
  <form id="form1" name="form1" method="post" action="#">

<p>
<select name="plan" id="plan" class="required chosen-select" data-placeholder="Choose Plan Name..." >
    <option value=""></option>
    <?php foreach($plan as $listplan){ ?>
    <option value="<?php echo $listplan['plan_id']; ?>"><?php echo $listplan['plan_name']; ?></option>
    <?php } ?>
    </select>
    </p>
    <p>
    <span id="getproject">
    <select name="project" id="project" required class="chosen-select" data-placeholder="Choose Project Name..."  >
	  <option value=""></option>
  </select>
  </span>
  </p>
  <p>
           <span id="getactivity">
         <select name="activity" id="activity" required class="chosen-select" data-placeholder="Choose Activity Name..."  >
	  <option value="" ></option>
         </select>   
         </span>   
    </p>
         <p>  
 <span id="getaid">
<select name="aid_id" id="aid_id" class="chosen-select required"  data-placeholder="Choose Aid Name..."  >
        <option></option>
    </select>
      </span>
      
      <input name="groupid" type="hidden" id="groupid" value="<?php echo $this->uri->segment(4); ?>" />
         <input name="projecttype" type="hidden" id="projecttype" value="<?php echo $this->uri->segment(5); ?>" />
      </p>
  </form>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css">
  <style>
  #tabs{
	   	  font-size:13px;
  }
  #tabs ul{
	  padding:0;
	 
  }
  #table_id a{
	  color:#e8f0de;

  }
  </style>
    <script>
  $(function() {
    $( "#tabs" ).tabs();
  });
  </script>
  

<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Recipient</a></li>
    <?php if($group['recipient_type'] == 1){ ?>
    <li><a href="#tabs-2">Non Recipient</a></li>
    <?php } ?>
  </ul>
  <div id="tabs-1">
  <div id="resultTable">
<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
  <tr>
    <th width="48">No.</th>
    <th width="186">Name</th>
    <th width="261">Course</th>
    <th width="291">Aid</th>
    <th width="164">Country</th>
    <?php  if( !empty( $menu ) ) { ?>
    <th>&nbsp;</th>
    <?php } ?>

    </tr>
  </thead>

  <tbody>
    <?php
  $count =1;
  foreach($all as $list){
  ?>
    <tr>
      <td align="center"><?php echo $count++; ?></td>
      <td><?php echo $list['prefix_id']."&nbsp;".$list['fullname']; ?></td>
      <td><?php echo $list['course']; ?></td>
      <td><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <td><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
      <?php  if( !empty( $menu ) ) { ?>
      <td width="149" align="right">
        <a href="<?php echo site_url(); ?>finance/pay/histotypay/<?php echo $list['aid_id']; ?>/<?php echo $list['rec_id']; ?>" class="button green" target="_blank">Detail</a>
        <a href="<?php echo site_url(); ?>finance/pay/editpay/<?php echo $list['aid_id']; ?>/<?php echo $list['rec_id']; ?>/" class="button blue">Pay</a>
        </td>
        <?php } ?>

    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
  </div>
 <?php if($group['recipient_type'] == 1){ ?>
  <div id="tabs-2">
 <div id="resultTable">
<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
    <tr>
      <th width="48">No.</th>
      <th width="291">Aid</th>
      <?php  if( !empty( $menu ) ) { ?>
      <th>&nbsp;</th>
      <?php } ?>
    </tr>
  </thead>
  <tbody>
    <?php
  $count =1;
  foreach($non as $listnon){
  ?>
    <tr>
      <td align="center"><?php echo $count++; ?></td>
      <td><?php 
	    $this->db->where('aid_id',$listnon['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <?php  if( !empty( $menu ) ) { ?>
      <td width="149" align="right"><a href="<?php echo site_url(); ?>finance/pay/histotypaynon/<?php echo $listnon['aid_id']; ?>/" class="button green" target="_blank">Detail</a> <a href="<?php echo site_url(); ?>finance/pay/editpaynon/<?php echo $listnon['aid_id']; ?>/" class="button blue">Pay</a></td>
      <?php } ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>

  </div>
<?php } ?>

</div>

