<script type="text/javascript">
$(document).ready(function(){
	$('#table_id').DataTable();
	$('tbody tr:even').addClass("alt-row");
});
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="54">Id</th>
          <th width="247">Aid Type</th>
          <th width="486">Aid  Name</th>
          <th>Aid Budget</th>
          <th>Status</th>

          <th>&nbsp;</th>

    
        </tr>
      </thead>
      <tbody>
  

<?php 
$count = 1;
foreach($all as $list){ ?>
        <tr>
          <td><?php echo $count++; ?></td>
          <td><?php 
		  $this->db->where('aid_type_id',$list['aid_type_id']);
			$QT = $this->db->get('aid_type');
			$typename = $QT->row_array();
			echo $typename['aid_type_name']
		    ?></td>
          <td><?php echo $list['aid_name']; ?></td>
          <td width="155" align="right"><?php echo $list['aid_budget']; ?></td>
          <td width="110" >
         <center>          <?php if($list['aid_status'] == 0){ ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/exclamation.png" title="Wait" width="16" height="16" />
        <?php }else if($list['aid_status'] == 1){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/tick_circle.png" title="Approve" width="16" height="16" />
        <?php } ?></center>
          </td>

          <td width="47" align="right"><a href="<?php echo site_url(); ?>finance/loan/editloan/<?php echo $list['aid_id']; ?>" class="button blue">Edit</a></td>

        </tr>
        <?php } ?>
      </tbody>
    </table>