<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
   
    function index(){
        $data['header'] = "Edit General menu";
	$data['edit'] = $this->MGeneral->getgenbyid();
        $data['menu'] = $this->MUser->getMenubyUser();  
    	$data['page'] = $this->config->item('wconfig_template_admin').'edit';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function update(){
        $this->MGeneral->update();
    	flashMsg('success',$this->lang->line('update_success'));
	    redirect( 'general/admin/','refresh');;
    }
}