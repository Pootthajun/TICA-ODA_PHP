<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MGeneral extends CI_Model{
	
    function getgenbyid(){ 
        //$data = array();
        $Q = $this->db->get('general');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }

    
    function update(){
        $data = array(
            'gen_name_th'=>$this->input->post('gen_name_th'),   
			'gen_name_eng'=>$this->input->post('gen_name_eng'),
                        'gen_email'=>$this->input->post('gen_email'), 
			'gen_tel'=>$this->input->post('gen_tel'),   
			'gen_fax'=>$this->input->post('gen_fax'),   
			'gen_website'=>$this->input->post('gen_website'),  
			'gen_division'=>$this->input->post('gen_division'),   
			'gen_ministry'=>$this->input->post('gen_ministry'),   
			'gen_comment'=>$this->input->post('gen_comment') 
        );
        $this->db->update('general',$data);
    }
   

}