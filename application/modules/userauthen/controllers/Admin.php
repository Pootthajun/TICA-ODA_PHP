<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Admin extends Admin_Controller{

	function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));
       // $this->bep_site->set_crumb($this->lang->line('user'),'user/admin'); 
	
    }

        
        function index(){
            $data['header'] ="User Authen Table";
            $data['user'] = $this->MUser->listalluser();
            $data['menu'] = $this->MUserauthen->getMenubyUser();      
			$data['page'] = $this->config->item('wconfig_template_admin').'index';
            $this->load->view($this->_container_admin,$data);
            
        }
        
        function userauthenedit($userid){
            $name = $this->MUser->getUserById($userid);
            $data['header'] ="User  Authen&nbsp;&nbsp;&nbsp;&nbsp;".$name['user_username'];
            $data['user'] = $name;
            $data['page'] = $this->config->item('wconfig_template_admin').'userauthenedit';
            $this->load->view($this->_container_admin,$data);
        }

        function updateauthen(){
            $this->MUserauthen->updateauthen();        
            flashMsg('success',$this->lang->line('update_success'));
            redirect( 'userauthen/admin/','refresh');
        }
        
}