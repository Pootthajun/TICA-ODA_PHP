<script>
$(document).ready(function() {
	
	///  setting menu
    $('.selecctsetting').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.setting').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.setting').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
	
		///  master menu
    $('.master').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.mastermenu').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.mastermenu').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
	
			/// content menu
    $('.content').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.contentmenu').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.contentmenu').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
	
			/// finance menu
    $('.finance').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.financemenu').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.financemenu').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
	
			/// report menu
    $('.report').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.reportmenu').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.reportmenu').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
   
});


</script>


<form name="form1" method="post" action="<?php echo site_url(); ?>user/updateuserauthene">

	<div class="clear">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td width="120"><input name="menu_id[]" type="checkbox"  class="selecctsetting" id="menu_id[]" value="1"/>
	        Setting</td>
	      <td width="75%">&nbsp;</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input name="menu_id[]" type="checkbox"  class="setting" id="menu_id[]" value="2"/>
	        
	        General</td>
	      </tr>
	    <tr>
	      <td><input name="menu_id[]" type="checkbox"  class="master" id="menu_id[]" value="20"/>

	        Master</td>
	      <td>&nbsp;</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="menu_id[" id="menu_id[" class="mastermenu" />
	        Plan</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox6" id="checkbox6" class="mastermenu"  />
	        Project Type</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox7" id="checkbox7" class="mastermenu"  />
	        Project</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox8" id="checkbox8" class="mastermenu" />
	        Activity</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox9" id="checkbox9" class="mastermenu" />
	        AID Type</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox10" id="checkbox10" class="mastermenu" />
	        AID</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox11" id="checkbox11" />
	        Budget Type</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox12" id="checkbox12" class="mastermenu" />
	        Recipient</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox13" id="checkbox13" class="mastermenu" />
	        Region</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox14" id="checkbox14" />
	        Country</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox15" id="checkbox15" class="mastermenu" />
	        Cooperation</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox16" id="checkbox16" class="mastermenu"  />
	        Cooperation Type</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox17" id="checkbox17"  class="mastermenu" />
	        OECD</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox18" id="checkbox18" class="mastermenu" />
	        Sector</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox19" id="checkbox19" class="mastermenu"  />
	        Subsector</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox20" id="checkbox20" class="mastermenu"  />
	        Funding</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox21" id="checkbox21"  class="mastermenu" />
	        Executing</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox22" id="checkbox22"  class="mastermenu" />
	        Multilateral</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox23" id="checkbox23" class="mastermenu"  />
	        kind</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox24" id="checkbox24" class="mastermenu" />
	        Multilateral</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox25" id="checkbox25" class="mastermenu" />
	        Expense</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox26" id="checkbox26" class="mastermenu" />
	        User</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox27" id="checkbox27" class="mastermenu" />
	        User Authen</td>
	      </tr>
	    <tr>
	      <td><input type="checkbox" name="checkbox28" id="checkbox28"   class="content"/>
	        Content</td>
	      <td>&nbsp;</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox29" id="checkbox29"  class="contentmenu"/>
	        About</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox30" id="checkbox30"  class="contentmenu" />
	        News</td>
	      </tr>
	    <tr>
	      <td><input type="checkbox" name="checkbox31" id="checkbox31"  class="finance"/>
	        Finance</td>
	      <td>&nbsp;</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox32" id="checkbox32"  class="financemenu"/>
	        Budget</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox33" id="checkbox33"   class="financemenu"/>
	        Pay</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox34" id="checkbox34"  class="financemenu" />
	        Loan</td>
	      </tr>
	    <tr>
	      <td><input type="checkbox" name="checkbox35" id="checkbox35" />
	        Process</td>
	      <td>&nbsp;</td>
	      </tr>
	    <tr>
	      <td><input type="checkbox" name="checkbox36" id="checkbox36"   class="report"/>
	        Report</td>
	      <td>&nbsp;</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox37" id="checkbox37"   class="reportmenu"/>
	        AID</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox38" id="checkbox38"  class="reportmenu"//>
	        Recipient</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox39" id="checkbox39" class="reportmenu"/ />
	        Sector</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox40" id="checkbox40"  class="reportmenu"//>
	        Agency</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox41" id="checkbox41" class="reportmenu"/ />
	        Project</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox42" id="checkbox42" class="reportmenu"/ />
	        Finance</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox43" id="checkbox43"  class="reportmenu"//>
	        Expense Type</td>
	      </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><input type="checkbox" name="checkbox44" id="checkbox44"  class="reportmenu"//>
	        Expense Loan</td>
	      </tr>
	    </table>
        
        <p>
<input type="button" class="button green" value="Update" style="float:right">
</p>
<p>
  <input name="userid" type="hidden" id="userid" value="<?php echo $user['user_id']; ?>" />
</p>
	</div>
</form>
