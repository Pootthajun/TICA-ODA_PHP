<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MUserauthen extends CI_Model{
    
    function getUserById($userid){
        $data = array();
        $this->db->where('user_id',$userid);
        $query = $this->db->get('user');
        $data = $query->row_array();
        $query->free_result();
	    return $data;                
    }
            
    function inseruser(){
        $data = array(
            'user_username'=>$_POST['user_username'],
            'user_password'=>md5($_POST['user_password']),
            'user_fullname'=>$_POST['user_fullname'],
            'user_email'=>$_POST['user_email'],
            'user_level'=>$_POST['user_level'],
            'user_tel'=>$_POST['user_tel'],
            'user_fax'=>$_POST['user_fax'],
            'user_update'=>date('Y-m-d H:i:s'),
            'user_create'=>date('Y-m-d H:i:s'),
            'user_by'=>$this->session->userdata('userid')
        );
        $this->db->insert('user',$data);
    }
    
    function updateuser(){
        $data = array(
            'user_username'=>$_POST['user_username'],
            'user_password'=>md5($_POST['user_password']),
            'user_fullname'=>$_POST['user_fullname'],
            'user_email'=>$_POST['user_email'],
            'user_level'=>$_POST['user_level'],
            'user_tel'=>$_POST['user_tel'],
            'user_fax'=>$_POST['user_fax'],
            'user_update'=>date('Y-m-d H:i:s'),
            'user_create'=>date('Y-m-d H:i:s'),
            'user_by'=>$this->session->userdata('userid')
        );
        $this->db->where('user_id',$_POST['editid']);
        $this->db->update('user',$data);
    }
            
    function listalluser(){
        $data = array();
        $this->db->order_by('user_id',"ASC");
        $Q = $this->db->get('user');
        foreach ($Q->result_array() as $row) {
		$data[] = $row;
	}
        $Q->free_result();
	return $data;
    }
    
    function deleteuser($userid){
        $this->db->where('user_id',$userid);
        $this->db->delete('user');
    }

    function updateauthen(){

        $this->db->where('user_id',$_POST['userid']);
        $this->db->delete('userauthen');


        for($i = 0;$i < count(@$_POST['menu_id']);$i++ ){
            if($_POST['menu_id'][$i] != null){
                $data  = array(
                    'menu_module' => $_POST['menu_id'][$i],
                    'user_id'=> $_POST['userid'],
                 );
                $this->db->insert('userauthen',$data);

            }

    }
    }

    function getMenubyUser(){
        $data = array();
      //  $QMENU =$this->db->query("select * from userauthen inner join menu on userauthen.menu_id = menu.menu_id  where userauthen.user_id = ".$this->session->userdata('userid')."" );
	    //$QMENU =$this->db->query("select * from userauthen where userauthen.user_id = ".$this->session->userdata('userid')." AND menu_name = "'.$this->uri->segment(1).'" " );
        $this->db->where('user_id',$this->session->userdata('userid'));
        $this->db->where('menu_module',$this->uri->segment(1));
        $QMENU = $this->db->get('userauthen');
        if($QMENU->num_rows() != 0)
        {
           // $data = $QMENU->row_array();
            $data['page'] = $this->config->item('wconfig_template_admin').'cannot';
        }else{
            $data['page'] = $this->config->item('wconfig_template_admin').'index';
        }
        $QMENU->free_result();
        return $data;
    }

}