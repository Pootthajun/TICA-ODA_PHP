<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Budget type Table";
        $data['all'] = $this->MBudgettype->listallBudgettype();
    	$data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addbudgettype(){
        $data['header'] = "Budget type table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addbudgettype';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertbudgettype(){
        $this->MBudgettype->insertbudgettype();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'budgettype/admin/','refresh');
    }
    
    function editbudgettype($budgettypeid){
        $data['header'] = "Edit Budget type table";
        $data['edit'] = $this->MBudgettype->getBudgettypetypebyid($budgettypeid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editbudgettype';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updatebudgettype(){
        $this->MBudgettype->updatebudgettype();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'budgettype/admin/','refresh');;
    }
    
    function deletebudgettype($budgettypeid){
        $query = $this->db->get_where('budget_project', array('bt_id' => $budgettypeid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'budgettype/admin/','refresh');            
        }else{
            $this->MBudgettype->deletebudgettype($budgettypeid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'budgettype/admin/','refresh');
        }
    }
}