<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MBudgettype extends CI_Model{
    
    function insertbudgettype(){
        $data = array(
            'bt_name'=>$this->input->post('bt_name'),           
        );
        $this->db->insert('budget_type',$data);
        
    }
    
    function listallBudgettype(){
        $data = array();
        $this->db->order_by('bt_id',"ASC");
        $Q = $this->db->get('budget_type');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getBudgettypetypebyid($budgettypeid){
        //$data = array();
        $this->db->where('bt_id',$budgettypeid);
        $Q = $this->db->get('budget_type');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;        
    }
    
    function updatebudgettype(){
        $data = array(
            'bt_name'=>$this->input->post('bt_name'),       
        );
        $this->db->where('bt_id',$this->input->post('budgettype'));
        $this->db->update('budget_type',$data);
    }
    
    function deletebudgettype($budgettypeid){              
           $this->db->where('bt_id',$budgettypeid);
           $this->db->delete('budget_type');       
    }
    

}