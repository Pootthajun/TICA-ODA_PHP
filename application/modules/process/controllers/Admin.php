<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Procress Table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addprocess(){
        $data['header'] = "Add Process";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addprocess';
    	$this->load->view($this->_container_admin,$data);
    }
}