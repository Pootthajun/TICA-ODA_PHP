<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MProjecttype extends CI_Model{
    
    function insertprojecttype(){
        $data = array(
            'project_type_name'=>$this->input->post('project_type_name'),
            'project_type_by'=>$this->session->userdata('userid'),
            'project_type_create'=>date('Y-m-d')            
        );
        $this->db->insert('project_type',$data);
        
    }
    
    function listallProjecttype(){
        $data = array();
        $this->db->order_by('project_type_id',"ASC");
        $Q = $this->db->get('project_type');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getProjecttypetypebyid($projecttypeid){
        //$data = array();
        $this->db->where('project_type_id',$projecttypeid);
        $Q = $this->db->get('project_type');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;        
    }
    
    function updteprojecttype(){
        $data = array(
            'project_type_name'=>$this->input->post('project_type_name'),
            'project_type_by'=>$this->session->userdata('userid'),
            'project_type_update'=>date('Y-m-d')            
        );
        $this->db->where('project_type_id',$this->input->post('projecttype'));
        $this->db->update('project_type',$data);
    }
    
    function deleteprojecttype($projecttypeid){              
           $this->db->where('project_type_id',$projecttypeid);
           $this->db->delete('project_type');       
    }
    

}