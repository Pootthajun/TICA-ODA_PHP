<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));      
		
		
	
    }
	
    
    function index(){

        $data['header'] = "Project type Table";		
        $data['all'] = $this->MProjecttype->listallProjecttype();
        $data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addprojecttype(){
        $data['header'] = "Project type table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addprojecttype';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertprojecttype(){
        $this->MProjecttype->insertprojecttype();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'projecttype/admin/','refresh');
    }
    
    function editprojecttype($projecttypeid){
        $data['header'] = "Edit Project type type table";
        $data['edit'] = $this->MProjecttype->getProjecttypetypebyid($projecttypeid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editprojecttype';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateprojecttype(){
        $this->MProjecttype->updteprojecttype();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'projecttype/admin/','refresh');;
    }
    
    function deleteprojecttype($projecttypeid){
        $query = $this->db->get_where('project', array('project_type_id' => $projecttypeid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'projecttype/admin/','refresh');            
        }else{
            $this->MProjecttype->deleteprojecttype($projecttypeid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'projecttype/admin/','refresh');
        }
    }
}