<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MActivity extends CI_Model{
    
    function listallActivity(){
        $data = array();
        $this->db->order_by('activity_id',"ASC");
        $Q = $this->db->get('activity');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }

    function insertactivity(){
        $data = array(
            'activity_name'=>$this->input->post('activity_name'), 
            'project_id'=>$this->input->post('project_id'), 
            'activity_by'=>$this->session->userdata('userid'), 
            'activity_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('activity',$data);
    }

    function deleteactivity($activityid){              
           $this->db->where('activity_id',$activityid);
           $this->db->delete('activity');       
    }

    function getActivitybyid($activityid){
        //$data = array();
        $this->db->where('activity_id',$activityid);
        $Q = $this->db->get('activity');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;        
    }

    function updteactivity(){
        $data = array(
            'activity_name'=>$this->input->post('activity_name'), 
            'project_id'=>$this->input->post('project_id'), 
            'activity_by'=>$this->session->userdata('userid'), 
            'activity_update'=>date('Y-m-d H:i:s'),        
        );
        $this->db->where('activity_id',$this->input->post('activityid'));
        $this->db->update('activity',$data);
    }
    
    function getactivitybyprojectid($projectid){
	$data = array();
        $this->db->where('project_id',$projectid);
        $Q = $this->db->get('activity');
        foreach($Q->result_array() as $row){
        $data[] = $row;
        }
        $Q->free_result();
        return $data;  
    }
}