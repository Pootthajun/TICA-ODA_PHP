<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Activity Table";
        $data['all'] = $this->MActivity->listallActivity();
	$data['project'] = $this->MProject->listallproject();
    	$data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function addactivity(){
        $projectid = $this->input->post('project');
        $data['header'] = "Add Activity";
        $data['project'] = $this->MProject->getProjectByid($projectid);
        //$data['project'] = $this->MProject->listallproject();
    	$data['page'] = $this->config->item('wconfig_template_admin').'addactivity';
    	$this->load->view($this->_container_admin,$data); 
    }

    function insertactivity(){
        $this->MActivity->insertactivity();
        flashMsg('success',$this->lang->line('insert_success'));
        redirect( 'activity/admin/','refresh');
    }

    function deleteactivity($activityid){
        $this->MActivity->deleteactivity($activityid);
        flashMsg('success',$this->lang->line('delete_success'));
        redirect( 'activity/admin/','refresh');
    }

    function editactivity($activityid){
        $data['header'] = "Edit Activity table";
        $data['edit'] = $this->MActivity->getActivitybyid($activityid);
         $data['project'] = $this->MProject->listallproject();
        $data['page'] = $this->config->item('wconfig_template_admin').'editactivity';
        $this->load->view($this->_container_admin,$data); 
    }

    function updateactivity(){
        $this->MActivity->updteactivity();
        flashMsg('success',$this->lang->line('update_success'));
        redirect( 'activity/admin/','refresh');;
    }
	
		function getacbyproject(){			
			   $id=$this->input->post('id');
               $Q=$this->db->query("select ac.*,project.project_name from activity as ac 
			   inner join project as project on project.project_id = ac.project_id 
			   where ac.project_id = '$id'
			   ");
		      $data['all'] = $Q->result_array();	
			  $data['menu'] = $this->MUser->getMenubyUser();     
			  $data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
			 $this->load->view($this->_view_subsector,$data); 
	}
	
	

	function getActivitybyprojectid(){
		   $id=$this->input->post('id');
           $Q=$this->db->query("select * from activity where  project_id='$id'"); 
           $data['option'] = $Q->result_array();	          
			$data['page'] = $this->config->item('wconfig_template_admin').'activitylist';      
			$this->load->view($this->_view_subsector,$data); 
	}
        
        function getactivityauthen(){
            $project_id=$this->input->post('project_id');
            $user_id = $this->session->userdata('userid');
            $project_authen = array();
            
            $this->db->where('project_id',$project_id);
            $Q = $this->db->get('project_assistant'); 
            foreach($Q->result_array() as $listp){
                    $project_authen[] = $listp['user_id'];  
            }

            $this->db->where('project_id',$project_id);
            $Q = $this->db->get('project'); 
            foreach($Q->result_array() as $listp){
                    $project_authen[] = $listp['project_owner'];  
            }
             
            $key = array_search($user_id, $project_authen);
          
             
            $user = $project_authen[$key];
          
            return $user;
	}
	
	function getuserproject(){
		$id=$this->input->post('id');
		$user = $this->session->userdata('userid');                
		$project_assistant = '';
                $project_owner = '';
                $this->db->where('project_id',$id);
                $Q = $this->db->get('project_assistant'); 
                foreach($Q->result_array() as $listp){
			$project_assistant .= $listp['user_id'].",";  
		}
                
                $this->db->where('project_id',$id);
                $Q = $this->db->get('project'); 
                foreach($Q->result_array() as $listp){
			$project_owner = $listp['project_owner'];  
		}
		
		$auser = $project_assistant.$project_owner;
 
		  $ex = explode(",",$auser);
			 for($i=0;$i<count($ex);$i++){ 
					  if($ex[$i] == $this->session->userdata('userid')){ 
					
							echo '<input type="submit" name="button" id="button" value="Add" class="button green" />';
					}
			 }
	}

}