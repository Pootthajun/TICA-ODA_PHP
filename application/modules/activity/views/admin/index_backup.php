<style>
.chosen-select{width:85%; float:left;}
</style>
<script type="text/javascript">
$(document).ready(function()
{
	
/////get Activity	
$("#project").change(function()
{
    var id=$(this).val();
    var dataString = "rand="+Math.random()+"&id="+ id; 
    $.ajax({ 
        type: "POST",
        url: "<?php echo base_url(); ?>activity/admin/getacbyproject/",
        data: dataString,
        cache: false,
        success: function(html)
        {
            $("#resultTable").fadeOut(100).html(html).fadeIn(500);
        } 
    });
});
///end get activity
$(".activity_add").click(function()
{
    var project_id=$("#project").val();  
    $.ajax({ 
        type: "POST",
        url: "<?php echo base_url(); ?>activity/admin/getactivityauthen/",
        data: "rand="+Math.random()+"&project_id="+ project_id,
        cache: false,
        success: function(res)
        { 
            if(res){   
                window.location= ("<?php echo site_url(); ?>activity/admin/addactivity");  
            }else{
                alert('You do not have permission to access this project.');
                return false;
            }
        } 
    });
});


});
</script>
   
        <p>

<select name="project" id="project" class="chosen-select" data-placeholder="Choose Project Name..." >
    <option value=""></option>
    <?php foreach($project as $listproject){ ?>
    <option value="<?php echo $listproject['project_id']; ?>"><?php echo $listproject['project_name']; ?></option>
    <?php } ?>
    </select>

            

  <a href="javascript:void(0);" class="button green activity_add" >Add </a>  

  
  
  
  
  </p>
  <div id="resultTable">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="83">Id</th>
          <th width="450">Project Name</th>
          <th width="497">Activity  Name</th>
          <?php  if( !empty( $menu ) ) { ?>
          <th>&nbsp;</th>
<?php } ?>
        </tr>
      </thead>
       <tbody>
<?php 
$count = 1;
foreach($all as $list){ ?>
        <tr>
          <td><?php echo $count++; ?></td>
          <td><?php 
		  $this->db->where('project_id',$list['project_id']);
			$QT = $this->db->get('project');
			$typename = $QT->row_array();
			echo $typename['project_name'];
  
		  ?></td>
          <td><?php echo $list['activity_name']; ?></td>
          
          <td width="120" align="right">
                <?php
 
		$project_assistant = '';
                $project_owner = '';
                $this->db->where('project_id',$list['project_id']);
                $Q = $this->db->get('project_assistant'); 
                foreach($Q->result_array() as $listp){
			$project_assistant .= $listp['user_id'].",";  
		}
                
                $this->db->where('project_id',$list['project_id']);
                $Q = $this->db->get('project'); 
                foreach($Q->result_array() as $listp){
			$project_owner = $listp['project_owner'];  
		}
		
		$auser = $project_assistant.$project_owner;
 
		  $ex = explode(",",$auser);
		 $Q->free_result();
		  ?>
                <?php  for($i=0;$i<count($ex);$i++){ 
		  if($ex[$i] == $this->session->userdata('userid')){ 
		?>
                        <a href="<?php echo site_url(); ?>activity/admin/editactivity/<?php echo $list['activity_id']; ?>" class="button blue">Edit</a><a href="<?php echo site_url(); ?>activity/admin/deleteactivity/<?php echo $list['activity_id']; ?>"  onclick="return confirm('Are you sure you want to delete Activity  name <?php echo $list['activity_name']; ?> ?')" class="button red"><?php echo $this->lang->line('delete'); ?></a>
		<?php }}?>
          
          </td>
         
          
          
          
          
        </tr>
        <?php } ?>
      </tbody>
    </table>
    </div>
