<?php

class MWeblink extends CI_Model{


	function listAllweblink(){
		$data = array();
		$this->db->order_by('w_id','asc');
		$Q = $this->db->get('weblink');
		foreach ($Q->result_array() as $row) {
			# code...
			$data[] = $row;
		}
		$Q->free_result();
		return $data;

	}

	function insertweblink(){
		$data = $this->_uploadFile();
		$this->db->insert('weblink',$data);
	}

	function _uploadFile(){
        $data = array(
            'w_name' => $_POST['w_name'],
            'w_link' => $_POST['w_link'],
        );
        $filename = date('YmdHis');
        		$config['upload_path'] ='./upload/weblink/';
				$config['allowed_types'] = 'jpg|png|gif';
				
				$config['remove_spaces']= true;
				$config['overwrite']=false;
                $config['file_name'] = $filename;
				$this->load->library('upload', $config);
                                $this->upload->initialize($config);
				if (strlen($_FILES['w_file']['name'])){
					if(!$this->upload->do_upload('w_file')){
						$this->upload->display_errors();
						 exit("Cannot upload");
					}
					$image = $this->upload->data();
					if ($image['file_name']){
    					$data['w_file'] = $image['file_name'];
					
					}
				}
          
			return $data;
    }

    function getweblinkbyid($bid){
    	$data = array();
		$this->db->where('w_id',$bid);
		$Q = $this->db->get('weblink');
		if($Q->num_rows() > 0){
			$data = $Q->row_array();
		}
		$Q->free_result();
		return $data;
    }

    function updateweblink(){
    	$data = $this->_uploadFile();
        $this->db->where('w_id',$_POST['bid']);
        $this->db->update('weblink',$data);
    }

    function deleteweblink($bid){
    	 $this->load->helper("file");
         $m =  $this->getweblinkbyid($bid);
         $del = "./upload/weblink/".$m['w_file'];
         @unlink($del);
         $this->db->where('w_id',$bid);
         $this->db->delete('weblink');
                
    }
}

?>