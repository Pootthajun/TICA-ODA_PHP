<?php

class Admin extends Admin_Controller{


	function index(){
		$data['header'] = "Web Link";
		$data['b'] = $this->MWeblink->listAllweblink();
       $data['menu'] = $this->MUser->getMenubyUser();
    	$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
		
	}

	function addweblink(){
	$data['header'] = "Web Link";
        $data['page'] = $this->config->item('wconfig_template_admin').'addweblink';
        $this->load->view($this->_container_admin,$data);
	}

	function insertweblink(){
	$this->MWeblink->insertweblink();
        flashMsg('success',$this->lang->line('insertsuccess'));
        redirect('weblink/admin/index','refresh');
	}

	function editweblink($bid){
	$data['header'] = "Web Link";
	$data['b'] = $this->MWeblink->getweblinkbyid($bid);
        $data['page'] = $this->config->item('wconfig_template_admin').'editweblink';
        $this->load->view($this->_container_admin,$data);
	}

	function updateweblink(){
		if(!empty($_FILES['b_file']['name'])){
        	$bid = $_POST['bid'];
        	$m =  $this->MWeblink->getweblinkbyid($bid);
	        $del = "./upload/weblink/".$m['b_file'];
	        @unlink($del);
        }

		$this->MWeblink->updateweblink();
        flashMsg('success',$this->lang->line('updatesuccess'));
        redirect('weblink/admin/index','refresh');
	}

	function deleteweblink($bid){
		$this->MWeblink->deleteweblink($bid);
        flashMsg('success',$this->lang->line('deletesuccess'));
        redirect('weblink/admin/index','refresh');
	}
}