<form action="<?php echo base_url(); ?>news/admin/update" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><strong>News Title</strong></td>
      <td><label for="news_title"></label>
      <input name="news_title" type="text" id="news_title" value="<?php echo $edit['news_title']; ?>"  class="text-input large-input" required="required" /></td>
    </tr>
    <tr>
      <td valign="top"><strong>Image</strong></td>
      <td>
        <?php if($edit['news_thumnail'] != ""){ ?>
  <input type="file" name="news_thumnail" id="news_thumnail"  onchange="readURL(this);" ><br />
    <img src="<?php echo base_url(); ?>upload/news/<?php echo $edit['news_thumnail']; ?>"  width="200px" id="blah" />
  <?php }else{ ?>
  <input type="file" name="news_thumnail" id="news_thumnail"  onchange="readURL(this);" required="required" ><br />
  <img id="blah" src="#" alt="image" />
  <?php } ?>
   
  <script>
  function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
  </script>
    </td>
    </tr>
    <tr>
      <td><strong>Detail</strong></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><label for="news_detail"></label>
      <textarea name="news_detail" id="news_detail" cols="45" rows="5"><?php echo $edit['news_detail']; ?></textarea></td>
    </tr>
    <tr>
      <td><input type="submit" name="button" id="button" value="Save"  class="button green"/>
      <input name="newsid" type="hidden" id="newsid" value="<?php echo $edit['news_id']; ?>" /></td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script>
CKEDITOR.replace( 'news_detail', {});
</script>
