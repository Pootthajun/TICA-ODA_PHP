<?php

class MNews extends CI_Model{

	function getAllnews(){
		$data = array();
		$this->db->order_by('news_id','DESC');
		$Q = $this->db->get('news');
		if($Q->num_rows() > 0){
			foreach ($Q->result_array() as $row) {
				# code...
				$data[] = $row;
			}
		}
		$Q->free_result();
		return $data;
	}

    function getAllnewsIndex(){
        $data = array();
        $this->db->order_by('news_id','DESC');
        $this->db->limit('6');
        $Q = $this->db->get('news');
        if($Q->num_rows() > 0){
            foreach ($Q->result_array() as $row) {
                # code...
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

	function insertnews(){
		$data = $this->_uploadFile();
		$this->db->insert('news',$data);
	}

	function _uploadFile(){
        $data = array(
            'news_title' => $_POST['news_title'],
            'news_detail' => $_POST['news_detail'],
            'news_by'=>$this->session->userdata('userid'),
            'news_create'=>date('Y-m-d'),
        );
            $filename_th = date('YmdHis');
        	$config['upload_path'] ='./upload/news/';
		$config['allowed_types'] = 'png|jpg|gif|JPEG|jpeg|';
                $config['file_name'] = $filename_th;
		$this->load->library('upload', $config);
                $this->upload->initialize($config);
		if (strlen($_FILES['news_thumnail']['name'])){
                    if(!$this->upload->do_upload('news_thumnail')){
                        $this->upload->display_errors();
			exit("Cannot upload");
                    }
		$image = $this->upload->data();
		if ($image['file_name']){
                    $data['news_thumnail'] = $image['file_name'];					
		}
	}          
	return $data;
    }




    function getNewsbyid($newsid){
    	$this->db->where('news_id',$newsid);
        $Q = $this->db->get('news');
        $data = $Q->row_array();
        $Q->free_result();
        return $data;
    }

    function updatenews(){
    	$data = $this->_uploadFile();
    	$this->db->where('news_id',$_POST['newsid']);
	$this->db->update('news',$data);        
    }

    
    function deletenews($newsid){
    	$t =  $this->getNewsbyid($newsid);
         $del = "./upload/news/".$t['news_thumnail'];
         @unlink($del);
         $this->db->where('news_id',$newsid);
         $this->db->delete('news');
    }


    public function fetch_News($limit, $start) {
    	$data = array();
      	$this->db->order_by('news_id','DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get("news");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        $query->free_result();
		return $data;
   }

   function updatenewsview($newsid){
     	$v = $this->getNewsbyid($newsid);
     	$view = $v['news_counter']+1;
     	$data = array(
     		'news_counter'=>$view,
     		);
     	$this->db->where('news_id',$newsid);
     	$this->db->update('news',$data);
     }





     

}

?>