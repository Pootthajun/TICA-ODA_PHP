<?php


class Admin extends Admin_Controller{
	function __construct(){
	parent::__construct(); 
        $this->module=  basename(dirname(dirname(__FILE__)));
	
    }
	function index(){
		$data['header'] = "News";
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
                 $data['menu'] = $this->MUser->getMenubyUser();
		$data['n'] = $this->MNews->getAllnews();
		$this->load->view($this->_container_admin,$data); 
	}


	function insertnews(){
	    $this->MNews->insertnews();
	    flashMsg('success',$this->lang->line('insertsuccess'));
	    redirect('news/admin/index','refresh');
	}

	function addnews($newsid = 0){
	$data['header'] = "News";
	$data['edit'] = $this->MNews->getNewsbyid($newsid);
        $data['page'] = $this->config->item('wconfig_template_admin').'editnews';
        $this->load->view($this->_container_admin,$data);
	}

	function update(){
	/*	if(!empty($_FILES['news_images_th']['name'])){
        	$newsid = $_POST['newsid'];
        	$m =  $this->MNews->getNewsbyid($newsid);
	        $clipdel = "./upload/".$m['news_images_th'];
	        @unlink($clipdel);
        }*/
            if($this->input->post('newsid') == 0){
            $this->insertnews();    
            }else{
	    $this->MNews->updatenews();
	    flashMsg('success',$this->lang->line('insert_success'));
	    redirect('news/admin/index','refresh');
            }
	}

	function deletenews($newsid){
		$this->MNews->deletenews($newsid);
        flashMsg('success',$this->lang->line('deletesuccess'));
        redirect('news/admin/index','refresh');
	}

	function deleteimage($imageid,$newsid){
				$this->MNews->deleteimage($imageid);
		        flashMsg('success',$this->lang->line('deletesuccess'));
		        redirect('news/admin/editnews/'.$newsid,'refresh');
	}

	
}