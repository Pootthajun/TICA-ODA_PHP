<?php

class News extends Welcome_Controller {

	function News()
    {
        parent::__construct();
        $this->module=  basename(dirname(dirname(__FILE__)));

        $this->load->library("pagination");
         $this->load->helper("thumb_helper");
	}

        function index(){
        $data['header'] ="News";
        $config = array();
        $config["base_url"] = base_url() . $this->uri->segment('1') ."/news/index/";
        $config["total_rows"] = count($this->MNews->getAllnews());
        $config["per_page"] = 12;
        $config["uri_segment"] = 4; 
        
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["results"] = $this->MNews->fetch_News($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
    	$data['page'] = $this->config->item('wconfig_template_welcome').'newslist';
    	$data['module'] = $this->module;
    	$this->load->view($this->_container_contact,$data); 
    	}

        function newsdetail($newsid){
        $data['header'] ="News Detail";
        $this->MNews->updatenewsview($newsid);
        $data['t'] = $this->MNews->getNewsbyid($newsid);
        $data['page'] = $this->config->item('wconfig_template_welcome').'newsdetail';
        $data['module'] = $this->module;
        $this->load->view($this->_container_page,$data); 
        }
		
		
        function listnews(){
        $data['header'] = $this->lang->line('sitepreference');
        $config = array();
        $config["base_url"] = base_url() . $this->uri->segment('1') ."/news/listnews/";
        $config["total_rows"] = count($this->MNews->getAllnews());
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
 
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["results"] = $this->MNews->fetch_News($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        $data['page'] = $this->config->item('wconfig_template_welcome').'newslist';
        $data['module'] = $this->module;
        $this->load->view($this->_container_page,$data); 
        }

}
?>