<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Cooperation Type Table";
        $data['all'] = $this->MCooperationtype->listallcooperationtype();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addcooperationtype(){
        $data['header'] = "Cooperation Type table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addcooperationtype';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertcooperationtype(){
        $this->MCooperationtype->insertcooperationtype();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'cooperationtype/admin/','refresh');
    }
    
    function editcooperationtype($cooperationtypeid){
        $data['header'] = "Edit Cooperation Type table";
        $data['edit'] = $this->MCooperationtype->getcooperationtypebyid($cooperationtypeid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editcooperationtype';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updatecooperationtype(){
        $this->MCooperationtype->updatecooperationtype();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'cooperationtype/admin/','refresh');;
    }
    
    function deletecooperationtype($cooperationtypeid){
        $query = $this->db->get_where('project', array('cooperation_type_id' => $cooperationtypeid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'cooperationtype/admin/','refresh');            
        }else{
            $this->MCooperationtype->deletecooperationtype($cooperationtypeid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'cooperationtype/admin/','refresh');
        }
    }
}