<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MCooperationtype extends CI_Model{
    
    function insertCooperationtype(){
        $data = array(
            'cooperation_type_name'=>$this->input->post('cooperation_type_name'), 
            'cooperation_type_by'=>$this->session->userdata('userid'), 
            'cooperation_type_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('cooperation_type',$data);
        
    }
    
    function listallCooperationtype(){
        $data = array();
        $this->db->order_by('cooperation_type_id',"ASC");
        $Q = $this->db->get('cooperation_type');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getCooperationtypebyid($cooperationtype){
        //$data = array();
        $this->db->where('cooperation_type_id',$cooperationtype);
        $Q = $this->db->get('cooperation_type');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updateCooperationtype(){
        $data = array(
            'cooperation_type_name'=>$this->input->post('cooperation_type_name'),   
            'cooperation_type_by'=>$this->session->userdata('userid'), 
            'cooperation_type_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('cooperation_type_id',$this->input->post('cooperationtype'));
        $this->db->update('cooperation_type',$data);
    }
    
    function deleteCooperationtype($cooperationtype){              
           $this->db->where('cooperation_type_id',$cooperationtype);
           $this->db->delete('cooperation_type');       
    }
    

}