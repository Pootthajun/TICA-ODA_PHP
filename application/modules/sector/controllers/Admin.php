<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Sector Table";
        $data['all'] = $this->MSector->listallSector();
    	$data['menu'] = $this->MUser->getMenubyUser();      
		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addSector(){
        $data['header'] = "Add Sector";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addsector';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertSector(){
        $this->MSector->insertsector();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'sector/admin/','refresh');
    }
    
    function editsector($sectorid){
        $data['header'] = "Edit Sector";
        $data['edit'] = $this->MSector->getsectorbyid($sectorid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editsector';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateSector(){
        $this->MSector->updateSector();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'sector/admin/','refresh');;
    }
    
    function deleteSector($Sectorid){
        $query = $this->db->get_where('project', array('sector_id' => $Sectorid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'sector/admin/','refresh');            
        }else{
            $this->MSector->deleteSector($Sectorid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'sector/admin/','refresh');
        }
    }
}