<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MSector extends CI_Model{
    
    function insertsector(){
        $data = array(
            'sector_name'=>$this->input->post('sector_name'), 
			 'sector_code'=>$this->input->post('sector_code'), 
            'sector_by'=>$this->session->userdata('userid'), 
            'sector_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('sector',$data);
        
    }
    
    function listallSector(){
        $data = array();
        $this->db->order_by('sector_name',"ASC");
        $Q = $this->db->get('sector');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getsectorbyid($sector){
        //$data = array();
        $this->db->where('sector_id',$sector);
        $Q = $this->db->get('sector');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updatesector(){
        $data = array(
            'sector_name'=>$this->input->post('sector_name'),   
			 'sector_code'=>$this->input->post('sector_code'), 
            'sector_by'=>$this->session->userdata('userid'), 
            'sector_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('sector_id',$this->input->post('sector'));
        $this->db->update('sector',$data);
    }
    
    function deletesector($sector){              
           $this->db->where('sector_id',$sector);
           $this->db->delete('sector');       
    }
    

}