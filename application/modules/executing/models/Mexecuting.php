<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MExecuting extends CI_Model{
    
    function insertexecuting(){
        $data = array(
            'executing_name'=>$this->input->post('executing_name'), 
			'funding_id'=>$this->input->post('funding_id'), 
            'executing_by'=>$this->session->userdata('userid'), 
            'executing_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('executing',$data);
        
    }
    
    function listallExecuting(){
        $data = array();
        $this->db->order_by('executing_id',"ASC");
        $Q = $this->db->get('executing');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getexecutingbyid($executing){
        //$data = array();
        $this->db->where('executing_id',$executing);
        $Q = $this->db->get('executing');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
	
	function getExecutingByfunding($funding){
            $data = array();
			$this->db->where('funding_id',$funding);
			$Q = $this->db->get('executing');
			foreach($Q->result_array() as $row){
           			 $data[] = $row;
			}
				$Q->free_result();
				return $data;
			}
    
    function updateexecuting(){
        $data = array(
            'executing_name'=>$this->input->post('executing_name'),   
			'funding_id'=>$this->input->post('funding_id'), 
            'executing_by'=>$this->session->userdata('userid'), 
            'executing_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('executing_id',$this->input->post('executing'));
        $this->db->update('executing',$data);
    }
    
    function deleteexecuting($executing){              
           $this->db->where('executing_id',$executing);
           $this->db->delete('executing');       
    }
    

}