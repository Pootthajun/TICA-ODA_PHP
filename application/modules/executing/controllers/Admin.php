<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Executing Table";
        $data['all'] = $this->MExecuting->listallExecuting();
    	$data['menu'] = $this->MUser->getMenubyUser();      
 		$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addExecuting(){
        $data['header'] = "Executing table";
        $data['funding'] = $this->MFunding->listallFunding();
    	$data['page'] = $this->config->item('wconfig_template_admin').'addexecuting';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertExecuting(){
        $this->MExecuting->insertexecuting();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'executing/admin/','refresh');
    }
    
    function editexecuting($executingid){
        $data['header'] = "Edit Executing name";
        $data['edit'] = $this->MExecuting->getexecutingbyid($executingid);
        $data['funding'] = $this->MFunding->listallFunding();
    	$data['page'] = $this->config->item('wconfig_template_admin').'editexecuting';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateExecuting(){
        $this->MExecuting->updateExecuting();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'executing/admin/','refresh');
    }
    
    function deleteExecuting($Executingid){
        $query = $this->db->get_where('project', array('executing_id' => $Executingid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'executing/admin/','refresh');            
        }else{
            $this->MExecuting->deleteExecuting($Executingid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'executing/admin/','refresh');
        }
    }
}