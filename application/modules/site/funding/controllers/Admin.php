<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Funding table";
        $data['all'] = $this->MFunding->listallFunding();
    	$data['page'] = $this->config->item('wconfig_template_admin').'funding';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addFunding(){
        $data['header'] = "Funding table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addfunding';
    	$this->load->view($this->_container_admin,$data); 
    }
        
    function insertFunding(){
        $this->MFunding->insertfunding();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'funding/admin/','refresh');
    }
    
    function editfunding($fundingid){
        $data['header'] = "Edit Funding name";
        $data['edit'] = $this->MFunding->getfundingbyid($fundingid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editfunding';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateFunding(){
        $this->MFunding->updateFunding();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'funding/admin/','refresh');;
    }
    
    function deleteFunding($Fundingid){
        $query = $this->db->get_where('project', array('bt_id' => $Fundingid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'funding/admin/','refresh');            
        }else{
            $this->MFunding->deleteFunding($Fundingid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'funding/admin/','refresh');
        }
    }
}