<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  MFunding extends CI_Model{
    
    function insertfunding(){
        $data = array(
            'funding_name'=>$this->input->post('funding_name'), 
            'funding_by'=>$this->session->userdata('userid'), 
            'funding_create'=>date('Y-m-d H:i:s'),        
        );
        $this->db->insert('funding',$data);
        
    }
    
    function listallFunding(){
        $data = array();
        $this->db->order_by('funding_id',"ASC");
        $Q = $this->db->get('funding');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getfundingbyid($funding){
        //$data = array();
        $this->db->where('funding_id',$funding);
        $Q = $this->db->get('funding');
        $data = $Q->row_array();
        $Q->free_result();
	    return $data;        
    }
    
    function updatefunding(){
        $data = array(
            'funding_name'=>$this->input->post('funding_name'),   
            'funding_by'=>$this->session->userdata('userid'), 
            'funding_update'=>date('Y-m-d H:i:s'),     
        );
        $this->db->where('funding_id',$this->input->post('funding'));
        $this->db->update('funding',$data);
    }
    
    function deletefunding($funding){              
           $this->db->where('funding_id',$funding);
           $this->db->delete('funding');       
    }
    

}