<p>
  <a href="<?php echo site_url(); ?>funding/admin/addfunding" class="button green">Add Funding </a>  
  </p>
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="13%">id</th>
          <th width="64%">Funding  name</th>
          <th width="14%"></th>
          <th width="9%"></th>
        </tr>
      </thead>
      <tbody>
<?php 
$count = 1;
foreach($all as $list){ ?>
        <tr>
          <td><?php echo $count++; ?></td>
          <td><?php echo $list['funding_name']; ?></td>
          <td><a href="<?php echo site_url(); ?>funding/admin/editfunding/<?php echo $list['funding_id']; ?>" class="button blue">Edit</a></td>
          <td><a href="<?php echo site_url(); ?>funding/admin/deletefunding/<?php echo $list['funding_id']; ?>"  onclick="return confirm('Are you sure you want to delete Funding  name <?php echo $list['funding_name']; ?> ?')" class="button red"><?php echo $this->lang->line('delete'); ?></a></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
