 <?php 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=aidtype_report.doc");
?>
<style>
<!--
 /* Style Definitions */
@page Section1{
            size: 29.7cm 21cm;
            margin: 2cm 2cm 2cm 2cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
        div.Section1 { page:Section1;}
-->
</style>
<div class="Section1">
   <?php
	$pay_date_from = $this->input->get('pay_date_from'); 
    $pay_date_to = $this->input->get('pay_date_to'); 
	$project= $this->input->get('projectid'); 
	$mpyear = $this->input->get('mpyear'); 
	$qaid = "";
    $qdate="";
    $qyear="";
	$yearcat="";
	
                          
        if((!empty($pay_date_from)) AND (!empty($pay_date_to))){
            $qdate = "and (M.mp_date BETWEEN  '$pay_date_from' AND '$pay_date_to') " ;
			$yearcat = "ระหว่างวันที่&nbsp;".$pay_date_from.'- '.$pay_date_to;
        }
        if((!empty($pay_date_from)) AND (empty($pay_date_to))){
            $qdate = "and M.mp_date =  '$pay_date_from'" ;
			$yearcat = "วันที่&nbsp;".$pay_date_from;
        }
        if((empty($pay_date_from)) AND (!empty($pay_date_to))){
            $qdate = "and M.mp_date =  '$pay_date_to'" ;
			$yearcat = "วันที่&nbsp;".$pay_date_to;
        }
	if(!empty($mpyear)){
            $qyear = "and M.mp_year = '$mpyear'" ;
			$yearcat = "ปีงบประมาณ&nbsp;".$mpyear;
	}
	if(!empty($project)){
		 $aidname = $this->MAid->getAidbYprojectid($project);
		 $aid = $aidname['aid_id'];
         $qaid = "and M.aid_id = '$aid'" ;
		 $yearcat = "โครงการ&nbsp;".$aidname['project_name'];
	}
      
   	$q = "
		select * from 
				aid_type 
				order by aid_type_name  ASC
					";
  	 $query = $this->db->query($q); 
	 $num = $query->num_rows();
	 
	 $sql = "select M.*,MP.expense_budget,sum(MP.expense_budget) as sum,R.country_id,A.aid_type_id,A.aid_name
                        FROM money_paid AS M
                        INNER JOIN recipient AS R ON R.rec_id = M.rec_id
                        LEFT JOIN aid AS A ON A.aid_id = M.aid_id       
						LEFT JOIN money_pay_expense AS MP ON MP.mp_id = M.mp_id           
                        ".$qdate."
                        ".$qaid."
                        ".$qyear."
                        ORDER BY M.mp_id DESC			
	  ";
	  $QM = $this->db->query($sql);
	  $m = $QM->row_array();
	  
  ?> 

 <center><h3>การให้ความช่วยเหลือแก่ต่างประเทศ<br/> <?php echo $yearcat; ?></h3></center>


<table width="800" border="1" align="center" cellpadding="0" cellspacing="0" >
<tr>
    <th style='font-size:10.0pt;font-family:"tahoma"' width="100" height="40" bgcolor="#FBFBFB">ประเทศ</th>
	  <?php 	 foreach($query->result_array() as $list){ ?>
    <th width="80" align="right" bgcolor="#FBFBFB" style='font-size:10.0pt;font-family:"tahoma"'>
	       <?php echo $list['aid_type_name']; 
			$arrayaidtype[] = $list['aid_type_id'];
			?>
    </th>
    <?php } ?>

    <th width="150" align="right" bgcolor="#FBFBFB" style='font-size:10.0pt;font-family:"tahoma"'>รวม</th>
  </tr>

  <?php
  $sqlcountry = "select c.country_name,r.country_id from recipient as r
  							left join country as c on c.country_id  = r.country_id
							where r.status = 2
							group by country_id
							order by country_name ASC
							
  ";
  $QC = $this->db->query($sqlcountry);
  foreach($QC->result_array() as $listcountry){
  ?>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"' height="40" bgcolor="#FFFFFF"><?php echo $listcountry['country_name']; ?></td>
       <?php
	 $col = count($arrayaidtype);
      for($i =0;$i<$col;$i++){    
   ?>
      <td align="right" bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php
	$sql = "select M.*,MP.expense_budget,sum(MP.expense_budget) as sum,R.prefix_id,R.country_id,A.aid_type_id
                        FROM money_paid AS M
                        INNER JOIN recipient AS R ON R.rec_id = M.rec_id
                        LEFT JOIN aid AS A ON A.aid_id = M.aid_id       
						LEFT JOIN money_pay_expense AS MP ON MP.mp_id = M.mp_id           
                        where R.country_id = ".$listcountry['country_id']."	
						and A.aid_type_id = ".$arrayaidtype[$i]."		
                        ".$qdate."
                        ".$qaid."
                        ".$qyear."
                        ORDER BY M.mp_id DESC			
	  ";
	  $QM = $this->db->query($sql);
	  $m = $QM->row_array();

	    echo number_format($m['sum'],2,'.',',');  
		
	  
	  ?></td>
      <?php } ?>
      <td align="right" bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php
	$sqlt = "select M.*,MP.expense_budget,sum(MP.expense_budget) as summ,R.prefix_id,R.country_id,A.aid_type_id
                        FROM money_paid AS M
                        INNER JOIN recipient AS R ON R.rec_id = M.rec_id
                        LEFT JOIN aid AS A ON A.aid_id = M.aid_id       
						LEFT JOIN money_pay_expense AS MP ON MP.mp_id = M.mp_id           
                        where R.country_id = ".$listcountry['country_id']."	
                        ".$qdate."
                        ".$qaid."
                        ".$qyear."
                        ORDER BY M.mp_id DESC		
		
	  ";
	  $QMT = $this->db->query($sqlt);
	  $mt = $QMT->row_array();
	  echo number_format($mt['summ'],2,'.',',');  
	  
	  ?></td>
    </tr>

    <?php } ?>
        <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"' height="40" bgcolor="#FFFFFF">รวม</td>
            <?php
	 $col = count($arrayaidtype);
      for($i =0;$i<$col;$i++){    
   ?>
      <td align="right" bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php
	$sqltotal = "select M.*,MP.expense_budget,sum(MP.expense_budget) as summ,R.prefix_id,R.country_id,A.aid_type_id
                        FROM money_paid AS M
                        INNER JOIN recipient AS R ON R.rec_id = M.rec_id
                        LEFT JOIN aid AS A ON A.aid_id = M.aid_id       
						LEFT JOIN money_pay_expense AS MP ON MP.mp_id = M.mp_id           
						and A.aid_type_id = ".$arrayaidtype[$i]."		
                        ".$qdate."
                        ".$qaid."
                        ".$qyear."
                        ORDER BY M.mp_id DESC						
	  ";
	  $QMT = $this->db->query($sqltotal);
	  $mt = $QMT->row_array();
	  @$gtotal += $mt['summ'];
	  echo number_format($mt['summ'],2,'.',',');  
	  
	  ?></td>
      <?php } ?>
      <td align="right" bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php 
	  echo @number_format($gtotal,2,'.',',');  
	  ?></td>
    </tr>

  </table>
</div>
