<script type="text/javascript">
$(document).ready(function(){
	$('#table_id').DataTable();
	$('tbody tr:even').addClass("alt-row");
});
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
  <tr>
    <th width="131">No.</th>
    <th width="235">Name</th>
    <th width="215">Country</th>
    <th width="286">Aid</th>
    <th width="215">Status</th>

    </tr>
  </thead>

  <tbody>
    <?php
  $count =1;
  foreach($all as $list){
  ?>
    <tr>
      <td align="center"><?php echo $count++; ?></td>
      <td><a href="<?php echo site_url(); ?>report/recipientreport/recipientdetail/<?php echo $list['rec_id']; ?>"><?php echo $list['prefix_id']."&nbsp;".$list['fullname']; ?></a></td>
      <td><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
      <td><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <td align="center">
        <?php if($list['status'] == 1){ ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/exclamation.png" title="Wait" width="16" height="16" />
        <?php }else if($list['status'] == 2){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/tick_circle.png" title="Approve" width="16" height="16" />
        <?php }else if($list['status'] == 3){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/cross.png" title="Reject" width="16" height="16" />
        <?php } ?>
      </td>
      
    </tr>
    <?php } ?>
  </tbody>
</table>