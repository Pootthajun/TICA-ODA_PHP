<?php
 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=project_detail.doc");
?>
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {margin:0cm;
    margin-bottom:.0001pt;
    font-size:12.0pt;
    font-family:"Times New Roman";}
@page Section1
    {size:595.3pt 841.9pt;
    margin:1cm 32.5pt 1cm 1cm;}
div.Section1
    {page:Section1;}
-->
</style>
<div class=Section1>
<table width="100%" border="0" align="left" cellpadding="3" cellspacing="1">
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Plan name</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $plan['plan_name']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Project type</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $ptname['project_type_name']; ?></td>
  </tr>
  <tr>
    <td width="175" valign="top" style='font-size:10.0pt;font-family:"tahoma"'>Budget Type</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><table width="100%" border="0" cellspacing="1" cellpadding="1" id="Tbudget" bgcolor="#CFCFCF">
  
      <tr class="recordbt">
        <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><strong>Budget Type Name</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><strong>Budget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
        <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'>&nbsp;</td>
        </tr>
            <?php foreach($bt as $listbt){?>
      <tr class="recordbt">
        <td width="45%" bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php 
		 $this->db->where('bt_id',$listbt['bt_id']);
		 $QB=$this->db->get('budget_type');
		 $nametb = $QB->row();
		 echo $nametb->bt_name;
		 ?></td>
        <td width="44%" bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php echo number_format($listbt['budget_money'],2,'.',','); ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td width="11%" bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'>THB.</td>
        </tr>
      <?php } ?>
    </table></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'> Project name</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['project_name']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Project Objective</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'> <?php echo $edit['project_object']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'> Project Officer</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php 	 echo $officer['user_fullname']; ?></td>
  </tr>
  <tr>
    <td width="175" valign="top" style='font-size:10.0pt;font-family:"tahoma"'>Project  Assistant</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php
	$cassitant = 1;
		$Q = $this->db->query("select project_assistant.user_id,user.user_fullname from project_assistant
		inner join user on project_assistant.user_id = user.user_id
		where project_assistant.project_id = '".$edit['project_id']."'
		");
		 foreach($Q->result_array() as $listuseras){
		echo "<p>";
		echo $cassitant++ .".".$listuseras['user_fullname']; 
		echo "</p>";
		}?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Project Start Date</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'> <?php echo $edit['project_start']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'> Project End Date</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['project_end']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Project budget</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'> <?php echo $edit['project_budget']; ?>&nbsp;THB.</td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Project Comment</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['project_comment']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>     Sector</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo  $sector['sector_name']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Sub Sector</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $subsector['subsector_name']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Cooperation Framework</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'>    <?php  echo $coop['cooperation_name'];?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>   Cooperation Type</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'>  <?php echo $cooptype['cooperation_type_name']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'> OECD Aid Type</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'> <?php echo  $oecd['oecd_name']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Funding Agency</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php  echo  $funding['funding_name']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'> Executing Agency</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $executing['executing_name']; ?></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>    Implementing Agency</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'>  <?php echo $im['implement_name']; ?></td>
  </tr>
  <?php if(($edit['project_type_id'] == "2")  OR ($edit['project_type_id'] == "3") ){ ?>
  <tr>
    <td width="175" valign="top" style='font-size:10.0pt;font-family:"tahoma"'>Co-Funding</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><table width="100%" border="0" cellspacing="0" cellpadding="0" id="customFields" bgcolor="#CFCFCF">
    
    <tr class="recordcountry">
      <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><strong>Country</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
      <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><strong>Budget</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'>&nbsp;</td>
      </tr>
      <?php foreach($co as $listco){ ?>
    <tr class="recordcountry">
    <td bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><?php 
		 $this->db->where('country_id',$listco['country_id']);
		 $QC=$this->db->get('country');
		 $namecountry = $QC->row();
		 echo $namecountry->country_name;
		 ?></td>
    <td bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><?php echo number_format($listco['co_budget'],2,'.',','); ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'>THB.</td>
    </tr>
  <?php } ?>

</table></td>
  </tr>
  <?php } ?>
  <tr>
    <td width="175" valign="top" style='font-size:10.0pt;font-family:"tahoma"'>Disbureseme</td>
    <td width="725"><table width="100%" border="0" cellspacing="1" cellpadding="1" id="Tdis" bgcolor="#CFCFCF">


  <tr class="recorddis">
    <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><strong>Titile </strong>&nbsp;&nbsp;</td>
    <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><strong>Country </strong>&nbsp;&nbsp; </td>
    <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><strong>Budget</strong>&nbsp;&nbsp;</td>
    <td align="center" bgcolor="#FAFAFA" style='font-size:10.0pt;font-family:"tahoma"'><strong>Date </strong>&nbsp;</td>
    </tr>
    <?php foreach($dis as $listdis){ ?>
  <tr class="recorddis">
    <td bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php if($listdis['title']==1){echo "Grant"; }else{echo "Loan"; } ?></td>
    <td bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php 
		 $this->db->where('country_id',$listdis['country_id']);
		 $QC=$this->db->get('country');
		 $namecountry = $QC->row();
		 echo $namecountry->country_name;
		 ?></td>
    <td bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php echo number_format($listdis['dis_budget'],2,'.',','); ?> THB.</td>
    <td bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'>&nbsp;<?php echo $listdis['dis_date']; ?></td>
    </tr>
  <?php } ?>

</table></td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'> Contact Person</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['contact_name']; ?>    </td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'> Contact Position </td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['contact_position']; ?>  </td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>  Contact Email</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['contact_email']; ?>    </td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'> Contact Telephone </td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'> <?php echo $edit['contact_tel']; ?>     </td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Contact Fax</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['contact_fax']; ?>      </td>
  </tr>
  <tr>
    <td width="175" style='font-size:10.0pt;font-family:"tahoma"'>Project Status</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'>  <?php if($edit['project_status'] == 0){ echo 'Running';}else{  echo 'Complete';}  ?></td>
  </tr>
  <tr>
    <td width="175" valign="top" style='font-size:10.0pt;font-family:"tahoma"'>Activity - Aid</td>
    <td width="725" style='font-size:10.0pt;font-family:"tahoma"'>
    <ul>
<?php 
$count = 0;
foreach($activity as $list){  $count++;?>

        <li><?php echo $count.'.'.$list['activity_name']; ?>
           	 <ul style="padding-left: 50px;font-family:"tahoma";">
				<?php 
                    $this->db->where('activity_id',$list['activity_id']);
                    $QAID = $this->db->get('aid');
                    $i=0;
                    foreach($QAID->result_array() as $listaid){
                        $i++;
                        ?>
                    <li  style=" line-height:25px;font-family:"tahoma";">
                    <?php echo $count.'.'.$i.".".$listaid['aid_name']; ?>
                    </li>
                <?php } ?> 
            </ul>
        </li>
        
          

        <?php } ?></ul>
</td>
  </tr>
</table>
</div>