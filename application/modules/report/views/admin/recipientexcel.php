<?php

ob_start();
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=recipient_detal.xls"); 
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); 
ob_end_flush();
?>
<table width="900" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21%" valign="top" bgcolor="#FFFFFF">Aid name</td>
    <td colspan="2" valign="top" bgcolor="#FFFFFF">
      <?php echo $aid['aid_name']; ?></td>
    <td width="29%" rowspan="8" align="left" valign="top" bgcolor="#FFFFFF">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="top"><?php if($edit['imageprofile'] != ""){ ?>
            <img src="<?php echo base_url(); ?>upload/profile/<?php echo $edit['imageprofile']; ?>"  width="150" height="200" id="blah" />
            <?php }else{ ?>
            <img id="blah" src="#" alt="Recipient image" />
          <?php } ?></td>
        </tr>
    </table></td>
  </tr>

  <tr valign="top">
    <td bgcolor="#FFFFFF">Full name</td>
    <td colspan="2" bgcolor="#FFFFFF">
      <?php echo $edit['prefix_id']; ?>
      <?php echo $edit['fullname']; ?>
</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Given name</td>
    <td colspan="2" bgcolor="#FFFFFF">
      <?php echo $edit['givenname']; ?>
    </td>
  </tr>
  <tr valign="top">
    <td  bgcolor="#FFFFFF">Sex</td>
    <td width="19%"  bgcolor="#FFFFFF">
     
        <?php  echo $edit['sex_id']; ?></td>
    <td width="31%" rowspan="3" bgcolor="#FFFFFF">
            <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                    <td  align="left" valign="top">Marital</td>
                    <td align="left" valign="top">
                        <?php echo $edit['marital']; ?> 
                    </td>
              </tr>
              <tr>
                <td align="left" valign="top">Age</td>
                <td align="left" valign="top">
                 <?php echo $agecal;?>
        
                </td>
              </tr>
              <tr>
                <td align="left" valign="top">Nationality</td>
                <td align="left" valign="top">
                <?php echo $edit['nationality']; ?>
                </td>
              </tr>
            </table>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Birthday</td>
    <td bgcolor="#FFFFFF">
      <?php echo $edit['birthday']; ?>
      &nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Religion</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['religion']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">City Of Birth</td>
    <td bgcolor="#FFFFFF">
     <?php echo $edit['city_of_birth']; ?>
    </td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Home Address</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['home_address']; ?></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Home Telphone</td>
    <td bgcolor="#FFFFFF">
    <?php echo $edit['home_tel']; ?>
    </td>
    <td colspan="2" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
          <td width="150">Home Fax</td>
          <td>
            <?php echo $edit['home_fax']; ?>
          </td>
        </tr>
    </table></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Student ID</td>
    <td bgcolor="#FFFFFF">
     <?php echo $edit['student_id']; ?>
    </td>
    <td colspan="2" bgcolor="#FFFFFF"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td width="150" align="left" valign="top">Course</td>
        <td align="left" valign="top"><?php echo $edit['course']; ?></td>
      </tr>
      </table></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Study status</td>
    <td colspan="3" bgcolor="#FFFFFF">
      <?php echo $edit['study_status']; ?> 
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Passport ID</td>
    <td bgcolor="#FFFFFF">
     <?php echo $edit['passport_id']; ?>
      &nbsp;&nbsp; </td>
    <td colspan="2" rowspan="2" bgcolor="#FFFFFF"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td width="150" align="left" valign="top">Passport expire</td>
        <td align="left" valign="top"><?php echo $edit['expire_passport']; ?></td>
      </tr>
      <tr>
        <td align="left" valign="top">Expiry Date of VISA</td>
        <td align="left" valign="top">
          <?php echo $edit['expire_visa']; ?>
      
          </td>
      </tr>
      </table></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Expiry Date of Insurance</td>
    <td bgcolor="#FFFFFF">
      <?php echo $edit['expire_insurance']; ?>
    &nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Work Address</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['work_address']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Departure</td>
    <td bgcolor="#FFFFFF">
    <?php echo $edit['departure']; ?>
    </td>
    <td bgcolor="#FFFFFF">Position
       <?php echo $edit['position']; ?>
    </td>
    <td bgcolor="#FFFFFF">Telephone
       <?php echo $edit['telephone']; ?>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Email</td>
    <td bgcolor="#FFFFFF">
     <?php echo $edit['email']; ?>
    </td>
    <td bgcolor="#FFFFFF">
      Mobile
    <?php echo $edit['mobile']; ?>
    </td>
    <td bgcolor="#FFFFFF">Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo $edit['fax']; ?>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Institute</td>
    <td colspan="3" bgcolor="#FFFFFF">
      <?php echo $in['institute_name']; ?>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Country</td>
    <td colspan="3" bgcolor="#FFFFFF">
   
      <?php echo $country['country_name']; ?>
    
   </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Emergency Name</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['emergency_name']; ?></td>
    <td colspan="2" bgcolor="#FFFFFF">Emergency Relation&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      <?php echo $edit['emergency_relation']; ?>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Emergency telephone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['emergency_telephone']; ?></td>
    <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Emergency Address</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['emergency_address']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Name organisation</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['gov_organisation']; ?></td>
    <td colspan="2" bgcolor="#FFFFFF">Goverment Title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo $edit['gov_title']; ?>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Goverment Date</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['gov_date']; ?></td>
    <td colspan="2" bgcolor="#FFFFFF">Gover duties&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo $edit['gov_duties']; ?>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Goverment post title</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['gov_post_title']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Goverment Address</td>
    <td colspan="3" bgcolor="#FFFFFF">
      
      <?php echo $edit['gov_address']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Previous Type</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['previous_type']; ?></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Previous Title</td>
    <td bgcolor="#FFFFFF">
      <?php echo $edit['previous_title']; ?>
    </td>
    <td colspan="2" bgcolor="#FFFFFF">Previous Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;
<?php echo $edit['previous_name']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Previous Date Start</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['previous_datestart']; ?></td>
    <td colspan="2" bgcolor="#FFFFFF">Previous Date End&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo $edit['previous_dateend']; ?>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Previous Detail</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['previous_detail']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Previous Address</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['previous_address']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Present Type</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['present_type']; ?></td>
    <td bgcolor="#FFFFFF">Present Title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo $edit['present_title']; ?></td>
    <td bgcolor="#FFFFFF">Present Name
      <?php echo $edit['present_name']; ?>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Present Date Start</td>
    <td bgcolor="#FFFFFF"><?php echo $edit['present_datestart']; ?></td>
    <td bgcolor="#FFFFFF">Present Date End
     <?php echo $edit['present_dateend']; ?></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Present Detail</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['present_detail']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Present Address</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['present_address']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Candidate</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['candidate']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Have you been Aid?</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php if($edit['before_come'] == "1"){ echo 'Yes';}else{echo 'No';} ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">&nbsp;</td>
    <td colspan="3" bgcolor="#FFFFFF"><table width="500" border="0" cellspacing="1" cellpadding="1" bgcolor="#BBBBBB">
      <tr>
        <td align="center" bgcolor="#F8F8F8">IELTS Score</td>
        <td align="center" bgcolor="#F8F8F8">TOEFL    Score</td>
        <td align="center" bgcolor="#F8F8F8">Other    Score</td>
      </tr>
      <tr>
        <td align="center" bgcolor="#FFFFFF"><?php echo $edit['ielts_score']; ?></td>
        <td align="center" bgcolor="#FFFFFF"><?php echo $edit['toefl_score']; ?></td>
        <td align="center" bgcolor="#FFFFFF"><?php echo $edit['orther_score']; ?></td>
      </tr>
    </table></td>
  </tr>
  <tr valign="top">
    <td colspan="4" bgcolor="#FFFFFF"><!--<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td bgcolor="#F3F3F3"><p>Mather Tongue
          <input name="mother_tongue" type="text" class="text-input" id="mother_tongue" value="<?php echo $edit['mother_tongue']; ?>"/>
        </p>
          <p>English Speak
            &nbsp;&nbsp;
            <input type="radio" name="eng_speak" id="radio4" value="3" <?php if($edit['eng_speak'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="eng_speak" id="radio5" value="2" <?php if($edit['eng_speak'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="eng_speak" id="radio6" value="1" <?php if($edit['eng_speak'] == "1"){ echo 'checked="checked"';} ?> />
            Fair</p>
          <p>English Write
            &nbsp;&nbsp;&nbsp;
            <input type="radio" name="eng_write" id="radio4" value="3" <?php if($edit['eng_write'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="eng_write" id="radio5" value="2" <?php if($edit['eng_write'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="eng_write" id="radio6" value="1" <?php if($edit['eng_write'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p>
          <p>English Read
            &nbsp;&nbsp;&nbsp; 
            <input type="radio" name="eng_read" id="radio4" value="3" <?php if($edit['eng_read'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="eng_read" id="radio5" value="2" <?php if($edit['eng_read'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="eng_read" id="radio6" value="1" <?php if($edit['eng_read'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p></td>
        <td bgcolor="#F8F8F8"><p>Other Language
          <input name="other_lang" type="text" class="text-input" id="other_lang" value="<?php echo $edit['other_lang']; ?>"/>
        </p>
          <p>Other Speak
            &nbsp;&nbsp;
            <input type="radio" name="other_speak" id="radio4" value="3" <?php if($edit['other_speak'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="other_speak" id="radio5" value="2" <?php if($edit['other_speak'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="other_speak" id="radio6" value="1" <?php if($edit['other_speak'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p>
          <p>Other Write
            &nbsp;&nbsp;&nbsp;
            <input type="radio" name="other_write" id="radio4" value="3" <?php if($edit['other_write'] == "3"){ echo 'checked="checked"';} ?>  />
            <span class="header">Excellent</span>
            <input type="radio" name="other_write" id="radio5" value="2" <?php if($edit['other_write'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="other_write" id="radio6" value="1" <?php if($edit['other_write'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p>
          <p>Other Read
            &nbsp;&nbsp;&nbsp;
            <input type="radio" name="other_read" id="radio4" value="3" <?php if($edit['other_read'] == "3"){ echo 'checked="checked"';} ?> />
            <span class="header">Excellent</span>
            <input type="radio" name="other_read" id="radio5" value="2" <?php if($edit['other_read'] == "2"){ echo 'checked="checked"';} ?> />
            Good
            <input type="radio" name="other_read" id="radio6" value="1" <?php if($edit['other_read'] == "1"){ echo 'checked="checked"';} ?> />
            Fair </p></td>
      </tr>
    </table>--></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">&nbsp;</td>
    <td colspan="3" bgcolor="#FFFFFF"><table width="500" border="0" cellspacing="1" cellpadding="3" bgcolor="#929292">
      <tr>
        <td align="center" bgcolor="#F8F8F8" class="header">Languages </td>
        <td align="center" bgcolor="#F8F8F8" class="header">Read</td>
        <td align="center" bgcolor="#F8F8F8" class="header">Write</td>
        <td align="center" bgcolor="#F8F8F8" class="header">Speak</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><span  class="labeltxt">Mother tongue :</span> <?php echo $edit['mother_tongue']; ?> </td>
        <td bgcolor="#FFFFFF"><p align="center">
          <?php if($edit['mother_read'] == "3"){ echo 'Excellent';}else if($edit['mother_read'] == "2"){ echo 'Good';}else if($edit['mother_read'] == "1"){ echo 'Fair';}  ?>
        </p>
          <label for="mother_speak"></label></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <?php if($edit['mother_write'] == "3"){ echo 'Excellent';}else if($edit['mother_write'] == "2"){ echo 'Good';}else if($edit['mother_write'] == "1"){ echo 'Fair';}  ?>
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <?php if($edit['mother_speak'] == "3"){ echo 'Excellent';}else if($edit['mother_speak'] == "2"){ echo 'Good';}else if($edit['mother_speak'] == "1"){ echo 'Fair';}  ?>
        </p></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><span class="labeltxt">English : </span></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <?php if($edit['eng_read'] == "3"){ echo 'Excellent';}else if($edit['eng_read'] == "2"){ echo 'Good';}else if($edit['eng_read'] == "1"){ echo 'Fair';}  ?>
        </p>
          <label for="mother_speak"></label></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <?php if($edit['eng_write'] == "3"){ echo 'Excellent';}else if($edit['eng_write'] == "2"){ echo 'Good';}else if($edit['eng_write'] == "1"){ echo 'Fair';}  ?>
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <?php if($edit['eng_speak'] == "3"){ echo 'Excellent';}else if($edit['eng_speak'] == "2"){ echo 'Good';}else if($edit['eng_speak'] == "1"){ echo 'Fair';}  ?>
        </p></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><span class="labeltxt">Other :</span> <?php echo $edit['other_lang']; ?> </td>
        <td bgcolor="#FFFFFF"><p align="center">
          <?php if($edit['other_read'] == "3"){ echo 'Excellent';}else if($edit['other_read'] == "2"){ echo 'Good';}else if($edit['other_read'] == "1"){ echo 'Fair';}  ?>
        </p>
          <label for="mother_speak"></label></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <?php if($edit['other_write'] == "3"){ echo 'Excellent';}else if($edit['other_write'] == "2"){ echo 'Good';}else if($edit['other_write'] == "1"){ echo 'Fair';}  ?>
        </p></td>
        <td bgcolor="#FFFFFF"><p align="center">
          <?php if($edit['other_speak'] == "3"){ echo 'Excellent';}else if($edit['other_write'] == "2"){ echo 'Good';}else if($edit['other_write'] == "1"){ echo 'Fair';}  ?>
        </p></td>
      </tr>
    </table></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Expect</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['expect']; ?></td>
  </tr>
  <tr valign="top">
    <td colspan="4" bgcolor="#FFFFFF">Signature
  
     <?php if(!empty($edit['signature'])){?> <img src="<?php echo base_url(); ?>upload/signature/<?php echo $edit['signature']; ?>" width="200"><?php } ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Printed
    <?php echo $edit['printed']; ?></td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Comment</td>
    <td colspan="3" bgcolor="#FFFFFF"><?php echo $edit['comment']; ?></td>
  </tr>
  <tr valign="top">
    <td colspan="4" bgcolor="#FFFFFF">Approve Status

 <?php if($edit['status'] == "1"){ echo 'Wait';}else if($edit['status'] == "2"){ echo 'Approve';}else if($edit['status'] == "2"){ echo 'Reject';}  ?>
    </td>
  </tr>
  <tr valign="top">
    <td bgcolor="#FFFFFF">Approve By</td>
    <td bgcolor="#FFFFFF">
      <?php 
	  echo   $user['user_fullname'];
	 ?>
    </td>
    <td colspan="2" bgcolor="#FFFFFF">Dates capital    <?php echo $edit['date_capital']; ?></td>
  </tr>
</table>

  </form>


