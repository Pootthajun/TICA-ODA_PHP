<?php
 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=financepay_detail.doc");
?>
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {margin:0cm;
    margin-bottom:.0001pt;
  }
@page Section1{
            size: 29.7cm 21cm;
            margin: 2cm 2cm 2cm 2cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
div.Section1
    {page:Section1;}
-->
</style>
<div class=Section1>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'width="120"><strong>Plan Name</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo $detail['plan_name']; ?>
</td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Project Name</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo $detail['project_name']; ?></td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Activity Name</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo $detail['activity_name']; ?></td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>AID Name</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo $detail['aid_name']; ?></td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Aid Pay</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><?php if($detail['aid_pay']=0){echo "Only One ";}else{echo " All AID";} ?></td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Recipient Name</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo $detail['prefix_id']."&nbsp;".$detail['fullname']; ?></td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'>&nbsp;</td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Sex</strong>&nbsp;&nbsp; <?php echo $detail['sex_id']; ?> <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Age</strong>&nbsp;&nbsp;&nbsp; <?php echo $detail['age']; ?> <strong>Year Old</strong></td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'>&nbsp;</td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Email</strong> &nbsp;&nbsp;<?php echo $detail['email']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Tel</strong> <?php echo $detail['telephone']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Country&nbsp;&nbsp;</strong><?php echo $detail['country_name']; ?></td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Date</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'width="580"><?php echo $edit['mp_date'];?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Year&nbsp;&nbsp;&nbsp;<?php echo $edit['mp_year'];?></strong></td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Amount</strong>&nbsp;</td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo number_format($edit['mp_amount'],2,'.',',');  ?> THB.</td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Title</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['mp_title'];?></td>
    </tr>
    <tr>
      <td style='font-size:10.0pt;font-family:"tahoma"'><strong>Comment</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['mp_comment']; ?></td>
    </tr>
  </table>

<p>
<table width="100" border="0" cellspacing="1" cellpadding="1" bgcolor="#A9A9A9" >
  <?php
  	$q = "
		select * from 
				money_paid 
				left join money_pay_expense on money_pay_expense.mp_id = money_paid.mp_id
				left join expense on expense.expense_id = money_pay_expense.expense_id 
		where
				money_paid.rec_id = '".$edit['rec_id']."'
				and money_paid.aid_id = '".$edit['aid_id']."' 
		group by  
				expense.expense_id
	";
  	 $query = $this->db->query($q); 
	 $num = $query->num_rows();
	 if($num > 0){
  ?>  
<thead>	  

  <tr>
    <th width="20" bgcolor="#FCFCFC" nowrap="nowrap" style='font-size:10.0pt;font-family:"tahoma"'>Date</th>
    <th width="20" bgcolor="#FCFCFC"  nowrap="nowrap" style='font-size:10.0pt;font-family:"tahoma"'>Title</th>
    <th width="20" bgcolor="#FCFCFC"  nowrap="nowrap" style='font-size:10.0pt;font-family:"tahoma"'>Comment</th>
    <?php 	 foreach($query->result_array() as $list){ ?>
    <th width="20"  nowrap="nowrap"  bgcolor="#FCFCFC">
	<?php 
 
	echo $list['expense_name']; 
 	$arrayexpenseid[] = $list['expense_id'];?>
    (THB.)</th>
   
    <?php } ?>
     <th width="20"  nowrap="nowrap"  bgcolor="#F7F7F7" style='font-size:10.0pt;font-family:"tahoma"'><strong>Total(THB.)</strong></th>
  </tr>
  </thead>
  <?php 
 
		 $this->db->where('rec_id',$edit['rec_id']);
		 $this->db->where('aid_id',$edit['aid_id']);
		 $Qdate = $this->db->get('money_paid');  
		foreach($Qdate->result_array() as $listdate){
		?>
  <tr>
  <td height="40" bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $listdate['mp_date']; ?></td>
  <td bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $listdate['mp_title']; ?></td>
  <td bgcolor="#FFFFFF" style='font-size:10.0pt;font-family:"tahoma"'><?php echo $listdate['mp_comment']; ?></td>
  <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td bgcolor="#FFFFFF" align="right" style='font-size:10.0pt;font-family:"tahoma"'>
    
    <?php

	$query =$this->db->query("select * from money_pay_expense where mp_id = ".$listdate['mp_id']." and rec_id = '".$listdate['rec_id']."' and expense_id = '". $arrayexpenseid[$i]."' ");
  $rowpay = $query->row_array();
    echo number_format($rowpay['expense_budget'],2,'.',','); 
	
	?>
    </td>

<?php } //end for ?>
    <td bgcolor="#F7F7F7" align="right" style='font-size:10.0pt;font-family:"tahoma"'>
	  <strong>
	  <?php

	$query =$this->db->query("select sum(expense_budget) as tsum from money_pay_expense where  mp_id = ".$listdate['mp_id']." and rec_id = '".$listdate['rec_id']."'  ");
  $rowpay = $query->row_array();
    echo number_format($rowpay['tsum'],2,'.',','); 
	
	?>
	  </strong></td>

  <?php } ?>
    </tr>
    
  <tr>
    <td height="40" colspan="3" bgcolor="#EAEAEA" style='font-size:10.0pt;font-family:"tahoma"'><strong>Total</strong></td>
    <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td bgcolor="#EAEAEA" align="right" style='font-size:10.0pt;font-family:"tahoma"'>
      <strong>
      <?php

	$query =$this->db->query("select sum(expense_budget) as tsum from money_pay_expense where  rec_id = '".$listdate['rec_id']."' and expense_id = '". $arrayexpenseid[$i]."' ");
  $rowpay = $query->row_array();
    echo number_format($rowpay['tsum'],2,'.',','); 
	
	?>
      </strong></td>
   
     <?php } ?>
     <td bgcolor="#EAEAEA" align="right" style='font-size:10.0pt;font-family:"tahoma"'><strong>
       <?php

	$query =$this->db->query("select sum(expense_budget) as tsum from money_pay_expense where  rec_id = '".$listdate['rec_id']."' ");
  $rowpay = $query->row_array();
    echo number_format($rowpay['tsum'],2,'.',','); 
	
	?>
     </strong></td>
  </tr>

 <?php } ?>
</table>
</p>
</div>
