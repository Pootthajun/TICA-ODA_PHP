
<?php
$startmonth = $this->input->post('startmonth');
$endmonth = $this->input->post('endmonth');
$yearstart = $this->input->post('year_start');
$yearend = $this->input->post('year_end');
$qmonthstart = "";
$qmonthstartp="";
$qmonthend = "";
if(!empty($startmonth)){
   $qmonthstart = "and month(dis_date) = $startmonth";
   $qmonthstartp = "and month(m.mp_date) = $startmonth";
}
if(!empty($endmonth)){
		   $qmonthend = "and month(dis_date) = $endmonth";
  		   $qmonthendp = "and month(m.mp_date) = $endmonth";
}


if(empty($yearstart )){
	$yearstart = date('Y')-3;  //ถอยไป 3 ปี
}else{
	$yearstart = $this->input->post('year_start')-543;
}
if(empty($yearend)){
	$yearend = date('Y');  //ปีปัจจุบัน
}else{
	$yearend = $this->input->post('year_end')-543;
}
?>

<center><h3>การให้ความช่วยเหลือต่างประเทศ ระหว่างปี  <?php echo ($yearstart+543) . "-" .($yearend+543); ?> </h3></center>
<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#646464">
<?php for($i = $yearstart; $i<=$yearend ;$i++){ ?>
  <tr>
    <td colspan="5" bgcolor="#E5E5E5">ปี <?php echo $i+543; ?></td>
  </tr>
  <tr>
    <td width="189" align="center" bgcolor="#FFFFFF"><strong>ประเทศ</strong></td>
    <td width="202" align="center" bgcolor="#FFFFFF"><strong>โครงการ</strong></td>
    <td width="185" align="center" bgcolor="#FFFFFF"><strong>กิจกรรม</strong></td>
    <td width="96" align="center" bgcolor="#FFFFFF"><strong>ทุน</strong></td>
    <td width="128" align="center" bgcolor="#FFFFFF"><strong>มูลค่า(บาท)</strong></td>
  </tr>
 <?php  
 //step 1
 //ต้องค้นหา disbure + pay ใช้ปีทีจ่าย join กัน แล้ว where ทั้งคู่ โดยใช้ or จะได้ประเทศทั้งหมดของปีที่ค้นหามา
$queryc = "select * from (
						select country_id from disbureseme 
						where   year(dis_date) = ($i)
						group by country_id 
				UNION 
						select country_id from recipient r left join money_paid m on r.rec_id = m.rec_id
						where   year(m.mp_date) = ($i)
						group by country_id 
		) country group by country.country_id	";
 $res = $this->db->query($queryc); 

        foreach($res->result_array() as $rowcountry){
	
 ?>
  <tr>
    <td bgcolor="#FFFFFF">
    <?php
				$this->db->where('country_id',$rowcountry['country_id']);
				$Q = $this->db->get('country');
				$cdata = $Q->row_array();
				echo $countryname = $cdata['country_name']; 
	?>
    </td>
<!--     //step 2
 //แล้วเอาประเทศมา loop แล้วคิวรี disbure+recipient join project ,activity,aid หาจำนวน where ด้วยประเทศ
 //คิวรี่ pay join recipient หาจำนวนเงิน  where ด้วยประเทศ-->
 
    <td align="center" bgcolor="#FFFFFF">
    <?php
	//count project
			 $cproject  = "select count('project_id')as countproject from disbureseme where 
						  year(dis_date) = ($i)  and country_id = ".$rowcountry['country_id']." ";
			$QC = $this->db->query($cproject);
			$countp = $QC->row_array();
		
			
			 $crecipient = "select * ,count(p.project_id) as countrproject from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where   year(m.mp_date) = ($i) and country_id = ".$rowcountry['country_id']." ";
						$QR = $this->db->query($crecipient);
						$countr = $QR->row_array();
						echo $tcountp = $countp['countproject'] + $countr['countrproject']; /// dis
			
			
	
	?>
    </td>
    <td align="center" bgcolor="#FFFFFF"><?php
	//count activity
			 $cproject  = "select count(ac.activity_id) as countactiviry from disbureseme  d
									left join project p on p.project_id = d.project_id
									left join activity ac on ac.project_id = ac.project_id
			 				where 
						  year(d.dis_date) = ($i)  and d.country_id = ".$rowcountry['country_id']." 
						  group by ac.activity_id
						  ";
			$QC = $this->db->query($cproject);
			$countp = $QC->row_array();
		
			
			 $crecipient = "select * ,count(ac.activity_id) as countractiviry from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where   year(m.mp_date) = ($i) and country_id = ".$rowcountry['country_id']." 
						group by ac.activity_id
						";
						$QR = $this->db->query($crecipient);
						$countr = $QR->row_array();
						echo $countp['countactiviry'] + $countr['countractiviry']; /// dis
			
			
	
	?></td>
    <td align="center" bgcolor="#FFFFFF">
	<?php
	//count aid
			 $caid  = "select count(a.aid_id) as countaid from disbureseme  d
									left join project p on p.project_id = d.project_id
									left join activity ac on ac.project_id = ac.project_id
									left join aid a on a.activity_id = ac.activity_id
			 				where 
						  year(d.dis_date) = ($i)  and d.country_id = ".$rowcountry['country_id']." 
						  group by a.aid_id
						  ";
			$QA = $this->db->query($caid);
			$counta = $QA->row_array();
		
			
			 $carecipient = "select * ,count(a.aid_id) as countraid from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where   year(m.mp_date) = ($i) and country_id = ".$rowcountry['country_id']." 
						 group by a.aid_id
									";
						$QRAID = $this->db->query($carecipient);
						$countra = $QRAID->row_array();
						echo $counta['countaid'] + $countra['countraid']; /// dis
			
			
	
	?></td>
    <td align="right" bgcolor="#FFFFFF"><?php
	//count aid
			 $csum = "select sum(dis_budget) as sumdis from disbureseme  d
			 				where 
						  year(d.dis_date) = ($i)  and d.country_id = ".$rowcountry['country_id']." 						 
						  ";
			$QS = $this->db->query($csum);
			$suma = $QS->row_array();
		
			
			 $csumrecipient = "select * ,sum(mx.expense_budget) as sumr from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join money_pay_expense mx on mx.mp_id = m.mp_id
						where   year(m.mp_date) = ($i) and country_id = ".$rowcountry['country_id']." 
									";
						$QRSUM = $this->db->query($csumrecipient);
						$sumr = $QRSUM->row_array();
						echo number_format($suma['sumdis'] + $sumr['sumr'],2,'.',',');
			
			
	
	?></td>
  </tr>
  <?php } ?>
  <tr>
    <td bgcolor="#FDFDFD"><strong>รวม(บาท)</strong></td>
    <td align="center" bgcolor="#FDFDFD"><strong>
      <?php
	//count project
			 $cproject  = "select count('project_id')as countproject from disbureseme where 
						  year(dis_date) = ($i)   ";
			$QC = $this->db->query($cproject);
			$countp = $QC->row_array();
		
			
			 $crecipient = "select * ,count(p.project_id) as countrproject from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where   year(m.mp_date) = ($i) ";
						$QR = $this->db->query($crecipient);
						$countr = $QR->row_array();
						echo $tcountp = $countp['countproject'] + $countr['countrproject']; /// dis
			
			
	
	?>
    </strong></td>
    <td align="center" bgcolor="#FDFDFD"><strong>
      <?php
	//count activity
			 $cproject  = "select count(ac.activity_id) as countactiviry from disbureseme  d
									left join project p on p.project_id = d.project_id
									left join activity ac on ac.project_id = ac.project_id
			 				where 
						  year(d.dis_date) = ($i) 
						  group by ac.activity_id
						  ";
			$QC = $this->db->query($cproject);
			$countp = $QC->row_array();
		
			
			 $crecipient = "select * ,count(ac.activity_id) as countractiviry from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where   year(m.mp_date) = ($i)
						group by ac.activity_id
						";
						$QR = $this->db->query($crecipient);
						$countr = $QR->row_array();
						echo $countp['countactiviry'] + $countr['countractiviry']; /// dis
			
			
	
	?>
    </strong></td>
    <td align="center" bgcolor="#FDFDFD"><strong>
      <?php
	//count aid
			 $caid  = "select count(a.aid_id) as countaid from disbureseme  d
									left join project p on p.project_id = d.project_id
									left join activity ac on ac.project_id = ac.project_id
									left join aid a on a.activity_id = ac.activity_id
			 				where 
						  year(d.dis_date) = ($i)
						  group by a.aid_id
						  ";
			$QA = $this->db->query($caid);
			$counta = $QA->row_array();
		
			
			 $carecipient = "select * ,count(a.aid_id) as countraid from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where   year(m.mp_date) = ($i) 
						 group by a.aid_id
									";
						$QRAID = $this->db->query($carecipient);
						$countra = $QRAID->row_array();
						echo $counta['countaid'] + $countra['countraid']; /// dis
			
			
	
	?>
    </strong></td>
    <td align="right" bgcolor="#FDFDFD"><strong>
      <?php
	//count aid
			 $csum = "select sum(dis_budget) as sumdis from disbureseme  d
			 				where 
						  year(d.dis_date) = ($i) 						 
						  ";
			$QS = $this->db->query($csum);
			$suma = $QS->row_array();
		
			
			 $csumrecipient = "select * ,sum(mx.expense_budget) as sumr from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join money_pay_expense mx on mx.mp_id = m.mp_id
						where   year(m.mp_date) = ($i) 
									";
						$QRSUM = $this->db->query($csumrecipient);
						$sumr = $QRSUM->row_array();
						echo number_format($suma['sumdis'] + $sumr['sumr'],2,'.',',');
			
			
	
	?>
    </strong></td>
  </tr>

  	
  <?php } 
  ?>
    <tr>
    <td bgcolor="#DEDEDE"><strong>รวมทั้งสิ้น(บาท)</strong></td>
    <td align="center" bgcolor="#DEDEDE">
      <strong>
      <?php
	//count project
			 $cproject  = "select count('project_id')as countproject from disbureseme  
						  		  where year(dis_date) between '".($yearstart)."' and  '".($yearend)."'   ";
			$QC = $this->db->query($cproject);
			$countp = $QC->row_array();
		
			
			 $crecipient = "select * ,count(p.project_id) as countrproject from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where year(m.mp_date) between '".($yearstart)."' and  '".($yearend)."' 
						 ";
						$QR = $this->db->query($crecipient);
						$countr = $QR->row_array();
						echo $tcountp = $countp['countproject'] + $countr['countrproject']; /// dis
			
			
	
	?>
      </strong></td>
    <td align="center" bgcolor="#DEDEDE"><strong>
      <?php
	//count activity
			 $cproject  = "select count(ac.activity_id) as countactiviry from disbureseme  d
									left join project p on p.project_id = d.project_id
									left join activity ac on ac.project_id = ac.project_id			 				
							 where year(dis_date) between '".($yearstart)."' and  '".($yearend)."'  
						 	 group by ac.activity_id
						  ";
			$QC = $this->db->query($cproject);
			$countp = $QC->row_array();
		
			
			 $crecipient = "select * ,count(ac.activity_id) as countractiviry from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where year(m.mp_date) between '".($yearstart)."' and  '".($yearend)."'
						group by ac.activity_id
						";
						$QR = $this->db->query($crecipient);
						$countr = $QR->row_array();
						echo $countp['countactiviry'] + $countr['countractiviry']; /// dis
			
			
	
	?>
    </strong></td>
    <td align="center" bgcolor="#DEDEDE"><strong>
      <?php
	//count aid
			 $caid  = "select count(a.aid_id) as countaid from disbureseme  d
									left join project p on p.project_id = d.project_id
									left join activity ac on ac.project_id = ac.project_id
									left join aid a on a.activity_id = ac.activity_id			 				 
						   where year(dis_date) between '".($yearstart)."' and  '".($yearend)."'  
						  group by a.aid_id
						  ";
			$QA = $this->db->query($caid);
			$counta = $QA->row_array();
		
			
			 $carecipient = "select * ,count(a.aid_id) as countraid from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where year(m.mp_date) between '".($yearstart)."' and  '".($yearend)."'
						 group by a.aid_id
									";
						$QRAID = $this->db->query($carecipient);
						$countra = $QRAID->row_array();
						echo $counta['countaid'] + $countra['countraid']; /// dis
			
			
	
	?>
    </strong></td>
    <td align="right" bgcolor="#DEDEDE"><strong>
      <?php
	//count aid
			 $csum = "select sum(dis_budget) as sumdis from disbureseme  d
			 				where year(dis_date) between '".($yearstart)."' and  '".($yearend)."'  				 
						  ";
			$QS = $this->db->query($csum);
			$suma = $QS->row_array();
		
			
			 $csumrecipient = "select * ,sum(mx.expense_budget) as sumr from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join money_pay_expense mx on mx.mp_id = m.mp_id
						where year(m.mp_date) between '".($yearstart)."' and  '".($yearend)."'
									";
						$QRSUM = $this->db->query($csumrecipient);
						$sumr = $QRSUM->row_array();
						echo number_format($suma['sumdis'] + $sumr['sumr'],2,'.',',');
			
			
	
	?>
    </strong></td>
  </tr>
</table>
