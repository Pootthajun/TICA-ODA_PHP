<?php
 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=project_detail.doc");
?>
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {margin:0cm;
    margin-bottom:.0001pt;
    font-size:12.0pt;
    font-family:"Times New Roman";}
@page Section1
    {size:595.3pt 841.9pt;
    margin:1cm 32.5pt 1cm 1cm;}
div.Section1
    {page:Section1;}
-->
</style>
<div class=Section1>
<table width="800" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Plan name</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $plan['plan_name']; ?></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Project type</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $ptname['project_type_name']; ?></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200" valign="top">Budget Type</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><table width="500" border="1" cellspacing="0" cellpadding="0" id="Tbudget" bgcolor="#CFCFCF">
  
      <tr class="recordbt">
        <td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FAFAFA"><strong>Budget Type Name</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td style='font-size:10.0pt;font-family:"tahoma"' align="right" bgcolor="#FAFAFA"><strong>Budget(THB.)</strong></td>
        </tr>
            <?php foreach($bt as $listbt){?>
      <tr class="recordbt">
        <td style='font-size:10.0pt;font-family:"tahoma"'width="45%" bgcolor="#FFFFFF"><?php 
		 $this->db->where('bt_id',$listbt['bt_id']);
		 $QB=$this->db->get('budget_type');
		 $nametb = $QB->row();
		 echo $nametb->bt_name;
		 ?></td>
        <td style='font-size:10.0pt;font-family:"tahoma"'width="44%" align="right" bgcolor="#FFFFFF"><?php 
		@$total +=$listbt['budget_money'];
		echo number_format($listbt['budget_money'],2,'.',','); ?></td>
        </tr>
         <?php } ?>
      <tr class="recordbt">
        <td style='font-size:10.0pt;font-family:"tahoma"'bgcolor="#FFFFFF"><strong>Total</strong></td>
        <td style='font-size:10.0pt;font-family:"tahoma"'align="right" bgcolor="#FFFFFF"><strong><?php echo @number_format($total,2,'.',','); ?></strong></td>
      </tr>
     
    </table></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200"> Project name</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $edit['project_name']; ?></td>
  </tr>
     <?php if(($edit['project_type_id'] == "1")  OR ($edit['project_type_id'] == "2") OR ($edit['project_type_id'] == "3") ){ ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Project Objective</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"> <?php echo $edit['project_object']; ?></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'>Project Detail</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo $edit['project_detail']; ?></td>
  </tr>
  <?php } ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Project Start Date</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"> <?php echo $edit['project_start']; ?></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200"> Project End Date</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $edit['project_end']; ?></td>
  </tr>
   <?php if(($edit['project_type_id'] == "1")  OR ($edit['project_type_id'] == "2") ){ ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Project budget</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"> <?php echo number_format($edit['project_budget'],2,'.',','); ?>&nbsp;THB.</td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Project Comment</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $edit['project_comment']; ?></td>
  </tr>
  <?php } ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">     Sector</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo  $sector['sector_name']; ?></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Sub Sector</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $subsector['subsector_name']; ?></td>
  </tr>
   <?php if(($edit['project_type_id'] == "1")  OR ($edit['project_type_id'] == "2") OR ($edit['project_type_id'] == "3")){ ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Cooperation Framework</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700">    <?php  echo $coop['cooperation_name'];?></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">   Cooperation Type</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700">  <?php echo $cooptype['cooperation_type_name']; ?></td>
  </tr>
  <?php } ?>
     <?php if(($edit['project_type_id'] == "1")  OR ($edit['project_type_id'] == "2") ){ ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200"> OECD Aid Type</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"> <?php echo  $oecd['oecd_name']; ?></td>
  </tr>
  <?php } ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Funding Agency</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php  echo  $funding['funding_name']; ?></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200"> Executing Agency</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $executing['executing_name']; ?></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">    Implementing Agency</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700">  <?php echo $im['implement_name']; ?></td>
  </tr>
    <?php if(($edit['project_type_id'] == "4") ){ ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'>Multilateral Organizations</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'><?php echo $mu['country_name']; ?></td>
  </tr>
  <?php } ?>
  <?php if(($edit['project_type_id'] == "1")  OR ($edit['project_type_id'] == "2") ){ ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200" valign="top">Co-Funding</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><table width="500" border="1" cellspacing="0" cellpadding="0" id="customFields" bgcolor="#CFCFCF">
    
    <tr class="recordcountry">
      <td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FAFAFA"><strong>Country</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
      <td style='font-size:10.0pt;font-family:"tahoma"' align="right" bgcolor="#FAFAFA"><strong>Budget(THB.)</strong>&nbsp;&nbsp;</td>
      </tr>
      <?php foreach($co as $listco){ ?>
    <tr class="recordcountry">
    <td style='font-size:10.0pt;font-family:"tahoma"'bgcolor="#FFFFFF"><?php 
		 $this->db->where('country_id',$listco['country_id']);
		 $QC=$this->db->get('country');
		 $namecountry = $QC->row();
		 echo $namecountry->country_name;
		 ?></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'align="right" bgcolor="#FFFFFF"><?php 
	@$totalco += $listco['co_budget'];
	echo number_format($listco['co_budget'],2,'.',','); ?></td>
    </tr>
    <?php } ?>
    <tr class="recordcountry">
      <td style='font-size:10.0pt;font-family:"tahoma"'bgcolor="#FFFFFF"><strong>Total</strong></td>
      <td style='font-size:10.0pt;font-family:"tahoma"'align="right" bgcolor="#FFFFFF"><strong>
        <?php 
	
	echo @number_format($totalco,2,'.',','); ?>
      </strong></td>
    </tr>
  

</table></td>
  </tr>
  <?php } ?>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200" valign="top">Disbureseme</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700">
    <?php if($edit['project_type_id'] != "4") { ?>
    <table width="500" border="1" cellspacing="0" cellpadding="0" id="Tdis" bgcolor="#CFCFCF">


  <tr class="recorddis">
    <td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FAFAFA"><strong>Titile </strong>&nbsp;&nbsp;</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FAFAFA"><strong>Country </strong>&nbsp;&nbsp; </td>
    <td style='font-size:10.0pt;font-family:"tahoma"' align="right" bgcolor="#FAFAFA"><strong>Budget(THB.)</strong>&nbsp;&nbsp;</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FAFAFA"><strong>Date </strong>&nbsp;</td>
    </tr>
    <?php foreach($dis as $listdis){ ?>
  <tr class="recorddis">
    <td style='font-size:10.0pt;font-family:"tahoma"'bgcolor="#FFFFFF"><?php if($listdis['title']==1){echo "Grant"; }else{echo "Loan"; } ?></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'bgcolor="#FFFFFF"><?php 
		 $this->db->where('country_id',$listdis['country_id']);
		 $QC=$this->db->get('country');
		 $namecountry = $QC->row();
		 echo $namecountry->country_name;
		 ?></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'align="right" bgcolor="#FFFFFF"><?php 
	@$totaldis += $listdis['dis_budget'];
	echo number_format($listdis['dis_budget'],2,'.',','); ?></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'align="center" bgcolor="#FFFFFF">&nbsp;<?php echo $listdis['dis_date']; ?></td>
    </tr>
      <?php } ?>
  <tr class="recorddis">
    <td style='font-size:10.0pt;font-family:"tahoma"'colspan="2" bgcolor="#FFFFFF"><strong>Total</strong></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'align="right" bgcolor="#FFFFFF"><strong>
      <?php 
	echo @number_format($totaldis,2,'.',','); ?>
    </strong></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'align="center" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>


</table>
<?php }else{ ?>

  <table width="500" border="1" cellspacing="0" cellpadding="0" id="Tdis" bgcolor="#CFCFCF">


  <tr class="recorddis">
    <td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FAFAFA"><strong>Titile </strong>&nbsp;&nbsp;</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FAFAFA"><strong>Country </strong>&nbsp;&nbsp; </td>
    <td style='font-size:10.0pt;font-family:"tahoma"' align="right" bgcolor="#FAFAFA"><strong>Budget(THB.)&nbsp;</strong>&nbsp;&nbsp;</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FAFAFA"><strong>Date </strong>&nbsp;</td>
    </tr>
    <?php foreach($dis as $listdis){ ?>
  <tr class="recorddis">
    <td style='font-size:10.0pt;font-family:"tahoma"'bgcolor="#FFFFFF"><?php if($listdis['title']==3){echo "ค่าบำรุงสมาชิก"; }else if($listdis['title']==4){echo "เงินสมทบกองทุน"; }else if($listdis['title']==5){echo "เงินอุดหนุน"; } ?></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'bgcolor="#FFFFFF"><?php 
		 $this->db->where('country_id',$listdis['country_id']);
		 $QC=$this->db->get('country');
		 $namecountry = $QC->row();
		 echo $namecountry->country_name;
		 ?></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'align="right" bgcolor="#FFFFFF"><?php 
	@$totaldis += $listdis['dis_budget'];
	echo number_format($listdis['dis_budget'],2,'.',','); ?></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'align="center" bgcolor="#FFFFFF">&nbsp;<?php echo $listdis['dis_date']; ?></td>
    </tr>
  <?php } ?>
    <tr class="recorddis">
    <td style='font-size:10.0pt;font-family:"tahoma"'colspan="2" bgcolor="#FFFFFF"><strong>Total</strong></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'align="right" bgcolor="#FFFFFF"><strong>
      <?php 
	echo @number_format($totaldis,2,'.',','); ?>
    </strong></td>
    <td style='font-size:10.0pt;font-family:"tahoma"'align="center" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>

</table>
  <?php } ?>

</td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200"> Contact Person</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $edit['contact_name']; ?>    </td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200"> Contact Position </td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $edit['contact_position']; ?>  </td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">  Contact Email</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $edit['contact_email']; ?>    </td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200"> Contact Telephone </td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"> <?php echo $edit['contact_tel']; ?>     </td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Contact Fax</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700"><?php echo $edit['contact_fax']; ?>      </td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200">Project Status</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700">  <?php if($edit['project_status'] == 0){ echo 'Running';}else{  echo 'Complete';}  ?></td>
  </tr>
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="200" valign="top">Activity - Aid</td>
    <td style='font-size:10.0pt;font-family:"tahoma"'width="700">
    <ul>
<?php 
$count = 0;
foreach($activity as $list){  $count++;?>

        <li><?php echo $count.'.'.$list['activity_name']; ?>
           	 <ul style="padding-left: 50px;">
				<?php 
                    $this->db->where('activity_id',$list['activity_id']);
                    $QAID = $this->db->get('aid');
                    $i=0;
                    foreach($QAID->result_array() as $listaid){
                        $i++;
                        ?>
                    <li  style=" line-height:25px;">
                    <?php echo $count.'.'.$i.".".$listaid['aid_name']; ?>
                    </li>
                <?php } ?> 
            </ul>
        </li>
        
          

        <?php } ?></ul>
</td>
  </tr>
</table>


</div>