<style>
.chosen-select{
width:250px;	
}


</style>

<script type="text/javascript">
$(document).ready(function(){
	$('#table_id').DataTable();
	$('tbody tr:even').addClass("alt-row");
});
</script>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="table_id">
      <thead>
        <tr>
          <th width="63">Id</th>
          <th width="168">Plan</th>
          <th width="147">Project Type</th>
          <th width="322">Project Name</th>
          <th width="122">Start</th>
          <th width="126">End</th>
           <?php  if( !empty( $menu ) ) { ?>
          <th>&nbsp;</th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
<?php 
$count = 1;
foreach($project as $list){ ?>
        <tr>
          <td><?php echo $count++; ?></td>
          <td>
		  <?php
		  	$this->db->where('plan_id',$list['plan_id']);
			$QP = $this->db->get('plan');
			$planname = $QP->row_array();
			echo $planname['plan_name'];
		  
		  ?></td>
          <td><?php 
		  $this->db->where('project_type_id',$list['project_type_id']);
			$QT = $this->db->get('project_type');
			$typename = $QT->row_array();
			echo $typename['project_type_name']
		  ?></td>
          <td><a href="<?php echo site_url(); ?>report/projectreport/projectdetail/<?php echo $list['project_id']; ?>"><?php echo $list['project_name']; ?></a></td>
          <td><?php echo $list['project_start']; ?></td>
          <td><?php echo $list['project_end']; ?></td>
          <?php  if( !empty( $menu ) ) { ?>
          <td width="120" align="right">

            <a href="<?php echo site_url(); ?>project/admin/editproject/<?php echo $list['project_id']; ?>" class="button blue">Edit</a>

            <a href="<?php echo site_url(); ?>project/admin/deleteproject/<?php echo $list['project_id']; ?>"  onclick="return confirm('Are you sure you want to delete?')" class="button red"><?php echo $this->lang->line('delete'); ?></a>

          </td>
          <?php } ?>
          </tr>
          <?php } ?>
      </tbody>
    </table>