



<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="18%">Plan name</td>
    <td width="82%"><?php echo $plan['plan_name']; ?></td>
  </tr>
  <tr>
    <td>Project type</td>
    <td><?php echo $ptname['project_type_name']; ?></td>
  </tr>
  <tr>
    <td valign="top">Budget Type</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="1" id="Tbudget" bgcolor="#CFCFCF">
  
      <tr class="recordbt">
        <td align="center" bgcolor="#FAFAFA"><strong>Budget Type Name</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td align="center" bgcolor="#FAFAFA"><strong>Budget&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
        <td align="center" bgcolor="#FAFAFA">&nbsp;</td>
        </tr>
            <?php foreach($bt as $listbt){?>
      <tr class="recordbt">
        <td width="45%" bgcolor="#FFFFFF"><?php 
		 $this->db->where('bt_id',$listbt['bt_id']);
		 $QB=$this->db->get('budget_type');
		 $nametb = $QB->row();
		 echo $nametb->bt_name;
		 ?></td>
        <td width="44%" bgcolor="#FFFFFF"><?php echo number_format($listbt['budget_money'],2,'.',','); ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td width="11%" bgcolor="#FFFFFF">THB.</td>
        </tr>
      <?php } ?>
    </table></td>
  </tr>
  <tr>
    <td> Project name</td>
    <td><?php echo $edit['project_name']; ?></td>
  </tr>
  <tr>
    <td>Project Objective</td>
    <td> <?php echo $edit['project_object']; ?></td>
  </tr>
  <tr>
    <td> Project Officer</td>
    <td><?php 	 echo $officer['user_fullname']; ?></td>
  </tr>
  <tr>
    <td valign="top">Project  Assistant</td>
    <td><?php
	$cassitant = 1;
		$Q = $this->db->query("select project_assistant.user_id,user.user_fullname from project_assistant
		inner join user on project_assistant.user_id = user.user_id
		where project_assistant.project_id = '".$edit['project_id']."'
		");
		 foreach($Q->result_array() as $listuseras){
		echo "<p>";
		echo $cassitant++ .".".$listuseras['user_fullname']; 
		echo "</p>";
		}?></td>
  </tr>
  <tr>
    <td>Project Start Date</td>
    <td> <?php echo $edit['project_start']; ?></td>
  </tr>
  <tr>
    <td> Project End Date</td>
    <td><?php echo $edit['project_end']; ?></td>
  </tr>
  <tr>
    <td>Project budget</td>
    <td> <?php echo $edit['project_budget']; ?>&nbsp;THB.</td>
  </tr>
  <tr>
    <td>Project File</td>
    <td><a href="<?php echo base_url(); ?>upload/project/<?php echo $edit['project_file']; ?>">File</a></td>
  </tr>
  <tr>
    <td>Project Comment</td>
    <td><?php echo $edit['project_comment']; ?></td>
  </tr>
  <tr>
    <td>     Sector</td>
    <td><?php echo  $sector['sector_name']; ?></td>
  </tr>
  <tr>
    <td>Sub Sector</td>
    <td><?php echo $subsector['subsector_name']; ?></td>
  </tr>
  <tr>
    <td>Cooperation Framework</td>
    <td>    <?php  echo $coop['cooperation_name'];?></td>
  </tr>
  <tr>
    <td>   Cooperation Type</td>
    <td>  <?php echo $cooptype['cooperation_type_name']; ?></td>
  </tr>
  <tr>
    <td> OECD Aid Type</td>
    <td> <?php echo  $oecd['oecd_name']; ?></td>
  </tr>
  <tr>
    <td>Funding Agency</td>
    <td><?php  echo  $funding['funding_name']; ?></td>
  </tr>
  <tr>
    <td> Executing Agency</td>
    <td><?php echo $executing['executing_name']; ?></td>
  </tr>
  <tr>
    <td>    Implementing Agency</td>
    <td>  <?php echo $im['implement_name']; ?></td>
  </tr>
  <tr>
    <td valign="top">Co-Funding</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" id="customFields" bgcolor="#CFCFCF">
    
    <tr class="recordcountry">
      <td align="center" bgcolor="#FAFAFA"><strong>Country</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
      <td align="center" bgcolor="#FAFAFA"><strong>Budget</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="center" bgcolor="#FAFAFA">&nbsp;</td>
      </tr>
      <?php foreach($co as $listco){ ?>
    <tr class="recordcountry">
    <td bgcolor="#FAFAFA"><?php 
		 $this->db->where('country_id',$listco['country_id']);
		 $QC=$this->db->get('country');
		 $namecountry = $QC->row();
		 echo $namecountry->country_name;
		 ?></td>
    <td bgcolor="#FAFAFA"><?php echo number_format($listco['co_budget'],2,'.',','); ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td bgcolor="#FAFAFA">THB.</td>
    </tr>
  <?php } ?>

</table></td>
  </tr>
  <tr>
    <td valign="top">Disbureseme</td>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="1" id="Tdis" bgcolor="#CFCFCF">


  <tr class="recorddis">
    <td align="center" bgcolor="#FAFAFA"><strong>Titile </strong>&nbsp;&nbsp;</td>
    <td align="center" bgcolor="#FAFAFA"><strong>Country </strong>&nbsp;&nbsp; </td>
    <td align="center" bgcolor="#FAFAFA"><strong>Budget</strong>&nbsp;&nbsp;</td>
    <td align="center" bgcolor="#FAFAFA"><strong>Date </strong>&nbsp;</td>
    </tr>
    <?php foreach($dis as $listdis){ ?>
  <tr class="recorddis">
    <td bgcolor="#FFFFFF"><?php if($listdis['title']==1){echo "Grant"; }else{echo "Loan"; } ?></td>
    <td bgcolor="#FFFFFF"><?php 
		 $this->db->where('country_id',$listdis['country_id']);
		 $QC=$this->db->get('country');
		 $namecountry = $QC->row();
		 echo $namecountry->country_name;
		 ?></td>
    <td bgcolor="#FFFFFF"><?php echo number_format($listdis['dis_budget'],2,'.',','); ?> THB.</td>
    <td bgcolor="#FFFFFF">&nbsp;<?php echo $listdis['dis_date']; ?></td>
    </tr>
  <?php } ?>

</table></td>
  </tr>
  <tr>
    <td> Contact Person</td>
    <td><?php echo $edit['contact_name']; ?>    </td>
  </tr>
  <tr>
    <td> Contact Position </td>
    <td><?php echo $edit['contact_position']; ?>  </td>
  </tr>
  <tr>
    <td>  Contact Email</td>
    <td><?php echo $edit['contact_email']; ?>    </td>
  </tr>
  <tr>
    <td> Contact Telephone </td>
    <td> <?php echo $edit['contact_tel']; ?>     </td>
  </tr>
  <tr>
    <td>Contact Fax</td>
    <td><?php echo $edit['contact_fax']; ?>      </td>
  </tr>
  <tr>
    <td>Project Status</td>
    <td>  <?php if($edit['project_status'] == 0){ echo 'Running';}else{  echo 'Complete';}  ?></td>
  </tr>
  <tr>
    <td valign="top">Activity - Aid</td>
    <td>
    <ul>
<?php 
$count = 0;
foreach($activity as $list){  $count++;?>

        <li><?php echo $count.'.'.$list['activity_name']; ?>
           	 <ul style="padding-left: 50px;">
				<?php 
                    $this->db->where('activity_id',$list['activity_id']);
                    $QAID = $this->db->get('aid');
                    $i=0;
                    foreach($QAID->result_array() as $listaid){
                        $i++;
                        ?>
                    <li  style=" line-height:25px;">
                    <?php echo $count.'.'.$i.".".$listaid['aid_name']; ?>
                    </li>
                <?php } ?> 
            </ul>
        </li>
        
          

        <?php } ?></ul>
</td>
  </tr>
</table>
