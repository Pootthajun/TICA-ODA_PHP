<?php 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=tri-coopertionreport.doc");
?> 
 
    <?php 
	$yearstart = $this->input->get('year');
	if(empty($yearstart)){
	$yearstart = date('Y');  //ปีปัจจุบัน
	}else{
		$yearstart = $this->input->get('year')-543;
	}
   $sqlco = "
   select * from 
   				co_funding c left join project p on p.project_id = c.project_id 
				left join country co on co.country_id = c.country_id
   where  
   				year(p.project_start) = $yearstart and p.project_status = 0
	group by 
				c.co_id  ";
   $rec = $this->db->query($sqlco);

  ?>
  <style>
<!--
 /* Style Definitions */
@page Section1{
            size: 29.7cm 21cm;
            margin: 2cm 2cm 2cm 2cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
        div.Section1 { page:Section1;}
-->
</style>
<div class="Section1">
<h3>ตารางแสดงความร่วมมือไตรภาคีปี   <?php echo $yearstart +543; ?></h3>
<table width="968" border="1" cellspacing="0" cellpadding="0" bgcolor="#747474">
  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"' width="150" align="center" bgcolor="#E3E3E3">ประเทศ/<br>
    แหล่งคู่ร่วมมือ</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' width="18" align="center" bgcolor="#E3E3E3">&nbsp;</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' width="300" align="center" bgcolor="#E3E3E3">ชื่อหลักสูตร</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' width="100" align="center" bgcolor="#E3E3E3">ประเภทกิจกรรม</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' width="300" align="center" bgcolor="#E3E3E3">ประเทศผู้รับความช่วยเหลือ</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' width="50" align="center" bgcolor="#E3E3E3">จำนวนทุน</td>
    <td style='font-size:10.0pt;font-family:"tahoma"' width="50" align="center" bgcolor="#E3E3E3">มูลค่าที่ไทย<br>
    รับผิดชอบ</td>
  </tr>
  <?php
  $i=0;
   foreach($rec->result_array() as $listrec){ 
  
   ?>

  <tr>
    <td style='font-size:10.0pt;font-family:"tahoma"' align="left" valign="top" bgcolor="#FFFFFF"><?php echo $listrec['country_name'];?></td>
    <td style='font-size:10.0pt;font-family:"tahoma"' colspan="6" bgcolor="#FFFFFF">
    
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <?php
						//loop course//
						$sqlc = " 
							select c.course_name,ac.activity_name,c.course_id from 
								recipient r left join aid a on r.aid_id = a.aid_id
								left join activity ac on a.activity_id = ac.activity_id
								left join project p on p.project_id = ac.project_id
								left join course c on r.course = c.course_id
								left join co_funding cf on p.project_id = cf.project_id 
							where
								cf.co_id = '".$listrec['co_id']."'  
							group by 
								r.course
							";
							$result = $this->db->query($sqlc);
					   		foreach($result->result_array() as $listco){ 
							if(empty($listco['course_name'])){ continue;}
							$i++;
						?>
                          <tr>
                            <td style='font-size:10.0pt;font-family:"tahoma"' align="left" bgcolor="#F5F5F5">&nbsp;<?php echo $i;?></td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' align="left" valign="top" bgcolor="#F5F5F5">&nbsp;<?php echo $listco['course_name'];?></td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' bgcolor="#F5F5F5"><?php echo $listco['activity_name'];?></td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' bgcolor="#F5F5F5"><?php
                            $q = "select r.course, r.country_id,c.country_name from recipient r left join country c on r.country_id = c.country_id where course = '". $listco['course_id']."' " ;
							$rowc = $this->db->query($q);
							$country_name = array();
							$j=0;
							foreach($rowc->result_array() as $listnamec){
								$qsum = "select rec_id from recipient  where course = '". $listnamec['course']."' and country_id = '". $listnamec['country_id']."'  " ;
								$QS = $this->db->query($qsum);
								$sumcountry = $QS->num_rows();  
								$country_name[] = $listnamec['country_name'].'('.$sumcountry.')' ;	
								
								$j++;
							}
							$result = array_unique($country_name);
							echo  implode(",", $result);
							?></td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#F5F5F5"><?php echo $j;?></td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' align="right" bgcolor="#F5F5F5">1</td>
                          </tr>
                          <?php  }?>
                          <tr>
                            <td style='font-size:10.0pt;font-family:"tahoma"' width="18" bgcolor="#E3E3E3">&nbsp;</td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' width="300" align="center" bgcolor="#E3E3E3">Sub Total</td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' width="100" bgcolor="#E3E3E3">&nbsp;</td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' width="300" bgcolor="#E3E3E3">&nbsp;</td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' width="50" bgcolor="#E3E3E3">&nbsp;</td>
                            <td style='font-size:10.0pt;font-family:"tahoma"' width="50" bgcolor="#E3E3E3">&nbsp;</td>
                          </tr>
                        </table>
    
    
    </td>
  </tr>
  <?php } ?>
</table>
</div>
