<script type="text/javascript">
$(document).ready(function(){
	var table = $('#table_id').DataTable();
	$('.toggle-vis').on( 'click', function (e) {
				//e.preventDefault(); 
				// Get the column API object
				var column = table.column( $(this).attr('data-column') ); 
				// Toggle the visibility
				column.visible( ! column.visible() );
    } );
	$('tbody tr:even').addClass("alt-row");
});
</script>
<div style="padding: 10px 0 30px 0;">
  <p>Colum Option: 
  </p>
  <p>
    <input type="checkbox" name="no" id="no" class="toggle-vis" data-column="0" checked="checked" />
    No.
    <input type="checkbox" name="name" id="name" class="toggle-vis" data-column="1" checked="checked" />Prefix
  <input type="checkbox" name="name" id="name" class="toggle-vis" data-column="2" checked="checked" />Name
  <input type="checkbox" name="givenname" id="givenname" class="toggle-vis" data-column="3" checked="checked" />Given Name
  <input type="checkbox" name="sex" id="sex" class="toggle-vis" data-column="4" checked="checked" />Sex
  <input type="checkbox" name="marital" id="marital" class="toggle-vis" data-column="5" checked="checked" />Marital
  <input type="checkbox" name="birthday" id="birthday" class="toggle-vis" data-column="6" checked="checked" />Birthday
  <input type="checkbox" name="age" id="age" class="toggle-vis" data-column="7" checked="checked" />Age
  <input type="checkbox" name="religion" id="religion" class="toggle-vis" data-column="8" checked="checked" />Religion
  <input type="checkbox" name="nationality" id="nationality" class="toggle-vis" data-column="9" checked="checked" />Nationality
  <input type="checkbox" name="cityofbirth" id="cityofbirth" class="toggle-vis" data-column="10" checked="checked" />City Of Birth
  <input type="checkbox" name="homeaddress" id="homeaddress" class="toggle-vis" data-column="11" checked="checked" />Home Address
    <input type="checkbox" name="hometelphone" id="hometelphone" class="toggle-vis" data-column="12" checked="checked" />
    Home Telphone</p>
  <p>
  <input type="checkbox" name="homefax" id="homefax" class="toggle-vis" data-column="13" checked="checked" />Home Fax
    <input type="checkbox" name="studentid" id="studentid" class="toggle-vis" data-column="14" checked="checked" />Student ID
    <input type="checkbox" name="course" id="course" class="toggle-vis" data-column="15" checked="checked" />Course
    <input type="checkbox" name="country" id="country" class="toggle-vis" data-column="16" checked="checked" />Country
    <input type="checkbox" name="aid" id="aid" class="toggle-vis" data-column="17" checked="checked" />Aid
    <input type="checkbox" name="status" id="status" class="toggle-vis" data-column="18" checked="checked" />Status  </p>
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
  <tr>
    <th  nowrap="nowrap">No.</th>
    <th nowrap="nowrap">Prefix</th>
    <th nowrap="nowrap">Name</th>
    <th nowrap="nowrap">Given Name</th>
    <th nowrap="nowrap">Sex</th>
    <th nowrap="nowrap"><strong>Marital</strong></th>
    <th nowrap="nowrap"><strong>Birthday</strong></th>
    <th nowrap="nowrap"><strong>Age</strong></th>
    <th nowrap="nowrap"><strong>Religion</strong></th>
    <th nowrap="nowrap"><strong>Nationality</strong></th>
    <th nowrap="nowrap"><strong>City Of Birth</strong></th>
    <th nowrap="nowrap"><strong>Home Address</strong></th>
    <th nowrap="nowrap"><strong>Home Telphone</strong></th>
    <th nowrap="nowrap"><strong>Home Fax</strong></th>
    <th nowrap="nowrap"><strong>Student ID</strong></th>
    <th nowrap="nowrap"><strong>Course</strong></th>
    <th nowrap="nowrap">Country</th>
    <th nowrap="nowrap">Aid</th>
    <th nowrap="nowrap">Status</th>

    </tr>
  </thead>

  <tbody>
    <?php
  $count =1;
  foreach($all as $list){
  ?>
    <tr>
      <td align="center" nowrap="nowrap"><?php echo $count++; ?></td>
      <td nowrap="nowrap"><?php echo $list['prefix_id']; ?></td>
      <td nowrap="nowrap"><?php echo $list['fullname']; ?></td>
      <td nowrap="nowrap"><?php echo $list['givenname']; ?></td>
      <td nowrap="nowrap"><?php echo $list['sex_id']; ?></td>
      <td nowrap="nowrap"><?php echo $list['marital']; ?></td>
      <td nowrap="nowrap"><?php echo $list['birthday']; ?></td>
      <td nowrap="nowrap"><?php echo $list['age']; ?></td>
      <td nowrap="nowrap"><?php echo $list['religion']; ?></td>
      <td nowrap="nowrap"><?php echo $list['nationality']; ?></td>
      <td nowrap="nowrap"><?php echo $list['city_of_birth']; ?></td>
      <td nowrap="nowrap"><?php echo $list['home_address']; ?></td>
      <td nowrap="nowrap"><?php echo $list['home_tel']; ?></td>
      <td nowrap="nowrap"><?php echo $list['home_fax']; ?></td>
      <td nowrap="nowrap"><?php echo $list['student_id']; ?></td>
      <td nowrap="nowrap"><?php echo $list['course']; ?></td>
      <td nowrap="nowrap"><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
      <td nowrap="nowrap"><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <td align="center" nowrap="nowrap">
        <?php if($list['status'] == 1){ ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/exclamation.png" title="Wait" width="16" height="16" />
        <?php }else if($list['status'] == 2){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/tick_circle.png" title="Approve" width="16" height="16" />
        <?php }else if($list['status'] == 3){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/cross.png" title="Reject" width="16" height="16" />
        <?php } ?>
      </td>
      
    </tr>
    <?php } ?>
  </tbody>
</table>