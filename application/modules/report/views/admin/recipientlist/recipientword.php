<?php
 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=recipientlist.doc");
?>
<style>
<!--
@page Section1{
            size: 29.7cm 21cm;
            margin: 2cm 2cm 2cm 2cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
        div.Section1 { page:Section1;}
-->
</style>
<div class=Section1>
<?php
            $fullname = $this->input->get('fullname');  
			$date_capital = $this->input->get('date_capital'); 
			$country = $this->input->get('country'); 
			echo $aid = $this->input->get('aid'); 
			$status = $this->input->get('status'); 
			$qfullname = "";
			$qdate="";
			$qcountry="";
			$qaid="";
                        $qstatus="";
                        
                        if(!empty($fullname)){
                                $qfullname = "and fullname like '%$fullname%'";
                        }
                            
                        if(!empty($date_capital)){
                                $qdate = "and date_capital = '$date_capital'" ;
                        }
			if(!empty($country)){
				$qcountry = "and country_id = '$country'" ;
			 }
			if(!empty($aid)){
				$qaid = "and aid_id = '$aid'" ;
			 }
			if(!empty($status)){
				$qstatus = "and status = '$status'" ;
			 }
                         $sql = "select *
                                from recipient
                                where 
                                rec_id <> ''
                                ".$qfullname."
                                ".$qdate."
                                ".$qcountry."
                                ".$qaid."
                                ".$qstatus."
			  ";
                        $Q=$this->db->query($sql);
			   //echo $sql;
			    $all = $Q->result_array();
?>
<table width="100%" border="1" cellspacing="0" cellpadding="0"  id="table_id">
  <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"' ead>
  <tr>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'   nowrap="nowrap">No.</th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap">Prefix</th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap">Name</th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap">Given Name</th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap">Sex</th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Marital</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Birthday</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Age</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Religion</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Nationality</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>City Of Birth</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Home Address</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Home Telphone</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Home Fax</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Student ID</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Course</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Country</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Aid</strong></th>
    <th style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><strong>Status</strong></th>

    </tr>
  </thead>

  <tbody>
    <?php
  $count =1;
  foreach($all as $list){
  ?>
    <tr>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  align="center" nowrap="nowrap"><?php echo $count++; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['prefix_id']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['fullname']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['givenname']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['sex_id']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['marital']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['birthday']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['age']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['religion']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['nationality']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['city_of_birth']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['home_address']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['home_tel']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['home_fax']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['student_id']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php echo $list['course']; ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  nowrap="nowrap"><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <td style=' font-size:10.0pt; font-weight:normal;font-family:"tahoma"'  align="center" nowrap="nowrap">
        <?php if($list['status'] == 1){ ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/exclamation.png" title="Wait" width="16" height="16" />
        <?php }else if($list['status'] == 2){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/tick_circle.png" title="Approve" width="16" height="16" />
        <?php }else if($list['status'] == 3){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/cross.png" title="Reject" width="16" height="16" />
        <?php } ?>
      </td>
      
    </tr>
    <?php } ?>
  </tbody>
</table>

  </form>

</div>
