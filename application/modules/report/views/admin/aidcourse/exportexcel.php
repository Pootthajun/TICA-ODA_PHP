<?php
ob_start();
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=aidcourse.xls"); 
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); 
ob_end_flush();
?>
<?php
$yearstart = $this->input->get('year_start');

if(empty($yearstart)){
	$yearstart = date('Y');  //ปีปัจจุบัน
}else{
	$yearstart = $this->input->get('year_start')-543;
}
?>
<center><h3>การให้ความช่วยเหลือต่างประเทศ</h3></center>
<table width="800" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#939393">

  <tr>
    <td colspan="4" bgcolor="#E4E4E4">ปี <?php echo $yearstart +543; ?></td>
  </tr>
  <tr>
    <td width="189" align="center" bgcolor="#FFFFFF"><strong>ประเทศ</strong></td>
    <td width="202" align="center" bgcolor="#FFFFFF"><strong>โครงการ</strong></td>
    <td width="185" align="center" bgcolor="#FFFFFF"><strong>สาขา</strong></td>
    <td width="128" align="center" bgcolor="#FFFFFF"><strong>มูลค่า(บาท)</strong></td>
  </tr>
  <?php  
 //step 1
 //ต้องค้นหา disbure + pay ใช้ปีทีจ่าย join กัน แล้ว where ทั้งคู่ โดยใช้ or จะได้ประเทศทั้งหมดของปีที่ค้นหามา
$queryc = "select * from (					
						select country_id from recipient r left join money_paid m on r.rec_id = m.rec_id
						where   year(m.mp_date) = ($yearstart )
						group by country_id 
		) country group by country.country_id	";
 $res = $this->db->query($queryc); 

        foreach($res->result_array() as $rowcountry){
	
 ?>
  <tr>
    <td bgcolor="#FFFFFF">
	<?php
				$this->db->where('country_id',$rowcountry['country_id']);
				$Q = $this->db->get('country');
				$cdata = $Q->row_array();
				echo $countryname = $cdata['country_name']; 
	?></td>
    <!--     //step 2
 //แล้วเอาประเทศมา loop แล้วคิวรี disbure+recipient join project ,activity,aid หาจำนวน where ด้วยประเทศ
 //คิวรี่ pay join recipient หาจำนวนเงิน  where ด้วยประเทศ-->
    <td align="left" bgcolor="#FFFFFF">
	<?php
	//count project			
			 $crecipient = "select *  from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where   year(m.mp_date) = ($yearstart ) and country_id = ".$rowcountry['country_id']." ";
						$QR = $this->db->query($crecipient);
						foreach($QR->result_array() as $rowprojectname) {
						echo  "-&nbsp;" .$rowprojectname['project_name']."<br/>";
						}
			
	
	?></td>
    <td align="left" bgcolor="#FFFFFF">
	<?php
	//count course
		
			
			 $crecipient = "select course  from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
						where   year(m.mp_date) = ($yearstart ) and country_id = ".$rowcountry['country_id']." 
						group by r.course
						";
						$QR = $this->db->query($crecipient);
						foreach($QR->result_array() as $rowcourse) {
						echo  "-&nbsp;" .$rowcourse['course']."<br/>";
						}
			
			
	
	?></td>
    <td align="right" bgcolor="#FFFFFF"><?php
	//count aid
		
			
			 $csumrecipient = "select * ,sum(mx.expense_budget) as sumr from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join money_pay_expense mx on mx.mp_id = m.mp_id
						where   year(m.mp_date) = ($yearstart ) and country_id = ".$rowcountry['country_id']." 
									";
						$QRSUM = $this->db->query($csumrecipient);
						$sumr = $QRSUM->row_array();
						echo number_format($sumr['sumr'],2,'.',',');
			
			
	
	?></td>
  </tr>
  <?php } 
  ?>
  <tr>
    <td bgcolor="#F9F9F9"><strong>รวม(บาท)</strong></td>
    <td align="center" bgcolor="#F9F9F9"><strong>
      <?php
	//count project
			
			 $crecipient = "select * ,count(p.project_id) as countrproject from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join aid a on r.aid_id = a.aid_id
											left join activity ac on ac.activity_id = a.activity_id
											left join project p on p.project_id = ac.project_id
						where   year(m.mp_date) = ($yearstart ) ";
						$QR = $this->db->query($crecipient);
						$countr = $QR->row_array();
						echo $tcountp = $countr['countrproject']; /// dis
			
			
	
	?>
    </strong></td>
    <td align="center" bgcolor="#F9F9F9">
	  <strong>
	  <?php
	//count activity	
			
			 $crecipient = "select count(course) as c from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
						where   year(m.mp_date) = ($yearstart)
						";
						$QR = $this->db->query($crecipient);
						$countr = $QR->row_array();
						echo $countr['c']; /// dis
			
			
	
	?>
    </strong></td>
    <td align="right" bgcolor="#F9F9F9"><strong>
      <?php
	//count aid
		
			
			 $csumrecipient = "select * ,sum(mx.expense_budget) as sumr from recipient r 
			                               left join money_paid m on r.rec_id = m.rec_id
										    left join money_pay_expense mx on mx.mp_id = m.mp_id
						where   year(m.mp_date) = ($yearstart)";
						$QRSUM = $this->db->query($csumrecipient);
						$sumr = $QRSUM->row_array();
						echo number_format($sumr['sumr'],2,'.',',');
			
			
	
	?>
    </strong></td>
  </tr>

</table>
