<?php
 
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=aid_finance.doc");
?>
<style>
<!--
 /* Style Definitions */
@page Section1{
            size: 29.7cm 21cm;
            margin: 2cm 2cm 2cm 2cm;
            mso-page-orientation: landscape;
            mso-footer:f1;
        }
        div.Section1 { page:Section1;}
-->
</style>
<div class="Section1">
<table width="1000" border="1" cellspacing="0" cellpadding="0" bgcolor="#BDBDBD">

  <tr>
    <th style='font-size:10.0pt;font-family:"tahoma"' width="10" bgcolor="#FCFCFC">No.</th>
    <th style='font-size:10.0pt;font-family:"tahoma"' width="150" bgcolor="#FCFCFC">Name</th>
    <th style='font-size:10.0pt;font-family:"tahoma"' width="150" bgcolor="#FCFCFC">Course</th>
    <th style='font-size:10.0pt;font-family:"tahoma"' width="150" bgcolor="#FCFCFC">Start-End</th>
    <th style='font-size:10.0pt;font-family:"tahoma"' width="64" bgcolor="#FCFCFC">Passport Expire</th>
    <th style='font-size:10.0pt;font-family:"tahoma"' width="70" bgcolor="#FCFCFC">Insurance Expire</th>
    <th style='font-size:10.0pt;font-family:"tahoma"' width="67" bgcolor="#FCFCFC">VISA Expire</th>
       <?php
  	$q = "
		select * from 
				money_paid 
				left join money_pay_expense on money_pay_expense.mp_id = money_paid.mp_id
				left join expense on expense.expense_id = money_pay_expense.expense_id 
		group by  
				expense.expense_id
	";
  	 $query = $this->db->query($q); 
	 $num = $query->num_rows();
  ?>  
      <?php 	 foreach($query->result_array() as $list){ ?>
    <th style='font-size:10.0pt;font-family:"tahoma"' width="50"  bgcolor="#FCFCFC">
	<?php 
 
	echo $list['expense_name']; 
 	$arrayexpenseid[] = $list['expense_id'];?>
    </th>
    <?php } ?>
  </tr>
  </thead>

  <tbody>
    <?php
  $count =1;
  foreach($all as $list){
  ?>
    <tr><td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FFFFFF"><?php echo $count++; ?></td><td style='font-size:10.0pt;font-family:"tahoma"' bgcolor="#FFFFFF"><?php echo $list['prefix_id']."&nbsp;".$list['fullname']; ?></td><td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FFFFFF"><?php echo $list['course']; ?></td><td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FFFFFF"><?php echo $list['project_start']; ?> - <?php echo $list['project_end']; ?></td><td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FFFFFF"><?php echo $list['expire_passport']; ?></td><td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FFFFFF"><?php echo $list['expire_insurance']; ?></td><td style='font-size:10.0pt;font-family:"tahoma"' align="center" bgcolor="#FFFFFF"><?php echo $list['expire_visa']; ?></td>
    
 <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?><td style='font-size:10.0pt;font-family:"tahoma"' bgcolor="#FFFFFF" align="right">
    
    <?php
	$query =$this->db->query("select sum(expense_budget) as total from money_pay_expense where  rec_id = '".$list['rec_id']."' and expense_id = '". $arrayexpenseid[$i]."' ");
  $rowpay = $query->row_array();
    echo $rowpay['total'];
	
	?>
    </td>
    <?php } //end for ?>
      
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>