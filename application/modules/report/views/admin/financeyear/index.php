<style>
.chosen-select{
width:100%;	
}
</style>
<script type="text/javascript">
$(document).ready(function()
{
	var year_start=$('#year_start').val();
	var startmonth=$('#startmonth').val();
	var plan=$('#plan').val();
	var project=$('#project').val();
	var activity=$('#activity').val();
	var aid=$('#aid').val();
	

$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>report/financeyear/searchresult/",
data: {"year_start": year_start,"startmonth":startmonth,"plan":plan,"project":project,"activity":activity,"aid":aid},
cache: false,
success: function(html)
{
$("#resultReport").html(html);
} 
});
$("#searchbtn").click(function()
{  
$("#resultReport").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
   var year_start=$('#year_start').val();
	var startmonth=$('#startmonth').val();	
	var plan=$('#plan').val();
	var project=$('#project').val();
	var activity=$('#activity').val();
	var aid=$('#aid').val();
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>report/financeyear/searchresult/",
data: {"year_start": year_start,"startmonth":startmonth,"plan":plan,"project":project,"activity":activity,"aid":aid},
cache: false,
success: function(html)
{
$("#resultReport").html(html);
} 
});
});

 $('#genpdf').click(function (event){
   var year_start=$('#year_start').val();
	var year_end=$('#year_end').val();
	var strWindowFeatures  = "?year_start="+year_start+"&year_end="+year_end+"";
    var url = $(this).attr("href")+strWindowFeatures;
    var windowName = "popUp";//$(this).attr("name");
 	window.open(url, windowName);
 	event.preventDefault(); 
     });	 
	$('#genword').click(function (event){
    var year_start=$('#year_start').val();
	var year_end=$('#year_end').val();
	var strWindowFeatures  = "?year_start="+year_start+"&year_end="+year_end+"";
    var url = $(this).attr("href")+strWindowFeatures;
    var windowName = "popUp";//$(this).attr("name");
 	window.open(url, windowName);
 	event.preventDefault(); 
     });	 
	$('#genexcel').click(function (event){
    var year_start=$('#year_start').val();
	var year_end=$('#year_end').val();
	var strWindowFeatures  = "?year_start="+year_start+"&year_end="+year_end+"";
    var url = $(this).attr("href")+strWindowFeatures;
    var windowName = "popUp";//$(this).attr("name");
 	window.open(url, windowName);
 	event.preventDefault(); 
     });
	 
$("#plan").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;
$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>report/financeyear/getprojectlistbyplanid/",
data: dataString,
cache: false,
success: function(html)
{
$("#getproject").fadeOut(100).html(html).fadeIn(500);
} 
});
});
///end plan change

});
</script>
<?php
$cmonth = date('m');
$cmonthend = date('m')+11;
$current = date('Y')+544;
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3">
    <select name="plan" id="plan" class=" chosen-select" data-placeholder="Choose Plan Name..." >
      <option value=""></option>
      <?php foreach($plan as $listplan){ ?>
      <option value="<?php echo $listplan['plan_id']; ?>"><?php echo $listplan['plan_name']; ?></option>
      <?php } ?>
    </select></td>
  </tr>
  <tr>
    <td colspan="3">
        <span id="getproject">
    <select name="project" id="project" ="" class="chosen-select" data-placeholder="Choose Project Name..."  >
      <option value=""></option>
    </select>
    </span>
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <span id="getactivity">
    <select name="activity" id="activity" ="" class="chosen-select" data-placeholder="Choose Activity Name..."  >
      <option value="" ></option>
    </select>
    </span>
    </td>
  </tr>
  <tr>
    <td colspan="3">
     <span id="getaid">
    <select name="aid_id" id="aid_id" class="chosen-select "  data-placeholder="Choose Aid Name..."  >
      <option></option>
    </select>
    </span>
    </td>
  </tr>
  <tr>
    <td width="45%"><select name="startmonth" class="chosen-select" id="startmonth"  data-placeholder="Choose Month.." >
      <option value=''></option>
      <option value='1' <?php if($cmonth == 1){ echo "selected='selected'";} ?>>Janaury</option>
      <option value='2' <?php if($cmonth == 2){ echo "selected='selected'";} ?>>February</option>
      <option value='3' <?php if($cmonth == 3){ echo "selected='selected'";} ?>>March</option>
      <option value='4' <?php if($cmonth == 4){ echo "selected='selected'";} ?>>April</option>
      <option value='5' <?php if($cmonth == 5){ echo "selected='selected'";} ?>>May</option>
      <option value='6' <?php if($cmonth == 6){ echo "selected='selected'";} ?>>June</option>
      <option value='7' <?php if($cmonth == 7){ echo "selected='selected'";} ?>>July</option>
      <option value='8' <?php if($cmonth == 8){ echo "selected='selected'";} ?>>August</option>
      <option value='9' <?php if($cmonth == 9){ echo "selected='selected'";} ?>>September</option>
      <option value='10' <?php if($cmonth == 10){ echo "selected='selected'";} ?>>October</option>
      <option value='11' <?php if($cmonth == 11){ echo "selected='selected'";} ?>>November</option>
      <option value='12' <?php if($cmonth == 12){ echo "selected='selected'";} ?>>December</option>
    </select></td>
    <td width="36%"><select name="year_start" id="year_start" class="chosen-select"  data-placeholder="Choose Year.." >
      <?php 
		  for($i = 2550;$i <= $current;$i++){ 
		  ?>
      <option value="<?php echo $i; ?>"  <?php if($i == (date('Y')+543)){ echo "selected='selected'";} ?>><?php echo $i; ?></option>
      <?php } ?>
    </select></td>
    <td width="19%"><span style="float:right; margin-bottom:5px;">
      <input type="button" name="button" id="searchbtn" value="Search" class="button green"/>
    </span></td>
  </tr>
</table>

<div id="resultReport"></div>