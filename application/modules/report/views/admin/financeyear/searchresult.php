   <?php
   $plan = $this->input->post('plan'); 
   $project = $this->input->post('project'); 
   $activity = $this->input->post('activity'); 
	$aid = $this->input->post('aid'); 
	$startmonth = $this->input->post('startmonth'); 
	$year_start = $this->input->post('year_start')-543; 
    $qstartmonth="";
	$qyearstart="";
	$qplan="";
	$qproject="";
	$qactivity="";
	$qaid ="";
    $planname="";
	$projectname="";
	$activityname="";
	$aidname="";
 
	  if((!empty($plan))){
	 
            $qplan = "and plan.plan_id  = $plan" ;
			$planid = $plan;
			$plan = $this->MPlan->getPlanbyid($planid);
			$planname = $plan['plan_name'];
        }
		 if((!empty($project))){
            $qproject = "and project.project_id  = $project" ;
			$projectid = $project;
			$pname = $this->MProject->getProjectByid($projectid);
			$projectname = $pname['project_name'];
        }
		if((!empty($activity))){
            $qactivity = "and activity.activity_id  = $activity" ;
			$activityid = $activity;
			$acname = $this->MActivity->getActivitybyid($activityid);
			$activityname = $acname['activity_name'];
        }
		if((!empty($aid))){
            $qaid = "and aid.aid_id  = $aid" ;
			$aid = $aid;
			$aname = $this->MAid->getAidbyid($aid);
			$aidname = $aname['aid_name'];
        }
		
                          
       if((!empty($startmonth)) AND (!empty($year_start))){
            $qstartmonth = "and ( (month(M.mp_date) = '$startmonth' ) and  (year(M.mp_date) =  '$year_start') ) " ;
        }
	 
  ?>
  <form action="<?php echo base_url(); ?>report/financeyear/genexport" method="post" name="form1" target="_blank" id="form1" >
  
<p><strong>รายงานผลการใช้จ่ายงบประมาณประจำปี <?php echo $year_start+543; ?>  ประจำเดือน  <?php echo $this->datethai->monththai('11'. sprintf("%02d",  $startmonth ) .'2015'); ?></strong>
  <input type="hidden" name="startmonth" id="startmonth"  value=" <?php echo $startmonth; ?>"/>
  <input type="hidden" name="year_start" id="year_start"  value="<?php echo $year_start; ?>"/>
  <input type="hidden" name="plan" id="plan" value="<?php $this->input->post('plan'); ?>" />
  <input type="hidden" name="project" id="project"  value="<?php $this->input->post('project'); ?>" />
  <input type="hidden" name="activiry" id="activiry"  value="<?php $this->input->post('activiry'); ?>" />
  <input type="hidden" name="aid" id="aid" value="<?php $this->input->post('aid'); ?>"  />
</p>
<p>&nbsp;</p>
<table width="100" border="0" cellspacing="0" cellpadding="0" style="float:right">
  <tr>
    <td>
      <select name="type" id="type" class="chosen-select"  style="width:80px;">
        <option value="pdf">PDF</option>
        <option value="word">Word</option>
        <option value="excel">Excel</option>
      </select></td>
    <td><input type="submit" name="Export" id="Export" value="Export" class="button blue"/></td>
  </tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>รายการค่าใช้จ่าย :</td>
    <td><input type="text" name="mp_title" id="mp_title"  class="text-input large-input" /></td>
  </tr>
  <tr>
    <td width="155">แผนงาน :</td>
    <td width="645"><input type="text" name="planname" id="plan"  class="text-input large-input" /></td>
  </tr>
  <tr>
    <td>ผลผลิต :</td>
    <td><input type="text" name="projectname" id="projectname"  class="text-input large-input" /></td>
  </tr>
  <tr>
    <td>กิจกรรม :</td>
    <td><input type="text" name="activityname" id="activityname"  class="text-input large-input" /></td>
  </tr>


  <tr>
    <td>ประเภทงบประมาณ :</td>
    <td><input type="text" name="bt_name" id="bt_name"  class="text-input large-input" /></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#202020">
  <tr>
    <td rowspan="2" align="center" nowrap bgcolor="#FCFCFC">ลำดับ</td>
    <td rowspan="2" align="center" nowrap bgcolor="#FCFCFC">รายการงบประมาณ<br>
    โครงการย่อย / กิจกรรมย่อย</td>
    <td colspan="4" align="center" nowrap bgcolor="#FCFCFC"><strong>เดือน <?php echo $this->datethai->monththai('11'. sprintf("%02d",  $startmonth ) .'2015'); ?> &nbsp;<?php echo $year_start+543; ?></strong></td>
    <td rowspan="2" align="center" nowrap bgcolor="#FCFCFC">หมายเหตุ</td>
  </tr>
  <tr>
    <td align="center" nowrap bgcolor="#FCFCFC">แผน/ครั้ง</td>
    <td align="center" nowrap bgcolor="#FCFCFC">งบประมาณที่ได้รับจัดสรร(บาท)</td>
    <td align="center" nowrap bgcolor="#FCFCFC">ผล<br>
    (ครั้ง)</td>
    <td align="center" nowrap bgcolor="#FCFCFC">งบประมาณที่ใช้(บาท)</td>
  </tr>
  
  <?php
  //aid(paid) loop//
  $count = 1;
    $sql = "select *
                        FROM money_paid AS M 
						LEFT JOIN aid ON aid.aid_id = M.aid_id  
						LEFT JOIN activity  ON activity.activity_id = aid.activity_id
						LEFT JOIN project  ON project.project_id = activity.project_id
						LEFT JOIN plan  ON plan.plan_id = project.plan_id  
                        where
						M.aid_id != ''
						$qstartmonth
						$qplan
						$qproject
						$qactivity
						$qaid
						group by M.aid_id
	  ";
	  $QM = $this->db->query($sql);
	  foreach($QM->result_array() as $list) {
  ?>
  <tr>
    <td bgcolor="#FFFFFF"><?php echo $count++; ?></td>
    <td bgcolor="#FFFFFF"><?php echo $list['mp_title']; ?></td>
    
    <td bgcolor="#FFFFFF">
 	<input name="budget_time[]" type="text" size="5" maxlength="20" />    
    </td>
    <td align="right" bgcolor="#FFFFFF">
        <?php
	//money_budget
	$QMB = "select sum(mb_amount) as total
                        FROM money_budget M
                        where
						M.aid_id = '".$list['aid_id']."'
						and ( (month(M.mb_date) = '$startmonth' ) and  (year(M.mb_date) =  '$year_start') )
						group by M.aid_id
		  ";
	  $RMB = $this->db->query($QMB);
	  $row = $RMB->row_array();
	  @$gsum += $row['total'];
	 echo number_format($row['total'],2,'.', ',');
	?>    
    </td>
    
    <td bgcolor="#FFFFFF"><input name="paid_time[]" type="text" size="5" maxlength="20" />    </td>
    <td align="right" bgcolor="#FFFFFF">
	<?php
	//money_paid
	$QMP = "select sum(mp_amount) as total
                        FROM money_paid M
                        where
						M.aid_id = '".$list['aid_id']."'
						and ( (month(M.mp_date) = '$startmonth' ) and  (year(M.mp_date) =  '$year_start') )
						group by M.aid_id
		  ";
	  $RMP = $this->db->query($QMP);
	  $row = $RMP->row_array();
	  @$gsum1 +=$row['total'];
	 echo number_format($row['total'],2,'.', ',');
	?> </td>
    <td bgcolor="#FFFFFF"><textarea name="comment[]" cols="15" rows="2"></textarea></td>
  </tr>
  
  <?php } ?>
  <tr>
    <td bgcolor="#F2F2F2">&nbsp;</td>
    <td bgcolor="#F2F2F2">รวมทั้งสิ้น(บาท)</td>
    <td bgcolor="#F2F2F2">&nbsp;</td>
    <td align="right" bgcolor="#F2F2F2"><?php echo  number_format(@$gsum,2,'.', ','); ?></td>
    <td bgcolor="#F2F2F2">&nbsp;</td>
    <td align="right" bgcolor="#F2F2F2"><?php echo  number_format(@$gsum1,2,'.', ','); ?></td>
    <td bgcolor="#F2F2F2">&nbsp;</td>
  </tr>
</table>
</form>

