<style>
#form1 select{width:100%}
</style>
  <script type="text/javascript">
$(document).ready(function()
{
/////get Project
$("#searchbtn").click(function()
{
	$("#resultReport").html('<center><br><br><img src="<?php echo base_url(); ?>assets/welcome/images/ajax-loader.gif" /></center>');
	var fullname=$('#fullname').val();
	var date_capital=$('.date_capital').val();
	var country=$('#country').val();
	var aid=$('#aid_id').val();
	var status=$('#status').val();

$.ajax
({
type: "POST",
url: "<?php echo base_url(); ?>report/recipientreport/getrecipientsearch/",
data: {"status": status, "fullname": fullname,"date_capital":date_capital,"country":country,"aid":aid},
cache: false,
success: function(html)
{
$("#resultReport").html(html);
} 
});
});
});

</script>

<style type="text/css">
	label 
	{
		width:auto;		
	}
</style>

<p>

<form id="form1" name="form1">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="300"><input type="text" name="fullname" id="fullname" class="text-input large-input" placeholder="Search Recipient Name"></td>
    <td width="300"><input type="text" name="date_capital" id="datepicker11" placeholder="Search Date Capital" class="text-input large-input date_capital" ></td>
    <td width="300"><select name="country" id="country" required class="chosen-select" data-placeholder="Choose Country Name..."  >
      <option value="" ></option>
      <?php foreach($country as $listcountry){?>
      <option value="<?php echo $listcountry['country_id']; ?>" ><?php echo $listcountry['country_name']; ?></option>
      <?php } ?>
    </select></td>
  </tr>
  <tr>
    <td colspan="2"><select name="aid_id" id="aid_id" class="chosen-select "  data-placeholder="Choose Aid Name..."  >
      <option></option>
      <?php foreach($aid as $listaid){?>
      <option value="<?php echo $listaid['aid_id']; ?>" ><?php echo $listaid['aid_name']; ?></option>
      <?php } ?>
    </select></td>
    <td><select name="status" id="status" class="chosen-select"  data-placeholder="Choose Status..."  >
      <option ></option>
      <option value="1">Wait</option>
      <option value="2">Approve</option>
      <option value="3">Reject</option>
    </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><span style="float:right; margin-bottom:10px;">
      <input type="button" name="button" id="searchbtn" value="Search" class="button green"/>
    </span></td>
  </tr>
</table>
 
</form>
  </p>
<div id="resultReport">
<table width="100%" border="0" cellspacing="0" cellpadding="0"  id="table_id">
  <thead>
  <tr>
    <th width="100">No.</th>
    <th width="100">Name</th>
    <th width="100">Country</th>
    <th width="286">Aid</th>
    <th width="100">Status</th>
    <?php  if( !empty( $menu ) ) { ?>
        <?php } ?>
    </tr>
  </thead>

  <tbody>
    <?php
  $count =1;
  foreach($all as $list){
  ?>
    <tr>
      <td align="center"><?php echo $count++; ?></td>
      <td><a href="<?php echo site_url(); ?>report/recipientreport/recipientdetail/<?php echo $list['rec_id']; ?>"><?php echo $list['prefix_id']."&nbsp;".$list['fullname']; ?></a></td>
      <td><?php 
	    $this->db->where('country_id',$list['country_id']);
	  $QC = $this->db->get('country');
	  $namec = $QC->row_array();
	  echo $namec['country_name']; 
	  $QC->free_result();
	  ?></td>
      <td><?php 
	    $this->db->where('aid_id',$list['aid_id']);
	  $QAID = $this->db->get('aid');
	  $nameaid = $QAID->row_array();
	  echo $nameaid['aid_name']; 
	  $QAID->free_result();
	  ?></td>
      <td align="center">
        <?php if($list['status'] == 1){ ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/exclamation.png" title="Wait" width="16" height="16" />
        <?php }else if($list['status'] == 2){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/tick_circle.png" title="Approve" width="16" height="16" />
        <?php }else if($list['status'] == 3){  ?>
        <img src="<?php echo base_url(); ?>assets/admin/images/icons/cross.png" title="Reject" width="16" height="16" />
        <?php } ?>
      </td>
     <!-- <td width="130" align="right">-->
 
<?php
 
               	$q = $this->db->query("SELECT activity.project_id FROM activity left join aid on aid.activity_id = activity.activity_id where aid.aid_id = '".$list['aid_id']."' ");
		$rowq = $q->row_array(); 
		$project_id = $rowq['project_id'];
                
	      	$project_assistant = '';
                $project_owner = '';
                $this->db->where('project_id',$project_id);
                $Q = $this->db->get('project_assistant'); 
                foreach($Q->result_array() as $listp){
			$project_assistant .= $listp['user_id'].",";  
		}
                
                $this->db->where('project_id',$project_id);
                $Q = $this->db->get('project'); 
                foreach($Q->result_array() as $listp){
			$project_owner = $listp['project_owner'];  
		}
		
		$auser = $project_assistant.$project_owner;
 
		  $ex = explode(",",$auser);
		 $Q->free_result();
		  ?>
        <!--      </td>-->
      
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
