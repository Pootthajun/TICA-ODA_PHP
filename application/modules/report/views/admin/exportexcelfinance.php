<?php

ob_start();
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=financepay_detal.xls"); 
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); 
ob_end_flush();
?>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="120"><strong>Plan Name</strong></td>
      <td><?php echo $detail['plan_name']; ?>
</td>
    </tr>
    <tr>
      <td><strong>Project Name</strong></td>
      <td><?php echo $detail['project_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Activity Name</strong></td>
      <td><?php echo $detail['activity_name']; ?></td>
    </tr>
    <tr>
      <td><strong>AID Name</strong></td>
      <td><?php echo $detail['aid_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Aid Pay</strong></td>
      <td><?php if($detail['aid_pay']=0){echo "Only One ";}else{echo " All AID";} ?></td>
    </tr>
    <tr>
      <td><strong>Recipient Name</strong></td>
      <td><?php echo $detail['prefix_id']."&nbsp;".$detail['fullname']; ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Sex</strong>&nbsp;&nbsp; <?php echo $detail['sex_id']; ?> <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Age</strong>&nbsp;&nbsp;&nbsp; <?php echo $detail['age']; ?> <strong>Year Old</strong></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong>Email</strong> &nbsp;&nbsp;<?php echo $detail['email']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Tel</strong> <?php echo $detail['telephone']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Country&nbsp;&nbsp;</strong><?php echo $detail['country_name']; ?></td>
    </tr>
    <tr>
      <td><strong>Date</strong></td>
      <td width="580"><?php echo $edit['mp_date'];?><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Year&nbsp;&nbsp;&nbsp;<?php echo $edit['mp_year'];?></strong></td>
    </tr>
    <tr>
      <td><strong>Amount</strong>&nbsp;</td>
      <td><?php echo number_format($edit['mp_amount'],2,'.',',');  ?>  THB.</td>
    </tr>
    <tr>
      <td><strong>Title</strong></td>
      <td><?php echo $edit['mp_title'];?></td>
    </tr>
    <tr>
      <td><strong>Comment</strong></td>
      <td><?php echo $edit['mp_comment']; ?></td>
    </tr>
</table>

<p>
<table width="100%" border="1" cellspacing="0" cellpadding="0" bgcolor="#A9A9A9" >
  <?php
  	$q = "
		select * from 
				money_paid 
				left join money_pay_expense on money_pay_expense.mp_id = money_paid.mp_id
				left join expense on expense.expense_id = money_pay_expense.expense_id 
		where
				money_paid.rec_id = '".$edit['rec_id']."'
				and money_paid.aid_id = '".$edit['aid_id']."' 
		group by  
				expense.expense_id
	";
  	 $query = $this->db->query($q); 
	 $num = $query->num_rows();
	 if($num > 0){
  ?>  
<thead>	  

  <tr>
    <th width="8%" bgcolor="#FCFCFC" nowrap="nowrap">Date</th>
    <th width="8%" bgcolor="#FCFCFC"  nowrap="nowrap">Title</th>
    <th width="8%" bgcolor="#FCFCFC"  nowrap="nowrap">Comment</th>
    <?php 	 foreach($query->result_array() as $list){ ?>
    <th  bgcolor="#FCFCFC"  nowrap="nowrap">
	<?php 
 
	echo $list['expense_name']; 
 	$arrayexpenseid[] = $list['expense_id'];?>
    (THB.)</th>
   
    <?php } ?>
     <th width="100"  nowrap="nowrap"  bgcolor="#F7F7F7"><strong>Total(THB.)</strong></th>
  </tr>
  </thead>
  <?php 
 
		 $this->db->where('rec_id',$edit['rec_id']);
		 $this->db->where('aid_id',$edit['aid_id']);
		 $Qdate = $this->db->get('money_paid');  
		foreach($Qdate->result_array() as $listdate){
		?>
  <tr>
  <td height="40" bgcolor="#FFFFFF"><?php echo $listdate['mp_date']; ?></td>
  <td bgcolor="#FFFFFF"><?php echo $listdate['mp_title']; ?></td>
  <td bgcolor="#FFFFFF"><?php echo $listdate['mp_comment']; ?></td>
  <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td bgcolor="#FFFFFF" align="right">
    
    <?php

	$query =$this->db->query("select * from money_pay_expense where mp_id = ".$listdate['mp_id']." and rec_id = '".$listdate['rec_id']."' and expense_id = '". $arrayexpenseid[$i]."' ");
  $rowpay = $query->row_array();
    echo number_format($rowpay['expense_budget'],2,'.',','); 
	
	?>
    </td>

<?php } //end for ?>
    <td bgcolor="#F7F7F7" align="right">
	  <strong>
	  <?php

	$query =$this->db->query("select sum(expense_budget) as tsum from money_pay_expense where  mp_id = ".$listdate['mp_id']." and rec_id = '".$listdate['rec_id']."'  ");
  $rowpay = $query->row_array();
    echo number_format($rowpay['tsum'],2,'.',','); 
	
	?>
	  </strong></td>

  <?php } ?>
    </tr>
    
  <tr>
    <td height="40" colspan="3" bgcolor="#EAEAEA"><strong>Total</strong></td>
    <?php

	 $col = count($arrayexpenseid);
   for($i =0;$i<$col;$i++){ 
   
   ?>
    <td bgcolor="#EAEAEA" align="right">
      <strong>
      <?php

	$query =$this->db->query("select sum(expense_budget) as tsum from money_pay_expense where  rec_id = '".$listdate['rec_id']."' and expense_id = '". $arrayexpenseid[$i]."' ");
  $rowpay = $query->row_array();
    echo number_format($rowpay['tsum'],2,'.',','); 
	
	?>
      </strong></td>
   
     <?php } ?>
     <td bgcolor="#EAEAEA" align="right"><strong>
       <?php

	$query =$this->db->query("select sum(expense_budget) as tsum from money_pay_expense where  rec_id = '".$listdate['rec_id']."' ");
  $rowpay = $query->row_array();
    echo number_format($rowpay['tsum'],2,'.',','); 
	
	?>
     </strong></td>
  </tr>

 <?php } ?>
</table>


