   <?php
	$country= $this->input->post('country'); 
	$year = $this->input->post('year'); 
	if(empty($year)){
	$year = date('Y')+543;  //ปีปัจจุบัน
	}else{
	$year = $this->input->post('year');
}
    $qcountry="";
    $qyear="";	

	if(!empty($year)){
            $qyear = "and M.mp_year = '$year'" ;
			$yearcat = "ปีงบประมาณ&nbsp;".$year;
	}
	if(!empty($country)){
         $qcountry = "and R.country_id = '$country'" ;
	}
      
   	$q = "
		select * from 
				aid_type 
				order by aid_type_name  ASC
					";
  	 $query = $this->db->query($q); 
	 $num = $query->num_rows();
	 
	 $sql = "select M.*,MP.expense_budget,sum(MP.expense_budget) as sum,R.country_id,A.aid_type_id,A.aid_name
                        FROM money_paid AS M
                        INNER JOIN recipient AS R ON R.rec_id = M.rec_id
                        LEFT JOIN aid AS A ON A.aid_id = M.aid_id       
						LEFT JOIN money_pay_expense AS MP ON MP.mp_id = M.mp_id           
                        ".$qcountry."
                        ".$qyear."
                        ORDER BY M.mp_id DESC			
	  ";
	  $QM = $this->db->query($sql);
	  $m = $QM->row_array();
	  $Countryid = $country;
	  $countryname = $this->MCountry->getCountrytypebyid($Countryid);
  ?> 
<center> <h3>การให้ความช่วยเหลือแก่ต่างประเทศ <?php echo $countryname['country_name']; ?></h3></center>

  <p><strong>ปี <?php echo $year; ?></strong></p>

<table width="100%" border="0" cellspacing="1" cellpadding="1" id="data"  bgcolor="#282828">
  <thead>

  <tr>
    <th width="400" bgcolor="#C8C8C8" nowrap="nowrap">โครงการ</th>
	  <?php 	 foreach($query->result_array() as $list){ ?>
    <th bgcolor="#C8C8C8"   nowrap="nowrap">
	       <?php echo $list['aid_type_name']; 
			$arrayaidtype[] = $list['aid_type_id'];
			?>
    (บาท)</th>
    <?php } ?>

    <th width="50" align="right"  nowrap="nowrap" bgcolor="#C8C8C8"><strong>มูลค่า(บาท)</strong></th>
  </tr>
  </thead>
  <tbody>
  <?php
$sqlproject = "select ac.activity_id,a.aid_id,p.project_name,p.project_id,R.country_id,M.mp_year from recipient as R  			
 							left join aid as a on a.aid_id  = R.aid_id
							left join activity as ac on ac.activity_id  = a.activity_id
							left join project as p on p.project_id  = ac.project_id
							left join money_paid as M on M.rec_id  = R.rec_id
							where R.status = 2
							 ".$qcountry."
                              ".$qyear."
							group by p.project_id
							order by p.project_name ASC
							
  ";
  $QC = $this->db->query($sqlproject);
  foreach($QC->result_array() as $listproject){
  ?>
    <tr>
      <td width="400" bgcolor="#FFFFFF"><?php echo $listproject['project_name']; ?></td>
       <?php
	 $col = count($arrayaidtype);
      for($i =0;$i<$col;$i++){    
   ?>
      <td width="500" bgcolor="#FFFFFF"><?php
	$sql = "select M.*,MP.expense_budget,sum(MP.expense_budget) as sum,p.project_id,R.country_id,A.aid_type_id
                        FROM money_paid AS M
                        INNER JOIN recipient AS R ON R.rec_id = M.rec_id
                        LEFT JOIN aid AS A ON A.aid_id = M.aid_id    
						left join activity as ac on ac.activity_id  = A.activity_id
						left join project as p on p.project_id  = ac.project_id   
						LEFT JOIN money_pay_expense AS MP ON MP.mp_id = M.mp_id           
                        where p.project_id = ".$listproject['project_id']."	
						and A.aid_type_id = ".$arrayaidtype[$i]."		
                        ".$qcountry."
                        ".$qyear."
                        ORDER BY M.mp_id DESC			
	  ";
	  $QM = $this->db->query($sql);
	  $m = $QM->row_array();
	  if($m['sum'] > 0){
	   echo @number_format($m['sum'],2,'.',',');	  
	  }
	  ?></td>
      <?php } ?>
      <td width="50" align="right" bgcolor="#FFFFFF"><?php
	$sql = "select M.*,MP.expense_budget,sum(MP.expense_budget) as sum,p.project_id,R.country_id,A.aid_type_id
                        FROM money_paid AS M
                        INNER JOIN recipient AS R ON R.rec_id = M.rec_id
                        LEFT JOIN aid AS A ON A.aid_id = M.aid_id    
						left join activity as ac on ac.activity_id  = A.activity_id
						left join project as p on p.project_id  = ac.project_id   
						LEFT JOIN money_pay_expense AS MP ON MP.mp_id = M.mp_id           
                        where p.project_id = ".$listproject['project_id']."	
                        ".$qcountry."
                        ".$qyear."
                        ORDER BY M.mp_id DESC			
	  ";
	  $QM = $this->db->query($sql);
	  $m = $QM->row_array();
	   if($m['sum'] > 0){
	    echo @number_format($m['sum'],2,'.',','); 
	   }
		?></td>
    </tr>

    <?php } ?>
        <tr>
      <td width="400" bgcolor="#FBFBFB"><strong>รวม(บาท)</strong></td>
            <?php
	 $col = count($arrayaidtype);
      for($i =0;$i<$col;$i++){    
   ?>
      <td width="500" bgcolor="#FBFBFB"><strong>
        <?php
	$sql = "select M.*,MP.expense_budget,sum(MP.expense_budget) as sum,p.project_id,R.country_id,A.aid_type_id
                        FROM money_paid AS M
                        INNER JOIN recipient AS R ON R.rec_id = M.rec_id
                        LEFT JOIN aid AS A ON A.aid_id = M.aid_id    
						left join activity as ac on ac.activity_id  = A.activity_id
						left join project as p on p.project_id  = ac.project_id   
						LEFT JOIN money_pay_expense AS MP ON MP.mp_id = M.mp_id           
                        where
						A.aid_type_id = ".$arrayaidtype[$i]."		
                        ".$qcountry."
                        ".$qyear."
                        ORDER BY M.mp_id DESC			
	  ";
	  $QM = $this->db->query($sql);
	  $m = $QM->row_array();
	  @$gtotal += $m['sum'];
	   if($m['sum'] > 0){
	   echo @number_format($m['sum'],2,'.',',');
	   }
	  
	  ?>
      </strong></td>
      <?php } ?>
      <td width="50" align="right" bgcolor="#FBFBFB"><strong>
        <?php  echo  @number_format(@$gtotal,2,'.',','); ?>
      </strong></td>
    </tr>
  </tbody>
</table>
