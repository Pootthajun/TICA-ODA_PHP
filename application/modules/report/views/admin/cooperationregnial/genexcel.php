<?php
ob_start();
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=cooperationregnial.xls"); 
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); 
ob_end_flush();
?>


<?php
$yearstart = $this->input->get('year_start');
$region =  $this->input->get('region');
$qregion = "";
$title="";
if(!empty($region)){
	$qregion = " and region_id = $region ";
	$title = "ภูมิภาค  $region";
}
if(empty($yearstart)){
	$yearstart = date('Y');  //ปีปัจจุบัน
}else{
	$yearstart = $this->input->get('year_start')-543;
}
?>
<center><h3>การให้ความช่วยเหลือต่างประเทศ <?php echo $title; ?>  ระหว่างปี  <?php echo ($yearstart+543); ?> </h3></center>
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#939393">

  <tr>
    <td width="150" rowspan="2" align="center" bgcolor="#FCFCFC"><strong>ภูมิภาค /ประเทศ</strong></td>
    <td width="180" align="center" bgcolor="#FCFCFC"><strong>โครงการที่อยุ่ระหว่างดำเนินการ</strong></td>
    <td width="180" align="center" bgcolor="#FCFCFC"><strong>ทุนศึกษา(TIPP)</strong></td>
    <td width="180" align="center" bgcolor="#FCFCFC"><strong>ทุนฝึกอบรม(AITC)</strong></td>
    <td colspan="2" align="center" bgcolor="#FCFCFC"><strong>รวม</strong></td>
  </tr>
  <tr>
    <td align="center" bgcolor="#FCFCFC"><strong>จำนวน</strong></td>
    <td align="center" bgcolor="#FCFCFC"><strong>จำนวน</strong></td>
    <td align="center" bgcolor="#FCFCFC"><strong>จำนวน</strong></td>
    <td width="80" align="center" bgcolor="#FCFCFC"><strong>จำนวน</strong>*</td>
    <td width="150" align="center" bgcolor="#FCFCFC"><strong>มูลค่า(บาท)</strong></td>
  </tr>
 <?php  
 //step 1
 //ต้องค้นหา disbure + pay ใช้ปีทีจ่าย join กัน แล้ว where ทั้งคู่ โดยใช้ or จะได้ประเทศทั้งหมดของปีที่ค้นหามา
$queryc = "select * from region where region_id  <> ''  ".$qregion." ";
 $res = $this->db->query($queryc); 

        foreach($res->result_array() as $rowcountry){
	
 ?>
  <tr>
    <td bgcolor="#FFFFFF">
    <?php
				echo $rowcountry['region_name'];
	?>
    </td>
    <td align="center" bgcolor="#FFFFFF"><?php
			//country in region
			 $cinregion  = "
			 				 select country.country_id from (
								select country_id  from disbureseme d left join project p on p.project_id = d.project_id
								where project_status = 0
						UNION
								select country_id from recipient r left join aid a on r.aid_id = a.aid_id
								left join activity ac on ac.activity_id = a.activity_id
								left join project p on p.project_id = ac.project_id
								where  project_status = 0
					) countryinregion
					left join country on countryinregion.country_id = country.country_id
					where 	country.region_id =  ".$rowcountry['region_id']."
					group by countryinregion.country_id 
			  "; 
			 $rescinregion = $this->db->query($cinregion);  
			 $countcountry = 0;
       		 foreach($rescinregion->result_array() as $rowcinregion){
				 	 $QL = $this->db->query("
					 select count(projectcount.project_id) as summary from (
									select p.project_id  from disbureseme d left join project p on p.project_id = d.project_id
									where p.project_status = 0 and d.country_id = '".$rowcinregion['country_id'] ."'
									group by p.project_id
							UNION
									select p.project_id from recipient r left join aid a on r.aid_id = a.aid_id
									left join activity ac on ac.activity_id = a.activity_id
									left join project p on p.project_id = ac.project_id
									where  project_status = 0
									and r.country_id =  '".$rowcinregion['country_id'] ."'
									group by p.project_id
						) projectcount  group by projectcount.project_id
				 	 ");
					 $cdata = $QL->row_array();
 					 $countcountry = $countcountry+$cdata['summary']; 
			 }
			@$c1 += (int)$countcountry;
			echo   (int)$countcountry;
			 
	
	?></td>
    <td align="center" bgcolor="#FFFFFF"><?php
	//TIPP = 3 
			 $countcountry = 0;
       		 foreach($rescinregion->result_array() as $rowcinregion){
				 	 $QL = $this->db->query("
					 select count(projectcount.project_id) as summary from (
									select p.project_id  from disbureseme d left join project p on p.project_id = d.project_id
									where 
									p.project_status = 0 
									and p.cooperation_type_id = 3
									and d.country_id = '".$rowcinregion['country_id'] ."'					
									group by p.project_id
							UNION
									select p.project_id from recipient r left join aid a on r.aid_id = a.aid_id
									left join activity ac on ac.activity_id = a.activity_id
									left join project p on p.project_id = ac.project_id
									where  
									project_status = 0 
									and p.cooperation_type_id = 3
									and r.country_id =  '".$rowcinregion['country_id'] ."'
									group by p.project_id
						) projectcount  group by projectcount.project_id
				 	 ");
					 $cdata = $QL->row_array();
 					 $countcountry = $countcountry+$cdata['summary']; 
			 }
			 $total =  $countcountry;
			 @$c2 += (int)$countcountry;
			echo   (int)$countcountry;
			
	
	?></td>
    <td align="center" bgcolor="#FFFFFF"><?php
// ATIC = 2  
			 $countcountry = 0;
       		 foreach($rescinregion->result_array() as $rowcinregion){
				 	 $QL = $this->db->query("
					 select count(projectcount.project_id) as summary from (
									select p.project_id  from disbureseme d left join project p on p.project_id = d.project_id
									where 
									p.project_status = 0 
									and p.cooperation_type_id = 2
									and d.country_id = '".$rowcinregion['country_id'] ."'					
									group by p.project_id
							UNION
									select p.project_id from recipient r left join aid a on r.aid_id = a.aid_id
									left join activity ac on ac.activity_id = a.activity_id
									left join project p on p.project_id = ac.project_id
									where  
									project_status = 0 
									and p.cooperation_type_id = 2
									and r.country_id =  '".$rowcinregion['country_id'] ."'
									group by p.project_id
						) projectcount  group by projectcount.project_id
				 	 ");
					 $cdata = $QL->row_array();
 					 $countcountry = $countcountry+$cdata['summary']; 
			 }
			 $total = $total+$countcountry;
			@ $c3 +=(int)$countcountry;
			echo   (int)$countcountry;
			 
			
			
	
	?></td>
    <td align="center" bgcolor="#FFFFFF"><?php
	//count aid
	@$c4 += $total;
		 echo $total;
			
	
	?></td>
    <td align="right" bgcolor="#FFFFFF"><?php
	//count aid
			$countcountry = 0;
       		 foreach($rescinregion->result_array() as $rowcinregion){
				 	 $QL = $this->db->query("
					 SELECT sum(budget_money) as sum FROM `budget_project`  where project_id in (
					 select projectcount.project_id from (
									select p.project_id  from disbureseme d left join project p on p.project_id = d.project_id
									where 
									p.project_status = 0 
									and p.cooperation_type_id in(3,2)
									and d.country_id = '".$rowcinregion['country_id'] ."'					
									group by p.project_id
							UNION
									select p.project_id from recipient r left join aid a on r.aid_id = a.aid_id
									left join activity ac on ac.activity_id = a.activity_id
									left join project p on p.project_id = ac.project_id
									where  
									project_status = 0 
									and p.cooperation_type_id in(3,2)
									and r.country_id =  '".$rowcinregion['country_id'] ."'
									group by p.project_id
						) projectcount  group by projectcount.project_id
						)
						
				 	 ");
					 $cdata = $QL->row_array();
 					 $countcountry =  $cdata['sum']; 
			 } 
			 @$grandtotal += (int)$countcountry;
			echo  number_format((int)$countcountry,2,'.',',');
	
	?></td>
<!--     //step 2
 //แล้วเอาประเทศมา loop แล้วคิวรี disbure+recipient join project ,activity,aid หาจำนวน where ด้วยประเทศ
 //คิวรี่ pay join recipient หาจำนวนเงิน  where ด้วยประเทศ-->  </tr>
  <?php } ?>
  <tr>
    <td bgcolor="#DEDEDE">รวมทั้งสิ้น(บาท)</td>
    <td align="center" bgcolor="#DEDEDE"><?php
	//count project
 	
			echo @$c1; 
	
	?></td>
    <td align="center" bgcolor="#DEDEDE"><?php
	//count activity
			 
			echo @$c2;
	
	?></td>
    <td align="center" bgcolor="#DEDEDE"><?php
	//count aid
	echo @$c3;	 
	
	?></td>
    <td align="center" bgcolor="#DEDEDE"><?php
	//count aid
		echo  @$c4;
	
	?></td>
    <td align="right" bgcolor="#DEDEDE"><?php
	//count aid
		 
			echo @number_format($grandtotal,2,'.',',');
	
	?></td>
  </tr>
</table>

