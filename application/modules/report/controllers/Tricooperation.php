<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tricooperation extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));    
    }
    
    
    function index(){
        $data['header'] = "Tri-Coopertion Table";
        $data['country'] = $this->MCountry->getAllCountrypay();
    	$data['page'] = $this->config->item('wconfig_template_admin').'tricooperation/index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function searchresult(){
        $data['header'] = "Tri-Coopertion";
    	$data['page'] = $this->config->item('wconfig_template_admin').'tricooperation/searchresult';
    	$this->load->view($this->_container_report,$data);
    }
    
    function exportpdftricooperation(){
        $data['header'] = "Cooperation of country";
        $data['country'] = $this->MCountry->getAllCountryIndex();
    	$data['page'] = $this->config->item('wconfig_template_admin').'tricooperation/genpdf';
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_report, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we ca
    }
    
    function exportwordtricooperation(){
        $data['header'] = "Cooperation of country";
        $data['country'] = $this->MCountry->getAllCountryIndex();
    	$data['page'] = $this->config->item('wconfig_template_admin').'tricooperation/genword';
        $this->load->view($this->_container_report, $data);
    }
    
    function exportexceltricooperation(){
        $data['header'] = "Cooperation of country";
        $data['country'] = $this->MCountry->getAllCountryIndex();
    	$data['page'] = $this->config->item('wconfig_template_admin').'tricooperation/genexcel';
        $this->load->view($this->_container_report, $data);
    }
    

}
