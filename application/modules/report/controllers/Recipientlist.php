<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recipientlist extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Recipient List Table";
        $data['all'] = $this->MRecipient->listAllrecipient();     
        $data['page'] = $this->config->item('wconfig_template_admin').'recipientlist/index';    
    	$this->load->view($this->_container_admin,$data);
    
    }
    
    function getrecipientsearch(){
                        $fullname = $this->input->post('fullname');  
			$date_capital = $this->input->post('date_capital'); 
			$country = $this->input->post('country'); 
			$aid = $this->input->post('aid'); 
			$status = $this->input->post('status'); 
			$qfullname = "";
			$qdate="";
			$qcountry="";
			$qaid="";
                        $qstatus="";
                        
                        if(!empty($fullname)){
                                $qfullname = "and fullname like '%$fullname%'";
                        }
                            
                        if(!empty($date_capital)){
                                $qdate = "and date_capital = '$date_capital'" ;
                        }
			if(!empty($country)){
				$qcountry = "and country_id = '$country'" ;
			 }
			if(!empty($aid)){
				$qaid = "and aid_id = '$aid'" ;
			 }
			if(!empty($status)){
				$qstatus = "and status = '$status'" ;
			 }
                         $sql = "select *
                                from recipient
                                where 
                                rec_id <> ''
                                ".$qfullname."
                                ".$qdate."
                                ".$qcountry."
                                ".$qaid."
                                ".$qstatus."
			  ";
                        $Q=$this->db->query($sql);
			   //echo $sql;
			    $data['all'] = $Q->result_array();
			    $data['menu'] = $this->MUser->getMenubyUser();    
			    $data['page'] = $this->config->item('wconfig_template_admin').'recipientlist/resultseachrecipient';      
			    $this->load->view($this->_view_subsector,$data);
    }
    

    private function calAgeDate($birthdate){ 
			if(!empty($birthdate)){
				$year_curent = date('Y',time());
				$month_curent = date('m',time()); 
				
				$date_from = explode('-',$birthdate);
				$year_from = $date_from[0];
				$month_from = $date_from[1];
	
				// Calculate total month of Age
				$total_mon = (12 - $month_from);
				$total_mon += (($year_curent - $year_from - 1)*12);
				$total_mon += $month_curent+1;

				// Get Year/Month of Age
				$year = floor($total_mon/12);
				$month = ($total_mon%12);
 				$agecal = '';
				if ($month==0) {
					 $agecal = $year . ' Year' ;
				} else {
					 $agecal = $year . ' Year '  .  $month .' Month';
				}

			} else {
				$agecal = '';
			}
			return $agecal;
		}
                
                function exportpdfrecipientlist(){
                    $data['header'] = "Recipient List Table";  
                    $data['page'] = $this->config->item('wconfig_template_admin').'recipientlist/recipientpdf';        
                    ini_set('memory_limit','512M');
                    $html = $this->load->view($this->_container_report, $data,TRUE);
                    $this->load->library('pdf');
                    $pdf = $this->pdf->load();
                    $pdf = new mPDF('UTF-8');   
                    $pdf = new mPDF('th_saraban');
                    $pdf = new mPDF('th');
                    $pdf->AddPage('L','', '', '', '','','','','','','');
                    $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
                    $pdf->WriteHTML($html,2); // write the HTML into the PDF
                    $pdf->Output($pdfFilePath, 'I'); // save to file because we can
                }
                
                function exportwordrecipientlist(){
                    $data['header'] = "Recipient List Table";  
                    $data['page'] = $this->config->item('wconfig_template_admin').'recipientlist/recipientword';        
                    $this->load->view($this->_container_report, $data);
                }
                
                function exportexcelrecipientlist(){
                    $data['header'] = "Recipient List Table";  
                    $data['page'] = $this->config->item('wconfig_template_admin').'recipientlist/recipientexcel';        
                    $this->load->view($this->_container_report, $data);
                }
       
}