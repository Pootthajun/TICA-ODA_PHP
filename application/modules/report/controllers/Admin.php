<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    function index(){
        $data['header'] = "Report Table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function reportproject(){
        $data['header'] = "Report Table";
    	$data['page'] = $this->config->item('wconfig_template_admin').'reportproject';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function projectword(){
        header("Content-Type: application/vnd.ms-word");
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header("Content-disposition: attachment; filename=\"mydocument_name.doc\"");
        $data['page'] = $this->config->item('wconfig_template_admin').'projectword';
        $output = $this->load->view($this->_container_report,$data);
    }
    
    function projectpdf(){
        
    }
    
    function projectexcel(){
        header("Content-Type: application/vnd.ms-word");
        header("Expires: 0");
        header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        header("Content-disposition: attachment; filename=\"mydocument_name.doc\"");
        $data['page'] = $this->config->item('wconfig_template_admin').'projectword';
        $output = $this->load->view($this->_container_report,$data);

    }
	
	function exportword(){
	}
}