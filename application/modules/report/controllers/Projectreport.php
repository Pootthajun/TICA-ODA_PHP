<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projectreport extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Report Project Table";
        $data['project'] = $this->MProject->listallproject();
        $data['user'] = $this->MUser->listalluser();
    	$data['page'] = $this->config->item('wconfig_template_admin').'project/project';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function getprojectsearch(){
        $searchproject=$this->input->post('searchproject');
	$project_start = $this->input->post('project_start');
        $project_end = $this->input->post('project_end');
        $project_owner= $this->input->post('project_owner');
        $project_status= $this->input->post('project_status');
        $searprojectname ="";
        $searchstart="";
        $searchend="";
        $searchown="";
        $searchstatus="";
        if(!empty($searchproject)){
            $searprojectname = " and project.project_id like '%$searchproject%'";
        }
        if(!empty($project_start) and !empty($project_end)){
            $searchstart = " and (project.project_start between '$project_start' and '$project_end') ";
        }
        if(!empty($project_owner)){
            $searchown = " and (project.project_owner ='$project_owner'  ) ";
        }
        if(!empty($project_status)){
            $searchstatus = " and project.project_status ='$project_status'";
        }
        $sql="  select 
                    project.*,p.plan_name,pt.project_type_name 
                from 
                    project
                    inner join plan as p on project.plan_id = p.plan_id 
                    inner join project_type as pt on project.project_type_id = pt.project_type_id
                where 
                    project.project_id <> ''
                    ".$searprojectname."
                    ".$searchstart."
                    ".$searchend."
                    ".$searchown."
                    ".$searchstatus."    
               "; 
        
        $Q=$this->db->query($sql);
        $data['project'] = $Q->result_array();	         
	$Q->free_result();
	$data['page'] = $this->config->item('wconfig_template_admin').'project/searchreportproject';      
	$this->load->view($this->_view_subsector,$data);
    }
    
    function projectdetail($projectid){
        $data['header'] = "Repirt Project";
        $type = $this->MProject->getProjectByid($projectid);
        $data['edit'] = $type;
        $sector = $type['sector_id'];
        $cooperationtype = $type['cooperation_type_id'];
        $cooperation = $type['cooperation_id'];
        $oecd = $type['oecd_id'];
        $funding = $type['funding_id'];
        $executing = $type['executing_id'];
        $implement = $type['implement_id'];
        $multilateral = $type['multilateral_id'];
        $planid = $type['plan_id'];
        $subsector = $type['subsector_id'];
        $userid = $type['project_owner'];
	$data['activity']= $this->MActivity->getactivitybyprojectid($projectid);
        $data['plan'] = $this->MPlan->getPlanbyid($planid);
        $data['sector'] = $this->MSector->getsectorbyid($sector);
        $data['subsector'] = $this->MSubsector->getsubsectorbyid($subsector);
        $data['cooptype'] = $this->MCooperationtype->getCooperationtypebyid($cooperationtype);
        $data['coop'] = $this->MCooperation->getCooperationbyid($cooperation);
        $data['oecd'] = $this->MOecd->getoecdbyid($oecd);
        $data['funding']= $this->MFunding->getfundingbyid($funding);	
	$data['executing'] =  $this->MExecuting->getexecutingbyid($executing);
	$data['im'] =  $this->MImplement->getimplementbyid($implement);	
        $data['file'] = $this->MProject->listallfilebyprojectid($projectid);
       // $data['ac'] = $this->MActivity->listallactivity();
        $data['mu'] = $this->MMultilateral->getmultilateralbyCountry($multilateral);
        $data['country'] = $this->MCountry->listallcountry();
        $data['officer'] = $this->MUser->getUserById($userid);
        $data['co'] = $this->MProject->getCofundingByprojectid($projectid);
	$data['dis'] = $this->MProject->getDisburesemeByprojectid($projectid);
	$data['bt'] = $this->MProject->getbudgetByprojectid($projectid);
        $data['bgtype'] = $this->MBudgettype->listallBudgettype();        
        $planid = $type['plan_id'];
        $projecttypeid = $type['project_type_id'];
        $data['planname'] = $this->MPlan->getPlanbyid($planid);
        $data['ptname'] = $this->MProjecttype->getProjecttypetypebyid($projecttypeid);

        $data['page'] = $this->config->item('wconfig_template_admin').'project/projectdetail';    
        $this->load->view($this->_container_admin,$data); 
    }
    
    function exportpdfprojectreport($projectid){        
        $data['header'] = "Repirt Project";
        $type = $this->MProject->getProjectByid($projectid);
        $data['edit'] = $type;
        $sector = $type['sector_id'];
        $cooperationtype = $type['cooperation_type_id'];
        $cooperation = $type['cooperation_id'];
        $oecd = $type['oecd_id'];
        $funding = $type['funding_id'];
        $executing = $type['executing_id'];
        $implement = $type['implement_id'];
        $multilateral = $type['multilateral_id'];
        $planid = $type['plan_id'];
        $subsector = $type['subsector_id'];
        $userid = $type['project_owner'];
	$data['activity']= $this->MActivity->getactivitybyprojectid($projectid);
        $data['plan'] = $this->MPlan->getPlanbyid($planid);
        $data['sector'] = $this->MSector->getsectorbyid($sector);
        $data['subsector'] = $this->MSubsector->getsubsectorbyid($subsector);
        $data['cooptype'] = $this->MCooperationtype->getCooperationtypebyid($cooperationtype);
        $data['coop'] = $this->MCooperation->getCooperationbyid($cooperation);
        $data['oecd'] = $this->MOecd->getoecdbyid($oecd);
        $data['funding']= $this->MFunding->getfundingbyid($funding);	
	$data['executing'] =  $this->MExecuting->getexecutingbyid($executing);
	$data['im'] =  $this->MImplement->getimplementbyid($implement);		
       // $data['ac'] = $this->MActivity->listallactivity();
        $data['mu'] = $this->MMultilateral->getmultilateralbyCountry($multilateral);
        $data['country'] = $this->MCountry->listallcountry();
        $data['officer'] = $this->MUser->getUserById($userid);
        $data['co'] = $this->MProject->getCofundingByprojectid($projectid);
	$data['dis'] = $this->MProject->getDisburesemeByprojectid($projectid);
	$data['bt'] = $this->MProject->getbudgetByprojectid($projectid);
        $data['bgtype'] = $this->MBudgettype->listallBudgettype();        
        $planid = $type['plan_id'];
        $projecttypeid = $type['project_type_id'];
        $data['planname'] = $this->MPlan->getPlanbyid($planid);
        $data['ptname'] = $this->MProjecttype->getProjecttypetypebyid($projecttypeid);

        
        $data['page'] = $this->config->item('wconfig_template_admin') . "project/exportpdfproject";    
       
        
        
            ini_set('memory_limit','512M');
            $html = $this->load->view($this->_container_report, $data,TRUE);
            $this->load->library('pdf');
            $pdf = $this->pdf->load();
            $pdf = new mPDF('UTF-8');   
            $pdf = new mPDF('th_saraban');
            //$pdf->SetAutoFont();
            $pdf = new mPDF('', 'A4', 0, '', 12, 12, 10, 10, 5, 5);
            $pdf = new mPDF('th');
           // $pdf->SetAutoFont(AUTOFONT_THAIVIET);
            //$stylesheet = file_get_contents("application/third_party/mpdf/css/pdf.css");
            //$stylesheet = file_get_contents(base_url()."assets/css/pdf.css");
            //$pdf->WriteHTML($stylesheet,1);
            $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
            $pdf->WriteHTML($html,2); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function exportwordprojectreport($projectid){        
        $data['header'] = "Repirt Project";
        $type = $this->MProject->getProjectByid($projectid);
        $data['edit'] = $type;
        $sector = $type['sector_id'];
        $cooperationtype = $type['cooperation_type_id'];
        $cooperation = $type['cooperation_id'];
        $oecd = $type['oecd_id'];
        $funding = $type['funding_id'];
        $executing = $type['executing_id'];
        $implement = $type['implement_id'];
        $multilateral = $type['multilateral_id'];
        $planid = $type['plan_id'];
        $subsector = $type['subsector_id'];
        $userid = $type['project_owner'];
	$data['activity']= $this->MActivity->getactivitybyprojectid($projectid);
        $data['plan'] = $this->MPlan->getPlanbyid($planid);
        $data['sector'] = $this->MSector->getsectorbyid($sector);
        $data['subsector'] = $this->MSubsector->getsubsectorbyid($subsector);
        $data['cooptype'] = $this->MCooperationtype->getCooperationtypebyid($cooperationtype);
        $data['coop'] = $this->MCooperation->getCooperationbyid($cooperation);
        $data['oecd'] = $this->MOecd->getoecdbyid($oecd);
        $data['funding']= $this->MFunding->getfundingbyid($funding);	
	$data['executing'] =  $this->MExecuting->getexecutingbyid($executing);
	$data['im'] =  $this->MImplement->getimplementbyid($implement);		
       // $data['ac'] = $this->MActivity->listallactivity();
        $data['mu'] = $this->MMultilateral->getmultilateralbyCountry($multilateral);
        $data['country'] = $this->MCountry->listallcountry();
        $data['officer'] = $this->MUser->getUserById($userid);
        $data['co'] = $this->MProject->getCofundingByprojectid($projectid);
	$data['dis'] = $this->MProject->getDisburesemeByprojectid($projectid);
	$data['bt'] = $this->MProject->getbudgetByprojectid($projectid);
        $data['bgtype'] = $this->MBudgettype->listallBudgettype();        
        $planid = $type['plan_id'];
        $projecttypeid = $type['project_type_id'];
        $data['planname'] = $this->MPlan->getPlanbyid($planid);
        $data['ptname'] = $this->MProjecttype->getProjecttypetypebyid($projecttypeid);

        
        $data['page'] = $this->config->item('wconfig_template_admin') . "project/exportwordproject";
        $this->load->view($this->_container_report,$data); 
    }
    
    function exportexcelprojectreport($projectid){
        $data['header'] = "Repirt Project";
        $type = $this->MProject->getProjectByid($projectid);
        $data['edit'] = $type;
        $sector = $type['sector_id'];
        $cooperationtype = $type['cooperation_type_id'];
        $cooperation = $type['cooperation_id'];
        $oecd = $type['oecd_id'];
        $funding = $type['funding_id'];
        $executing = $type['executing_id'];
        $implement = $type['implement_id'];
        $multilateral = $type['multilateral_id'];
        $planid = $type['plan_id'];
        $subsector = $type['subsector_id'];
        $userid = $type['project_owner'];
	$data['activity']= $this->MActivity->getactivitybyprojectid($projectid);
        $data['plan'] = $this->MPlan->getPlanbyid($planid);
        $data['sector'] = $this->MSector->getsectorbyid($sector);
        $data['subsector'] = $this->MSubsector->getsubsectorbyid($subsector);
        $data['cooptype'] = $this->MCooperationtype->getCooperationtypebyid($cooperationtype);
        $data['coop'] = $this->MCooperation->getCooperationbyid($cooperation);
        $data['oecd'] = $this->MOecd->getoecdbyid($oecd);
        $data['funding']= $this->MFunding->getfundingbyid($funding);	
	$data['executing'] =  $this->MExecuting->getexecutingbyid($executing);
	$data['im'] =  $this->MImplement->getimplementbyid($implement);		
       // $data['ac'] = $this->MActivity->listallactivity();
        $data['mu'] = $this->MMultilateral->getmultilateralbyCountry($multilateral);
        $data['country'] = $this->MCountry->listallcountry();
        $data['officer'] = $this->MUser->getUserById($userid);
        $data['co'] = $this->MProject->getCofundingByprojectid($projectid);
	$data['dis'] = $this->MProject->getDisburesemeByprojectid($projectid);
	$data['bt'] = $this->MProject->getbudgetByprojectid($projectid);
        $data['bgtype'] = $this->MBudgettype->listallBudgettype();        
        $planid = $type['plan_id'];
        $projecttypeid = $type['project_type_id'];
        $data['planname'] = $this->MPlan->getPlanbyid($planid);
        $data['ptname'] = $this->MProjecttype->getProjecttypetypebyid($projecttypeid);

        
        $data['page'] = $this->config->item('wconfig_template_admin') . "project/exportexcelproject";
        $this->load->view($this->_container_report,$data); 
    }
    
}