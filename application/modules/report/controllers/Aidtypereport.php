<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aidtypereport extends Admin_Controller{
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));	
    }
    
        function index(){
	$data['header'] = "Aid Type Table";
        $data['project'] = $this->MProject->listAllproject();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidtype/aidtype';
    	$this->load->view($this->_container_admin,$data); 
	}
        
        function searchresult(){
        $data['header'] = "Aid Type";
        $data['project'] = $this->MProject->listAllproject();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidtype/searchresult';
    	$this->load->view($this->_container_report,$data);
        }
        
        function exportpdfaidtypereport(){
        $data['header'] = "Aid Type";
        $data['project'] = $this->MProject->listAllproject();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidtype/genpdf';
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_report, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }
        
        function exportwordaidtypereport(){
        $data['header'] = "Aid Type";
        $data['project'] = $this->MProject->listAllproject();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidtype/genword';
        $this->load->view($this->_container_report, $data);    
        }
        
        function exportexcelaidtypereport(){
        $data['header'] = "Aid Type";
        $data['project'] = $this->MProject->listAllproject();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidtype/genexcel';
        $this->load->view($this->_container_report, $data);  
        }
}
