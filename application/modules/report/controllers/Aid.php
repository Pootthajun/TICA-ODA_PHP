<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Aid extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));	
    }
    
        function index(){
	$data['header'] = "Finance Aid";
        $data['all'] = $this->MReport->listAllrecipientreport();
        $data['plan'] = $this->MPlan->listAllplan();
    	$data['page'] = $this->config->item('wconfig_template_admin').'financeaid';
    	$this->load->view($this->_container_admin,$data); 
	}
        
        function getprojectlistbyplanid(){
            $id=$this->input->post('id');
            $Q=$this->db->query("select * from project where  plan_id='$id'"); 
            $data['option'] = $Q->result_array();	          
            $data['page'] = $this->config->item('wconfig_template_admin').'projectlist';      
            $this->load->view($this->_view_subsector,$data);
        }
        function getActivitylistbyprojectid(){
            $id=$this->input->post('id');
            $Q=$this->db->query("select * from activity where  project_id='$id'"); 
            $data['option'] = $Q->result_array();	          
            $data['page'] = $this->config->item('wconfig_template_admin').'activityselectlist';      
            $this->load->view($this->_view_subsector,$data); 
	}
        function getAidlistbyActiivityid(){
            $id=$this->input->post('id');
            $Q=$this->db->query("select * from aid where  activity_id='$id'"); 
            $data['option'] = $Q->result_array();	          
            $data['page'] = $this->config->item('wconfig_template_admin').'aidselectlist';      
            $this->load->view($this->_view_subsector,$data); 
	}
        
       
       function getrecipientsearch(){
            $data['header'] = "Result Search";
            $data['plan'] = $this->MPlan->listAllplan();
            $plan=$this->input->get('plan'); 
            $project=$this->input->get('project');  
            $activity=$this->input->get('activity');  
            $aid=$this->input->get('aid_id');  
            $project_start=$this->input->get('project_start');  
            $project_end=$this->input->get('project_end'); 
            
            $qplan = "";
            $qproject = "";
            $qactivity = "";
            $qaid = "";
            $qstart = "";
            
            if(!empty($plan)){
            $qplan = " and project.plan_id = '$plan'";
            }
            if(!empty($project)){
            $qproject = " and project.project_id = '$project'";
            }
            if(!empty($activity)){
            $qactivity = " and activity.activity_id = '$activity'";
            }
            if(!empty($aid)){
            $qaid = " and a.aid_id = '$aid'";
            }
            if(!empty($project_start) and !empty($project_end)){
            $qstart = " and (project.project_start between '$project_start' and '$project_end') ";
            }
            
            $sql = "select r.*,mp.*,a.aid_name,a.activity_id,c.country_name,
            p.plan_id,project.project_start,project.project_end,activity.project_id
            from recipient as r 
            inner join money_paid as mp on r.rec_id = mp.rec_id
            inner join aid as a on r.aid_id = a.aid_id
            inner join country as c on r.country_id = c.country_id
            inner join activity as activity on activity.activity_id=a.activity_id
            inner join project as project on project.project_id=activity.project_id
            inner join plan as p on p.plan_id=project.plan_id
            where r.status = '2'
            ".$qplan."
            ".$qproject."
            ".$qactivity."
            ".$qaid."
            ".$qstart."   
            ";
            
            $Q=$this->db->query($sql);
            //$Qrow = $Q->row_array();
            $data['all'] = $Q->result_array();
            //$data['header'] = $Qrow['aid_name'];
            $data['menu'] = $this->MUser->getMenubyUser();    
            $data['page'] = $this->config->item('wconfig_template_admin').'resultseach';      
            $this->load->view($this->_container_admin,$data); 
       }
               
       function exportpdfaid(){
           $plan=$this->input->get('plan'); 
            $project=$this->input->get('project');  
            $activity=$this->input->get('activity');  
            $aid=$this->input->get('aid');  
            $project_start=$this->input->get('project_start');  
            $project_end=$this->input->get('project_end'); 
            
            $qplan = "";
            $qproject = "";
            $qactivity = "";
            $qaid = "";
            $qstart = "";
            
            if(!empty($plan)){
            $qplan = " and project.plan_id = '$plan'";
            }
            if(!empty($project)){
            $qproject = " and project.project_id = '$project'";
            }
            if(!empty($activity)){
            $qactivity = " and activity.activity_id = '$activity'";
            }
            if(!empty($aid)){
            $qaid = " and a.aid_id = '$aid'";
            }
            if(!empty($project_start) and !empty($project_end)){
            $qstart = " and (project.project_start between '$project_start' and '$project_end') ";
            }
            
            $sql = "select r.*,mp.*,a.aid_name,a.activity_id,c.country_name,
            p.plan_id,project.project_start,project.project_end,activity.project_id
            from recipient as r 
            inner join money_paid as mp on r.rec_id = mp.rec_id
            inner join aid as a on r.aid_id = a.aid_id
            inner join country as c on r.country_id = c.country_id
            inner join activity as activity on activity.activity_id=a.activity_id
            inner join project as project on project.project_id=activity.project_id
            inner join plan as p on p.plan_id=project.plan_id
            where r.status = '2'
            ".$qplan."
            ".$qproject."
            ".$qactivity."
            ".$qaid."
            ".$qstart."   
            ";
            $Q=$this->db->query($sql);
            $data['all'] = $Q->result_array();
            
        $data['header'] = "Finance Aid";
    	$data['page'] = $this->config->item('wconfig_template_admin').'exportpdfaid';
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_report, $data,TRUE);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can 
       }
       
       function exportwordaid(){
           $plan=$this->input->get('plan'); 
            $project=$this->input->get('project');  
            $activity=$this->input->get('activity');  
            $aid=$this->input->get('aid');  
            $project_start=$this->input->get('project_start');  
            $project_end=$this->input->get('project_end'); 
            
            $qplan = "";
            $qproject = "";
            $qactivity = "";
            $qaid = "";
            $qstart = "";
            
            if(!empty($plan)){
            $qplan = " and project.plan_id = '$plan'";
            }
            if(!empty($project)){
            $qproject = " and project.project_id = '$project'";
            }
            if(!empty($activity)){
            $qactivity = " and activity.activity_id = '$activity'";
            }
            if(!empty($aid)){
            $qaid = " and a.aid_id = '$aid'";
            }
            if(!empty($project_start) and !empty($project_end)){
            $qstart = " and (project.project_start between '$project_start' and '$project_end') ";
            }
            
            $sql = "select r.*,mp.*,a.aid_name,a.activity_id,c.country_name,
            p.plan_id,project.project_start,project.project_end,activity.project_id
            from recipient as r 
            inner join money_paid as mp on r.rec_id = mp.rec_id
            inner join aid as a on r.aid_id = a.aid_id
            inner join country as c on r.country_id = c.country_id
            inner join activity as activity on activity.activity_id=a.activity_id
            inner join project as project on project.project_id=activity.project_id
            inner join plan as p on p.plan_id=project.plan_id
            where r.status = '2'
            ".$qplan."
            ".$qproject."
            ".$qactivity."
            ".$qaid."
            ".$qstart."   
            ";
            $Q=$this->db->query($sql);
            $data['all'] = $Q->result_array();
            
        $data['header'] = "Finance Aid";
    	$data['page'] = $this->config->item('wconfig_template_admin').'exportwordaid';
        $this->load->view($this->_container_report, $data);
       }
       
       function exportexcelaid(){
           $plan=$this->input->get('plan'); 
            $project=$this->input->get('project');  
            $activity=$this->input->get('activity');  
            $aid=$this->input->get('aid');  
            $project_start=$this->input->get('project_start');  
            $project_end=$this->input->get('project_end'); 
            
            $qplan = "";
            $qproject = "";
            $qactivity = "";
            $qaid = "";
            $qstart = "";
            
            if(!empty($plan)){
            $qplan = " and project.plan_id = '$plan'";
            }
            if(!empty($project)){
            $qproject = " and project.project_id = '$project'";
            }
            if(!empty($activity)){
            $qactivity = " and activity.activity_id = '$activity'";
            }
            if(!empty($aid)){
            $qaid = " and a.aid_id = '$aid'";
            }
            if(!empty($project_start) and !empty($project_end)){
            $qstart = " and (project.project_start between '$project_start' and '$project_end') ";
            }
            
            $sql = "select r.*,mp.*,a.aid_name,a.activity_id,c.country_name,
            p.plan_id,project.project_start,project.project_end,activity.project_id
            from recipient as r 
            inner join money_paid as mp on r.rec_id = mp.rec_id
            inner join aid as a on r.aid_id = a.aid_id
            inner join country as c on r.country_id = c.country_id
            inner join activity as activity on activity.activity_id=a.activity_id
            inner join project as project on project.project_id=activity.project_id
            inner join plan as p on p.plan_id=project.plan_id
            where r.status = '2'
            ".$qplan."
            ".$qproject."
            ".$qactivity."
            ".$qaid."
            ".$qstart."   
            ";
            $Q=$this->db->query($sql);
            $data['all'] = $Q->result_array();
            
        $data['header'] = "Finance Aid";
    	$data['page'] = $this->config->item('wconfig_template_admin').'exportexcelaid';
        $this->load->view($this->_container_report, $data);
       }
    
}