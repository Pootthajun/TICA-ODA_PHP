<?php

class Aidcourse extends Admin_Controller {
    
    function index(){
        $data['header'] = "Aid Course Table";
        $data['project'] = $this->MProject->listAllproject();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidcourse/index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function searchresult(){
        $data['header'] = "Aid Course";
        $data['country'] = $this->MCountry->getAllCountryIndex();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidcourse/searchresult';
    	$this->load->view($this->_container_report,$data);
    }
    
    function exportpdfaidcourse(){
        $data['header'] = "Aid Course";
        $data['country'] = $this->MCountry->getAllCountryIndex();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidcourse/genpdf';
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_report, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('P','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we ca
    }
    
    function exportwordaidcourse(){
        $data['header'] = "Cooperation of country";
        $data['country'] = $this->MCountry->getAllCountryIndex();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidcourse/genword';
        $this->load->view($this->_container_report, $data);       

    }
    
    function exportexcelaidcourse(){
        $data['header'] = "Cooperation of country";
        $data['country'] = $this->MCountry->getAllCountryIndex();
    	$data['page'] = $this->config->item('wconfig_template_admin').'aidcourse/exportexcel';
        $this->load->view($this->_container_report, $data);       

    }
    
}
