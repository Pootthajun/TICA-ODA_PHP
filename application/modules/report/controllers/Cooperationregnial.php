<?php

class Cooperationregnial extends Admin_Controller {
    
    function index(){
        $data['header'] = "Cooperation Of Regional Table";
        $data['region'] = $this->MRegion->listallregion();
    	$data['page'] = $this->config->item('wconfig_template_admin').'cooperationregnial/index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function searchresult(){
        $data['header'] = "Cooperation Of Regional";
    	$data['page'] = $this->config->item('wconfig_template_admin').'cooperationregnial/searchresult';
    	$this->load->view($this->_container_report,$data);
    }
    
    function exportpdfcooperationregnial(){
        $data['header'] = "Cooperation Of Regional";
    	$data['page'] = $this->config->item('wconfig_template_admin').'cooperationregnial/genpdf';
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_report, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('P','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }
    
    function exportwordcooperationregnial(){
        $data['header'] = "Cooperation Of Regional";
    	$data['page'] = $this->config->item('wconfig_template_admin').'cooperationregnial/genword';
        $this->load->view($this->_container_report, $data);       
    }
    
    function exportexcelcooperationregnial(){
        $data['header'] = "Cooperation Of Regional";
    	$data['page'] = $this->config->item('wconfig_template_admin').'cooperationregnial/genexcel';
        $this->load->view($this->_container_report, $data); 
    }
    
    
}
