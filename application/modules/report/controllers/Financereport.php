<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financereport extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $status = 1;
        $data['header'] = "Finance Table";
        $data['country'] = $this->MCountry->listallcountry();
        $data['all'] = $this->MFinance->GetMoneypay();
    	$data['page'] = $this->config->item('wconfig_template_admin').'financepay';
    	$this->load->view($this->_container_admin,$data);     
    }
    
    function paydetail($recid){
        $data['header'] = "Pay Finance";
	$data['ex'] = $this->MExpense->listallexpense();
        $data['detail'] = $this->MRecipient->getRecipientJoin($recid);
        $data['edit'] = $this->MFinance->getPaybyrecid($recid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'paydetail';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function searchresult(){
        $fullname = $this->input->post('fullname');  
	$pay_date_from = $this->input->post('pay_date_from'); 
        $pay_date_to = $this->input->post('pay_date_to'); 
	$country= $this->input->post('country'); 
	$mpyear = $this->input->post('mpyear'); 
	$qfullname = "";
        $qdate="";
	$qcountry="";
        $qyear="";
                        
        if(!empty($fullname)){
            $qfullname = "and R.fullname like '%$fullname%'";
        }                            
        if((!empty($pay_date_from)) AND (!empty($pay_date_to))){
            $qdate = "and (M.mp_date BETWEEN  '$pay_date_from' AND '$pay_date_to') " ;
        }
        if((!empty($pay_date_from)) AND (empty($pay_date_to))){
            $qdate = "and M.mp_date =  '$pay_date_from'" ;
        }
        if((empty($pay_date_from)) AND (!empty($pay_date_to))){
            $qdate = "and M.mp_date =  '$pay_date_to'" ;
        }
	if(!empty($country)){
            $qcountry = "and R.country_id = '$country'" ;
	}
	if(!empty($mpyear)){
            $qyear = "and M.mp_year = '$mpyear'" ;
	}
            $sql = "select M.*,R.prefix_id,R.fullname,R.country_id,C.country_name,C.country_id,A.aid_name
                        FROM money_paid AS M
                        INNER JOIN recipient AS R ON R.rec_id = M.rec_id
                        LEFT JOIN country AS C ON R.country_id = C.country_id
                        LEFT JOIN aid AS A ON A.aid_id = M.aid_id                
                        where M.rec_id <> '' 
                        ".$qfullname."
                        ".$qdate."
                        ".$qcountry."
                        ".$qyear."
                        ORDER BY M.mp_id DESC
                    ";
            
            //echo $sql;
            $Q=$this->db->query($sql);
            $data['all'] = $Q->result_array();
            $data['menu'] = $this->MUser->getMenubyUser();    
            $data['page'] = $this->config->item('wconfig_template_admin').'resultseachpay';      
            $this->load->view($this->_view_subsector,$data);
    }
    
    function exportpdffinancereport($recid){
        $data['header'] = "Pay Finance";
	$data['ex'] = $this->MExpense->listallexpense();
        $data['detail'] = $this->MRecipient->getRecipientJoin($recid);
        $data['edit'] = $this->MFinance->getPaybyrecid($recid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'exportpdffinance';
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_report, $data,TRUE);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        //$pdf->SetAutoFont();
        $pdf = new mPDF('', 'A4', 1, '', 12, 12, 10, 10, 5, 5);
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        //$stylesheet = 
        // $pdf->SetAutoFont(AUTOFONT_THAIVIET);
        //$stylesheet = file_get_contents("application/third_party/mpdf/css/pdf.css");
        //$stylesheet = file_get_contents(base_url()."assets/css/pdf.css");
        //$pdf->WriteHTML($stylesheet,1);
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I'); // save to file because we can        
    }
    
    function exportwordfinancereport($recid){
        $data['header'] = "Pay Finance";
	$data['ex'] = $this->MExpense->listallexpense();
        $data['detail'] = $this->MRecipient->getRecipientJoin($recid);
        $data['edit'] = $this->MFinance->getPaybyrecid($recid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'exportwordfinance';
        $this->load->view($this->_container_report, $data);
    }
    
    function exportexcelfinancereport($recid){
        $data['header'] = "Pay Finance";
	$data['ex'] = $this->MExpense->listallexpense();
        $data['detail'] = $this->MRecipient->getRecipientJoin($recid);
        $data['edit'] = $this->MFinance->getPaybyrecid($recid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'exportexcelfinance';
        $this->load->view($this->_container_report, $data);
    }
}