<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Financeyear
 *
 * @author pussadee
 */
class Financeyear extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));
        $this->load->library('datethai');
	
    }
    
    function index(){
	$data['header'] = "Finance of year Table";
        $data['plan'] = $this->MPlan->listAllplan();
    	$data['page'] = $this->config->item('wconfig_template_admin').'financeyear/index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function searchresult(){ 
        
        $data['header'] = "Finance of year";        
    	$data['page'] = $this->config->item('wconfig_template_admin').'financeyear/searchresult';
    	$this->load->view($this->_container_report,$data);
    }
    
    function getprojectlistbyplanid(){
		$id=$this->input->post('id');
                $Q=$this->db->query("select * from project where  plan_id='$id'"); 
                $data['option'] = $Q->result_array();	          
		$data['page'] = $this->config->item('wconfig_template_admin').'financeyear/projectlist';      
		$this->load->view($this->_view_subsector,$data); 
    }
    
    function getActivitylistbyprojectid(){
		$id=$this->input->post('id');
          	$Q=$this->db->query("select * from activity where  project_id='$id'"); 
                $data['option'] = $Q->result_array();	          
		$data['page'] = $this->config->item('wconfig_template_admin').'financeyear/activityselectlist';      
		$this->load->view($this->_view_subsector,$data); 
    }
    function getAidlistbyActiivityid(){
		$id=$this->input->post('id');
          	$Q=$this->db->query("select * from aid where  activity_id='$id'"); 
                $data['option'] = $Q->result_array();	          
		$data['page'] = $this->config->item('wconfig_template_admin').'financeyear/aidselectlist';      
		$this->load->view($this->_view_subsector,$data); 
    }
    
    function exportpdffinanceyear(){
        $data['header'] = "Finance of year";        
    	$data['page'] = $this->config->item('wconfig_template_admin').'financeyear/genpdf';
    	$this->load->view($this->_container_report,$data);
    }
    
    function genexport(){
        if($this->input->post('type') == "pdf"){
        $data['header'] = "Finance of year";        
    	$data['page'] = $this->config->item('wconfig_template_admin').'financeyear/genpdf';
    	ini_set('memory_limit','512M');
        $html = $this->load->view($this->_container_report, $data,TRUE);       
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf = new mPDF('UTF-8');   
        $pdf = new mPDF('th_saraban');
        $pdf = new mPDF('th');
        $pdf->AddPage('L','', '', '', '','','','','','','');
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
        $pdf->WriteHTML($html,2); // write the HTML into the PDF
        $pdf->Output($pdfFilePath, 'I');
        }else if($this->input->post('type') == "word"){
        $data['header'] = "Finance of year";  
        $data['page'] = $this->config->item('wconfig_template_admin').'financeyear/genword'; 
        $this->load->view($this->_container_report, $data); 
        }else if($this->input->post('type') == "excel"){
        $data['header'] = "Finance of year";  
        $data['page'] = $this->config->item('wconfig_template_admin').'financeyear/genexcel'; 
        $this->load->view($this->_container_report, $data);
    }
    }

}
