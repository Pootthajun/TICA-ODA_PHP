<?php

class MPlan extends CI_Model{
    
    function insertplan(){
        $data = array(
            'plan_name'=>$this->input->post('plan_name'),
            'plan_by'=>$this->session->userdata('userid'),
            'plan_create'=>date('Y-m-d')            
        );
        $this->db->insert('plan',$data);
    }
    
    function listAllplan(){
        $data = array();
        $this->db->order_by('plan_id',"ASC");
        $Q = $this->db->get('plan');
        foreach($Q->result_array() as $row){
            $data[] = $row;
        }
        $Q->free_result();
        return $data;
    }
    
    function getPlanbyid($planid){
        //$data = array();
        $this->db->where('plan_id',$planid);
        $Q = $this->db->get('plan');
        $data = $Q->row_array();
        $Q->free_result();
	return $data;        
    }
    
    function updateplan(){
        $data = array(
            'plan_name'=>$this->input->post('plan_name'),
            'plan_by'=>$this->session->userdata('userid'),
            'plan_update'=>date('Y-m-d')            
        );
        $this->db->where('plan_id',$this->input->post('planid'));
        $this->db->update('plan',$data);
    }
    
    function deleteplan($planid){
        $this->db->where('plan_id',$planid);
        $this->db->delete('plan');  
    }
    
}
