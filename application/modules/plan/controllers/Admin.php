<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{
    
    function __construct(){
	parent::__construct(); 
        $this->module =  basename(dirname(dirname(__FILE__)));        
	
    }
    
    function index(){
        $data['header'] = "Plan Table";
        $data['all'] = $this->MPlan->listAllplan();
	    $data['menu'] = $this->MUser->getMenubyUser();      
        $data['page'] = $this->config->item('wconfig_template_admin').'index';
    	$this->load->view($this->_container_admin,$data); 
    }
    
    function addplan(){
        $data['header'] = "Add Plan";
    	$data['page'] = $this->config->item('wconfig_template_admin').'addplan';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function insertplan(){
        $this->MPlan->insertplan();
        flashMsg('success',$this->lang->line('insert_success'));
	redirect( 'plan/admin/','refresh');
    }
    
    function editplan($planid){
        $data['header'] = "Edit Plan";
        $data['edit'] = $this->MPlan->getPlanbyid($planid);
    	$data['page'] = $this->config->item('wconfig_template_admin').'editplan';
    	$this->load->view($this->_container_admin,$data);
    }
    
    function updateplan(){
        $this->MPlan->updateplan();
    	flashMsg('success',$this->lang->line('update_success'));
	redirect( 'plan/admin/','refresh');
    }
    
    function deleteplan($planid){
        $query = $this->db->get_where('project', array('plan_id' => $planid));
        if($query->num_rows() > 0) {
            flashMsg('warning',$this->lang->line('cannot_delete'));
            redirect( 'plan/admin/','refresh');            
        }else{
            $this->MPlan->deleteplan($planid);
            flashMsg('success',$this->lang->line('delete_success'));
            redirect( 'plan/admin/','refresh');
        }
    }
	

    
}