<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Welcome_Controller
 *
 * @author pussadee
 */
class Welcome_Controller extends MY_Controller{
    
    private $_container;
	function __construct(){
		parent::__construct();
                
    $this->load->model('welcome/MWelcome');
    $data['general']=$this->MGeneral->getgenbyid();
    
    $this->_container_index = $this->config->item("wconfig_template_welcome")."container_index.php";
    $this->_container_page = $this->config->item("wconfig_template_welcome")."container_page.php";
    $this->_container_contact = $this->config->item("wconfig_template_welcome")."container_contact.php";
    $this->_container_result = $this->config->item("wconfig_template_welcome")."container_result.php";
}
}
