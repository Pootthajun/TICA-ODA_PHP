<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	private $_container;
	function __construct(){
		parent::__construct();
                
                $this->load->helper('status/status');
                $this->load->helper('html');
                $this->load->library('status/status');
                $this->load->library('site/bep_site');
                $this->load->model('user/MUser');
                $this->load->model('userauthen/MUserauthen');
                $this->load->model('expense/MExpense');
                $this->load->model('aid/MAid');
                $this->load->model('aidtype/MAidtype');
                $this->load->model('activity/MActivity');
                $this->load->model('projecttype/MProjecttype');
				$this->load->model('kindtype/MKindtype');
                $this->load->model('plan/MPlan');
                $this->load->model('budgettype/MBudgettype');
                $this->load->model('project/MProject');
                $this->load->model('region/MRegion');
                $this->load->model('country/MCountry');
                $this->load->model('cooperationtype/MCooperationtype');
                $this->load->model('cooperation/MCooperation');
                $this->load->model('oecd/MOecd');
                $this->load->model('sector/MSector');
                $this->load->model('funding/MFunding');
                $this->load->model('executing/MExecuting');  
                $this->load->model('multilateral/MMultilateral');
                //$this->load->model('kind/MKind');
                $this->load->model('subsector/MSubsector');
                $this->load->model('institute/MInstitute');
                $this->load->model('recipient/MRecipient');
                $this->load->model('implement/MImplement');
                $this->load->model('pages/MPage');
                $this->load->model('general/MGeneral');
                $this->load->model('transfer/MTransfer');
                $this->load->model('finance/MFinance');
                $this->load->model('finance/MBudget');
                $this->load->model('finance/MLoan');
                $this->load->model('about/MAbout');
                $this->load->model('news/MNews');
				$this->load->model('feature/MFeature');
                $this->load->model('weblink/MWeblink');
                $this->load->model('report/MReport');
				$this->load->model('countrygroup/MCountrygroup');
				$this->load->model('policy/MPolicy');
				
                
                $this->_container_login = $this->config->item("wconfig_template_login")."login.php";
                
        }
}