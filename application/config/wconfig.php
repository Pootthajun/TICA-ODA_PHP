<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * TQF  VERSION
 */

define('WCONFIG', '0.1');

/*
 |--------------------------------------------------------------------------
 | TQF Database Table Prefix
 |--------------------------------------------------------------------------
 | This is the table prefix which will be placed before
 | each table name which BackendPro uses
 */

//$config['wconfig_table_prefix'] = 'web_';

$config['wconfig_template_dir'] = "";
$config['wconfig_template_login'] = $config['wconfig_template_dir']."login/";
$config['wconfig_template_admin'] = $config['wconfig_template_dir']."admin/";
$config['wconfig_template_welcome'] = $config['wconfig_template_dir']."welcome/";
?>