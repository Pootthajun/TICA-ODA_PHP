$(document).ready(function(){
	
	//Sidebar Accordion Menu:
		
		$("#main-nav li ul").hide(); // Hide all sub menus
		$("#main-nav li a.current").parent().find("ul").slideToggle("slow"); // Slide down the current menu item's sub menu
		
		$("#main-nav li a.nav-top-item").click( // When a top menu item is clicked...
			function () {
				$(this).parent().siblings().find("ul").slideUp("normal"); // Slide up all sub menus except the one clicked
				$(this).next().slideToggle("normal"); // Slide down the clicked sub menu
				return false;
			}
		);
		
		$("#main-nav li a.no-submenu").click( // When a menu item with no sub menu is clicked...
			function () {
				window.location.href=(this.href); // Just open the link instead of a sub menu
				return false;
			}
		); 

    // Sidebar Accordion Menu Hover Effect:
		
		$("#main-nav li .nav-top-item").hover(
			function () {
				$(this).stop().animate({ paddingRight: "25px" }, 200);
			}, 
			function () {
				$(this).stop().animate({ paddingRight: "15px" });
			}
		);

    //Minimize Content Box
		
		$(".content-box-header h3").css({ "cursor":"s-resize" }); // Give the h3 in Content Box Header a different cursor
		$(".closed-box .content-box-content").hide(); // Hide the content of the header if it has the class "closed"
		$(".closed-box .content-box-tabs").hide(); // Hide the tabs in the header if it has the class "closed"
		
		$(".content-box-header h3").click( // When the h3 is clicked...
			function () {
			  $(this).parent().next().toggle(); // Toggle the Content Box
			  $(this).parent().parent().toggleClass("closed-box"); // Toggle the class "closed-box" on the content box
			  $(this).parent().find(".content-box-tabs").toggle(); // Toggle the tabs
			}
		);

    // Content box tabs:
		
		$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
		$('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
		$('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
		
		$('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
			function() { 
				$(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
				$(this).addClass('current'); // Add class "current" to clicked tab
				var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
				$(currentTab).siblings().hide(); // Hide all content divs
				$(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
				return false; 
			}
		);

    //Close button:
		
		$(".close").click(
			function () {
				$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
					$(this).slideUp(400);
				});
				return false;
			}
		);

    // Alternating table rows:
		
		$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

    // Check all checkboxes when the one in a table head is checked:
		
		$('.check-all').click(
			function(){
				$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));   
			}
		);


			$('#table_id').DataTable();
			$('#tableresult').DataTable();
			
			
			//$("#form1").validate();
			$(function () {
    $.validator.setDefaults({
       // errorClass: 'text-danger',
        ignore: ':hidden:not(.chosen-select)',
        errorPlacement: function (error, element) {
            if (element.hasClass('chosen-select')) error.insertAfter(element.siblings(".chosen-container"));
            else {
                error.insertAfter(element);
            }
        }
    });

    //rules and messages objects
    $("#form1").validate({
		errorPlacement: function(error, element) {},		
        highlight: function (element) {
            if ($(element).hasClass('chosen-select')) {
                $(element).siblings('.chosen-container').removeClass('green-select').addClass('red-select');
            }
            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
            $(element).closest('.text-input').removeClass('text-input').addClass('has-error');
            }
        },
        unhighlight: function (element) {
            if ($(element).hasClass('chosen-select')) {
                $(element).siblings('.chosen-container').removeClass('re-select').addClass('green-select');
            }
            if (! ($(element).hasClass('ignoreEmpty') && $(element).is(':blank'))) {
                $(element).closest('.text-input').removeClass('has-error').addClass('has-success');
            }
        }
		
    });

/*    $('.taskDatasetValidation').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Choose  Project Assitant"
            }
        });	
    });*/

    $('.chosen-select').on('change', function () {
        $(this).valid();
    });

});
			
			    $( "#datepicker1" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				$( "#datepicker2" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				$( "#datepicker3" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				$( "#datepicker4" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				$( "#datepicker5" ).datepicker({ dateFormat: "yy-mm-dd" ,changeMonth: true,changeYear: true});
				$( "#datepicker6" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				$( "#datepicker7" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				$( "#datepicker8" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				$( "#datepicker9" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				$( "#datepicker10" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				$( "#datepicker11" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
				<!-- $( ".datedis" ).datepicker({ dateFormat: "yy-mm-dd ,changeMonth: true, changeYear: true" });  -->
				  $( ".datefunded" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});       
				
				$( "#date_receipion_birth" ).datepicker({ 
						dateFormat: "yy-mm-dd"  ,
						      changeMonth: true,
    					  changeYear: true,
						  yearRange: '1965:'+(new Date).getFullYear()  
				}).on("change", function (ev) {

						   var datepicker = ev.target.value;
						   var fromdate = datepicker.slice(8, 10);
						   fromdate = parseInt(fromdate);
						   var frommonth = datepicker.slice(5, 7);
						   frommonth = parseInt(frommonth);
						   var fromyear = datepicker.slice(0, 4); 
						   fromyear = parseInt(fromyear);
						    
					//console.log("Date changed: ",ev.target.value);
    				//alert(fromyear+'-'+frommonth+'-'+fromdate);
					
						   var current = new Date();
						   var current_year = current.getFullYear();
						   var current_month = current.getMonth();
						   var current_day = current.getDay();
						   
						   var todate = parseInt(current_day);
						   var tomonth = parseInt(current_month)+1;
						   var toyear = parseInt(current_year);
						   
						   // Calculate total month of Age
							var total_mon = parseInt(12 - frommonth) ;
							total_mon += parseInt((current_year - fromyear - 1)*12) ;
							total_mon += tomonth ;
							
							// Get Year/Month of Age
							var year = Math.floor(total_mon/12) ;
							var mon = parseInt(total_mon%12) ;
							var text ;
							if (mon==0) {
						  		 text = year + ' Year' ;
						 	 } else {
								 text = year + ' Year '  +  mon +' Month';
							  }
						 $("#age").val(text);
						 if(mon){
							 year=year+1;
						 }
						 $("#age_real").val(year);
				});

			
			    var config = {
   					   '.chosen-select': {allow_single_deselect:true,no_results_text:'Oops, nothing found!'},
			    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
			$('.number').number( true, 2 );


});







  
  
  