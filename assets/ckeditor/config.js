/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

var url  = "/assets";
CKEDITOR.editorConfig = function(config) {
// ...
   config.filebrowserBrowseUrl = url+'/kcfinder/browse.php?opener=ckeditor&type=files';
   config.filebrowserImageBrowseUrl = url+'/kcfinder/browse.php?opener=ckeditor&type=images';
   config.filebrowserFlashBrowseUrl =url+ '/kcfinder/browse.php?opener=ckeditor&type=flash';
   config.filebrowserUploadUrl = url+'/kcfinder/upload.php?opener=ckeditor&type=files';
   config.filebrowserImageUploadUrl =url+'/kcfinder/upload.php?opener=ckeditor&type=images';
   config.filebrowserFlashUploadUrl = url+'/kcfinder/upload.php?opener=ckeditor&type=flash';
// ...
};

