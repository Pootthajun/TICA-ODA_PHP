$(document).ready(function(){
	$("#form1").validate({
		errorPlacement: function(error, element) {},
		});
		
		$( "#datepicker1" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
		$( "#datepicker2" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
		$( "#datepicker3" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
		$( "#datepicker4" ).datepicker({dateFormat: "yy-mm-dd" ,changeMonth: true, changeYear: true});
		
		
		$( "#date_receipion_birth" ).datepicker({ 
						dateFormat: "yy-mm-dd"  ,
						      changeMonth: true,
    					  changeYear: true,
						  yearRange: '1965:'+(new Date).getFullYear()  
				}).on("change", function (ev) {

						   var datepicker = ev.target.value;
						   var fromdate = datepicker.slice(8, 10);
						   fromdate = parseInt(fromdate);
						   var frommonth = datepicker.slice(5, 7);
						   frommonth = parseInt(frommonth);
						   var fromyear = datepicker.slice(0, 4); 
						   fromyear = parseInt(fromyear);
						    
					//console.log("Date changed: ",ev.target.value);
    				//alert(fromyear+'-'+frommonth+'-'+fromdate);
					
						   var current = new Date();
						   var current_year = current.getFullYear();
						   var current_month = current.getMonth();
						   var current_day = current.getDay();
						   
						   var todate = parseInt(current_day);
						   var tomonth = parseInt(current_month)+1;
						   var toyear = parseInt(current_year);
						   
						   // Calculate total month of Age
							var total_mon = parseInt(12 - frommonth) ;
							total_mon += parseInt((current_year - fromyear - 1)*12) ;
							total_mon += tomonth ;
							
							// Get Year/Month of Age
							var year = Math.floor(total_mon/12) ;
							var mon = parseInt(total_mon%12) ;
							var text ;
							if (mon==0) {
						  		 text = year + ' Year' ;
						 	 } else {
								 text = year + ' Year '  +  mon +' Month';
							  }
						 $("#age").val(text);
						 if(mon){
							 year=year+1;
						 }
						 $("#age_real").val(year);
				});
});

  function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(170)
                        .height(170);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
		
		  function readsignature(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(170)
                        .height(63);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }